#ifndef SOURCENAVIGATOR_HPP_INCLUDED
#define SOURCENAVIGATOR_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"



#include "../cuon_window.hpp"

#include "../Windows/windows.hpp"

#include "SingleSourcenavigatorProject.hpp"
#include "SingleSourcenavigatorPart.hpp"
#include "SingleSourcenavigatorModule.hpp"
#include "SingleSourcenavigatorFile.hpp"
#include "SingleSourcenavigatorMaterialResources.hpp"
#include "SingleSourcenavigatorStaffResources.hpp"

#include "../DMS/dms.hpp"

#include "..//Addresses/SingleAddress.hpp"

#define tabpro_Sourcenavigator 0
#define tabpro_SourcenavigatorPart 1
#define tabpro_SourcenavigatorModule 2
#define tabpro_SourcenavigatorFile 3
#define tabpro_SourcenavigatorModuleMaterial 4
#define tabpro_SourcenavigatorModuleStaff 5


class sourcenavigator:public windows, public cuon_window {
public:
     int addressID ;

     SingleSourcenavigatorProject *singleSourcenavigator ;
     SingleSourcenavigatorPart *singleSourcenavigatorPart ;
     SingleSourcenavigatorModule *singleSourcenavigatorModule ;
     SingleSourcenavigatorFile *singleSourcenavigatorFile ;
     SingleDMS *singleDMS ;


     SingleAddress *singleAddress ;

     sourcenavigator();
     ~sourcenavigator();
     
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_bSearch_clicked();
      void on_bEdit_clicked();
     void tabChanged();
     void on_eAddressNumber_changed();
     void on_bProjectDMS_clicked();
     virtual int initAll();
     

     
     void on_tvProject_activated(const Gtk::TreeModel::Path& path,
						  Gtk::TreeViewColumn*);

     void on_tvPart_activated(const Gtk::TreeModel::Path& path,
						  Gtk::TreeViewColumn*);

     void on_tvModul_activated(const Gtk::TreeModel::Path& path,
						  Gtk::TreeViewColumn*);

     void on_tvFiles_activated(const Gtk::TreeModel::Path& path,
                               Gtk::TreeViewColumn*);

     // virtual void execute(xmlrpc_c::paramList const& paramList,xmlrpc_c::value * const retvalP) ;

   
protected:

private:

};


#endif // Sourcenavigator_HPP_INCLUDED
