
#ifndef CUON_WINDOW_HPP
#define CUON_WINDOW_HPP



#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <string>
#include <map>
#include <stdlib.h>

class cuon_window{
public:
     
     cuon_window();
     ~cuon_window();
     

     virtual int initAll();
  virtual int initOrder( map <string,string>* dicOrder=NULL, bool newOrder = false, int orderid = 0,string OrderType="Order");
};



#endif
