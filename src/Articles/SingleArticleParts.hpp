#ifndef SINGLEARTICLEPARTS_HPP_INCLUDED
#define SINGLEARTICLEPARTS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleArticleParts:public Singledata{
    public:
        SingleArticleParts();
        ~SingleArticleParts();
    protected:

    private:

};

#endif // SINGLEARTICLEPARTS_HPP_INCLUDED
