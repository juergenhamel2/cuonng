/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Articles/SingleArticle.hpp>

SingleArticle::SingleArticle()
{
     sNameOfTable = "articles";
     sTree = "tv_article";
     sSort = "number,designation";
     sWhere = "";



     newEntry.sNameOfTable = sNameOfTable ;
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;
     loadEntryField("articles");


     //names of database fields
    a1= {"number as name", "designation", "fct_getValueAsCurrency(sellingprice1) as s1", "fct_getValueAsCurrency(sellingprice2) as s2", "fct_getValueAsCurrency(sellingprice3) as s3", "fct_getValueAsCurrency(sellingprice4) as s4", "unit as s5", "weight as f1","id"};

     //tree header
    h1 = {_("Number"), _("Designation"),_("Price 1"),_("Price 2"),_("Price 3"),_("Price 4"),_("Unit"),  _("Weight"), _("ID") };

     // type
    t1= {"string","string","string","string","string","string","string","float","int"};



     // names of tree colums
     c1 = {"name","designation","s1","s2","s3","s4","s5","f1","id"} ;

     setLiFields();
}

SingleArticle::~SingleArticle() {}

