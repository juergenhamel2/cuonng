#ifndef SINGLEARTICLEWEBSHOP_HPP_INCLUDED
#define SINGLEARTICLEWEBSHOP_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleArticleWebshop:public Singledata
{
    public:
        SingleArticleWebshop();
        ~SingleArticleWebshop();
    protected:

    private:

};

#endif // SINGLEARTICLEWEBSHOP_HPP_INCLUDED
