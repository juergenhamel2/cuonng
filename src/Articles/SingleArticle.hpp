#ifndef SINGLEARTICLES_HPP_INCLUDED
#define SINGLEARTICLES_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include "../Databases/Singledata.hpp"

class SingleArticle:public Singledata {
public:
     SingleArticle();
     ~SingleArticle();
protected:

private:

};

#endif // SINGLEARTICLES_HPP_INCLUDED
