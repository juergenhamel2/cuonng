#ifndef PROJECT_HPP_INCLUDED
#define PROJECT_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include "../Windows/windows.hpp"

#include "../Project/SingleProject.hpp"
#include "../Project/SingleProjectPhases.hpp"
#include "../Project/SingleProjectTasks.hpp"
#include "../Project/SingleProjectMaterialResources.hpp"
#include "../Project/SingleProjectStaffResources.hpp"
#include "../Project/SingleProjectProgramming.hpp"
#include "../Project/SingleSourcefile.hpp"
#include "../DMS/dms.hpp"

#include "../Addresses/SingleAddress.hpp"

#define tabpro_Project 0
#define tabpro_Phases 1
#define tabpro_Tasks 2
#define tabpro_TasksMaterial 3
#define tabpro_TasksStaff 4
#define tabpro_Programming 5
#define tabpro_Sourcefiles 6

#include "../cuon_window.hpp"



class project:public windows,public cuon_window {
public:
     int addressID ;
    
     SingleProject *singleProject ;
     SingleProjectPhases *singleProjectPhases ;
     SingleProjectTasks *singleProjectTasks ;
     SingleProjectProgramming *singleProjectProgramming ;
     SingleSourcefile *singleSourcefile ;
     SingleDMS *singleDMS ;


     SingleAddress *singleAddress ;

     project();
     ~project();
     
     virtual int initAll();
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     
     void on_bSearch_clicked();
     void tabChanged();
    void on_eAddressNumber_changed();

protected:

private:

};

#endif // PROJECT_HPP_INCLUDED
