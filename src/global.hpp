
#ifndef GLOBAL_HPP // header guards
#define GLOBAL_HPP
#include <time.h>
#include <map>
#include <pqxx/pqxx>
#include "User/User.hpp"

#define MY_LINUX64 1
#define MY_LINUX32 301
#define MY_WIN32 1001


// extern tells the compiler this variable is declared elsewhere
// used as global varibale

extern pqxx::connection* Conn;
extern User oUser ;
extern map<string,string> cuonSystem ;
extern int ipc_port1 ;
#endif
