
CREATE OR REPLACE FUNCTION fct_getHibernationForAddressID(iAddressID int ) returns setof text as '
 DECLARE
     iClient int ;
    r record;
    searchsql text := '' '';

    BEGIN



        searchsql := ''select hibernation_number , sequence_of_stock, hibernation.id as hib_id from hibernation where addressnumber = '' || iAddressID ||  fct_getWhere(2,'''') ;
        FOR r in execute(searchsql)  LOOP


            return next r.hibernation_number || '', '' || r.sequence_of_stock || ''  ###'' || r.hib_id ;

        END LOOP ;

    END ;



     ' LANGUAGE 'plpgsql';
