/*
 * myxml.hpp
 *
 *  Created on: 26.09.2023
 *      Author: jhamel
 */
#include <libxml/parser.h>

#ifndef MYXML_MYXML_HPP_
#define MYXML_MYXML_HPP_

#include "../TypeDefs/basics.hpp"



class myxml : public basics {

public:
	xmlDoc *doc = NULL;
	  xmlNode *root_element = NULL;

	myxml();
	~myxml();
	map < string,string > getEntry(string sFile);

};

#endif /* MYXML_MYXML_HPP_ */
