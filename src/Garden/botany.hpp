#ifndef BOTANY_HPP_INCLUDED
#define BOTANY_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include "../Windows/windows.hpp"
#include "SingleBotanyDivisio.hpp"
#include "../cuon_window.hpp"
#define tabart_Divisio 0





class botany:public windows,public cuon_window{
    public:

    SingleBotanyDivisio *singleBotanyDivisio ;

        botany();
        ~botany();

     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     virtual int  initAll();
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};



#endif // BOTANY_HPP_INCLUDED
