#include "basics.hpp"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <iterator>
#include "../global.hpp"


basics::basics() {
    
	 MN["Mainwindow"] = 10;
     MN["Client"] = 1000;
     
     MN["Address"] = 2000;
     MN["Address_info"] = 2001;
     MN["Address_stat_caller"] = 2002;
     MN["Address_stat_rep"] = 2003;
     MN["Address_stat_salesman"] = 2004;
     MN["Address_stat_schedul"] = 2005;
     
     MN["Partner"] = 2100;
     MN["Partner_info"] = 2101;
      
     MN["Notes"] = 2110;
     MN["Partner_Schedul"] = 2200;
     MN["Partner_Schedul_info"] = 2201;
     
     MN["Articles"] = 3000;
     MN["Articles_stat_misc1"] = 3001;
     
     MN["MaterialGroups"] = 23000;
     
     
     
     MN["Order"] = 4000;
     
     MN["Order_stat_misc1"] = 4001;
     MN["Order_stat_global1"] = 4002;
     MN["Order_stat_caller1"] = 4003;
     MN["Order_stat_rep1"] = 4004;
     MN["Order_stat_salesman1"] = 4005;
     MN["PROPOSAL"] = 4500;
     
     MN["Stock"] = 5000;
     MN["Staff"] = 6000;
     MN["StaffFee"] = 6100;
     
     MN["Invoice"] = 7000;
     MN["Invoice_info"] = 7001;
     MN["Invoice_stat_taxvat1"] = 7101;
     
     
     MN["DMS"] = 11000;
     MN["Forms_Address_Notes_Misc"] = 11010;
     MN["Forms_Address_Notes_Contacter"] = 11011;
     MN["Forms_Address_Notes_Rep"] = 11012;
     MN["Forms_Address_Notes_Salesman"] = 11013;
     MN["Newsletter"] = 11100;
     
     MN["Biblio"] = 12000;
     MN["AI"] = 13000;
     
     MN["Project"] = 14000;
     MN["Project_info"] = 14001;
     MN["Project_stat_misc1"] = 14011;
     MN["Project_phase"] = 14100;
     MN["Project_task"] = 14200;
     MN["Project_staff_resources"] = 14300;
     MN["Project_material_resources"] = 14400;
     MN["Project_programming"] = 14500;
     
     MN["Sourcecode_project"] = 15000;
     MN["Sourcecode_part"] = 15100;
     MN["Sourcecode_modul"] = 15200;
     MN["Sourcecode_file"] = 15300;
     
     
     MN["SupportTicket"]  = 17000;
     
     
     MN["Web2"] = 20000;
     MN["Stats"] = 22000;
     MN["Calendar"] = 28000;
     MN["Think"] = 29000;
     
     MN["Leasing"] = 30001;;
     
     MN["Grave"] = 40000;
     MN["GraveServiceNotesService"] = 40100;
     MN["GraveServiceNotesSpring"] = 40101;
     MN["GraveServiceNotesSummer"] = 40102;
     MN["GraveServiceNotesAutumn"] = 40103;
     MN["GraveServiceNotesWinter"] = 40104;
     MN["GraveServiceNotesAnnual"] = 40105;
     MN["GraveServiceNotesUnique"] = 40106;
     MN["GraveServiceNotesHolliday"] = 40107;
     
     MN["Graveyard"] = 41000;
     MN["Garden"] = 42000;
     
     MN["Hibernation"] = 110000  ;
     MN["HibernationPlantNumber"] = 110100;
     
     
     MN["Botany"] = 110500;
     MN["Divisio"] = 110510;  



     mime_type["txt"] = "text/plain";
     mime_type["html"] = "text/html";
     mime_type["sql"] = "text/x-sql";
     mime_type["ini"] = "text/x-ini";
     mime_type["c"] = "text/x-csrc";
     mime_type["h"] = "text/x-csrc";
     
     mime_type["py"] = "text/x-python";
     mime_type["lua"] = "text/x-lua";
     mime_type["java"] = "text/x-java";
     mime_type["hpp"] = "text/x-c++src";
     mime_type["cpp"] = "text/x-c++src";
     
     
}
basics::~basics() {}



void basics::printlog(string s1)
{
     cout << s1 << endl ;
}

void basics::printlog(string s1, string s2)
{
     cout << s1 << ": " << s2 << endl ;
}

void basics::printlog(string s1, string s2, int i1)
{
     cout << s1 << ": " << s2 << ": " << i1 << endl ;
}

void basics::printlog(string s1, int i1)
{
     cout << s1 << ": " << i1 << endl ;
     
}
void basics::printlog(string s1, int i1, int i2)
{
     cout << s1 << ": " << i1 << ", " << i2 <<endl ;
     
}
void basics::printlog(string s1, int i1, string s2, int i2)
{
     cout << s1 << ": " << i1 << s2 << i2 << endl ;
}
void basics::printlog(string s1, float f1)
{
     cout << s1 << ": " << f1 << endl ;
}

bool basics::file_exists (string name)
{
     if (FILE *file = fopen(name.c_str(), "r")) {
          fclose(file);
          return true;
     }
     else {
          return false;
     }
}

string basics::stripchars(std::string str, const std::string &chars)
{
    str.erase(
        std::remove_if(str.begin(), str.end(), [&](char c){
            return chars.find(c) != std::string::npos;
        }),
        str.end()
    );
    return str;
}

string basics::getNullUUID(){
     return "00000000-0000-0000-0000-000000000000" ;
}



string basics::getNewUUID(){
     int i;
     char* c;		
     string sUUID  ;


     srand(static_cast<unsigned int> (time(NULL)));  /*Randomize based on time.*/
     
     
     
     /*Data1 - 8 characters.*/

     for(i = 0; i < 8; i++){
          
          
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
          
          
          
          
     }
     /*Data2 - 4 characters.*/
	
     sUUID += '-' ;
     
     for(i = 0; i < 4; i++){
          
		  int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     /*Data3 - 4 characters.*/
     
     sUUID += '-' ;    
     for(i = 0; i < 4; i++){
          //	((*pGuidStr = (rand() % 16)) < 10) ? *pGuidStr += 48 : *pGuidStr += 55;
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     /*Data4 - 4 characters.*/
     
     sUUID += '-' ;
     for(i = 0; i < 4; i++){
          //	((*pGuidStr = (rand() % 16)) < 10) ? *pGuidStr += 48 : *pGuidStr += 55;
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     /*Data5 - 12 characters.*/
     
     sUUID += '-' ;   
     for(i = 0; i < 12; i++){
          
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     
     
     
     
     cout <<  sUUID << endl ;
     
     return sUUID ;
     
}
string basics::getRandomFilename(string sPrefix){
     // cout << getNewUUID() << sPrefix << endl ;
     string dirname = (string) getenv("HOME") + "/cuondata/";
     string sFilename = dirname + getNewUUID() + sPrefix ;
     cout << sFilename << endl;
     return sFilename ;
     

}

void basics::writeFile(string sName, string* sData) {

     
     printlog("length of sData = " , toString(sData->size()) );
     ofstream myfile (sName,ofstream::binary);
     if (myfile.is_open())
     {
          cout << "myfile is open " << endl ;
          myfile << *sData ;
           cout << "myfile is open2 " << endl ;
           myfile.close();
            cout << "myfile is open3 " << endl ;
     }
     else cout << "Unable to open file";
     
}


string* basics::readFile(string sName) {

     
     string* sData = new string();
     
     ifstream myfile (sName,ios::ate | ios::binary);
     if (myfile.is_open())
     {
          ifstream::pos_type pos = myfile.tellg();

          std::vector<char>  result(pos);

          myfile.seekg(0, ios::beg);
          myfile.read(&result[0], pos);
          
          
          sData = new string(result.begin(),result.end() ) ;
          cout <<"length of file : "<< pos << endl;
          myfile.close();

          
         
     }
     else cout << "Unable to open file";
    
     
     return sData;
}


string basics::getDate(){

     string sDateTime = getDateTime();
     printlog (sDateTime );
     string sDate = sDateTime.substr(0,10);
     printlog (sDate );
     

     return sDate;

}


string basics::getDateTime(){
     
 printlog ("getDateTime" );

  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);
   printlog ("getDateTime2" );

 
  string sFormat = oUser.getLocaleDateFormat();
  printlog ("getDateTime3 = ", sFormat );

  
  strftime(buffer,80,sFormat.c_str(),timeinfo);
  std::string str(buffer);

  printlog( str) ;

  return str;

}
int basics::executePrg(vector <string> prg)
{
     string sPrg ;
     string sPrg2 ;
     

     pid_t pid = vfork();
     if(pid == -1) {
          perror("vfork");
          return 1;
     } else if(pid == 0) {
          
          size_t lV = prg.size();
          cout << "size of the Vector: " << lV << endl ;
          switch (lV) {
          case 1:
               sPrg = prg[0];
               sPrg = "nohup " + sPrg + " & ";
               cout << "sPrg = " << sPrg << endl;
               if( execlp("nohup",sPrg.c_str(),sPrg.c_str(), NULL) == -1) {
                    perror("execlp");
                    return 1;

               }
               break;
          case 2:
               sPrg = prg[0];
               sPrg2 = prg[1];
              
               cout << "sPrg 2 = " << sPrg << ", " << sPrg2 << endl;
               if(execlp("nohup", sPrg.c_str(),sPrg.c_str(),sPrg2.c_str(), NULL) == -1) {
                    perror("execlp");
                    return 1;

               }
               break;
          }
          return 1;

     }

   return 1;
}

int basics::String2Int(string sInt){
     
     int value = atoi(sInt.c_str());
     
     return value ;
}

vector <string> basics::split(const  string &theString,
       const  string  &theDelimiter)
{
     size_t  start = 0, end = 0;
     vector <string> vReturn ;
    while ( end != string::npos)
    {
        end = theString.find( theDelimiter, start);

        // If at end, use length=maxLength.  Else use length=end-start.
        vReturn.push_back( theString.substr( start,
                       (end == string::npos) ? string::npos : end - start));

        // If at end, use start=maxSize.  Else use start=end+delimiter.
        start = (   ( end > (string::npos - theDelimiter.size()) )
                  ?  string::npos  :  end + theDelimiter.size());
    }

    return vReturn ;
}

std::string get_file_as_string(string filename)
{
	std::ifstream in(filename);
	std::ostringstream s;
	s << in.rdbuf(); // std::ostream& operator<<(std::ostream&, std::streambuf*) liest den kompletten Inhalt
	std::string myData = s.str();
	return myData;
	}

