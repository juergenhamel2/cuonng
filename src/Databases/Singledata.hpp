#ifndef SINGLEDATA_HPP
#define SINGLEDATA_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>

#include "datatypes.hpp"
#include "../Windows/setOfEntries.hpp"

#include "../Windows/gladeXml.hpp"
#include "../Databases/sql.hpp"


class Singledata : public datatypes {

public:
     string sNameOfTable ;
     string sSort;
     string sWhere;
     string sListheader ;
     int sep_info_1 ;
     int sep_info_2 ;
     int sep_info_3 ;
     int tmp_ID ;
     int ModulNumber ;

     sigc::connection selection_signal_handler ;
     
   
     //names of database fields
    
     vector < string> a1;
     //tree header
     
     vector < string> h1;
     // type
     
     vector < string> t1;


     // names of tree colums
    
     vector < string> c1;
     //Searchfields
     vector <vector <string> > liSearchfields ;
      
     MySql x1;
      vector <string>  firstRecord ;
     int ID;
     string sTree;
     map<string,vector <string> > dicFields ;
     map <string,string> dicEntries ;
     vector <string> liHeader ;
     GtkCellRenderer     *renderer;
     gladeXml glx ;
     GtkWidget           *view;
     Gtk::TreeView *pTree1;
     setOfEntries oEntries;
	map<string, string> dataEntries;
     listEntry newEntry;

     Glib::RefPtr<Gtk::ListStore> listStore;
     Glib::RefPtr<Gtk::TreeView::Selection> selection ;

     Gtk::TreeModel::Row row ;
     Columns cols ;
     Gtk::TreeIter   iter;
     Gtk::TreePath path;
     Gtk::Statusbar *pStatusbar ;

     vector<string> aFields ;

     Singledata();
     ~Singledata();


     void loadEntryField(string sName);
     void load(int id);
     void newData();
     void save( );
    
    
     virtual void init();
     void saveValues();
     void saveValuesBE();
     void deleteRecord();
     void setDicEntries(setOfEntries _entries);
     void getListEntries();
     void setLiFields();
     void setTree1();
     void setListEntries();
     void clearAllFields();
     void fillEntries(int i);
     virtual void fillOtherEntries();
     virtual void fillExtraEntries();
     void treeSelectRow();
     void setNullIter();
     void readEntries();
     virtual void readExtraEntries();
     void on_selection_changed();
     void setWindow(Glib::RefPtr<Gtk::Builder>) ;
     virtual void setTreeFields();
     virtual void setStatusbar();
     virtual string getStatusbar();
     virtual void setSearchString();
     virtual string postFindData(string searchString);
     virtual void readNonWidgetEntries();
	

};

#endif

