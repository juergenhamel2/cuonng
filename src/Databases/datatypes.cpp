#include <map>
#include <list>
#include <gtkmm.h>

#include "datatypes.hpp"

Columns::Columns()
{

     // integer
     Gtk::TreeModelColumn<unsigned> id ;
     iCol["id"] = id ;
     Gtk::TreeModelColumn<unsigned> partner_id ;
     iCol["partner_id"] = partner_id ;
     Gtk::TreeModelColumn<unsigned> customer_id ;
     iCol["customer_id"] = customer_id ;
     Gtk::TreeModelColumn<unsigned> status ;
     iCol["status"] = status ;
     Gtk::TreeModelColumn<unsigned> days ;
     iCol["days"] = days ;
     Gtk::TreeModelColumn<unsigned> i1 ;
     iCol["i1"] = i1 ;
     Gtk::TreeModelColumn<unsigned> i2 ;
     iCol["i2"] = i2 ;
     Gtk::TreeModelColumn<unsigned> i3 ;
     iCol["i3"] = i3 ;
     // string
     Gtk::TreeModelColumn<Glib::ustring> name ;
     sCol["name"] = name ;
     Gtk::TreeModelColumn<Glib::ustring> name2 ;
     sCol["name2"] = name2 ;
     Gtk::TreeModelColumn<Glib::ustring> firstname ;
     sCol["firstname"] = firstname ;
     Gtk::TreeModelColumn<Glib::ustring> description ;
     sCol["description"] = description ;
     Gtk::TreeModelColumn<Glib::ustring> street ;
     sCol["street"] = street ;
     Gtk::TreeModelColumn<Glib::ustring> zip ;
     sCol["zip"] = zip ;
     Gtk::TreeModelColumn<Glib::ustring> city ;
     sCol["city"] = city ;
     Gtk::TreeModelColumn<Glib::ustring> s1 ;
     sCol["s1"] = s1 ;
     Gtk::TreeModelColumn<Glib::ustring> s2 ;
     sCol["s2"] = s2 ;
     Gtk::TreeModelColumn<Glib::ustring> s3 ;
     sCol["s3"] = s3 ;
     Gtk::TreeModelColumn<Glib::ustring> s4 ;
     sCol["s4"] = s4 ;
     Gtk::TreeModelColumn<Glib::ustring> s5 ;
     sCol["s5"] = s5 ;
     Gtk::TreeModelColumn<Glib::ustring> s6 ;
     sCol["s6"] = s6 ;
      Gtk::TreeModelColumn<Glib::ustring> s7 ;
      sCol["s7"] = s7 ;
       Gtk::TreeModelColumn<Glib::ustring> s8 ;
       sCol["s8"] = s8 ;
        Gtk::TreeModelColumn<Glib::ustring> s9 ;
        sCol["s9"] = s9 ;
         Gtk::TreeModelColumn<Glib::ustring> s10 ;
     sCol["s10"] = s10 ;
     Gtk::TreeModelColumn<Glib::ustring> designation ;
     sCol["description"] = designation ;
     //float
     Gtk::TreeModelColumn<float> amount;
     fCol["amount"] = amount ;
     Gtk::TreeModelColumn<float> f1;
     fCol["f1"] = f1 ;
     Gtk::TreeModelColumn<float> f2;
     fCol["f2"] = f2 ;
     Gtk::TreeModelColumn<float> f3;
     fCol["f3"] = f3 ;
     Gtk::TreeModelColumn<float> f4;
     fCol["f4"] = f4 ;
     Gtk::TreeModelColumn<float> f5;
     fCol["f5"] = f5 ;

     // bool
     Gtk::TreeModelColumn<bool> b1 ;
     bCol["b1"] = b1;

}
Columns::~Columns()
{
}
void Columns::addColumn(string sName, string sType)
{
     if (sType == "string") {
          add(sCol[sName]);
     }

     else if (sType == "int") {
          add(iCol[sName]);
     } else if (sType == "float") {
          add(fCol[sName]);
     } else if (sType == "bool") {
          add(bCol[sName]);
     }



}
Gtk::TreeModelColumn<Glib::ustring> Columns::getStringColumn(string sName)
{
     return sCol[sName] ;

}
Gtk::TreeModelColumn<unsigned> Columns::getIntColumn(string sName)
{
     return iCol[sName] ;

}
Gtk::TreeModelColumn<float> Columns::getFloatColumn(string sName)
{
     return fCol[sName] ;

}
Gtk::TreeModelColumn<bool> Columns::getBoolColumn(string sName)
{
     return bCol[sName] ;

}

listEntry::listEntry()
{
     bDistinct = false;
}
listEntry::~listEntry()
{

}


sendValues::sendValues() {}
sendValues::~sendValues() {}


Record::Record() {}
Record::~Record() {}




datatypes::datatypes()
{


}
datatypes::~datatypes()
{


}
