/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with this program; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <pqxx/pqxx>
#include <algorithm>

#include "../global.hpp"

#include "sql.hpp"



#include "../TypeDefs/iniReader.hpp"



MySql::MySql()
{
  // conn = new pqxx::connection(
  //   "username=temp "
  //   "host=db.corral.tacc.utexas.edu "
  //   "password=timelione "
  //   "dbname=temp");
 
}

result MySql::callRP(string rp,vector <string> v1){

  work w (*Conn);
  string value ;
  for (auto s : v1){
    value += "'" + s +"'" ;
    value += ",";
  }

  string value1 = value.substr(0,value.size()-1);
  cout << "value1 = " << value1 << endl ;
  cout << "rp = " << "select * from " + rp +"( " + value1 +")"   << endl ;

  result res =  w.exec("select * from " + rp +"( array [ " + value1 +" ])" ) ;
for (auto const &row: res)
{
   for (auto const &field: row) std::cout << field.c_str() << '\t';
   std::cout << '\n';

}
  w.commit();
  
  return res;
  
}
result MySql::callRP(string rp,string s1){

  work w (*Conn);


  result res =  w.exec("select * from " + rp +"('" + s1 +"')" ) ;
for (auto const &row: res)
{
   for (auto const &field: row) std::cout << field.c_str() << '\t';
   std::cout << '\n';

}
  w.commit();

  return res;

}

result MySql::callRP(string rp,string s1,string s2){

  work w (*Conn);


  result res =  w.exec("select * from " + rp +"('" + s1 +"', '" + s2 + "' )" ) ;
for (auto const &row: res)
{
   for (auto const &field: row) std::cout << field.c_str() << '\t';
   std::cout << '\n';

}
  w.commit();

  return res;

}

result MySql::callRP(string rp,string s1,string s2, string s3){

  work w (*Conn);


  result res =  w.exec("select * from " + rp + "( '" + s1 + "', '" + s2 + "', '" + s3 + "' )" ) ;
for (auto const &row: res)
{
   for (auto const &field: row) std::cout << field.c_str() << '\t';
   std::cout << '\n';

}
  w.commit();

  return res;

}
result MySql::callRP(string rp,string s1,string s2, string s3,string s4, int i1){

  work w (*Conn);

  string sExec = "select * from " + rp + "( '" + s1 + "', '" + s2 + "', '" + s3 + "','" + s4 + "'," + to_string(i1) + " ) as ("; ;
  //s2.replace(s2.begin(),s2.end(),':',' ');
  ranges::replace(s2, ':', ' ');
  cout << "s2 = " << s2 << endl;
  sExec += s2 + ") ";
  cout << sExec << endl ;

  result res =  w.exec(sExec ) ;
  for ( int i=0;i< res.columns();i++ ) std::cout << res.column_name(i)<< '\t';
  std::cout << '\n';
  cout << res.columns() << endl ;
for (auto const &row: res)
{
	cout << row.size() << endl ;

	cout << row[0].name() << endl ;
	cout << row[1].name() << endl ;
   for (auto const &field: row) std::cout << field << '\t';
   std::cout << '\n';

}
  w.commit();

  return res;

}


MySql::~MySql(){
}
