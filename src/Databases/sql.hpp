#ifndef SQL_HPP_INCLUDED
#define  SQL_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pqxx/pqxx>
#include "../global.hpp"
#include "datatypes.hpp"


//#include "/Clients/SingleClient.hpp>
//#include "/Addresses/SingleAddress.hpp>
#define tab_Data 0
using namespace pqxx;
using namespace std;

class MySql
{

public:

  MySql();
  ~MySql();
  result  callRP(string rp,vector <string> v1);
  result  callRP(string rp,string s1);
  result  callRP(string rp,string s1, string s2);
  result  callRP(string rp,string s1, string s2, string s3);
  result  callRP(string rp,string s1, string s2, string s3,string s4,int i1=0);

    protected:

    private:

};

#endif //  SQL_HPP_INCLUDED
