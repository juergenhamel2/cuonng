
#ifndef menu_hpp
#define menu_hpp

#define Uses_TKeys
#define Uses_TApplication
#define Uses_TEvent
#define Uses_TRect
#define Uses_TDialog
#define Uses_TStaticText
#define Uses_TButton
#define Uses_TMenuBar
#define Uses_TSubMenu
#define Uses_TMenuItem
#define Uses_TStatusLine
#define Uses_TStatusItem
#define Uses_TStatusDef
#define Uses_TDeskTop



#include <tvision/tv.h>
#include <stdlib.h>

const int GreetThemCmd = 100;
const int LoginCmd = 101;
const int AddressCmd = 200;


class menu {


public:



  //mainmenus

  std::string miConnection =  "~C~onnection" ;

  std::string miLogin = "~L~ogin";
  std::string miLogout = "Logout";
  std::string miData = "~D~ata";
  std::string miWork =  "~W~ork";

  std::string miAddress = "~A~ddress" ;

  std::string miC2D =  "Connect to Database";


  menu(TRect r );
  ~menu();

  TMenuBar* getMenuBar(TRect r  , std::string sWindow);

};


#endif
