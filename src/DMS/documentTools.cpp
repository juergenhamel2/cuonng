/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/DMS/documentTools.hpp>
#include <cuon/TypeDefs/basics.hpp>
#include <fstream>


documentTools::documentTools(){
   

}

documentTools::~documentTools(){
   
}

void documentTools::viewDocument(SingleDMS* singleDMS,map <string,string>* dicVars,string* Action, vector <map<string,string> >* liEmailAddresses ){
     string sExe ;
     
     
     singleDMS->printlog("load document from singledms");
     singleDMS->loadDocument();
     
     if (singleDMS->imageData != NULL ){
          singleDMS->getFileExePrg();
          singleDMS->printlog("image is loaded");
          sExe = singleDMS->getFileExePrg();
          singleDMS->printlog("sExe = ",sExe);
          string sFile =  singleDMS->getRandomFilename("__dms." + singleDMS->getFileSuffix() );
          singleDMS->printlog ("sFile ", sFile, (int)singleDMS->imageData->size() );
          
          
          singleDMS->writeFile(sFile,singleDMS->imageData);
          
          
          singleDMS->printlog("Now execute the given program");
          vector<string> vPrg {sExe,sFile} ;
          singleDMS->executePrg(vPrg);
          
          singleDMS->printlog("start external prg in foreground");
   
     }
}
     
// void compressBZ2(std::string file)
// {
// std::ifstream infile;
// int fileDestination = infile.open(file.c_str());

// char bz2Filename[] = "file.bz2";
// FILE *bz2File = fopen(bz2Filename, "wb");
// int bzError;
// const int BLOCK_MULTIPLIER = 7;
// BZFILE *myBZ = BZ2_bzWriteOpen(&bzError, bz2File, BLOCK_MULTIPLIER, 0, 0);

// const int BUF_SIZE = 10000;
// char* buf = new char[BUF_SIZE];
// ssize_t bytesRead;

// while ((bytesRead = read(fileDestination, buf, BUF_SIZE)) > 0)
// {
//     BZ2_bzWrite(&bzError, myBZ, buf, bytesRead);
// }

// BZ2_bzWriteClose(&bzError, myBZ, 0, NULL, NULL);

// delete[] buf;
// }
