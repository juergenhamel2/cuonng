/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/DMS/dms.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/DMS/documentTools.hpp>

dms::dms(int module, map <string,int>* sep_info, map <string,string>* dicVars, map <string,string>* dicExtInfo )
{
     //myXmlRpc x1 = myXmlRpc();
     documentTools*  oDocumentTools = new documentTools() ;
     
     tree1Name = "tv_address";


     v_new.push_back( "new1");


     menuItems["new"] = v_new;

     v_save.push_back( "save1");

     menuItems["save"] = v_save;

     v_delete.push_back("clear1");

     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;


     // Toolbar Buttons
     v_tnew.push_back( "tbNew");
     toolbarItems["new"] = v_tnew;

     v_tsave.push_back( "tbSave");
     toolbarItems["save"] = v_tsave;

     v_tdelete.push_back( "tbDelete");
     toolbarItems["delete"] = v_tdelete;
     
     v_tend.push_back( "tbExit");
     toolbarItems["quit"] = v_tend;
     
     

     init("dms","DMSMainwindow","dms");

     singleDMS = new SingleDMS();

    

     singleDMS->setWindow(window) ;
     singleDMS->setTree1() ;
     
     //signals from buttons
     window->get_widget("bView", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bView_clicked));

     window->get_widget("bImport", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bImport_clicked));

     
     // signals from the Tree
      window->get_widget("tree1", pTreeAll);
      pTreeAll->signal_row_activated().connect(sigc::mem_fun(this, &dms::on_row_activated));
     //connectSignal("mi_new1",new1);
      ModulNumber = MN["DMS"] ;


      window->get_widget("bSearch", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bSearch_clicked));

      
      if(module >0){
           ModulNumber = module;
      }
      printlog("ModulNumber = ", ModulNumber);
      
      if (ModulNumber != MN["DMS"]){

           singleDMS->sWhere  =  " where insert_from_module = " + toString(ModulNumber) + " and  sep_info_1 = " +  toString(sepInfo["1"] )  ;
           printlog ("sWhere = ", singleDMS->sWhere );
      }
      else{
                //scd = cuon.Misc.cuon_dialog.cuon_dialog()

      }
      if (dicExtInfo != NULL && dicExtInfo->find("LastDoc") != dicExtInfo->end() ){
           
     }

      
     tabChanged();

}
dms::~dms() {

     if( singleDMS) delete  singleDMS;
     if (oDocumentTools) delete  oDocumentTools;
     if (sWhereStandard) delete sWhereStandard ;
     
}



void dms::on_new_activate()
{
     printlog ("dms new 0") ;
     switch (tabOption) {

     case tabDMS:

          singleDMS->newData();
          string sDate = getDate();
          
          // dicDate = self.getActualDateTime()

          Gtk::Entry* pE1 ;
          window->get_widget("eDocumentDate",pE1);
          pE1->set_text(sDate);
          break;
     }
}

void dms::on_save_activate(){
  printlog ("dms save 0") ;
     switch (tabOption) {

     case tabDMS:
          //singleDMS->save();
          auto oldID = singleDMS->ID;
          singleDMS->sep_info_1 = sepInfo["1"];
          singleDMS->ModulNumber = ModulNumber;
          printlog("oldID  = ", oldID);
          
          singleDMS->saveBE();
          auto newID = singleDMS->ID;

          printlog("oldID, newID ", oldID, newID);

          if( oldID == -1 and newID > 0){
               //is a new entry
               printlog( "file suffix = ", singleDMS->fileSuffix);
               string* s;
               x1.callRP("Misc.getTextExtract", x1.add(newID, singleDMS->fileSuffix ) ,*s);
          }
          tabChanged();


          
     }
}



void dms::on_delete_activate(){
  printlog ("dms delete 0") ;
     switch (tabOption) {

     case tabDMS:
          singleDMS->deleteRecord();
     }

}

void dms::on_bView_clicked(){
    
     // singleDMS->loadDocument();
     oDocumentTools->viewDocument(singleDMS, dicVars);
     printlog("started external prg in foreground and back");
     
}

void dms::on_bImport_clicked(){
     string sFilename ;
     if(lastDoc.size() > 0){
          sFilename = lastDoc;

     }
     else{
          printlog("start search for filename");
          sFilename = getFilename("gfcb_ImportFile");

          printlog("sfilename ", sFilename);
     }
     singleDMS->imageData = readFile(sFilename);
     printlog("new imagedata size ", (int) singleDMS->imageData->size() );
}

void dms::on_bSearch_clicked(){

}


void dms:: on_row_activated(const Gtk::TreeModel::Path& path , Gtk::TreeViewColumn* ){
     on_bView_clicked();

}



void dms::tabChanged()
{
     switch(tabOption) {

     case tabDMS:
           printlog("start tab dms");

            if (singleDMS->imageData){
                 delete singleDMS->imageData;
                 singleDMS->imageData = NULL ;
            }
           
           printlog("start tab dms2");
           singleDMS->getListEntries();
          
          break;
     }
     oldTab = tabOption;

}


