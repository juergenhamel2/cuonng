#ifndef DOCUMENTTOOLS_HPP_INCLUDED
#define DOCUMENTTOOLS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include "SingleDMS.hpp"



class documentTools
{
public:

    
     
     documentTools();
     ~documentTools();
     void viewDocument(SingleDMS* singleDMS,map <string,string>* dicVars,string* Action =NULL, vector <map<string,string> >* liEmailAddresses=NULL ) ;   
     
protected:

private:

};


#endif // DOCUMENTTOOLS_HPP_INCLUDED
