#ifndef DMS_HPP_INCLUDED
#define DMS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include "../Windows/windows.hpp"
#include "SingleDMS.hpp"
#include "documentTools.hpp"

#define tabDMS  0


class dms : public windows
{
    public:

     documentTools*  oDocumentTools;
     map<string,string>* dicVars;
     map <string,int> sepInfo;
     SingleDMS* singleDMS;
     string lastDoc ;
     
     dms(int module = 0, map <string,int>* sep_info = NULL , map <string,string>* dicVars = NULL, map <string,string>* dicExtInfo = NULL );
     ~dms();
     virtual void tabChanged();
     
     void on_bView_clicked();
     void on_bImport_clicked();
     
     void on_row_activated(const Gtk::TreeModel::Path& path , Gtk::TreeViewColumn* );
     void on_new_activate();
     void on_save_activate();
     void on_delete_activate();

     void on_bSearch_clicked();
     
    protected:

    private:

};

#endif // DMS_HPP_INCLUDED
