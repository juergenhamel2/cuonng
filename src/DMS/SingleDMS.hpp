#ifndef SINGLEDMS_HPP_INCLUDED
#define SINGLEDMS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include "../Databases/Singledata.hpp"
#include "../Encoder/smallBase64.hpp"


class SingleDMS : public Singledata
{
public:
     
     string* imageData ;
     string fileFormat;
     string fileSuffix;
     smallBase64* base64 ;
     string imageDataZ ;
        SingleDMS();
     ~SingleDMS();
     void loadDocument();
     void loadMainLogo();
     string getFileFormatType();
     string getFileExePrg();
     string  getFileSuffix();
     void saveBE( );
    protected:

    private:

};

#endif // SINGLEDMS_HPP_INCLUDED
