#ifndef ORDER_HPP_INCLUDED
#define ORDER_HPP_INCLUDED


#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Order/SingleOrder.hpp>


#define tabOrder_Order 0
//#define tabstock_Goods 1


class order:public windows, public cuon_window {
     
    public:

     
     
     SingleOrder *singleOrder ;
     SingleArticle *singleArticle;
     

        order();
     ~order();
     //virtual int initAll();
     virtual int initOrder( map <string,string>* dicOrder, bool newOrder, int orderid ,string OrderType);
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(order, cuon_window);



#endif // ORDER_HPP_INCLUDED
