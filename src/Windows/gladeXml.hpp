#ifndef GLADEXML_HPP
#define GLADEXML_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>

#include "setOfEntries.hpp"
#include "../Databases/datatypes.hpp"
#include "../Databases/sql.hpp"
#include "../TypeDefs/basics.hpp"
#include "../MyXML/myxml.hpp"

#define new1 1
#define save1 3
#define delete1 5



class gladeXml : public basics {

public:

	setOfEntries entries;
     typedef void (gladeXml::*pConnectItem) ();
     pConnectItem ItemFunc ;

  MySql x1;
     int tabOption ;
     myxml *MyXml ;
  //localCache* Cache ;
     Glib::RefPtr<Gtk::Builder> window ;
     Gtk::Builder*  pWindow ;
     string tree1Name;
     map <string,vector <string> > menuItems ;
     map <string, vector <string> > toolbarItems ;
     vector <string> v_new;
     vector <string> v_save;
     vector <string> v_delete;
     vector <string> v_end;

     vector <string> v_tnew;
     vector <string> v_tsave;
     vector <string> v_tdelete;
     vector <string> v_tend;

     Gtk::Window *pWin;
     Gtk::Window *pMainWindow;
     
     Gtk::Notebook *pNotebook;
     Gtk::Notebook *pNotebook2;
     Gtk::TreeView *pTree1;
     Gtk::TreeView *pTreeAll;
     
     Gtk::MenuItem* pEnd;
     Gtk::MenuItem* pNew;
     Gtk::MenuItem* pEdit;
     Gtk::MenuItem* pSave;
     Gtk::MenuItem* pDelete;
     Gtk::MenuItem* pItem;
     Gtk::Button* pButton;
     Gtk::FileChooserWidget* pFileChooser;
     Gtk::Entry *pEntry ;
     Gtk::TextView *pTextview ;
     Gtk::Statusbar *pStatusbar ;

     gladeXml();
     ~gladeXml();


    



     
     /// @param gladeName Name of the the glade file
     /// @param sMainWindow Name of the top window widget in the glade file
     /// @param gladePath Path to the Glade file on the Harddisk
     /// @return the correspondending glade object
     
    void  loadGlade(string gladeName, string sMainWindow, string gladePath)  ;
     void loadGlade(string gladeName, string sMainWindow);
    void init(string gladeName,string MainWindow, string EntryName);

     void setTitle(string s);
     map<string,vector <string> > loadEntries(string EntryName);
     void fetchDefaultGUI(string sMainWindow);
	void setText(setOfEntries _entry, string _Value);
	string getText(setOfEntries _entry);

     virtual void initKeys();
     
     virtual void onNotebook1PageSwitch ( Gtk::Widget *page, guint page_num );
     virtual void onNotebook2PageSwitch ( Gtk::Widget *page, guint page_num );
     virtual void on_tree1_row_activated(const Gtk::TreeModel::Path &, Gtk::TreeViewColumn*);
     virtual void on_end_activate();
     virtual void on_edit_activate();
     virtual void on_new_activate();
     virtual void on_save_activate();
     virtual void on_delete_activate();
     virtual void tabChanged();
     virtual void tabChanged2();

     virtual void on_signal_connect();
     void connectMenuItem(string sItem) ;
     
     void connectSignal(string sItem);
     void connectToolbarSignal(string sItem);



     virtual void on_new1_activate() ;
     virtual void on_save1_activate();
      virtual void on_edit1_activate();
     virtual void on_delete1_activate() ;

     void getWidget(string sName, Gtk::Entry *entry);
     void getWidget(string sName, Gtk::MenuItem *item);
     void getWidget(string sName, Gtk::Menu *item);
     void getWidget(string sName,Gtk::ScrolledWindow* sc );
     void getWidget(string sName, Gtk::Button *item);
     void getWidget(string sName, Gtk::ComboBox *combobox);
     void getWidget(string sName, Gtk::FileChooserButton *gfcb);


     string getFilename(string sName);
     void setComboBoxEntries(string sWidget,string sProcedure);
	int getCBActive(setOfEntries _entry);
	bool getCBState(setOfEntries _entry);
     void enableMenuItem(string sName);
     string getPrefix();
};

#endif

