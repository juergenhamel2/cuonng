#include <map>
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <stdio.h>
#include <unistd.h>

#include "windows.hpp"

using namespace std;

windows::windows()
{
     oldTab = -1;
     ModulNumber = 0;
     sWhereStandard = NULL;
     


}
windows::~windows()
{
     if(sWhereStandard) delete sWhereStandard ;
}

bool windows::onKeyPressAction(GdkEventKey *event){
     printlog("key pressed" + toString(event->keyval) );
     setNotebookPages(event);
     doNormalKeyEvents(event);
     doKeyAction(event);

}

void windows::doNormalKeyEvents(GdkEventKey *event){
     if (event->state & GDK_CONTROL_MASK){
          switch (event->keyval){
          case GDK_KEY_s:
               on_save_activate();
               break;
          case GDK_KEY_e:
               on_edit_activate();
               break;
          case GDK_KEY_d:
               on_delete_activate();
               break;
          case GDK_KEY_n:
               on_new_activate();
               break;


          }
     }
}
void windows::doKeyAction(GdkEventKey *event){

}

void windows::setNotebookPages(GdkEventKey *event){
     if (event->keyval == GDK_KEY_F1 ){
          printlog ("F1 pressed");
          pNotebook->set_current_page(0);
     }
     else if (event->keyval == GDK_KEY_F2 ){
          printlog ("F2 pressed");
          pNotebook->set_current_page(1);
     }
     else if (event->keyval == GDK_KEY_F3 ){

          pNotebook->set_current_page(2);
     }
     else if (event->keyval == GDK_KEY_F4 ){

          pNotebook->set_current_page(3);
     }
     else if (event->keyval == GDK_KEY_F5 ){

          pNotebook->set_current_page(4);
     }
     else if (event->keyval == GDK_KEY_F6 ){

          pNotebook->set_current_page(5);
     }
     else if (event->keyval == GDK_KEY_F7 ){

          pNotebook->set_current_page(6);
     }
     else if (event->keyval == GDK_KEY_F8 ){

          pNotebook->set_current_page(7);
     }
     else if (event->keyval == GDK_KEY_F9 ){

          pNotebook->set_current_page(8);
     }
     else if (event->keyval == GDK_KEY_F10 ){

          pNotebook->set_current_page(9);
     }

     else if (event->keyval == GDK_KEY_F11 ){

          pNotebook->set_current_page(10);
     }
     else if (event->keyval == GDK_KEY_F12 ){

          pNotebook->set_current_page(11);
     }
}

void windows::initKeys(){
     pWin->add_events( Gdk::KEY_PRESS_MASK );

     pWin->signal_key_press_event().connect ( sigc::mem_fun(*this, &windows::onKeyPressAction) );
     

}
void windows::tabChanged()
{

     cout << "tab changed at window" << endl;
}
void windows::tabChanged2()
{
}


void windows::closeWindow(){
     if(pMainWindow){
          pMainWindow->hide();
     }
}

void windows::setChooseButton(GtkButton*  button){
     chooseButton = button ;
}


        
       
void windows::setChooseEntry( Gtk::Entry* entry){
     // sName = name of Menuitem for choose
     // entry = entry to set value
     printlog("<<<<<<<<<<<<<<< setCooseEntry <<<<<<<<<<<<<<<<<<<<<");

     chooseEntry = entry;
     
     chooseMenuItem->set_sensitive(true);
}

/*

    def pressChoosebutton(self):
        if self.chooseButton:
            self.activateClick(self.chooseButton, 'clicked')
    */

void windows::setChooseValue(int chooseValue){

     printlog("chooseValue = " + toString(chooseValue) );
     chooseEntry->set_text(toString(chooseValue));
     
     if( chooseMenuItem){
          chooseMenuItem->set_sensitive(false);
          closeWindow();
     }

}
/*
    def getChangedValue(self, sName):
        iNumber = 0
        s = self.getWidget( sName).get_text()
        if s:
            if len(string.strip(s)) > 0:
                if s.isdigit():
                    iNumber = long(s)
                else:
                    s = string.strip(s)
                    if s[len(s) - 1] == 'L':
                        iNumber = long(s[0:len(s) -1])
                    
        return iNumber
    
    */

