#ifndef SETOFENTRIES_HPP_INCLUDED
#define SETOFENTRIES_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// #include "../global.hpp"



class setOfEntries
{
    public:

	map<string, string> mEntry;
	map<string, map<string, string> > Entries;
	string nameOfEntry;
	string typeOfEntry;
	bool bDuty;
	int Round;
	string nextWidget;
	string verifyType;
	string SqlField;



        setOfEntries();
        ~setOfEntries();

	// set/get routines
	void set_nameOfEntry(string _nameOfEntry);
	string get_nameOfEntry();

	void set_typeOfEntry(string _typeOfEntry);
	string get_typeOfEntry();

	void set_verifyTypeOfEntry(string _verifyTypeOfEntry);
	string get_verifyTypeOfEntry();

	void set_nameOfSql(string _nameOfSql);
	string get_nameOfSql();

	void set_Round(int _iRound);
	int get_Round();


	void addEntry(map<string, string> _mEntry);
        void setDic();
        map<string,string> getDicEntries();
	map<string, map<string, string> > getEntries();

	void set_actualEntry(string Name);

	map<string, string> get_actualEntry();

};


#endif // SETOFENTRIES_HPP_INCLUDED
