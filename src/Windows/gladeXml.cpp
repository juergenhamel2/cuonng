/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


/**
 * @file   gladeXml.cpp
 * @author  <jhamel@x6>
 * @date   Tue Mar 10 19:16:36 2015
 * 
 * @brief  
 * 
 * 
 */

#include <map>
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <exception>

#include "../global.hpp"

#include "gladeXml.hpp"
#include "setOfEntries.hpp"
#include "../TypeDefs/basics.hpp"
#include "../Databases/localCache.hpp"
#include "../Databases/sql.hpp"
#include "../MyXML/myxml.hpp"

using namespace std;


gladeXml::gladeXml()
{

	pMainWindow = NULL ;
	MyXml = new myxml();
	//Cache = new localCache();
	ItemFunc = &gladeXml::on_signal_connect ;
	tree1Name = "tree1";
	//     menuItems["quit"] = "mi_quit1";
	//     menuItems["new"] = "mi_new1";
	//     menuItems["edit"] = "mi_edit1";
	//     menuItems["save"] = "mi_save1";
	//     menuItems["delete"] = "mi_clear1";



	tabOption = 0;
}

gladeXml::~gladeXml()
{
}
void gladeXml::loadGlade(string gladeName, string sMainWindow, string gladePath)
{
	window = Gtk::Builder::create_from_file("../" + gladeName + ".xml");
	cout << "window create 0 " << endl ;
	window->get_widget(sMainWindow,pMainWindow);
	fetchDefaultGUI(sMainWindow);


	return ;
}

void gladeXml::loadGlade(string gladeName, string sMainWindow)
{
	string sGlade;
	string sPrefix = getPrefix();
	vector <string> vGlade ;

	string sName =  sPrefix + gladeName + ".glade" ;
	cout << "User: " << oUser.sUser["Name"] << ", " <<  sName << endl;

	// string sKey = Cache->getKCache(sName);
	// cout << "key from Cache = " << sKey << endl ;

	// x1.callRP("Database.getGladeKey",x1.add(sName,sKey,false),vGlade);

	// //to it = dicGlade->find(sKey);
	// // (it != dicGlade->end()){

	// if(vGlade[0] == sKey){
	//      //cout << "Key is in Cache !!!!!!!" << endl ;

	//   sGlade = *Cache->getSCache(sKey);
	//cout << "sGlade from Cache = " << sGlade << endl ;
	// }
	// else{
	//cout << "Key is not in Cache -------" << endl ;

	//sGlade = (string)vGlade[1];
	//   Cache->writeKCache(sName,(string)vGlade[0]);
	//Cache->writeSCache((string)vGlade[0], sGlade);
	//cout << "new Glade is now " << (string)vGlade[0] <<  endl ;
	//}

	//cout << sGlade << endl ;
	try{
		//window = Gtk::Builder::create_from_string(sName);
		window = Gtk::Builder::create_from_file(sName);

		cout << "window create 0 " << endl ;
		window->get_widget(sMainWindow,pMainWindow);
		fetchDefaultGUI(sMainWindow);
	}
	catch (Glib::MarkupError &ex) {
		cout << ex.what() << endl ;
	}



	return ;

}


void gladeXml::init(string gladeName,string MainWindow, string EntryName="NONE" )
{

	loadGlade(gladeName,MainWindow);
	//oEntries = loadEntries(EntryName);
	initKeys();



}

void gladeXml::initKeys(){

}

string gladeXml::getPrefix(){
	string sPrefix ;

	if (oUser.sUser["GUI"] == "NO"){
		sPrefix = "GUI/GTK3/";
	}
	else{
		sPrefix = "GUI/" + oUser.sUser["GUI"] +"/" ;
	}
	return sPrefix ;


}

map<string,vector <string> > gladeXml::loadEntries(string EntryName)
{
	setOfEntries mEntries ;
	string sEntry;

	vector <map <string,string> > Entries ;
	string sPrefix = getPrefix() ;
	printlog( " Test loadEntries 0 ") ;
	if (EntryName != "NONE") {

		EntryName =  "entry_" + EntryName + ".xml" ;


		//printlog("load Entry = entry_" + sPrefix + EntryName);


		// string sKey = Cache->getKCache ("entry_" + sPrefix + EntryName);
		// //cout << "key from Cache = " << sKey << endl ;
		// if (sKey == getNullUUID() ){
		//      /* Key not found, set to standard */
		//      sKey =  Cache->getKCache( "entry_" + EntryName);
		//      if (sKey == "NONE" ){
		//           sKey = getNullUUID();
		//      }
		// }
		//cout << "key from Cache2 = " << sKey << endl ;
		auto sKey = getNullUUID();
		string sEntry = sPrefix + "entry_" + EntryName +".xml";
		//x1.callRP("Database.getEntryKey",x1.add(paEntry,false),Entries);
		printlog( " Test loadEntries 2 " + sEntry) ;

		mEntries.addEntry(MyXml->getEntry(sEntry));

		//          map <string,string> dbEntryName = Entries.back();
		//          printlog( " Test loadEntries 2.1 ") ;
		//          Entries.pop_back();
		//          map <string,string> dbUUID = Entries.back();
		//          printlog( " Test loadEntries 2.3 ") ;
		//          Entries.pop_back();
		//          printlog( " Test loadEntries 4 ") ;
		//          //cout << "Keys = " << dbUUID["___UUID"] << ", " <<  sKey << endl ;
		//          if(dbUUID["___UUID"] == sKey){
		//               //cout << "Key is in Cache !!!!!!! for " << dbEntryName["___EntryName"] << endl ;
		//
		//               //sEntry = *Cache->getSCache(sKey);
		//               //cout << "sKey from Cache = " << sKey << endl ;
		//               //cout << "Data = " << sEntry << endl ;
		//               vector <string> vMaps = split(sEntry,"+");
		//               //cout << "size of vMaps = " << vMaps.size() << endl ;
		//                map <string,string> _newMap;
		//               for (auto it = vMaps.begin(); it != vMaps.end();it++){
		//                    vector <string> vValues = split(*it,"|");
		//
		//                    for (auto iM = vValues.begin(); iM != vValues.end(); iM ++ ){
		//
		//                         //cout << "Values " << *iM <<  endl ;
		//
		//                         vector <string> vNewEntry = split(*iM,",");
		//                         if(vNewEntry.size() >1){
		//
		//                              //cout << "insert to map: " << vNewEntry[0] << " = " << vNewEntry[1] << endl ;
		//                              _newMap[vNewEntry[0]] = vNewEntry[1];
		//
		//                         }
		//
		//
		//                    }
		//
		//
		//
		//               }
		//                Entries.push_back(_newMap) ;
		//
	}
	//          else{
	//               //cout << "Key is not in Cache -------" << endl ;
	//               string newKey = dbUUID["___UUID"];
	//
	//               string newName = dbEntryName["___EntryName"];
	//               string newData ;
	//
	//               //cout << "Data1 = " << newKey << newName << endl ;
	//
	//               for (auto i =Entries.begin(); i != Entries.end(); i++) {
	//
	//
	//                    map <string,string> newMap = *i;
	//                    for(auto it = newMap.begin(); it != newMap.end(); it++){
	//                         newData += it->first + "," + it->second + "|"  ;
	//                    }
	//                    //newData.erasenewData.end()-1,newData.end());
	//                    newData += "+";
	//
	//          }

	//	if (newKey == "NONE"){
	//		//cout << "Not a UUID" << endl;
	//	}
	//	else{
	//		//Cache->writeKCache(newName,newKey);
	//		//Cache->writeSCache(newKey,newData);
	//		//cout << "new Entry  is now " << newName << " = Key " << newKey << "Data = " << newData << endl ;
// }
//}




// x1.callRP("Database.getEntry",x1.add( "entry_" + sPrefix + EntryName,false),Entries);
// if (Entries.size()  == 0){
//      printlog("###### Error -- Definition for  entry_" + sPrefix +EntryName + " not found, try to load Standard");
//      x1.callRP("Database.getEntry",x1.add( "entry_" + EntryName,false),Entries);
// }

//typedef vector < map<string,string> > ::const_iterator iV ;
//for (auto i =Entries.begin(); i != Entries.end(); i++) {
//
//	map <string,string> newMap = *i;
//	//for(auto it = newMap.cbegin(); it != newMap.cend(); ++it){
//	//    printlog(it->first + ", " + it->second);
//	//}
//	_entries.addEntry(newMap);
//}
//
//}
return oEntries ;
}
void gladeXml::fetchDefaultGUI(string sMainWindow)
{
	try {


		//cout << "win 0" << endl ;
		window->get_widget(sMainWindow,pWin);
		//        cout << "win 1" << endl ;
		// if(pWin) pWin->show();
		cout << "win 2" << endl ;
	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}
	///Connect Notebook signals
	try {
		Glib::RefPtr< Glib::Object > o = window->get_object("notebook1");

		cout << "notebook 0" << endl ;
		if (o) {
			cout << "notebook 1" << endl ;
			window->get_widget("notebook1", pNotebook);
			cout << "notebook 2" << endl ;
			if(pNotebook) pNotebook->signal_switch_page().connect(sigc::mem_fun(this, &gladeXml::onNotebook1PageSwitch));
			cout << "notebook 3" << endl ;
		}
	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}
	///Connect Notebook2 signals
	try {
		Glib::RefPtr< Glib::Object > o = window->get_object("notebook2");
		if(o) {
			window->get_widget("notebook2", pNotebook2);
			if(pNotebook2) pNotebook2->signal_switch_page().connect(sigc::mem_fun(this, &gladeXml::onNotebook2PageSwitch));
		}
	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}
	///get statusbar
	try {
		Glib::RefPtr< Glib::Object > o = window->get_object("statusbar1");
		if(o) {
			window->get_widget("statusbar1", pStatusbar);

		}
	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}

	///Connect tree1 signals
	try {
		Glib::RefPtr< Glib::Object > o = window->get_object(tree1Name);
		if(o) {
			window->get_widget(tree1Name, pTree1);
			if(pTree1) pTree1->signal_row_activated().connect(sigc::mem_fun(this, &gladeXml::on_tree1_row_activated));
		}
	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}


	typedef  map<string,vector <string> >::const_iterator iM ;
	for (iM i = menuItems.begin(); i!= menuItems.end(); i++) {
		connectSignal(i->first);


	}

	typedef  map<string,vector <string> >::const_iterator iM2 ;
	for (iM2 i = toolbarItems.begin(); i!= toolbarItems.end(); i++) {
		connectToolbarSignal(i->first);


	}

}

void gladeXml::setTitle(string s)
{

	pWin->set_title(s);
}


void gladeXml::setText(setOfEntries _entry, string _Value)
{


	printlog("---- set Text to " +_entry.nameOfEntry +" as " + _Value );
	printlog("---- type = " +  _entry.get_verifyTypeOfEntry() + ", " + _entry.get_typeOfEntry() );

	if (_entry.get_typeOfEntry() == "checkbox"){
		Gtk::CheckButton *e1 ;
		window->get_widget(_entry.nameOfEntry, e1);

		if (String2Int(_Value) == 0){

			e1->set_active(false);
		}
		else{
			e1->set_active(true);
		}
	}
	else if (_entry.get_typeOfEntry() == "combobox") {

		Gtk::ComboBox *e1 ;
		window->get_widget(_entry.nameOfEntry, e1);
		e1->set_active(String2Int(_Value) ) ;
	}
	else if(_entry.get_verifyTypeOfEntry() == "datetime") {

		//cout << "set Text to " <<_entry.nameOfEntry << " type = " << _entry.get_typeOfEntry() << endl;


		Gtk::Entry *e1 ;
		window->get_widget(_entry.nameOfEntry, e1);
		if (_Value == "NONE") {
			_Value = "";
		}

		e1->set_text(_Value) ;


	}

	else if(_entry.get_verifyTypeOfEntry() == "string") {

		//cout << "set Text to " <<_entry.nameOfEntry << " type = " << _entry.get_typeOfEntry() << endl;
		if (_entry.get_typeOfEntry() == "textfield") {

		} else {

			Gtk::Entry *e1 ;
			window->get_widget(_entry.nameOfEntry, e1);
			if (_Value == "NONE") {
				_Value = "";
			}
			e1->set_text(_Value) ;
		}

	}



	else{

		Gtk::Entry *e1 ;
		window->get_widget(_entry.nameOfEntry, e1);
		if (_Value == "NONE") {
			_Value = "";
		}
		e1->set_text(_Value) ;

	}
}




string gladeXml::getText(setOfEntries _entry)
{

	string _Value ;

	if (_entry.get_typeOfEntry() == "textfield") {

	}

	else if (_entry.get_typeOfEntry() == "checkbox"){
		Gtk::CheckButton *e1 ;
		window->get_widget(_entry.nameOfEntry, e1);
		_Value = toString(e1->get_active()) ;
	}
	else {

		Gtk::Entry *e1 ;
		window->get_widget(_entry.nameOfEntry, e1);

		_Value = e1->get_text() ;
	}

	return _Value ;
}

void gladeXml::setComboBoxEntries(string sWidget,string sProcedure)
{
	try{
		vector <string> v1;
		//x1.callRP(sProcedure,x1.add("NO_ENTRY"),v1) ;
		Gtk::ComboBox *e1 ;
		window->get_widget(sWidget, e1);
		Glib::RefPtr<Gtk::TreeStore> treeStore;
		//Gtk::ListStore *liststore ;
		Gtk::TreeModel::Row row ;
		Columns cols ;
		cols.addColumn("s1", "string");
		e1->set_model(treeStore = Gtk::TreeStore::create(cols));
		e1->set_entry_text_column(cols.getStringColumn("s1"));
		typedef vector<string>::const_iterator iV ;
		for (iV i =v1.begin(); i != v1.end(); i++) {
			//cout << *i << endl ;
			Gtk::TreeModel::Row row = *(treeStore->append());
			row[cols.getStringColumn("s1")] = *i ;



		}
		e1->pack_start(cols.getStringColumn("s1"));
		e1->set_active(0);
		e1->show_all_children();
	}
	catch (exception const &e1){
		cout << e1.what() << endl ;
	}

	//            for oneStatus in liProjectStatus:
	//                liststore.append([oneStatus])
	//            cbProjectStatus.set_model(liststore)
	//            self.setComboBoxTextColumn(cbProjectStatus)
	//            cbProjectStatus.show()

}



int gladeXml::getCBActive(setOfEntries _entry) {
	int _Value = 0;
	Gtk::ComboBox *e1 ;
	window->get_widget(_entry.get_nameOfEntry(), e1);

	_Value = e1->get_active_row_number() ;
	return _Value ;
}



bool gladeXml::getCBState(setOfEntries _entry) {
	int _Value = 0;
	Gtk::CheckButton *e1 ;
	window->get_widget(_entry.get_nameOfEntry(), e1);

	_Value = e1->get_active() ;
	return _Value ;
}

void  gladeXml::getWidget(string sName, Gtk::Entry *entry){
	window->get_widget(sName,entry);
}
void  gladeXml::getWidget(string sName, Gtk::MenuItem *item){

	window->get_widget(sName,item);
}

void  gladeXml::getWidget(string sName, Gtk::Button *item){

	window->get_widget(sName,item);
}

void  gladeXml::getWidget(string sName, Gtk::Menu *item){
	window->get_widget(sName,item);
}


void  gladeXml::getWidget(string sName, Gtk::ComboBox *combobox){
	window->get_widget(sName,combobox);
}

void  gladeXml::getWidget(string sName, Gtk::ScrolledWindow* sc){

	printlog("get the scrollwindow");
	window->get_widget(sName,sc);

}

void  gladeXml::getWidget(string sName, Gtk::FileChooserButton *gfcb){
	window->get_widget(sName,gfcb);
	cout << "Filename at glade = " << gfcb->get_filename() << endl ;

}

string  gladeXml::getFilename(string sName){

	Gtk::FileChooserButton *gfcb ;
	window->get_widget(sName,gfcb);
	return gfcb->get_filename() ;

}

void gladeXml::on_signal_connect() {}


void gladeXml::connectMenuItem(string sItem){
	window->get_widget("end1", pItem);

	pItem->signal_activate().connect(sigc::mem_fun(this, ItemFunc ) );

}


void gladeXml::connectSignal(string sItem)
{
	try {

		Gtk::MenuItem* pM ;

		cout << "set signal to item = " << sItem << endl ;
		typedef vector <string>::const_iterator iV ;
		vector <string> v1 = menuItems[sItem] ;
		for (iV i = v1.begin(); i != v1.end(); i++) {

			window->get_widget(*i, pM);



			if (sItem == "new")    pM->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_new_activate));
			else if (sItem == "save") pM->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_save_activate));
			else if ( sItem == "delete") pM->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_delete_activate));
			else if ( sItem == "quit") pM->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_end_activate));



		}

	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}

}

void gladeXml::connectToolbarSignal(string sItem)
{
	try {

		Gtk::ToolButton* pM ;
		typedef vector <string>::const_iterator iV ;
		vector <string> v1 = toolbarItems[sItem] ;
		for (iV i = v1.begin(); i != v1.end(); i++) {

			cout << "set signal to item = " << sItem << endl ;
			window->get_widget(*i, pM);



			if (sItem == "new")    pM->signal_clicked().connect(sigc::mem_fun(this, &gladeXml::on_new_activate));
			else if (sItem == "save") pM->signal_clicked().connect(sigc::mem_fun(this, &gladeXml::on_save_activate));
			else if ( sItem == "delete") pM->signal_clicked().connect(sigc::mem_fun(this, &gladeXml::on_delete_activate));
			else if ( sItem == "edit") pM->signal_clicked().connect(sigc::mem_fun(this, &gladeXml::on_edit_activate));
			else if ( sItem == "quit") pM->signal_clicked().connect(sigc::mem_fun(this, &gladeXml::on_end_activate));
		}


	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}

}


void gladeXml::on_end_activate()
{
	cout << "glade end called" << endl ;

	if(pWin) pWin->hide();


}

void gladeXml::on_edit_activate()
{
	cout << "glade edit called" << endl ;


}

void gladeXml::on_new_activate()
{
	cout << "glade new called" << endl ;


}

void gladeXml::on_save_activate()
{
	cout << "glade save called" << endl ;



}

void gladeXml::on_delete_activate()
{
	cout << "glade delete called" << endl ;


}

void gladeXml::on_new1_activate() {}
void gladeXml::on_save1_activate() {}
void gladeXml::on_edit1_activate() {}
void gladeXml::on_delete1_activate() {}




void gladeXml::onNotebook1PageSwitch ( Gtk::Widget *page, guint page_num )
{
	tabOption = page_num ;
	tabChanged();
	cout << "Page gladeXml = " << page_num << endl;

}
void gladeXml::onNotebook2PageSwitch ( Gtk::Widget *page, guint page_num )
{
	tabChanged2();
	cout << "Page2 gladeXml = " << page_num << endl;

}

void gladeXml::on_tree1_row_activated(const Gtk::TreeModel::Path &, Gtk::TreeViewColumn*)
{

}


void gladeXml::tabChanged()
{

	cout << "tab changed at glade" << endl;
}
void gladeXml::tabChanged2()
{
}

void gladeXml::enableMenuItem(string sName){
	try{
		//cout << "Item = " << sName << endl ;

		Gtk::MenuBar* pMenuBar = NULL ;
		Gtk::MenuItem* pMenu = NULL ;


		window->get_widget(sName,pMenu);
		if (pMenu){

			cout << "Menu found" << endl ;
			pMenu->set_visible(true);
			pMenu->set_sensitive(true);

		}


	} catch(exception const& e) {

		cerr << "Client threw error: " << e.what() << endl;
	}

}





