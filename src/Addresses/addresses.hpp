#ifndef ADDRESSES_HPP_INCLUDED
#define ADDRESSES_HPP_INCLUDED

#include "../Windows/windows.hpp"
#include "SingleAddress.hpp"
#include "SinglePartner.hpp"
#include "SingleMisc.hpp"
#include "SingleBank.hpp"
#include "SingleNote.hpp"




#define tabadr_Address 0
#define tabadr_Bank 1
#define tabadr_Misc 2
#define tabadr_Partner 3


#define tabadr_Schedul 4
#define tabadr_Notes 5
#define tabadr_Proposal 6
#define tabadr_Order 7
#define tabadr_Invoice 8
#define tabadr_Project 9
#define tabadr_Html 10

#define tabadr_Graves 11
#define tabadr_Hibernation 12


class addresses:public windows {

public:

     int addressID ;

     
     SingleAddress *singleAddress ;
     SinglePartner *singlePartner ;
     SingleMisc *singleMisc ;
     SingleBank *singleBank ;
     SingleNote *singleNote ;
     addresses();
     ~addresses();


     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_chooseAddress_activate();
     void on_bSearch_clicked();
     void tabChanged();


};



#endif // ADDRESSES_HPP_INCLUDED
