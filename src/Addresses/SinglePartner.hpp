#ifndef SINGLEPARTNER_HPP_INCLUDED
#define SINGLEPARTNER_HPP_INCLUDED

#include "../Databases/Singledata.hpp"

class SinglePartner:public Singledata {
public:
     int addressID ;
     SinglePartner();
     ~SinglePartner();
     virtual void readExtraEntries();
};

#endif // SINGLEPARTNER_HPP_INCLUDED
