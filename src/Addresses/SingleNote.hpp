#ifndef SINGLENOTE_HPP_INCLUDED
#define SINGLENOTE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../global.hpp"
#include <gtksourceviewmm.h>

#include "../Databases/Singledata.hpp"



class SingleNote: public Singledata {
public:
     int addressID ;
     string NotesMisc ;
     string NotesContacter ;
     string NotesRep ;
     string NotesSalesman ;
     string NotesCompany ;
     
      Gsv::View* svMisc;
      Gsv::View* svContacter;
      Gsv::View* svRep;
      Gsv::View* svSalesman;
      Gsv::View* svCompany;
     SingleNote();
     ~SingleNote();
     virtual void readNonWidgetEntries();
     virtual void fillOtherEntries();
protected:

private:

};

#endif // SINGLENOTE_HPP_INCLUDED
