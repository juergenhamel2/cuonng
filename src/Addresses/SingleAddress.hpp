#ifndef SINGLEADDRESS_HPP_INCLUDED
    #define SINGLEADDRESS_HPP_INCLUDED

    #include "../Databases/Singledata.hpp"

    class SingleAddress:public Singledata{
        public:
            SingleAddress();
            ~SingleAddress();
            string getAddress(int _id);

    };

#endif // SINGLEADDRESS_HPP_INCLUDED
