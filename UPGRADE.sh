#!/bin/bash

#Upgrade from cuon to cuonng:



#all tables need this:
#for example:
#alter table TABLE  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;


echo "DROP INDEX index_address_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_address_bank_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_address_misc_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_partner_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_partner_schedul_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_address_notes_id ;" | psql -Ucuon_admin cuon

echo "DROP INDEX index_articles_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  articles_idx_designation ;" | psql -Ucuon_admin cuon
echo "DROP INDEX articles_idx_gin_designation ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  articles_idx_gin_number ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   articles_idx_number ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_articles_stock_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_articles_stock_id ;" | psql -Ucuon_admin cuon

echo "DROP INDEX index_staff_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_staff_disease_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_staff_fee_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_staff_misc_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX index_staff_vacation_id ;" | psql -Ucuon_admin cuon

echo "DROP INDEX bank_idx_bcn ;" | psql -Ucuon_admin cuon

echo "DROP INDEX botany_idx_all1 ;" | psql -Ucuon_admin cuon

echo "DROP INDEX  cash_desk_idx_date ;" | psql -Ucuon_admin cuon
echo "DROP INDEX cash_desk_idx_desk_number ;" | psql -Ucuon_admin cuon
echo "DROP INDEX cash_desk_idx_procedure ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  cash_desk_idx_user_short_cut ;" | psql -Ucuon_admin cuon

echo "DROP INDEX contact_idx_address_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX contact_idx_char_length_schedul_date ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  contact_idx_contacter_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  contact_idx_date_century_insert_time ;" | psql -Ucuon_admin cuon
echo "DROP INDEX contact_idx_date_day_insert_time  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX contact_idx_date_decade_insert_time ;" | psql -Ucuon_admin cuon
echo "DROP INDEX contact_idx_date_part_month_insert_time ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   contact_idx_date_part_quarter_insert_time ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  contact_idx_date_part_week_insert_time ;" | psql -Ucuon_admin cuon
echo "DROP INDEX contact_idx_date_part_year_insert_time ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  contact_idx_process_status ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  contact_idx_status_client ;" | psql -Ucuon_admin cuon

echo "DROP INDEX  in_payment_idx_account_id  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_account_info_id;" | psql -Ucuon_admin cuon
echo "DROP INDEX  index_account_plan_id  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  index_account_sentence_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX    index_account_system_id;" | psql -Ucuon_admin cuon
echo "DROP INDEX  index_biblio_id  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  index_botany_class_id  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  index_botany_divisio_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_botany_family_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_botany_genus_id  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_botany_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_botany_kingdom_id;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_botany_ordo_id  ;" | psql -Ucuon_admin cuon
echo "DROP INDEX  index_clients_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_dms_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_country_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_cuon_erp1_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_cuon_user_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_cuon_values_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_district_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_enquiry_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_enquiryget_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_enquiryinvoice_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_enquirymisc_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_enquiryposition_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_enquirysupply_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_invoice_info_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_service_notes_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_autumn_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_holiday_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_maintenance_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_single_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_spring_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_summer_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_winter_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_grave_work_year_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_graveyard_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_hibernation_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_hibernation_plant_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_in_payment_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_list_of_deliveries_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_list_of_hibernation_incoming_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_list_of_hibernation_outgoing_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_list_of_hibernation_pickup_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_list_of_invoices_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_list_of_pickups_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_lock_process_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_logistics_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_material_group_accounts_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_material_group_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_mindmap_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_misc_data_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_orderbook_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_orderget_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_orderinvoice_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_ordermisc_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_orderposition_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_ordersupply_id ;" | psql -Ucuon_admin cuon

echo "DROP INDEX   index_planet_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_precurementposition_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_preferences_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_procurement_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_project_phases_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_project_task_material_res_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_project_task_staff_res_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_project_tasks_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_projects_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_proposal_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_proposalget_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_proposalinvoice_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_proposalmisc_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_proposalposition_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_proposalsupply_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_sourcecode_file_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_sourcecode_modul_material_res_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_sourcecode_module_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_sourcecode_module_staff_res_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_sourcecode_parts_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_sourcecode_projects_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_states_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_stock_goods_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_stocks_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_support_project_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_support_ticket_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_t_address_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_t_grave_work_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_t_partner_schedul_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_t_sourcecode_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_t_standard_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_tax_vat_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_terms_of_payment_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_user_info_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_usergroups_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_userlogin_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_web2_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_xmlvalues_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_zipcodes_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_cash_desk_book_number_id ;" | psql -Ucuon_admin cuon
echo "DROP INDEX   index_city_id ;" | psql -Ucuon_admin cuon


echo "alter table address  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table address_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table address_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE address ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon


echo "alter table address_bank  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table address_bank_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table address_bank_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE address_bank ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon

echo "alter table addresses_misc ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table addresses_misc_history ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table addresses_misc_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE addresses_misc ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon

echo "alter table partner  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table partner_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table partner_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE partner ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon

echo "alter table address_notes  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table address_notes_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table address_notes_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE address_notes ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon

echo "alter table articles  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon

echo "alter table articles_stock  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_stock_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_stock_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_stock  ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon

echo "alter table articles_barcode  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_barcode_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_barcode ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  articles_barcode_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table articles_customers  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_customers_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_customers ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  articles_customers_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table articles_parts_list  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_parts_list_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_parts_ ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  articles_parts__history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table articles_purchase  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_purchase_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_purchase ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  articles_purchase_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table articles_sales  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_sales_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_sales ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  articles_sales_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table articles_webshop ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table articles_webshop_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE articles_webshop ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  articles_webshop_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table bank ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table bank_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE bank ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table bank_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table botany ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table cash_desk ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table cash_desk_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table contact ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table contact_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table account_info ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table account_info_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table account_plan ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table account_plan_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table account_sentence ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table account_sentence_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table account_system ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table account_system_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table in_payment ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table in_payment_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon



echo "alter table  botanyALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table botany_class ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_class_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table  botany_divisio ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_divisio_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table botany_family ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_family_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table botany_genus ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_genus_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table botany_kingdom ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_kingdom_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table botany_ordo ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table botany_ordo_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table clients ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table clients_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon



echo "alter table biblio ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table biblio_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table cash_desk_book_number ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table cash_desk_book_number_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table city ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table city_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table clients ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table clients_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table contact ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table contact_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table country ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table country_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table cuon_clients ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table cuon_clients_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table cuon_erp1 ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table cuon_erp1_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table cuon_user ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table cuon_user_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table cuon_values ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table cuon_values_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table district ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table district_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table dms ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table dms_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE dms ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  dms_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon
echo "alter table  dms ADD COLUMN uploadpath text  ;"  | psql -Ucuon_admin cuon
echo "alter table  dms_history ADD COLUMN uploadpath text  ;"  | psql -Ucuon_admin cuon

echo "alter table enquiry ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table enquiry_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table enquiryget ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table enquiryget_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table enquiryinvoice ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table enquiryinvoice_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table enquirymisc ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table enquirymisc_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table enquiryposition ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table enquiryposition_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table enquirysupply ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table enquirysupply_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_invoice_info ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_invoice_info_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_service_notes ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_service_notes_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_autumn ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_autumn_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_holiday ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_holiday_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_maintenance ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_maintenance_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_single ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_single_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_spring ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_spring_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table grave_work_summer ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_summer_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_winter ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_winter_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table grave_work_year ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table grave_work_year_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table graveyard ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table graveyard_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table hibernation ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table hibernation_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table in_payment ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table in_payment_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table hibernation_plant ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table hibernation_plant_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table list_of_deliveries ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table list_of_deliveries_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table list_of_hibernation_incoming ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table list_of_hibernation_incoming_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table list_of_hibernation_outgoing ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table list_of_hibernation_outgoing_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table list_of_hibernation_pickup ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table list_of_hibernation_pickup_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table list_of_invoices ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table list_of_invoices_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table list_of_pickups ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table list_of_pickups_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table lock_process ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table lock_process_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table logistics ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table logistics_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table material_group_accounts ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table material_group_accounts_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table material_group ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table material_group_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table mindmap ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table mindmap_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table misc_data ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table misc_data_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table orderbook ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table orderbook_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table orderget ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table orderget_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table orderinvoice ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table orderinvoice_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table ordermisc ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table ordermisc_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table orderposition ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table orderposition_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table ordersupply ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table ordersupply_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table partner_schedul ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table partner_schedul_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table planet ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table planet_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table precurementposition ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table precurementposition_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table preferences ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table preferences_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table procurement ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table procurement_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table project_phases ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table project_phases_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE project_phases ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table project_phases_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table project_task_material_res ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table project_task_material_res_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE project_task_material_res ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table project_task_material_res_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table project_task_staff_res ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table project_task_staff_res_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE project_task_staff_res ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table project_task_staff_res_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table project_tasks ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table project_tasks_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE project_tasks ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table roject_tasks_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table projects ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table projects_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE projects ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table projects_history  ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon


echo "alter table proposal ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table proposal_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table proposalget ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table proposalget_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table proposalinvoice ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table proposalinvoice_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table proposalmisc ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table proposalmisc_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table proposalposition ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table proposalposition_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table proposalsupply ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table proposalsupply_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table sourcecode_file ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table sourcecode_file_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table sourcecode_modul_material_res ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table sourcecode_modul_material_res_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table sourcecode_module ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table sourcecode_module_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table sourcecode_module_staff_res ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table sourcecode_module_staff_res_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table sourcecode_parts ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table sourcecode_parts_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table sourcecode_projects ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table sourcecode_projects_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon


echo "alter table staff  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table staff_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE staff  ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  staff _history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon


echo "alter table staff_disease  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table staff_disease_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE staff_disease ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  staff_disease_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon


echo "alter table staff_fee  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table staff_fee_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE staff_fee ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  staff_fee_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon


echo "alter table staff_misc  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table staff_misc_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE staff_misc ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  staff_misc_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon


echo "alter table staff_vacation ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table staff_vacation_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "ALTER TABLE staff_vacation ADD PRIMARY KEY (id) ;"  | psql -Ucuon_admin cuon
echo "alter table  staff_vacation_history ADD COLUMN old_id int ;"  | psql -Ucuon_admin cuon

echo "alter table states ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table states_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table stock_goods ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table stock_goods_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table stocks ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table stocks_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table support_project ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table support_project_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table support_ticket ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table support_ticket_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table t_address ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table t_address_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table t_grave_work ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table t_grave_work_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table t_partner_schedul ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table t_partner_schedul_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table t_sourcecode ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table t_sourcecode_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table t_standard ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table t_standard_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table  tax_vat ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table tax_vat_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table terms_of_payment ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table terms_of_payment_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table user_info ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table user_info_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table usergroups ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table usergroups_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table userlogin ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table userlogin_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table web2 ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table web2_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table xmlvalues ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table xmlvalues_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

echo "alter table zipcodes ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon
echo "alter table zipcodes_history  ALTER COLUMN id add GENERATED ALWAYS AS IDENTITY;"  | psql -Ucuon_admin cuon

# perhaps add the primary key:
# for example:
# echo " ALTER TABLE table_name ADD PRIMARY KEY (id) ;" | psql -Ucuon_admin cuon
# mostly it is done in it.sql

# create the functions

psql -Ucuon_admin cuon < DATABASE/sql/basics.sql
 psql -Ucuon_admin cuon < DATABASE/sql/it.sql

 echo " select * from fct_create_allindex() ;"  | psql -Ucuon_admin cuon
 echo " select * from fct_create_alltrigger()  ;"  | psql -Ucuon_admin cuon
