--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6 (Debian 11.6-0+deb10u1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-1+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: return_float3; Type: TYPE; Schema: public; Owner: cuon_admin
--

CREATE TYPE public.return_float3 AS (
	a double precision,
	b double precision,
	c double precision
);


ALTER TYPE public.return_float3 OWNER TO cuon_admin;

--
-- Name: return_misc3; Type: TYPE; Schema: public; Owner: cuon_admin
--

CREATE TYPE public.return_misc3 AS (
	a integer,
	b double precision,
	c text
);


ALTER TYPE public.return_misc3 OWNER TO cuon_admin;

--
-- Name: return_text3; Type: TYPE; Schema: public; Owner: cuon_admin
--

CREATE TYPE public.return_text3 AS (
	a text,
	b text,
	c text
);


ALTER TYPE public.return_text3 OWNER TO cuon_admin;

--
-- Name: fct_add_months(date, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_add_months(date, integer) RETURNS date
    LANGUAGE plpgsql
    AS $_$ 
    DECLARE 
       base_date ALIAS FOR $1 ; 
       add_days ALIAS FOR $2 ; 
       ret_val DATE ; 
       sSql text ;
       
    BEGIN 
        sSql := 'SELECT date(date(' || quote_literal(base_date) || ') + interval  ' || quote_literal(add_days || ' month' ) || ') ' ;
        execute sSql into ret_val ;
       return ret_val; 
    END; 

    $_$;


ALTER FUNCTION public.fct_add_months(date, integer) OWNER TO cuon_admin;

--
-- Name: fct_addpositiontoinvoice(text, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_addpositiontoinvoice(sservice text, ineworderid integer, ilastorder integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
  
    iClient int ;
    sSql text   ;
    sSql2 text ;
    sSql3 text ;
    sSql4 text ;
    sSql5 text ;
    sSql_date text ;
    r1 record ;
    r2 record ;
    ok boolean ;
    igrave_id int ;
    last_position int ;
    article_headline int ;
    article_headline_designation text ;
    fTaxVat float ;
    existID int ;
    article_designation text ;
    BEGIN
    sSql_date := ' ' ;
        ok := true ;
        existID := -1 ;
         iClient = fct_getUserDataClient(  ) ;
         raise notice ' Client Number = %',iClient ;
         
         
        sSql := 'select  from_modul_id from orderbook where id = ' || iNewOrderID  ;
        raise notice ' fetch grave id sSql = % ', sSql ;
        
        execute(sSql) into igrave_id ;
          
        raise notice 'execute (sSql) ; grave id = % ', igrave_id ;
        
        sSql := ' select gm.* from ' ;
          
        if sService = 'Service' then
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_service_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_maintenance as gm ' ;
            sSql2 := ' update grave_work_maintenance set created_order = 1 where id = '  ;
            
        ELSEIF sService = 'Spring' THEN
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_spring_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_spring as gm ' ;
            sSql2 := ' update grave_work_spring set created_order = 1 where id = '  ;     
            
        
         
            
            ELSEIF sService = 'Summer' THEN
            article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_summer_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
            IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_summer as gm ' ;
            sSql2 := ' update grave_work_summer set created_order = 1 where id = '  ;     
                
          ELSEIF sService = 'Autumn' THEN
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_autumn_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_autumn as gm ' ;
            sSql2 := ' update grave_work_autumn set created_order = 1 where id = '  ;     
            
          ELSEIF sService = 'Winter' THEN
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_winter_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_winter as gm ' ;
            sSql2 := ' update grave_work_winter set created_order = 1 where id = '  ;     
         
           ELSEIF sService = 'Holliday' THEN
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_holliday_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_holiday as gm ' ;
            sSql2 := ' update grave_work_holiday set created_order = 1 where id = '  ;     
            
            
          ELSEIF sService = 'Unique' THEN
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_Unique_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
               
            END IF ;
            
            sSql := sSql || 'grave_work_single as gm ' ;
            sSql2 := ' update grave_work_single set created_order = 1 where id = '  ;     
            sSql_date := ' and gm.unique_date < now() ' ;
            
          ELSEIF sService = 'Yearly' THEN
           article_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_Yearly_headline_articles_id') ;
            -- raise notice ' article headline = %', article_headline ;
            
           IF article_headline IS NOT NULL THEN 
               sSql4 := 'select designation from articles where id = ' || article_headline ;
               
               execute(sSql4) into article_headline_designation ;
              
            END IF ;
            
            sSql := sSql || 'grave_work_year as gm ' ;
            sSql2 := ' update grave_work_year set created_order = 1 where id = '  ;     
            sSql_date := ' and to_date(gm.annual_day || ''-'' || gm.annual_month || ''-'' ||  EXTRACT(YEAR FROM  now()) ,''DD-MM-YYYY'' ) < now() ' ;    
        end if ;
        
        
       
        sSql := sSql || ' where grave_id = ' || igrave_id ;
        sSql := sSql || sSql_date ;
        sSql := sSql || ' '  || fct_getWhere(2,'') ;
        
        raise notice ' fetch positions  sSql = % ', sSql ;
        FOR r1 in execute(sSql)  LOOP
            existID := existID + r1.id ;
        END LOOP ;
          
        if existID > -1 then
            select into last_position max(position) from orderposition where orderid = iNewOrderID ;
            
            if last_position IS NULL then 
                last_position = 0 ;
            end if ;
             last_position := last_position + 1 ;
             
               sSql5 := 'insert into orderposition ( id, orderid , articleid , designation, amount  ,  position, price)  values (  (select nextval(''orderposition_id'') ) , ' ;
                sSql5 := sSql5 ||  iNewOrderID || ', ' || article_headline ||  ', ' ||  quote_literal('') || ', ' || 1   || ', ' || last_position  || ' ,0 ) ' ;
              execute (sSql5) ;  
              
              
            raise notice ' look at r1 for position   sSql = % ', sSql ;
            FOR r1 in execute(sSql)  LOOP
                if r1.created_order is null then
                    r1.created_order = 0 ;
                end if ;
                if r1.created_order = 0 then
                    IF r1.service_price IS NULL THEN
                        r1.service_price = 0 ;
                    END IF ;
                    IF r1.service_price = 0  THEN
                        r1.service_price := fct_get_price_for_pricegroup(  'orderposition', iNewOrderID, r1.article_id)  ;
                    END IF;
                    if r1.service_designation is null then 
                        article_designation := '' ;
                    else 
                        article_designation := r1.service_designation ;
                    end if ;
                    
                    -- select into article_designation designation from articles where id = r1.article_id ;
                    IF sService = 'Unique' THEN
                        article_designation := article_designation || ' / ' || to_char(r1.unique_date,'DD.MM.YYYY' ) ;
                    end if ;
                      IF sService = 'Yearly' THEN
                        article_designation := article_designation || ' / ' || r1.annual_day ||'.' ||  r1.annual_month ;
                    end if ;
                    if article_designation is null then 
                        article_designation := '' ;
                    end if ;
                    
                    last_position := last_position + 1 ;
                    fTaxVat := fct_get_new_tax_vat_for_article_1(r1.article_id);
                    
                    sSql4 := 'insert into orderposition ( id, orderid , articleid , designation, amount  ,  position , price, tax_vat ) values (  (select nextval(''orderposition_id'') ) , ' ;
                    sSql4 := sSql4 ||  iNewOrderID || ', ' || r1.article_id ||  ', ''' || article_designation || ''', ' || r1.service_count   || ', ' || last_position ;
                    sSql4 := sSql4 || ', ' || r1.service_price || ', ' || fTaxVat || ' ) ' ;
                    raise notice ' insert position sSql4 = % ', sSql4 ;
                    execute (sSql4) ;
                    
                    if iLastOrder = 1 then 
                        sSql3 := sSql2 || r1.id ;
                        raise notice ' update = % ', sSql3 ;
                        execute(sSql3);
                    end if ;
                    
                    
                end if ;
            END LOOP ;
            
        end if ;
        return ok;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_addpositiontoinvoice(sservice text, ineworderid integer, ilastorder integer) OWNER TO cuon_admin;

--
-- Name: fct_addresslist(text[], text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_addresslist(searchfields text[], skey text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    sSql2    text ;

    sWhere text;
    recAddress  record;

    lastname_from text;
    lastname_to text;
    firstname_from text;
    firstname_to text;
    city_from text;
    city_to  text;
    country_from text;
    country_to text;
    info_contains text;
    newsletter_contains text;
    
    iWhere bool;
    BEGIN
	 iWhere = false ;
    
	 lastname_from := searchfields[1];
  	 lastname_to  := searchfields[2];
	 firstname_from  := searchfields[3];
    	 firstname_to  := searchfields[4];
    	 city_from  := searchfields[5];
    	 city_to   := searchfields[6];
    	 country_from  := searchfields[7];
    	 country_to  := searchfields[8];
    	 info_contains  := searchfields[9];
    	 newsletter_contains  := searchfields[10];

	 
	 sWhere := ' ' ;
	 if (char_length(lastname_from) > 0 and char_length(lastname_to) > 0 and lastname_from != 'NONE' and lastname_to != 'NONE' ) then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := ' where ' ;
	    else
	       sWhere := ' and ' ;
	    end if;

	    sWhere = sWhere || ' lastname between ' || quote_literal(lastname_from) || ' and ' || quote_literal(lastname_to) || ' ' ;


	 end if;
	 if (char_length(firstname_from) > 0 and char_length(firstname_to) > 0 and firstname_from != 'NONE' and firstname_to != 'NONE') then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := ' where ' ;
	        else
	       sWhere := ' and ' ;
	    end if;

	    sWhere = sWhere || ' firstname between ' || quote_literal(firstname_from) || ' and ' || quote_literal(firstname_to) || ' ' ;


	 end if;

	 if (char_length(city_from) > 0 and char_length(city_to) > 0 and city_from != 'NONE' and city_to != 'NONE') then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := ' where ' ;
	        else
	       sWhere := ' and ' ;
	    end if;

	    sWhere = sWhere || ' city between ' || quote_literal(city_from) || ' and ' || quote_literal(city_to) || ' ' ;

	 end if ;

	 if (char_length(country_from) > 0 and char_length(country_to) > 0 and country_from != 'NONE' and country_to != 'NONE') then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := ' where ' ;
	        else
	       sWhere := ' and ' ;
	    end if;

	    sWhere = sWhere || ' country between ' || quote_literal(country_from) || ' and ' || quote_literal(country_to) || ' ' ;


	 end if;
 	 if (char_length(info_contains) > 0 and info_contains != 'NONE' ) then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := ' where ' ;
	        else
	       sWhere := ' and ' ;
	    end if;

	    sWhere = sWhere || ' status_info ~* ' || quote_literal(info_contains) || ' ' ;

	 end if;

	 if (char_length(newsletter_contains) > 0  and newsletter_contains != 'NONE' ) then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := ' where ' ;
	        else
	       sWhere := ' and ' ;
	    end if;

	    sWhere = sWhere || ' newsletter ~* ' || quote_literal(newsletter_contains) || ' ' ;

	 end if;

	 sSql := 'select id, lastname::text, lastname2::text,firstname::text,street::text,country::text, zip::text, city::text,phone::text,phone_handy::text, email::text, fax::text, status_info::text ,newsletter::text from address ' || sWhere || ' ' ||   fct_getWhere(2,' ') ;

	 raise notice 'sSql = % ', sSql ;

	 for recAddress in execute sSql LOOP 
	 
	     return next recAddress;

	 end loop ;

	 
	
      
	
       END ;
    $$;


ALTER FUNCTION public.fct_addresslist(searchfields text[], skey text) OWNER TO cuon_admin;

--
-- Name: fct_beforecashdesk(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_beforecashdesk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE  

 

    BEGIN

    if fct_set_block('cash_desk', NEW.ORDER_ID) then

       
       return NEW ;

       else
       return NULL;

    END IF ;
    
     

    END ;
    
     $$;


ALTER FUNCTION public.fct_beforecashdesk() OWNER TO cuon_admin;

--
-- Name: fct_beforeinpayment(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_beforeinpayment() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE  

 

    BEGIN

    if fct_set_block('in_payment', NEW.ORDER_ID) then

       return NEW ;

       else
       return NULL;

    END IF ;
    
     

    END ;
    
     $$;


ALTER FUNCTION public.fct_beforeinpayment() OWNER TO cuon_admin;

--
-- Name: fct_calc_all_prices(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_calc_all_prices() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
 ok boolean ;
 BEGIN
        ok := true ;
        ok := fct_calc_all_prices_for_db('grave_work_maintenance') ;
        ok := fct_calc_all_prices_for_db('grave_work_spring') ;
        ok := fct_calc_all_prices_for_db('grave_work_summer') ;
        ok := fct_calc_all_prices_for_db('grave_work_autumn') ;
        ok := fct_calc_all_prices_for_db('grave_work_winter') ;
        ok := fct_calc_all_prices_for_db('grave_work_holiday') ;
        ok := fct_calc_all_prices_for_db('grave_work_year') ;
         ok := fct_calc_all_prices_for_db('grave_work_single') ;
        return ok ;
      END ;
    
     $$;


ALTER FUNCTION public.fct_calc_all_prices() OWNER TO cuon_admin;

--
-- Name: fct_calc_all_prices_for_db(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_calc_all_prices_for_db(sdb text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
    sSql text   ;
    sSql2 text ;
    r0 record ;
    newPrice float ;
    ok boolean ;
    BEGIN
        sSql := 'select id, grave_id, article_id, automatic_price from ' || sDB || '  ' || fct_getWhere(1,'') || ' order by id ' ;
        
        FOR r0 in execute(sSql) LOOP 
            if r0.automatic_price is NULL then
                r0.automatic_price := 0 ;
            end if ;
            if r0.automatic_price = 1 then 
            
                newPrice = fct_get_price_for_pricegroup('grave',r0.grave_id , r0.article_id )  ;
                -- raise notice ' new price for article % , grave % = %', r0.article_id, r0.grave_id, newPrice ;
                if newPrice > 0 then 
                
                    sSql2 := 'update ' || sDB || ' set service_price = ' || newPrice || ' where id = ' || r0.id  ;
                    execute(sSql2) ;
                end if ;
            end if ;
        END LOOP ;
        ok = true ;
        return ok ;
      END ;
    
     $$;


ALTER FUNCTION public.fct_calc_all_prices_for_db(sdb text) OWNER TO cuon_admin;

--
-- Name: fct_changeenquiry2proposal(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_changeenquiry2proposal(ienquiryid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    sSql text ;
    sSql2 text ;
    bOK bool ;
    r record ;
    BEGIN
        iNewOrderID := 0 ;
        
        sSql := 'select * from fct_duplicate_table_entry(' || quote_literal('enquiry') || ',' || quote_literal('proposal') || ',' || iEnquiryID || ', ' || quote_literal('proposal_id') || ')  as newid ' ;
        raise notice ' sSql = %',sSql ;
        execute(sSql) into iNewOrderID;
        raise notice 'new order id = %',iNewOrderID ;
        
        IF iNewOrderID > 0 THEN 
            -- copy the enquirypositions 
            
            sSql := ' select id from enquiryposition where orderid = ' || iEnquiryID  || ' ' || fct_getWhere(2,' ')  ;
            
            FOR r in execute(sSql)  LOOP
                sSql2 := 'select * from fct_duplicate_table_entry(' || quote_literal('enquiryposition') || ',' || quote_literal('proposalposition') || ',' || r.id || ', ' || quote_literal('proposalposition_id') || ')  as newid ' ; 
               
                raise notice ' sSql2 = %',sSql2 ;
                execute(sSql2) into iNewPositionID;
                update proposalposition set orderid = iNewOrderID where id =  iNewPositionID ;
                
            END LOOP ;
            
            
            
            
            sSql = 'update proposal set process_status = 300,orderedat = now(), enquiry_id = ' || iEnquiryID || ' where id =  ' || iNewOrderID ;
            execute(sSql);
             
            select into bOK * from fct_setNumberAndDescription(iNewOrderID, 'Proposal') ;
            
            
          
        END IF ;
            
       return iNewOrderID ;
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_changeenquiry2proposal(ienquiryid integer) OWNER TO cuon_admin;

--
-- Name: fct_changeorderbookprocessstatus(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_changeorderbookprocessstatus(iorderid integer, inewprocessstatus integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    iStatus int ;
    
    BEGIN
     iStatus = 0;
    update orderbook set process_status = iNewProcessStatus where id = iOrderID ;
   
       iStatus = 1 ;
      
       
    return iStatus ;
   END ;
    
     $$;


ALTER FUNCTION public.fct_changeorderbookprocessstatus(iorderid integer, inewprocessstatus integer) OWNER TO cuon_admin;

--
-- Name: fct_changeproposal2order(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_changeproposal2order(iproposalid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    sSql text ;
    sSql2 text ;
    bOK bool ;
    r record ;
    BEGIN
        iNewOrderID := 0 ;
        
        sSql := 'select * from fct_duplicate_table_entry(' || quote_literal('proposal') || ',' || quote_literal('orderbook') || ',' || iProposalID || ', ' || quote_literal('orderbook_id') || ')  as newid ' ;
        raise notice ' sSql = %',sSql ;
        execute(sSql) into iNewOrderID;
        raise notice 'new order id = %',iNewOrderID ;
        
        IF iNewOrderID > 0 THEN 
            -- copy the proposalpositions 
            
            sSql := ' select id from proposalposition where orderid = ' || iProposalID  || ' ' || fct_getWhere(2,' ')  ;
            
            FOR r in execute(sSql)  LOOP
                sSql2 := 'select * from fct_duplicate_table_entry(' || quote_literal('proposalposition') || ',' || quote_literal('orderposition') || ',' || r.id || ', ' || quote_literal('orderposition_id') || ')  as newid ' ; 
               
                raise notice ' sSql2 = %',sSql2 ;
                execute(sSql2) into iNewPositionID;
                update orderposition set orderid = iNewOrderID where id =  iNewPositionID ;
                
            END LOOP ;
            
            
            
            
            update orderbook set process_status = 500 where id =  iNewOrderID ;
            update orderbook set orderedat = now() where id =  iNewOrderID ;
            select into bOK * from fct_setNumberAndDescription(iNewOrderID, 'Order') ;
            
        END IF ;
            
       return iNewOrderID;
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_changeproposal2order(iproposalid integer) OWNER TO cuon_admin;

--
-- Name: fct_check_age_of_order(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_check_age_of_order(order_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$

       DECLARE

	sSql text ;
 	bOK bool ;
	this_day date;
	orderbook_month int;
 r record ;
     	BEGIN
	
	select into orderbook_month  extract(year from age(orderedat)) * 12 + extract(month from age(orderedat))  from orderbook where id = order_id ;
	if orderbook_month is NULL then
	   orderbook_month = 0 ;
	end if ;
	   

    	return orderbook_month;
        
    	END ;
    
     $$;


ALTER FUNCTION public.fct_check_age_of_order(order_id integer) OWNER TO cuon_admin;

--
-- Name: fct_checkcolumn(text, text, text, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_checkcolumn(tablename text, columnname text, typename text, first_size integer, second_size integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    -- check and alter table and column
     
    DECLARE
    ok bool;
    sSql text ;
    
    
    BEGIN
        ok = false ;
        
        sSql := 'SELECT a.attname as a_column, pg_catalog.format_type(a.atttypid, a.atttypmod) as p_datatype FROM  pg_catalog.pg_attribute a ' ;
        sSql := sSql || ' WHERE  a.attnum > 0  AND NOT a.attisdropped AND a.attrelid = ( SELECT c.oid FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace  WHERE c.relname = ' || quote_literal(tablename )  ;
        sSql := sSql || ' AND pg_catalog.pg_table_is_visible(c.oid)  ) ' ;

        raise notice ' sSql = %',sSql ;
        
        
        return ok ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_checkcolumn(tablename text, columnname text, typename text, first_size integer, second_size integer) OWNER TO cuon_admin;

--
-- Name: fct_checkopenorder(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_checkopenorder(snumber text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE  

   rData record ;
    rRow record ;
    sCursor text ;
    sSql text ;
 curs1 refcursor;   

    BEGIN

        sCursor := 'select  number,id from orderbook where addressnumber = ' || sNumber || ' ' || fct_getWhere(2,' ') || ' order by id desc ' ;
	raise notice  'cursor  = % ',sCursor ;

	OPEN curs1 FOR EXECUTE sCursor 	;

	fetch curs1 INTO rData ;      

	WHILE FOUND = true LOOP	 
   	   
	   	
		sSql := 'select id from list_of_invoices where  list_of_invoices.order_number = ' || rData.id || ' ' ||  fct_getWhere(2,' ')  ;
		raise notice  'sSql  = % ',sSql ;

		execute(sSql) into rRow ;

		raise notice 'rRow.id = %',rRow.id ;
		if rRow.id is NULL then 

				return next rData ;
				EXIT;
		end if ;

	        fetch curs1 INTO rData ;    

	END LOOP;


    END ;
    
     $$;


ALTER FUNCTION public.fct_checkopenorder(snumber text) OWNER TO cuon_admin;

--
-- Name: fct_checkuserprofile(character, character); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_checkuserprofile(suser character, sclient character) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    -- set the userdata for the current work
     
    DECLARE
    sSql text ;
    sSql1 text ;  
    r1 record ;
    ok boolean ;
    newUUID char(36) ;

   BEGIN
	-- test if standard profile exists
	ok = false ;
	newUUID = fct_null_uuid();
	raise notice 'newUUID 1  = % ',newUUID ;
	sSql := 'select is_standard_profile, uuid, id  from preferences where username = ' || quote_literal(sUser) || ' and client = ' || sClient  || ' and status != '|| quote_literal('delete') ;
	
	for r1 in execute sSql  
	LOOP
		if r1.uuid is null then 
		   newUUID =  fct_new_uuid();
		   sSql = 'update preferences set uuid = ' || quote_literal(newUUID) ||'  where id = ' || r1.id ;
		   execute sSql ;
		   r1.uuid := newUUID ;
    	   end if;
		   

		if r1.is_standard_profile is not null and r1.is_standard_profile = true then 
		   ok = true ;
		   
		   newUUID = r1.uuid ;
	raise notice 'newUUID 2 = % ',newUUID ;
		end if ;
	
	

	END LOOP ;
	if ok = false then 
	   newUUID = fct_new_uuid();
	   sSql = 'insert into preferences(id,uuid,username,client) values ( select nextval(' || quote_literal('preferences_id') || ', ' || quote_literal(newUUID) || ', ' ||  quote_literal(sUser) || ', ' || sClient || ') ' ;
	   raise notice 'sSql-11 = % ',sSql ;
	raise notice 'newUUID 3 = % ',newUUID ;
	end if ;
	
	   
	-- set missing values
	raise notice 'newUUID 4 = % ',newUUID ;
	sSql := 'select * from preferences where uuid = ' || quote_literal(newUUID)  ;
	raise notice 'sSql-14 = % ',sSql ;

	for r1 in execute sSql
	
	LOOP
		-- dms vars
		if r1.exe_oowriter is null then 
		   sSql1 := 'update preferences set exe_oowriter = ''/usr/bin/libreoffice'' where id = ' || r1.id ;
		   execute sSql ;
		end if ;
  		if r1.exe_oocalc is null then 
		   sSql1 := 'update preferences set exe_oocalc = ''/usr/bin/libreoffice'' where id = ' || r1.id ;
		   execute sSql ;
		end if ;


	END LOOP ;
        return 1 ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_checkuserprofile(suser character, sclient character) OWNER TO cuon_admin;

--
-- Name: fct_clean_orderbook(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_clean_orderbook() RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
  iStatus int ;
    sSql text ;
    iID int ;
    r1 record ;
    sSql2 text ;
    r2 record ;
    sSql3 text ;
    s1 int ;
    s2 int ;
    
    
   BEGIN
        iStatus = 0;
	sSql := 'select id from orderbook where process_status < 800 ' || fct_getWhere(2,' ')  ;
	 FOR r1 in execute(sSql)  LOOP
	     if r1.id is not null then 
	     	sSql2 := 'select id from list_of_invoices where order_number = ' || r1.id || ' ' ||  fct_getWhere(2,' ')  ;
	     	raise notice ' sSql2 = %',sSql2 ;
	      	FOR r2 in execute(sSql2)  LOOP
	            raise notice ' r1, r2 = %,%',r1.id, r2.id ;
	            if r2.id is not NULL then
	              sSql3 = 'select * from fct_changeOrderbookProcessStatus(' || r2.id || ',' || 800 || ')' ;
		      execute sSql3 into s1 ;
	           end if;
	        end loop ;  
	        sSql3 = 'select * from fct_delete_zero_order( ' || r1.id || ' )';
	        execute sSql3 ;
	   end if ;  
	 end loop ;
	 
		
    return iStatus ;
   END ;
    
     $$;


ALTER FUNCTION public.fct_clean_orderbook() OWNER TO cuon_admin;

--
-- Name: fct_create_allindex(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_allindex() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  DECLARE
    bRet bool := True ;

    BEGIN
        -- First create primary index
        perform fct_create_primary_index() ;

        -- cuon_user
        perform fct_create_index('cuon_user_idx_name', 'cuon_user', 'username', ' ' );

        -- address
        perform fct_create_index('address_idx_uuid', 'address', 'uuid', ' ' );
        perform fct_create_index('address_idx_lastname', 'address', 'lastname', ' ' );
        perform fct_create_index('address_idx_firstname', 'address', 'firstname', ' ' );
        perform fct_create_index('address_idx_phone', 'address', 'phone', ' ' );
        perform fct_create_index('address_idx_caller_id', 'address', 'caller_id', ' ' );
        perform fct_create_index('address_idx_rep_id', 'address', 'rep_id', ' ' );
        perform fct_create_index('address_idx_salesman_id', 'address', 'salesman_id', ' ' );
        perform fct_create_index('address_idx_status_info', 'address', 'status_info', ' ' );
        perform fct_create_index('address_idx_status_client', 'address', 'status,client', ' ' );
        perform fct_create_index('address_idx_newsletter', 'address', 'newsletter', ' ' );

        -- partner
        perform fct_create_index('partner_idx_uuid', 'partner', 'uuid', ' ' );
        perform fct_create_index('partner_idx_addressid', 'partner', 'addressid', ' ' );
        perform fct_create_index('partner_idx_status_client', 'partner', 'status,client', ' ' );

        -- partner_schedul
        perform fct_create_index('partner_schedul_idx_schedul_insert_time_day', 'partner_schedul', 'date_part(''day'',insert_time)', ' ' );

         perform fct_create_index('partner_schedul_idx_schedul_insert_time_week', 'partner_schedul', 'date_part(''week'',insert_time)', ' ' );

        perform fct_create_index('partner_schedul_idx_schedul_insert_time_month', 'partner_schedul', 'date_part(''month'',insert_time)', ' ' );
      perform fct_create_index('partner_schedul_idx_schedul_insert_time_quarter', 'partner_schedul', 'date_part(''quarter'',insert_time)', ' ' );
      perform fct_create_index('partner_schedul_idx_schedul_insert_time_year', 'partner_schedul', 'date_part(''year'',insert_time)', ' ' );
        perform fct_create_index('partner_schedul_idx_sep_info_3', 'partner_schedul', 'sep_info_3', ' ' );
        perform fct_create_index('partner_schedul_idx_date', 'partner_schedul', 'schedul_date', ' ' );
        perform fct_create_index('partner_schedul_idx_uuid', 'partner_schedul', 'uuid', ' ' );
        perform fct_create_index('partner_schedul_idx_partnerid', 'partner_schedul', 'partnerid', ' ' );
        perform fct_create_index('partner_schedul_idx_process_status', 'partner_schedul', 'process_status', ' ' );
        perform fct_create_index('partner_schedul_idx_status_client', 'partner_schedul', 'status,client', ' ' );
        perform fct_create_index('partner_schedul_idx_user_id', 'partner_schedul', 'user_id', ' ' );
        perform fct_create_index('partner_schedul_idx_schedul_staff_id', 'partner_schedul', 'schedul_staff_id', ' ' );

        -- address_notes
        perform fct_create_index('address_notes_idx_uuid', 'address_notes', 'uuid', ' ' );
        perform fct_create_index('address_notes_idx_address_id', 'address_notes', 'address_id', ' ' );
        perform fct_create_index('address_notes_idx_status_client', 'address_notes', 'status,client', ' ' );

        perform fct_create_index('bank_idx_bcn', 'bank', 'bcn', ' ' );

	-- articles
        perform fct_create_index('articles_idx_designation', 'articles', 'designation', ' ' );
	perform fct_create_index('articles_idx_number', 'articles', 'number', ' ' );

	-- cash_desk
	 perform fct_create_index('cash_desk_idx_date', 'cash_desk', 'cash_date', ' ' );
 	 perform fct_create_index('cash_desk_idx_time', 'cash_desk', 'cash_time', ' ' );
	 perform fct_create_index('cash_desk_idx_date_time', 'cash_desk', 'cash_date,cash_time', ' ' );
	 perform fct_create_index('cash_desk_idx_desk_number', 'cash_desk', 'cash_desk_number', ' ' );
	 perform fct_create_index('cash_desk_idx_user_short_cut', 'cash_desk', 'cash_desk_user_short_cut', ' ' );
	 perform fct_create_index('cash_desk_idx_procedure', 'cash_desk', 'cash_procedure', ' ' );


	 --orderbook



	 -- list_of_invoice


	 --in_payment
	  perform fct_create_index('in_payment_idx_invoice_number', 'in_payment', 'invoice_number', ' ' );


	 -- special indexe for regex
	 begin
		create extension pg_trgm ;
	exception when others then
	  raise notice 'error extension pg_trgm, perhaps exist ';
      	end ;

 	perform fct_create_index_gin('address_idx_gin_lastname', 'address', 'lastname', ' ' );
	perform fct_create_index_gin('address_idx_gin_lastname2', 'address', 'lastname2', ' ' );
        perform fct_create_index_gin('address_idx_gin_firstname', 'address', 'firstname', ' ' );
	perform fct_create_index_gin('address_idx_gin_newsletter', 'address', 'newsletter', ' ' );
	perform fct_create_index_gin('address_idx_gin_city', 'address', 'city', ' ' );
	perform fct_create_index_gin('address_idx_gin_zip', 'address', 'zip', ' ' );

	perform fct_create_index_gin('dms_idx_gin_title1', 'dms', 'title', ' ' );
	perform fct_create_index_gin('dms_idx_gin_sub1', 'dms', 'sub1', ' ' );
	perform fct_create_index_gin('dms_idx_gin_sub2', 'dms', 'sub2', ' ' );
	perform fct_create_index_gin('dms_idx_gin_sub3', 'dms', 'sub3', ' ' );
	perform fct_create_index_gin('dms_idx_gin_sub4', 'dms', 'sub4', ' ' );
	perform fct_create_index_gin('dms_idx_gin_sub5', 'dms', 'sub5', ' ' );

	perform fct_create_index_gin('dms_idx_gin_search1', 'dms', 'search1', ' ' );
	perform fct_create_index_gin('dms_idx_gin_search2', 'dms', 'search2', ' ' );
	perform fct_create_index_gin('dms_idx_gin_search3', 'dms', 'search3', ' ' );
	perform fct_create_index_gin('dms_idx_gin_search4', 'dms', 'search4', ' ' );


	perform fct_create_index_gin('articles_idx_gin_designation', 'articles', 'designation', ' ' );
	perform fct_create_index_gin('articles_idx_gin_number', 'articles', 'number', ' ' );

	perform fct_create_index_gin('orderbook_idx_gin_designation', 'orderbook', 'designation', ' ' );
	perform fct_create_index_gin('orderbook_idx_gin_number', 'orderbook', 'number', ' ' );


	perform fct_create_index_gin('proposal_idx_gin_designation', 'proposal', 'designation', ' ' );
	perform fct_create_index_gin('proposal_idx_gin_number', 'proposal', 'number', ' ' );

	perform fct_create_index_gin('enquiry_idx_gin_designation', 'enquiry', 'designation', ' ' );
	perform fct_create_index_gin('enquiry_idx_gin_number', 'enquiry', 'number', ' ' );

	perform fct_create_index_gin('projects_idx_gin_designation', 'projects', 'designation', ' ' );
	perform fct_create_index_gin('projects_idx_gin_name', 'projects', 'name', ' ' );
	perform fct_create_index_gin('projects_idx_gin_internal_project_number', 'projects', 'internal_project_number', ' ' );


      -- Foreign Keys
      -- ALTER TABLE child_table ADD CONSTRAINT constraint_name FOREIGN KEY (fk_columns) REFERENCES parent_table (parent_key_columns);

      ALTER TABLE partner DROP CONSTRAINT f_PartnerAddress ;
      ALTER TABLE partner ADD CONSTRAINT f_PartnerAddress FOREIGN KEY (addressid) REFERENCES address (id);

      ALTER TABLE address_bank DROP CONSTRAINT f_AddressbankAddress ;
      ALTER TABLE address_bank ADD CONSTRAINT f_AddressbankAddress FOREIGN KEY (address_id) REFERENCES address (id);
      ALTER TABLE address_bank DROP CONSTRAINT f_AddressbankBank ;
      ALTER TABLE address_bank ADD CONSTRAINT f_AddressbankBank FOREIGN KEY (bank_id) REFERENCES bank (id);



        return bRet ;
    END ;

    $$;


ALTER FUNCTION public.fct_create_allindex() OWNER TO cuon_admin;

--
-- Name: fct_create_alltrigger(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_alltrigger() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  DECLARE
    bRet bool := True ;
    vList text[]  ;
    array_size integer ;
    sSql text ;
    BEGIN

       vlist := fct_create_list_of_all_table() ;

        array_size := array_length(vList,1) ;
       begin
            sSql := 'DROP TRIGGER trg_updatelistofinvoices  ON  list_of_invoices ' ;
            -- raise notice 'sSql = %', sSql ;
            execute sSql ;
            exception when others then
                raise notice 'error 1';
                raise notice 'SQL = %', sSql ;
        end ;

        for ct in 1..array_size loop
            -- raise notice ' ct = %, array = % ', ct,vList[ct] ;
            sSql := 'select * from fct_create_Trigger(''trg_insert' || vList[ct] || ''',' || quote_literal(vList[ct]) ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_insert()'' )' ;  -- EXECUTE PROCEDURE fct_insert()
            -- raise notice 'alltrigger sSql = %', sSql ;
            execute sSql ;
            sSql := 'select * from fct_create_Trigger(''trg_delete' || vList[ct] || ''',' || quote_literal(vList[ct]) ||', ''before delete'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_delete()'' )' ;  -- EXECUTE PROCEDURE fct_insert()
            -- raise notice 'alltrigger sSql = %', sSql ;
            execute sSql ;

            sSql := 'select * from fct_create_Trigger(''trg_update' || vList[ct] || ''',' || quote_literal(vList[ct]) ||', ''before update'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_update()'' )' ;  -- EXECUTE PROCEDURE fct_insert()
         -- raise notice 'alltrigger sSql = %', sSql ;
            execute sSql ;




        end loop;

    -- Special trigger

    -- lock process only insert

 sSql := 'select * from fct_create_Trigger(''trg_t_new_cash_desk_special_id_'',' || quote_literal(' cash_desk_book_number') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_new_cash_desk_special_id() '' )' ;

      -- raise notice 'alltrigger Special  sSql = %', sSql ;
       execute sSql ;

   sSql := 'select * from fct_create_Trigger(''trg_t_insert_cash_desk_book_number_'',' || quote_literal(' cash_desk_book_number') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_insert() '' )' ;

      -- raise notice 'alltrigger Special  sSql = %', sSql ;
       execute sSql ;

     sSql := 'select * from fct_create_Trigger(''trg_insert_cuon'',' || quote_literal('cuon') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_cuon_insert()'' )' ;
      -- raise notice 'alltrigger Special  sSql = %', sSql ;
       execute sSql ;

     sSql := 'select * from fct_create_Trigger(''trg_update_cuon'',' || quote_literal('cuon') ||', ''before update'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_cuon_insert()'' )' ;
      -- raise notice 'alltrigger Special  sSql = %', sSql ;
       execute sSql ;


     sSql := 'select * from fct_create_Trigger(''trg_insert_partner_schedul'',' || quote_literal('partner_schedul') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_insert()'' )' ;
      -- raise notice 'alltrigger Special  sSql = %', sSql ;
       execute sSql ;

     sSql := 'select * from fct_create_Trigger(''trg_update_partner_schedul'',' || quote_literal('partner_schedul') ||', ''before update'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_partner_schedul_change( )'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;

       sSql := 'select * from fct_create_Trigger(''trg_insert_pick_schedul'',' || quote_literal('orderget') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE  fct_pickup_schedul_insert( )'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;

            sSql := 'select * from fct_create_Trigger(''trg_update_pick_schedul'',' || quote_literal('orderget') ||', ''before update'', ''FOR EACH ROW'',''EXECUTE PROCEDURE  fct_pickup_schedul_update( )'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;




 sSql := 'select * from fct_create_Trigger(''trg_new_orderpositon'',' || quote_literal('orderposition') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE  fct_orderposition_insert () '' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;





 sSql := 'select * from fct_create_Trigger(''trg_new_stock_goods'',' || quote_literal('stock_goods') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE  fct_stock_goods_insert() '' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;



 sSql := 'select * from fct_create_Trigger(''trg_delete_pick_schedul'',' || quote_literal('orderget') ||', ''before delete'', ''FOR EACH ROW'',''EXECUTE PROCEDURE  fct_pickup_schedul_delete( )'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;

    sSql := 'select * from fct_create_Trigger(''trg_update_orderbook_gift1'',' || quote_literal('orderbook') ||', ''before update'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_getOrderGifts()'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;

      sSql := 'select * from fct_create_Trigger(''trg_insert_orderbook_gift1'',' || quote_literal('orderbook') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_getOrderGifts()'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;

    sSql := 'select * from fct_create_Trigger(''trg_update_dms_pairedID'',' || quote_literal('dms') ||', ''before update'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_setDMSPairedID()'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;


      sSql := 'select * from fct_create_Trigger(''trg_insert_dms_pairedID'',' || quote_literal('dms') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_setDMSPairedID()'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;

     sSql := 'select * from fct_create_Trigger(''trg_orderbook_delete_invoice'',' || quote_literal('orderbook') ||', ''after delete'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_orderbook_delete_invoice()'' )' ;
     -- raise notice 'alltrigger Special sSql = %', sSql ;
            execute sSql ;



     sSql := 'select * from fct_create_Trigger(''trg_insert_lock_process'',' || quote_literal('lock_process') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_insert()'' )' ;  -- EXECUTE PROCEDURE fct_insert()


      raise notice 'alltrigger Special lock process  sSql = %', sSql ;
  execute sSql ;


   sSql := 'select * from fct_create_Trigger(''trg_insertz1_inpayment'',' || quote_literal('in_payment') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_beforeInpayment()'' )' ;  -- EXECUTE PROCEDURE fct_insert()


      raise notice 'alltrigger Special insert inpayment  sSql = %', sSql ;
  execute sSql ;


   sSql := 'select * from fct_create_Trigger(''trg_insertz1_cashdesk'',' || quote_literal('cash_desk') ||', ''before insert'', ''FOR EACH ROW'',''EXECUTE PROCEDURE fct_beforeCashDesk()'' )' ;  -- EXECUTE PROCEDURE fct_insert()


      raise notice 'alltrigger Special insert inpayment  sSql = %', sSql ;
  execute sSql ;



      return bRet ;

    END ;


    $$;


ALTER FUNCTION public.fct_create_alltrigger() OWNER TO cuon_admin;

--
-- Name: fct_create_index(text, text, text, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_index(sname text, stable text, scolumn text, sunique text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
    bRet bool := False ;
    sSql text ;

    BEGIN

      begin
        sSql := 'DROP  INDEX ' || sName  ;
        execute sSql ;

        exception when others then
          raise notice 'error Index Drop';
          raise notice 'SQL = %', sSql ;
      end;

      begin
      sSql := 'CREATE ' || sUnique ||' INDEX ' || sName || ' ON ' || sTable || ' (' || sColumn|| ')' ;
      raise notice 'SQL = %', sSql ;
      execute sSql ;
      exception when others then
          raise notice 'error Index Create';
          raise notice 'SQL = %', sSql ;
      end;


    return bRet ;
    END ;

     $$;


ALTER FUNCTION public.fct_create_index(sname text, stable text, scolumn text, sunique text) OWNER TO cuon_admin;

--
-- Name: fct_create_index_gin(text, text, text, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_index_gin(sname text, stable text, scolumn text, sunique text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
    bRet bool := False ;
    sSql text ;

    BEGIN

      begin
        sSql := 'DROP  INDEX ' || sName  ;
        execute sSql ;
        exception when others then
          raise notice 'error Index Drop';
          raise notice 'SQL = %', sSql ;
      end;

      begin
      sSql := 'CREATE ' || sUnique ||' INDEX ' || sName || ' ON ' || sTable || ' USING gin (' || sColumn|| ' gin_trgm_ops)' ;
      raise notice 'SQL = %', sSql ;
      execute sSql ;
      exception when others then
          raise notice 'error Index Create';
          raise notice 'SQL = %', sSql ;
      end;


    return bRet ;
    END ;

     $$;


ALTER FUNCTION public.fct_create_index_gin(sname text, stable text, scolumn text, sunique text) OWNER TO cuon_admin;

--
-- Name: fct_create_list_of_all_table(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_list_of_all_table() RETURNS text[]
    LANGUAGE plpgsql
    AS $$
  DECLARE
    bRet bool := True ;
    vList text[]  ;
    array_size integer ;
    sSql text ;
    BEGIN
 -- vList := array_append(vList,'') ;
	vList[1] := 'address' ;
        vList := array_append(vList,'partner') ;
        vList := array_append(vList,'address_notes') ;
	vList := array_append(vList,'address_misc') ;
	vList := array_append(vList,'address_bank') ;
	vList := array_append(vList,'account_system') ;
	vList := array_append(vList,'account_info') ;
	vList := array_append(vList,'account_plan') ;
	vList := array_append(vList,'account_sentence') ;


        vList := array_append(vList,'bank') ;

        vList := array_append(vList,'articles') ;
        vList := array_append(vList,'articles_parts_list') ;
	vList := array_append(vList,'articles_purchase') ;
        vList := array_append(vList,'articles_sales') ;
	vList := array_append(vList,'articles_stock') ;
	vList := array_append(vList,'articles_webshop') ;
	vList := array_append(vList,'articles_barcode') ;


        vList := array_append(vList,'dms') ;

    vList := array_append(vList,'clients') ;
    vList := array_append(vList,'biblio') ;
    vList := array_append(vList,'staff') ;

    	vList := array_append(vList,'enquiry') ;
        vList := array_append(vList,'enquiryget') ;
        vList := array_append(vList,'enquiryinvoice ') ;
        vList := array_append(vList,'enquirymisc') ;
	vList := array_append(vList,'enquiryposition') ;
	vList := array_append(vList,'enquirysupply') ;

        vList := array_append(vList,'list_of_deliveries') ;
        vList := array_append(vList,'proposal') ;
        vList := array_append(vList,'proposalposition') ;
        vList := array_append(vList,'proposalget') ;
        vList := array_append(vList,'proposalmisc') ;
        vList := array_append(vList,'proposalsupply') ;

    	vList := array_append(vList,'orderbook') ;
	vList := array_append(vList,'orderposition') ;
        vList := array_append(vList,'stock_goods') ;
	vList := array_append(vList,'orderinvoice') ;
       	vList := array_append(vList,'cash_desk_book_number') ;
        vList := array_append(vList,'list_of_invoices') ;
        vList := array_append(vList,'in_payment') ;
	vList := array_append(vList,'cash_desk') ;

       vList := array_append(vList,'support_ticket') ;
  vList := array_append(vList,'support_project') ;

        vList := array_append(vList,'projects') ;
	vList := array_append(vList,'project_phases ') ;
        vList := array_append(vList,'project_tasks') ;
        vList := array_append(vList,'project_task_material_res') ;
        vList := array_append(vList,'projects_task_staff_res') ;

        vList := array_append(vList,'sourcecode_file') ;
	vList := array_append(vList,'sourcecode_module') ;
	vList := array_append(vList,'sourcecode_module_staff_res') ;
	vList := array_append(vList,'sourcecode_modul_material_res') ;
	vList := array_append(vList,'sourcecode_modul_material_res') ;
        vList := array_append(vList,'sourcecode_parts') ;
	vList := array_append(vList,'sourcecode_projects') ;

  vList := array_append(vList,'hibernation') ;
  vList := array_append(vList,'hibernation_plant') ;


  vList := array_append(vList,'graveyard') ;
        vList := array_append(vList,'grave') ;
        vList := array_append(vList,'grave_work_maintenance') ;
        vList := array_append(vList,'grave_work_spring') ;
        vList := array_append(vList,'grave_work_summer') ;
        vList := array_append(vList,'grave_work_autumn') ;
        vList := array_append(vList,'grave_work_winter') ;
        vList := array_append(vList,'grave_work_single') ;
        vList := array_append(vList,'grave_work_holiday') ;
        vList := array_append(vList,'grave_work_year') ;
  vList := array_append(vList,'grave_service_notes') ;

         vList := array_append(vList,'botany') ;
       vList := array_append(vList,'botany_class') ;
  vList := array_append(vList,'botany_divisio') ;
  vList := array_append(vList,'botany_family') ;
  vList := array_append(vList,'botany_genus') ;
  vList := array_append(vList,'botany_ordo') ;
  vList := array_append(vList,'botany_kingdom') ;


     return vlist ;
   END ;

    $$;


ALTER FUNCTION public.fct_create_list_of_all_table() OWNER TO cuon_admin;

--
-- Name: fct_create_primary_index(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_primary_index() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  DECLARE
    bRet bool := True ;
    vList text[]  ;
    array_size integer ;
    sSql text ;
    BEGIN

       vlist := fct_create_list_of_all_table() ;

        array_size := array_length(vList,1) ;
       for ct in 1..array_size loop
            raise notice ' ct = %, array = % ', ct,vList[ct] ;
            begin
            sSql := 'ALTER TABLE ' || vList[ct] || ' ADD PRIMARY KEY (id) ' ;
            raise notice 'primary key  sSql = %', sSql ;
            perform execute sSql ;
            exception when others then
                raise notice 'error 4';
                raise notice 'SQL = %', sSql ;
              end ;


          end loop;
      return bRet ;
    END ;

    $$;


ALTER FUNCTION public.fct_create_primary_index() OWNER TO cuon_admin;

--
-- Name: fct_create_single_account(integer, text, text, date, integer, integer, double precision, integer, character varying); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_single_account(i_client integer, i_number text, t_designation text, d_date date, account_id integer, counter_account_id integer, f_amount double precision, address_id integer, cuuid character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    
    
   
    sSql text ;
    sSql2 text ;
    rID int ;
    cNormalUUID varchar(36) ;
   
    
    
    BEGIN
        cNormalUUID = fct_new_uuid() ;
        
        sSql := ' insert into account_system (id, uuid, user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,document_number,document_designation,account_id,contra_account_id,amount,address_id, actual_date, transaction_uuid ) values ' ;
        -- raise notice 'sSql1 = %', sSql ;
        sSql := sSql || '( nextval(' || quote_literal('account_system_id') || '),' || quote_literal(cNormalUUID) || ', current_user,' || quote_literal('insert') ||', ' ||  quote_literal(now()) || ',NULL,NULL,' || i_client || ',0,0,0,nextval(' || quote_literal('numerical_accounting_booknumber_client_'  || i_client ) || '), ' ;
        
        -- raise notice 'sSql2 = %', sSql ;
        sSql := sSql || quote_literal(t_designation) || ', ' ; 
        -- raise notice 'sSql3 = %', sSql ;
        
        sSql := sSql || account_id  || ', ' || counter_account_id || ', ' || f_amount || ', ' ;
        
        -- raise notice 'sSql4 = %', sSql ;
        sSql := sSql || address_id ||', ' || quote_literal(d_date) || ', ' || quote_literal(cUUID )|| ') ' ;
        
        raise notice 'sSql9 = % ', sSql ;
        execute(sSql);
        sSql := 'select id from account_system where uuid = ' || quote_literal(cNormalUUID );
        execute(sSql) into rID ;
        RETURN rID ;

    END ;
    
     $$;


ALTER FUNCTION public.fct_create_single_account(i_client integer, i_number text, t_designation text, d_date date, account_id integer, counter_account_id integer, f_amount double precision, address_id integer, cuuid character varying) OWNER TO cuon_admin;

--
-- Name: fct_create_trigger(text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_create_trigger(sname text, stable text, saction text, scursor text, stext text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
    bRet bool := False ;
    sSql text ;
    BEGIN
      begin
        sSql := 'DROP TRIGGER ' || sName || ' ON ' || sTable ;
        -- raise notice 'sSql = %', sSql ;
        execute sSql ;
        exception when others then
          raise notice 'error 1';
          raise notice 'SQL = %', sSql ;
      end;
      begin
	sSql := 'update ' || sTable || ' set status = ' || quote_literal('insert') || ' where status IS NULL ' ;
	raise notice 'sSql = %', sSql ;
	execute sSql ;
        exception when others then
          raise notice 'error set status ';
          raise notice 'SQL = %', sSql ;
      end;

      begin
        sSql := 'CREATE TRIGGER ' || sName ||' ' || sAction ||' ON ' || sTable || ' ' ||sCursor || ' ' || sText ;
        raise notice 'sSql = %', sSql ;
        execute sSql ;
        exception when others then
          raise notice 'error 2';
          raise notice 'SQL ERROR AT = %', sSql ;
      end;

    return bRet ;
    END ;

    $$;


ALTER FUNCTION public.fct_create_trigger(sname text, stable text, saction text, scursor text, stext text) OWNER TO cuon_admin;

--
-- Name: fct_createallnewinvoices(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_createallnewinvoices(cash_id integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
 
        igrave_id int ;
        sSql text ;
        r1 record ;
        
    BEGIN
    
        sSql := 'select id, type_of_paid from grave where (created_order is NULL or created_order = 0) and (contract_ends_at IS NULL or contract_ends_at > now() or contract_ends_at =  date ' || quote_literal('1900-01-01') ||  ' and status != ' || quote_literal('delete' ) || ') ';
        FOR r1 in execute(sSql)  LOOP
            if r1.type_of_paid is null then 
                r1.type_of_paid = -1 ;
            end if ;
            
            if cash_id < 0 then 
                return next r1.id ;
            else 
                if r1.type_of_paid = cash_id then 
                    return next r1.id ;
                end if ;
            end if ;
            
        END LOOP ;
        
        
 
    END ;
     $$;


ALTER FUNCTION public.fct_createallnewinvoices(cash_id integer) OWNER TO cuon_admin;

--
-- Name: fct_createmodulschedule(integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_createmodulschedule(imodule integer, scheduleuuid text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    -- working standalone but can called from a trigger
    
    r record;
    s record ;
    t record ;
    u record ;
    v record ;
    q record ;
    o record ;
    
    searchsql text := '';
    sSql1 text := '';
    sSql2 text := '';
    sSql3 text := '';
    sSql4 text := '';
    sSql5 text := '';
    sSql6 text := '';
    sSql7 text := '';
    
    newID int := 0 ;
    idFound int := 0;
    idPartner int := 0;
    iClient int := 0 ;
    sSchedulStaffs text := '';
    iStaffID int := 0 ;
    aStaff text array ;
    GraveServiceNotesAnnual int = 40105 ;
    sStaff text ;
    array_size int ;
    iArray int ;
    newUUID text ;
    newDate text ;
    newDate2 text ;
    sServiceDesignation text ;
    sServiceNote text ;
    
    BEGIN
       
        -- Search if schedule with the given uuid exist

            
        searchsql := 'select id from partner_schedul  where schedul_uuid = ' || quote_literal(scheduleUUID) || ' ' || fct_getWhere(2,'') ;
        raise notice ' searchsql = %', searchsql ;
        FOR r in execute(searchsql)  LOOP
         
            if r.id is not null then 
                if r.id > 0 then
                    idFound := r.id ;
                    -- if exist delete it
                    raise notice ' delete id found = %', idFound ;
                    
                    sSql1 := 'delete from partner_schedul where id = ' || idFound ;
                    execute(sSql1) ;
                    raise notice ' delete this %', sSql1 ;
                end if ;
            end if;

        END LOOP ;

        raise notice ' id found = %', idFound ;
        
        
        
        if iModule = GraveServiceNotesAnnual then
            searchsql := 'select * from grave_work_year where uuid = ' || quote_literal(scheduleUUID)  ;
            FOR r in execute(searchsql)  LOOP
         
                if r.id > 0 then
                    if r.grave_id is not null then 
                        searchsql := 'select * from grave where id = ' || r.grave_id || ' and ( contract_ends_at is null or  (contract_ends_at is not null and contract_ends_at > now() ) or  contract_ends_at = date(' || quote_literal('1900-01-01') || ' ) ) '  ;
                        raise notice 's sql =  %', searchsql ;
                        execute(searchsql) into s ;
                        
                        if s.graveyardid is not null then 
                            execute(' select * from graveyard where id = ' || s.graveyardid) into q ;
                        end if ;
                        if r.article_id is not null then 
                            execute('select * from articles where id = ' || r.article_id ) into o ;
                        end if ;
                        
                        if s.addressid is not null then 
                            searchsql := 'select * from address where id = ' || s.addressid ;
                            execute(searchsql) into t ;
                            -- raise notice 't = %', t ;
                            if t.id is not null then  
                                searchsql := 'select * from partner where addressid = ' || t.id ;
                                execute(searchsql) into u ;
                                raise notice 'u = %', u ;
                                if u.id > 0 then 
                                    idPartner := u.id ;
                                else
                                    -- generate partner
                                    sSql2 := 'select * from fct_generatePartner(' || t.id || ' ) ' ;
                                    execute(sSql2) into newID ;
                                    
                                    idPartner := newID ;
                                    
                                end if ;
                                -- raise notice 'idPartner = %', idPartner ;
                                sSql1 := 'select * from fct_getUserDataClient()' ;
                                execute(sSql1) into iClient ;
                                
                                sSql1 :=  'select * from fct_get_config_option(' || iClient || ', ' || quote_literal('clients.ini') || ', ' || quote_literal('CLIENT_' || 1) || ', ' || quote_literal('user_grave_schedule_yearly') || ') ' ;
                                -- raise notice 'sSql1 = %',sSql1 ;
                                execute(sSql1) into sSchedulStaffs ;    
                                -- raise notice 'sSchedulStaffs = %',sSchedulStaffs ;
                                aStaff = string_to_array(sSchedulStaffs,',') ;
                                
                                array_size := array_length(aStaff,1) ;
                                if r.service_designation is null then 
                                    sServiceDesignation := 'Yearly Graves' ;
                                elseif char_length(r.service_designation) < 1 then 
                                    sServiceDesignation := 'Yearly Graves' ;
                                else 
                                   select into  sServiceDesignation substring(r.service_designation from 1 for 59) ;
                                end if ;
                             
                                   
                                -- r = grave_work ;
                                -- s = grave ;
                                -- t = address ;
                                -- q = graveyard ;
                                -- o = article ;
                                
                                sServiceNote := '';
                                sServiceNote := sServiceNote || 'Grave: ' ;
                               
                                if s.firstname is not null then 
                                    sServiceNote := sServiceNote ||  s.firstname ;
                                end if ;
                                 if s.lastname is not null then 
                                    sServiceNote := sServiceNote || ' ' ||  s.lastname ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                if s.detachment is not null then 
                                    sServiceNote := sServiceNote ||  s.detachment ;
                                end if ;
                                if s.grave_number is not null then 
                                    sServiceNote := sServiceNote ||  ', ' || s.grave_number;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                
                                sServiceNote := sServiceNote || 'Address: ' ;
                                  
                                if t.lastname is not null then 
                                    sServiceNote := sServiceNote ||  t.lastname ;
                                end if ;
                                if t.firstname is not null then 
                                    sServiceNote := sServiceNote || ', ' || t.firstname ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                
                                sServiceNote := sServiceNote || 'Graveyard: ' ;
                                if q.shortname is not null then 
                                    sServiceNote := sServiceNote || ', ' || q.shortname ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                
                                sServiceNote := sServiceNote || 'Article: ' ;
                                 if r.service_count is not null then 
                                    sServiceNote := sServiceNote ||  r.service_count || 'x  ' ;
                                 else 
                                    sServiceNote := sServiceNote ||   '1x  ' ;
                                end if ;
                                
                                if o.id is not null then 
                                    sServiceNote := sServiceNote ||  o.id ;
                                end if ;
                                if o.number is not null then 
                                    sServiceNote := sServiceNote || ', ' || o.number ;
                                end if ;
                                if r.service_price is not null then 
                                    sServiceNote := sServiceNote || ', Price =  ' || r.service_price || '  Total = ' || r.service_price * r.service_count;
                                end if ;
                                if o.designation is not null then 
                                    sServiceNote := sServiceNote || chr(10) || o.designation ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                sServiceNote := sServiceNote || chr(10) || chr(10) ;
                                if r.service_notes is not null then
                                  
                                    sServiceNote := sServiceNote || r.service_notes ;
                                end if ;
                                
                                -- raise notice 'ServiceNote = %', sServiceNote ;
                                
                                for iArray in 1..array_size loop
                                    execute('select * from fct_new_uuid()') into newUUID ;
                                    -- raise notice 'single staff = %',aStaff[iArray] ;
                                    newDate = date_part('YEAR', now()) || '-' || r.annual_month || '-' ||r.annual_day  ;
                                    newDate2 = fct_date2String(date(newDate) ) ;
                                    

                                    -- insert into the partner_schedul 
                                    sSql1 := 'insert into partner_schedul (id, uuid,status, process_status, partnerid ,schedul_staff_id, dschedul_date, dschedul_date_end,schedul_date, schedul_date_end, schedul_time_begin,schedul_time_end, short_remark, notes, schedul_uuid, schedul_insert_from) values (nextval(' || quote_literal('partner_schedul_id') || '),' || quote_literal(newUUID) || ', ' || quote_literal('insert') ||',1,' || idPartner || ', ' || aStaff[iArray] || ', ' || quote_literal(newDate) || ', ' || quote_literal(newDate ) || ', ' || quote_literal(newDate2 ) || ', ' || quote_literal(newDate2 ) || ', '|| 0 ||', ' || 0 || ', '  || quote_literal( sServiceDesignation) ||', '|| quote_literal(sServiceNote) ||', ' || quote_literal(scheduleUUID) || ', ' ||  iModule || ' ) ' ;
                                    -- raise notice 'sSql1 = %', sSql1 ;
                                    execute (sSql1) ;
                                end loop ;
                            end if ;
                            
                        end if ; 
                    end if;
                end if ; 

            END LOOP ;
         END IF ;
        
        
        return idFound ;
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_createmodulschedule(imodule integer, scheduleuuid text) OWNER TO cuon_admin;

--
-- Name: fct_createnewinvoice(text, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_createnewinvoice(sservice text, igraveid integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    sSql text   ;
    iNewOrderID int ;
     r0 record ;
    counter int ; 
     iDiscount float ;   
    
     
    BEGIN
      
        counter := 1 ;
       
            
         if counter = 1 then 
            sSql := ' select grave.id , grave.addressid,inv.address_id as partner_id ,inv.part, sum(inv.part) as sum_part from grave left join grave_invoice_info inv on  grave.id =  inv.grave_id where grave.id = ' || iGraveID || ' group by grave.id, grave.addressid, inv.address_id, inv.part '  ;
            FOR r0 in execute(sSql)  LOOP
                    if r0.sum_part IS NULL then
                        iDiscount := 0 ;
                    else 
                        iDiscount :=  r0.sum_part ;
                    END IF ;
                    
                    iNewOrderID := fct_createSingleNewInvoice(sService , iGraveID  , r0.addressid, iDiscount ) ;
                    counter := 2 ;
                    return next iNewOrderID ;
                    
            END LOOP ;
        END IF ;
        
            
            if counter = 2  then 
            sSql := ' select grave.id , grave.addressid,inv.address_id as partner_id ,inv.part, sum(inv.part) as sum_part from grave left join grave_invoice_info inv on  grave.id =  inv.grave_id where grave.id = ' || iGraveID || ' group by grave.id, grave.addressid, inv.address_id, inv.part '  ;
            FOR r0 in execute(sSql)  LOOP
                if r0.partner_id IS NOT NULL THEN
                    if r0.part IS NULL then 
                        iDiscount := 0 ;
                    else 
                        iDiscount :=  100 - r0.part ;
                    END IF ;
                    
                    iNewOrderID := fct_createSingleNewInvoice(sService , iGraveID  , r0.partner_id, iDiscount ) ;
                    
                    return next iNewOrderID ;
            
                END IF ;
            END LOOP ;
            
        END IF ;
           
    
      END ;
    
     $$;


ALTER FUNCTION public.fct_createnewinvoice(sservice text, igraveid integer) OWNER TO cuon_admin;

--
-- Name: fct_createsinglenewinvoice(text, integer, integer, double precision); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_createsinglenewinvoice(sservice text, igraveid integer, iaddressid integer, idiscount double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    
    iClient int ;
    sSql text   ;
    iNewOrderID int ;
   
    r1 record ;
    r2 record ;
    last_position int ;
    grave_headline int ;
    grave_headline_designation text ;
    allInvoices int [] ;
    grave_part int ;
    iOrderType int;

    
    BEGIN
        iNewOrderID := -1 ;
	iOrderType := 8 ;

        iClient = fct_getUserDataClient(  ) ;
        raise notice ' Client Number = %',iClient ;
        grave_headline = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_main_headline_articles_id') ;
        
        
        select into iNewOrderID nextval('orderbook_id' ) ;
    
        raise notice ' new OrderNumber = %', iNewOrderID ;
        
        sSql := ' select grave.*, graveyard.shortname as graveyard_shortname from grave, graveyard ' ;
        
        IF sService = 'Service' THEN
            sSql = sSql || ', grave_work_maintenance as gm ' ;

        ELSEIF sService = 'Spring' THEN
            sSql = sSql || ', grave_work_spring as gm ' ;
            
         ELSEIF sService = 'Summer' THEN
            sSql = sSql || ', grave_work_summer as gm ' ;
            
         ELSEIF sService = 'Autumn' THEN
            sSql = sSql || ', grave_work_autumn as gm ' ;
            
        ELSEIF sService = 'Winter' THEN
            sSql = sSql || ', grave_work_winter as gm ' ;   
            
         ELSEIF sService = 'Holliday' THEN
            sSql = sSql || ', grave_work_holiday as gm ' ;
            
         ELSEIF sService = 'Unique' THEN
            sSql = sSql || ', grave_work_single as gm ' ;
            
         ELSEIF sService = 'Yearly' THEN
            sSql = sSql || ', grave_work_year as gm ' ;    
            
        END IF ;
        
        sSql := sSql || ' where graveyard.id = grave.graveyardid and grave.id = gm.grave_id and grave.id = ' || iGraveID || ' '  || fct_getWhere(2,'grave.') ;
        execute(sSql) into r1 ;
        
        if r1.id is null then 
            
            return  iNewOrderID;
        else 
            raise notice 'r1 = % ', r1.id ;
    
            sSql := ' insert into orderbook (id,addressnumber,ready_for_invoice,pricegroup1,pricegroup2,pricegroup3,pricegroup4,pricegroup_none, orderedat, from_modul, from_modul_id,deliveredat, discount,order_type) values (' ;
            sSql := sSql || iNewOrderID || ', ' || iAddressID || ', true, ' || r1.pricegroup1 || ', ' || r1.pricegroup2 || ', ' || r1.pricegroup3 || ', ' || r1.pricegroup4 || ', ' || r1.pricegroup_none  ;
            sSql := sSql || ', ' ||  quote_literal(current_date ) || ' , 40000, ' || r1.id || ', '  ||  quote_literal(current_date)  || ', ' || iDiscount  || ', ' || iOrderType || ' ) '  ;
            
            
            raise notice ' new sSql insert = % ', sSql ;
            
            
            execute (sSql) ;    
            
            select into last_position max(position) from orderposition where orderid = iNewOrderID ;
            if last_position IS NULL then 
                last_position = 0 ;
            end if ;
            
            
            
            if iDiscount > 0 THEN 
                 grave_part = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient , 'order_part_id') ;
                  last_position := last_position + 1 ;
                   sSql := 'insert into orderposition ( id, orderid , articleid , designation, amount  ,  position, price)  values (  (select nextval(''orderposition_id'') ) , ' ;
            sSql := sSql ||  iNewOrderID || ', ' || grave_part ||  ', ' || quote_literal(iDiscount || '%') || ', ' || 1   || ', ' || last_position  || ', 0 ) ' ;
            raise notice 'SQL for Discount %' , sSql ;
              execute (sSql) ; 
            END IF ;
                 
            grave_headline_designation := r1.lastname || ', Nr: ' || r1.detachment || ' / ' || r1.grave_number || ',  ' || r1.graveyard_shortname ;
             last_position := last_position + 1 ;
             
             
            sSql := 'insert into orderposition ( id, orderid , articleid , designation, amount  ,  position, price)  values (  (select nextval(''orderposition_id'') ) , ' ;
            sSql := sSql ||  iNewOrderID || ', ' || grave_headline ||  ', ' || quote_literal(grave_headline_designation) || ', ' || 1   || ', ' || last_position  || ', 0 ) ' ;
            raise notice 'SQL for headline %' , sSql ;
              execute (sSql) ;  
             
           
            
            return  iNewOrderID;
        end if ;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_createsinglenewinvoice(sservice text, igraveid integer, iaddressid integer, idiscount double precision) OWNER TO cuon_admin;

--
-- Name: fct_cuon_insert(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_cuon_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

  BEGIN
          NEW.uuid  := fct_new_uuid() ;
          
        RETURN NEW; 
    END;
     
    $$;


ALTER FUNCTION public.fct_cuon_insert() OWNER TO cuon_admin;

--
-- Name: fct_date2string(date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_date2string(ddate date) RETURNS text
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
        sDate text ;
        sDateFormat text ;
        
    BEGIN
        select into sDateFormat type_c from cuon_values  where name = 'SQLDateFormat'  ;
        
        select into sDate to_char(dDate,sDateFormat);
        RETURN sDate ;
    END;
     
     $$;


ALTER FUNCTION public.fct_date2string(ddate date) OWNER TO cuon_admin;

--
-- Name: fct_delete(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values at a delete 
    -- if delete a record, dont realy delete, set status to delete 
     
    DECLARE
    f_upd     varchar(400);
    v_delete varchar(20) ;
    BEGIN
        v_delete   := 'delete' ;
        f_upd := ' update ' || TG_RELNAME  || ' set status =  '|| quote_literal(v_delete) || '  where id = ' || OLD.id   ;
        -- RAISE NOTICE ' table-name =  % ', TG_RELNAME ;
        -- RAISE NOTICE ' sql =  % ',f_upd  ;
        execute f_upd ;
        -- RAISE NOTICE ' Name =  % ', TG_NAME ;
        RETURN NULL ;
    END;
     
     $$;


ALTER FUNCTION public.fct_delete() OWNER TO cuon_admin;

--
-- Name: fct_delete_cash_desk_from_deleted_order(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_delete_cash_desk_from_deleted_order() RETURNS boolean
    LANGUAGE plpgsql
    AS $$

       DECLARE

	sSql text ;
 	bOK bool ;
 r record ;
     	BEGIN
		bOK := true ;

		sSql := 'select orderbook.id as order_id, cash_desk.id as cash_desk_id from orderbook,cash_desk where cash_desk.order_id = orderbook.id and orderbook.status = ' || quote_literal('delete') ;


		 FOR r in execute(sSql)  LOOP

		     raise notice ' Delete this cash_desk entry for id = % ', r.cash_desk_id ;
		     delete from cash_desk where id = r.cash_desk_id ;
		 
		 END LOOP ;	 

     return bOK ;
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_delete_cash_desk_from_deleted_order() OWNER TO cuon_admin;

--
-- Name: fct_delete_zero_order(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_delete_zero_order(iorderid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    iStatus int ;
    sSql text ;
    sSql2 text ;
    sSql3 text ;
    possum int ;
    posid int ;
    r record ;
    r2 record ;
    
    BEGIN
     iStatus = 0;
     possum = 0;
     posid = 0;
     sSql := ' select max(id) as id  from orderposition where orderid = ' || iOrderID || ' and status != ''delete'' ' ;
     raise notice 'sSql = %',sSql ;

     for r in execute (sSql) loop
      possum = r.id ;
      raise notice 'possum = %',possum  ;	

      if possum is NULL  then
      	 raise notice 'possum2 = %',possum  ;
	 sSql2 = 'SELECT max(id) as id FROM orderbook WHERE insert_time < now() - ''1 day''::interval; ' ;
	 for r2 in execute ( sSql2) loop
	    posid = r2.id ;
	    if posid is NULL then
	      iStatus = 0 ;
	    else
	    
	       sSql3 = ' delete from orderbook where id = ' || iOrderID ;
	       raise notice 'sSql --final = %', sSql ;
	       execute sSql3 ;
	       
	        iStatus = 1 ;
	    end if ;
	 end loop ;
	 
       	
      end if ;
	  
     end loop ;
       
    return iStatus ;
   END ;
    
     $$;


ALTER FUNCTION public.fct_delete_zero_order(iorderid integer) OWNER TO cuon_admin;

--
-- Name: fct_duplicate_table(text, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_duplicate_table(tablename text, tablename2 text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    -- insert whole table1 in table2
     
    DECLARE
    ok bool;
    sSql text ;
    sExe text ;
    sExe2 text ;
    sSql2 text ;
    ri record ;
    oldrecord record ;
    len int ;
    
    
    BEGIN
        ok = true ;
    
        sExe := ' insert into ' || tablename2 || ' ( ' ;
        sExe2 := ' select  ' ;
        sSql := ' SELECT ordinal_position, column_name, data_type  FROM information_schema.columns   WHERE    table_schema = ' || quote_literal('public') || '  AND table_name = ' || quote_literal(tablename) || ' ORDER BY ordinal_position ' ;
        
        sSql2 := 'select id from ' || tablename ;
        raise notice ' sSql= %', sSql ;
        raise notice ' sSql2= %', sSql2 ;
        FOR oldrecord in execute(sSql2) LOOP
            raise notice 'start loop1 ' ;
            FOR ri in execute(sSql) LOOP
        
        
           
                sExe := sExe || ri.column_name || ', ' ;
                sExe2 := sExe2 || ri.column_name || ', ' ;
                      
                        
            END LOOP;
            raise notice 'end loop2 ' ;
            len := length(sExe);    
            sExe := substring(sExe from 1 for (len-2) ) ;
            sExe := sExe || ' ) ' ;
            
            len := length(sExe2);    
            sExe2 := substring(sExe2 from 1 for (len-2) ) ;
            
            
            
            sExe := sExe || sExe2 || ' from ' || quote_ident(tablename) || ' where id = ' || oldrecord.id ;
            raise notice ' sExe = %', sExe ;
            -- execute sExe ;
        
         END LOOP;
      return ok ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_duplicate_table(tablename text, tablename2 text) OWNER TO cuon_admin;

--
-- Name: fct_duplicate_table_entry(text, text, integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_duplicate_table_entry(tablename text, tablename2 text, oldid integer, id_sequence text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    -- insert whole table1 in table2
     
    DECLARE
    ok bool;
    sSql text ;
    sExe text ;
    sExe2 text ;
    sSql2 text ;
    ri record ;
    oldrecord record ;
    len int ;
    newid int ;
    sSql3 text ;
    BEGIN
        ok = true ;
        sSql3 = 'select nextval(' || quote_literal(id_sequence) || ') ' ;
        execute(sSql3) into newid ;
        
        sExe := ' insert into ' || tablename2 || ' ( ' ;
        sExe2 := ' ( select  ' ;
        sSql := ' SELECT ordinal_position, column_name, data_type  FROM information_schema.columns   WHERE    table_schema = ' || quote_literal('public') || '  AND table_name = ' || quote_literal(tablename) || ' ORDER BY ordinal_position ' ;
        
        sSql2 := 'select id from ' || tablename || ' where id = ' || oldid  ;
        raise notice ' sSql= %', sSql ;
        raise notice ' sSql2= %', sSql2 ;
        FOR oldrecord in execute(sSql2) LOOP
            raise notice 'start loop1 ' ;
            FOR ri in execute(sSql) LOOP
        
        
                IF ri.column_name = 'id' THEN
                    sExe := sExe || ri.column_name || ', ' ;
                    sExe2 := sExe2 || newid || ' , ' ;
                ELSE 
                    sExe := sExe || ri.column_name || ', ' ;
                    sExe2 := sExe2 || ri.column_name || ', ' ;
                END IF ;
                      
                        
            END LOOP;
            raise notice 'end loop2 ' ;
            len := length(sExe);    
            sExe := substring(sExe from 1 for (len-2) ) ;
            sExe := sExe || ' ) ' ;
            
            len := length(sExe2);    
            sExe2 := substring(sExe2 from 1 for (len-2) ) ;
            
            
            
            sExe := sExe || sExe2 || ' from ' || quote_ident(tablename) || ' where id = ' || oldid  || ' ) ' ;
            raise notice ' sExe = %', sExe ;
            execute sExe ;
        
         END LOOP;
      return newid ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_duplicate_table_entry(tablename text, tablename2 text, oldid integer, id_sequence text) OWNER TO cuon_admin;

--
-- Name: fct_duplicatearticle(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_duplicatearticle(iarticleid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE
        sSql    text ;
        newID integer ;
         rData  record ;
         
    BEGIN 
        newID = 0 ;
        
        select nextval('articles_id') into newID ;
         
        select into rData * from articles where articles.id = iArticleID ;
        
        insert into articles (id , client, status, sep_info_1 , sep_info_2 , sep_info_3 , number , designation  , wrapping , quantumperwrap , unit , manufactor_id , weight , sellingprice1 , sellingprice2 , sellingprice3 , sellingprice4 , tax_vat , material_group , associated_with , associated_id , tax_vat_id ) values (newID ,  rData.client, 'insert', rData.sep_info_1 , rData.sep_info_2 , rData.sep_info_3 , 'NEW-' || rData.number , rData.designation  , rData.wrapping , rData.quantumperwrap , rData.unit , rData.manufactor_id , rData.weight , rData.sellingprice1 ,  rData.sellingprice2 ,  rData.sellingprice3 ,  rData.sellingprice4 , rData. tax_vat ,  rData.material_group ,  rData.associated_with , rData.associated_id ,  rData.tax_vat_id );
        
        
        return newID ;
    END ;
    $$;


ALTER FUNCTION public.fct_duplicatearticle(iarticleid integer) OWNER TO cuon_admin;

--
-- Name: fct_duplicateorder(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_duplicateorder(iorderid integer, ordertype integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
     
    newOrderID int ;
     new_table   varchar(400) ;
     
      sSql text ;
    sSql2 text ;
        sExe text ;
        sExe2 text ;
        sTable  text;
        rData  record ;
        sCursor text ;
        iPosID int ;
        cur1 refcursor ;
        partPrefix text ;
    BEGIN
    partPrefix = '' ;
    
    select nextval('orderbook_id') into newOrderID ;
    
       select into rData user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,  number ,  designation ,         orderedat , deliveredat , packing_cost , postage_cost , misc_cost ,build_retry , type_retry  , supply_retry, gets_retry , invoice_retry , custom_retry_days , modul_number  ,                modul_order_number, discount  , ready_for_invoice , process_status   , proposal_number , customers_ordernumber , customers_partner_id , project_id , versions_number ,  versions_uuid   , staff_id , addressnumber from orderbook where id =  iOrderID    ;
    
        
        RAISE NOTICE 'status by rdata = %', rData.status;
        
        
    insert into orderbook (id, user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,  number ,  designation ,         orderedat , deliveredat , packing_cost , postage_cost , misc_cost ,build_retry , type_retry  , supply_retry, gets_retry , invoice_retry , custom_retry_days , modul_number  ,                modul_order_number, discount  , ready_for_invoice , process_status   , proposal_number , customers_ordernumber , customers_partner_id , project_id , versions_number  ,versions_uuid   , staff_id , addressnumber) values ( newOrderID, rData.user_id , rData.status ,  rData.insert_time , rData.update_time, rData.update_user_id  ,  rData.client ,  rData.sep_info_1 ,  rData.sep_info_2 ,  rData.sep_info_3 ,  'NEW-' || rData.number || partPrefix ,  rData.designation ,         rData.orderedat , rData.deliveredat , rData.packing_cost , rData.postage_cost , rData.misc_cost ,rData.build_retry , rData.type_retry  , rData.supply_retry, rData.gets_retry , rData.invoice_retry , rData.custom_retry_days , rData.modul_number  , rData.modul_order_number, rData.discount  , rData.ready_for_invoice , rData.process_status   , rData.proposal_number , rData.customers_ordernumber , rData.customers_partner_id , rData.project_id , 0,   fct_new_uuid()  , rData.staff_id, rData.addressnumber) ;
            
       
     
        sCursor := 'SELECT id from orderposition WHERE  orderid = '|| iOrderID || ' ' || fct_getWhere(2,' ')  ;
       
        OPEN cur1 FOR EXECUTE sCursor ;
        FETCH cur1 INTO iPosID ;

     

        WHILE FOUND LOOP
        RAISE NOTICE 'iPosID % ', iPosID ;
        select into rData user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,orderid,   articleid, designation, amount,  position,   price , tax_vat,  discount,  versions_number  ,versions_uuid  from orderposition where id = iPosID ;
        
        insert into orderposition (id, user_id , status ,  insert_time , update_time, update_user_id  ,  client ,  sep_info_1 ,  sep_info_2 ,  sep_info_3 ,orderid,   articleid, designation, amount,  position,   price , tax_vat,  discount,  versions_number  ,versions_uuid ) values ( nextval('orderposition_id'), rData.user_id , rData.status ,  rData.insert_time , rData.update_time, rData.update_user_id  ,  rData.client ,  rData.sep_info_1 ,  rData.sep_info_2 ,  rData.sep_info_3 , newOrderID,   rData.articleid, rData.designation, rData.amount,  rData.position,   rData.price , rData.tax_vat,  rData.discount,    0,   fct_new_uuid()  )  ; 
           
        FETCH cur1 INTO iPosID ;
    END LOOP ;
       
    close cur1 ;
    
      
       
       
     --  RAISE NOTICE 'sql = %', sSql ; 
      --  execute(sSql) ;
    return newOrderID ;    
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_duplicateorder(iorderid integer, ordertype integer) OWNER TO cuon_admin;

--
-- Name: fct_executechecked(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_executechecked(ssqlb64 text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

    DECLARE
    ok boolean ;
    success int ;
    sSql text ;
    BEGIN
	 -- raise notice 'sSqlb64 =  %', sSqlb64 ;

	-- BEGIN
		sSql = CONVERT_FROM(decode(sSqlb64,'base64'),'UTF-8');
	-- EXCEPTION
		-- ok := false;
--  raise notice 'wrong utf-code % ',1 ;
	--  raise notice 'sSql =  %', sSql ;
	BEGIN  
    	   execute (sSql) ;
	   success = 1;
	   raise notice 'success = %', success ;
	EXCEPTION
	   when others then 
	    success = 0;
	   raise notice 'success = %', success ;
        END;
    return success ;

    END ;
    
     $$;


ALTER FUNCTION public.fct_executechecked(ssqlb64 text) OWNER TO cuon_admin;

--
-- Name: fct_executetest(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_executetest() RETURNS boolean
    LANGUAGE plpgsql
    AS $$

    DECLARE
    ok boolean ;
    success int ;
    sSql text ;
    
    BEGIN

    sSql := 'insert into DMS (id,sub1) values (nextval( ' || quote_literal('dms_id') || '), ' || quote_literal('12345loooooooooookkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk3434341234567890kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk123456789012345678902123456789031234567890') || ')' ;
    success =  fct_executeChecked(encode(sSql,'base64'));
    
      return success ;

    END ;
    
     $$;


ALTER FUNCTION public.fct_executetest() OWNER TO cuon_admin;

--
-- Name: fct_extracttextfromdms(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_extracttextfromdms(dms_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sText text ;
        sSql text ;
        
        BEGIN
          
      
    
            select into iPaired_ID paired_id from dms where id = dms_id ;
            if iPaired_ID is null then 
                iPaired_ID = 0 ;
            end if ;
            
            if iPaired_ID > 0 then 
                iSearchID := iPaired_ID ;
            else
                iSearchID := dms_id ;
            end if ;
            
            sSql := 'select dms_extract from dms where id = ' || iSearchID ;
            -- set later more restrictions
            
            execute(sSql) into sText ;
            
            if sText is null then 
                sText = 'NONE' ;
            end if ;
                
            return sText ;
            
        END ;
    $$;


ALTER FUNCTION public.fct_extracttextfromdms(dms_id integer) OWNER TO cuon_admin;

--
-- Name: fct_findallactualgraves(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_findallactualgraves(cash_id integer) RETURNS SETOF integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
 
        igrave_id int ;
        sSql text ;
        r1 record ;
        
    BEGIN
    
        sSql := 'select id from grave where (contract_ends_at IS NULL or contract_ends_at > now() or contract_ends_at =  date ' || quote_literal('1900-01-01') ||  ' and status != ' || quote_literal('delete' ) || ') ';
        FOR r1 in execute(sSql)  LOOP
            return next r1.id ;
            
        END LOOP ;
        
        
 
    END ;
     $$;


ALTER FUNCTION public.fct_findallactualgraves(cash_id integer) OWNER TO cuon_admin;

--
-- Name: fct_generatepartner(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_generatepartner(iaddressid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    sSql2   text ;
    recAddress  record;
    sLastname text ;
    iNewID int := 0 ;
    newUUID text ;
    
    BEGIN
    execute('select * from fct_new_uuid()') into newUUID ;
    raise notice 'new uuid at partner = %',newUUID ;
    sSql := 'select * from address where id = ' || iAddressID  || ' ' || fct_getWhere(2,' ') ;
    raise notice ' sSql = %',sSql ;
    execute(sSql) into recAddress ;
    raise notice ' rec = %',recAddress ;
    if recAddress.lastname is null then 
       recAddress.lastname = '';
    end if ;
    if recAddress.firstname is null then 
       recAddress.firstname = '';
    end if ;
    if recAddress.street is null then 
       recAddress.street = '';
    end if ;

    if recAddress.zip is null then 
       recAddress.zip = '';
    end if ;
    if recAddress.city is null then 
       recAddress.city = '';
    end if ;

    if recAddress.phone is null then 
       recAddress.phone = '';
    end if ;


    sSql2 := ' insert into partner (id, uuid,lastname, firstname,street,zip,city,phone, addressid) values (nextval(' || quote_literal('partner_id') || '),' || quote_literal(newUUID) || ',' || quote_literal(recAddress.lastname) || ',' || quote_literal(recAddress.firstname) || ', ' || quote_literal(recAddress.street) || ', ' || quote_literal(recAddress.zip) || ', ' || quote_literal(recAddress.city) || ', '  || quote_literal(recAddress.phone) || ', ' || iAddressID || ' ) ' ;
    raise notice ' sSql = %',sSql2 ;
    execute (sSql2);
    sSql := 'select id from partner where uuid = ' || quote_literal(newUUID) ;
    execute(sSql) into iNewID ;
    return iNewID ;
       END ;
    $$;


ALTER FUNCTION public.fct_generatepartner(iaddressid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_account_id(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_account_id(acc_system_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
  iClient int ;
    
    iNewAccountingID int ;
    sSql text ;
    
    
  BEGIN
    
        iClient = fct_getUserDataClient(  ) ;
        sSql := 'select id  from account_info where accounting_system_type = ' || quote_literal(acc_system_id) || fct_getWhere(2,' ')  ;
        execute(sSql) into iNewAccountingID ;
        if iNewAccountingID is null then 
            iNewAccountingID := 0;
        end if ;
        
        return iNewAccountingID; 
        
     
       
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_get_account_id(acc_system_id integer) OWNER TO cuon_admin;

--
-- Name: fct_get_all_order_without_invoice(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_all_order_without_invoice() RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$

       DECLARE
       	sSql text ;
	sSql2 text;
	sSql3 text;
	r1 record ;
	cur1 refcursor ;
	iID  int;
	iMaxInvoice int ;
	sView text;
	BEGIN
		sSql  = 'select id from orderbook  where process_status = 500 and ready_for_invoice = true ' || fct_getWhere(2,'orderbook.') || '  order by id desc ' ;
		sView = replace (fct_new_uuid(),'-','_') ;
		
		sSql3 = 'create temp table t_' ||  sView || '  (id int) ' ;
		execute sSql3 ;
		
		 OPEN cur1 FOR EXECUTE sSql ;
		 FETCH cur1 INTO iID ;
		 while found loop
		       raise notice ' id by orderbook = %', iID ;
		       sSql2 = ' select max(invoice_number) as max_invoice_number from list_of_invoices where order_number = ' || iID || ' ' || fct_getWhere(2,'') ;
		       execute sSql2 into iMaxInvoice ;
		       raise notice ' Max Invoice = % ', iMaxInvoice ;
		       if iMaxInvoice IS NULL then
		       	  iMaxInvoice = 0 ;
		       end if ;

		       if iMaxInvoice > 0 then
		       	  update orderbook set process_status = 900 where orderbook.id = iID ;
			else
			   sSql3 = 'insert into t_' || sView || ' (id) values (' || iID || ' )' ;
			   execute sSql3 ;
			  
		       end if ;
			  
			FETCH cur1 INTO iID ;  
			  
		 end loop  ;		 
		  close cur1 ;
		  sSql = 'select id from t_' || sView ;
		  
		  for r1 in execute sSql  loop 
		  	  return next r1 ;
		  end loop ;
				  
			 
	END ;
    
     $$;


ALTER FUNCTION public.fct_get_all_order_without_invoice() OWNER TO cuon_admin;

--
-- Name: fct_get_config_option(integer, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_config_option(iclientid integer, sfile character varying, cpsection character varying, soptions character varying) RETURNS text
    LANGUAGE plpgsql
    AS $$ 
    DECLARE 
     r1 record ;
     svalue text ;
     ok boolean ;
     sSql text ;
    BEGIN 
     -- raise notice ' config options = % , %, %, %', iClientID, sFile,cpSection,sOptions ;
        sSql := 'select value from cuon_config where client_id = ' || iClientID || ' and config_file = '  || quote_literal(sFile) || '  and  section = ' || quote_literal(cpSection) || ' and  option = ' || quote_literal(sOptions )  ;
        -- raise notice ' sSql = %', sSql ;
        execute(sSql) into svalue ;
        
        return svalue ;

    END; 

    
    $$;


ALTER FUNCTION public.fct_get_config_option(iclientid integer, sfile character varying, cpsection character varying, soptions character varying) OWNER TO cuon_admin;

--
-- Name: fct_get_database(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_database(stype text) RETURNS public.return_text3
    LANGUAGE plpgsql
    AS $$
    
     
    DECLARE
    sDB text ;
    sDBPositions text ;
    sDBInvoice text ;
    
    rText3 return_text3 ;
    
    BEGIN
     
       
            
                
            if stype = 'Proposal' then
                sDB = 'proposal';
                sDBPositions = 'proposalposition';
                sDBInvoice = 'proposalinvoice';
                
            elseif stype = 'Enquiry' then
                sDB = 'enquiry' ;
                sDBPositions = 'enquiryposition' ;
                sDBInvoice = 'enquiryinvoice' ;
            elseif stype = 'Project' then 
                 
                sDB = 'projects' ;
                sDBPositions = '' ;
                sDBInvoice = ''   ; 
                
            else 
                sDB = 'orderbook' ;
                sDBPositions = 'orderposition' ;
                sDBInvoice = 'orderinvoice' ;
                
            end if ;
        
        rText3.a = sDB ;
        rText3.b = sDBPositions ;
        rText3.c = sDBInvoice ;
        
        return rText3 ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_get_database(stype text) OWNER TO cuon_admin;

--
-- Name: fct_get_dms_document_image(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_dms_document_image(dms_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
        
        BEGIN
          
      
    
            select into iPaired_ID paired_id from dms where id = dms_id ;
            if iPaired_ID is null then 
                iPaired_ID = 0 ;
            end if ;
            
            if iPaired_ID > 0 then 
                iSearchID := iPaired_ID ;
            else
                iSearchID := dms_id ;
            end if ;
            
            sSql := 'select document_image from dms where id = ' || iSearchID ;
            -- set later more restrictions
            
            execute(sSql) into sImage ;
            
            return sImage ;
            
        END ;
    $$;


ALTER FUNCTION public.fct_get_dms_document_image(dms_id integer) OWNER TO cuon_admin;

--
-- Name: fct_get_lastreservedvalue(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_lastreservedvalue(iarticleid integer, istockid integer, iid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
	
    BEGIN

	sSql := 'select actual_reserved  from stock_goods where id < ' || iID || ' and  stock_id = ' || iStockID || ' and article_id = ' || iArticleID  || ' ' ||  fct_getWhere(2,'')|| ' order by date_of_change, id desc ' ;
	
	OPEN cur1 FOR EXECUTE sSql ;
        FETCH cur1 INTO actual_value ;

   	close cur1 ;

	if NOT FOUND then
	   actual_value = 0;
	end if ;
	
		
	-- raise notice ' Actual Reserved = % ', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;
	


   END ;
    $$;


ALTER FUNCTION public.fct_get_lastreservedvalue(iarticleid integer, istockid integer, iid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_laststockvalue(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_laststockvalue(iarticleid integer, istockid integer, iid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
	
    BEGIN

	sSql := 'select actual_stock  from stock_goods where id < ' || iID || ' and  stock_id = ' || iStockID || ' and article_id = ' || iArticleID  || ' ' ||  fct_getWhere(2,'')|| ' order by date_of_change, id desc ' ;
	
	OPEN cur1 FOR EXECUTE sSql ;
        FETCH cur1 INTO actual_value ;

	if NOT FOUND then
	   actual_value = 0;
	end if ;

	close cur1 ;    

		
	-- raise notice ' Actual Stock = % ', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;



   END ;
    $$;


ALTER FUNCTION public.fct_get_laststockvalue(iarticleid integer, istockid integer, iid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_modul_name(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_modul_name(imodul integer) RETURNS text
    LANGUAGE plpgsql
    AS $$

    DECLARE
	sSql text ;  
     	sValue text ;
     	iClient int ;
     	sModul text ;
   
     Begin

	
     	return sModul ;
     END ;
     
     $$;


ALTER FUNCTION public.fct_get_modul_name(imodul integer) OWNER TO cuon_admin;

--
-- Name: fct_get_net_for_article(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_net_for_article(iarticleid integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 
    DECLARE
        sSql    text ;
        bNet bool ;
        net bool ;
        rData  record ;
    BEGIN 
        bNet := true ;
        raise notice ' net value is %',bNet ;
        select into rData price_type_net from material_group, articles where articles.id = iArticleID and articles.material_group = material_group.id ;
        
        if rData.price_type_net is null then 
            bNet := true ;
        else
            bNet := rData.price_type_net ;
        end if ;
        
        raise notice ' net value is %',bNet ;
       return bNet ;
    END ;
    $$;


ALTER FUNCTION public.fct_get_net_for_article(iarticleid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_new_tax_vat_for_article_1(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_new_tax_vat_for_article_1(iarticleid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
 DECLARE
    sSql text   ;
     r0 record ;
    iNewTaxVat int ;
    sNewTaxVat text ;
    iMGroup record ;
    aMGroups int[] ;
    aMGroupsString text[] ;
    
    sMGroups text ;
    iMGroupsTaxVat int ;
    iClient int ;
    i int ;
    fTaxVat float ;
    
    BEGIN
        iNewTaxVat = -1;
        fTaxVat := 0.00 ;
        
        iClient = fct_getUserDataClient(  ) ;
        sMGroups = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_grave_materialgroups_for_tax_vat_1') ;
        sNewTaxVat = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_grave_materialgroups_new_tax_vat_id_1') ;
        
        sSql := 'select articles.material_group  from articles where articles.id = ' || iArticleID  ;
        execute(sSql) into r0 ;
       
        aMGroupsString =  string_to_array(sMGroups, ',') ; 
        
        IF r0.material_group IS NOT NULL THEN 
            FOR i IN 1..array_length(aMGroupsString, 1) LOOP
                IF r0.material_group = aMGroupsString[i]::INTEGER THEN 
                    iNewTaxVat =sNewTaxVat::integer ;
                END IF ;
            END LOOP; 
  
        END IF ;
    
        -- IF iNewTaxVat > -1 THEN 
        --     select into fTaxVat vat_value from tax_vat where id = iNewTaxVat ;
        -- END IF ;
        
        if iNewTaxVat = -1 then
            iNewTaxVat = 0 ;
        end if ;
        return iNewTaxVat::float;
    END ;
    
     $$;


ALTER FUNCTION public.fct_get_new_tax_vat_for_article_1(iarticleid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_price_for_pricegroup(character varying, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_price_for_pricegroup(smodul character varying, imodulid integer, iarticleid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
 
    DECLARE
        sSql    text ;
        fPrice float ;
        iAddressID integer ;
        recData record ;
        pricegroupID integer ;
        
    BEGIN 
        fPrice := 0.00 ;
           IF sModul = 'grave' THEN
               sSql := ' select pricegroup1, pricegroup2,pricegroup3,pricegroup4,pricegroup_none from grave where grave.id = ' || iModulID ;
            ELSEIF sModul = 'orderposition' THEN
               sSql := ' select pricegroup1, pricegroup2,pricegroup3,pricegroup4,pricegroup_none from orderbook where id = ' || iModulID ;
               
            END IF ;
            execute(sSql) into recData ;
            
            if recData.pricegroup1 IS NULL then
                pricegroupID :=5 ;
            ELSEIF recData.pricegroup1 = TRUE THEN 
                pricegroupID :=1 ;
            ELSEIF recData.pricegroup2 = TRUE THEN 
                pricegroupID :=2 ;
            ELSEIF recData.pricegroup3 = TRUE THEN 
                pricegroupID :=3 ;
            ELSEIF recData.pricegroup4 = TRUE THEN 
                pricegroupID :=4 ;
            ELSEIF recData.pricegroup_none = TRUE THEN 
                pricegroupID :=5 ;
            ELSE 
                pricegroupID :=5 ;
            END IF ;
            
            raise notice ' pricegroup id = % ',pricegroupID ;
            
            
            
            IF pricegroupID = 5 then
                
            IF sModul = 'grave' THEN
                select into recData addressid from grave where id = iModulID ;
                -- raise notice ' address id = % ',recData.addressid ;
                iAddressID = recData.addressid ;
            ELSEIF sModul = 'orderposition' THEN
                select into recData addressnumber from orderbook  where orderbook.id = iModulID ;
                -- raise notice ' address id = % ',recData.addressnumber;
                iAddressID = recData.addressnumber ;
            END IF ;
            
            
                
                
                
                select into recData pricegroup1, pricegroup2,pricegroup3,pricegroup4,pricegroup_none from addresses_misc where address_id = iAddressID ;
                -- raise notice ' pricegroup by address = %, % , %',recData.pricegroup1, recData.pricegroup2,recData.pricegroup3;
                if recData.pricegroup1 IS NULL then
                    pricegroupID :=1 ;
                ELSEIF recData.pricegroup1 = TRUE THEN 
                    pricegroupID :=1 ;
                ELSEIF recData.pricegroup2 = TRUE THEN 
                    pricegroupID :=2 ;
                ELSEIF recData.pricegroup3 = TRUE THEN 
                    pricegroupID :=3 ;
                ELSEIF recData.pricegroup4 = TRUE THEN 
                    pricegroupID :=4 ;
                ELSEIF recData.pricegroup_none = TRUE THEN 
                    pricegroupID :=5 ;
                ELSE 
                    pricegroupID :=1 ;
                END IF ;
            END IF ;
        
        -- raise notice ' pricegroup id last value = % ',pricegroupID ;
        
        if pricegroupID < 5 then 
            sSql = 'select sellingprice'||pricegroupID||' as price from articles where id = ' || iArticleID ;
            -- raise notice ' sql  = % ',sSql ;
            
            for recData in execute sSql 
                LOOP 
            END LOOP ;
        
            -- raise notice ' price = % ', recData.price ;
         
            fPrice := recData.price ;
            if fPrice IS NULL THEN 
                fPrice := 0.00 ;
            END IF ;
            -- raise notice ' price = % ',fPrice ;
        END IF ;
         
        return fPrice ;
    END ;
    $$;


ALTER FUNCTION public.fct_get_price_for_pricegroup(smodul character varying, imodulid integer, iarticleid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_reservedsum(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_reservedsum(iarticleid integer, istockid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
        rec1 record ;
	
    BEGIN
    actual_value = 0.00 ;

	sSql := 'select to_embed, roll_out from stock_goods where stock_id = ' || iStockID || ' and article_id = ' || iArticleID  || ' and reserved_bit = 1 ' ||  fct_getWhere(2,'') ;
	
    raise notice 'sSql = %', sSql ;
	OPEN cur1 FOR EXECUTE sSql ;
    FETCH cur1 INTO rec1 ;

    While FOUND LOOP


    	if rec1.to_embed IS NULL then
	       rec1.to_embed = 0.00;
        end if ;
	    if rec1.roll_out IS NULL then
	       rec1.roll_out = 0.00;
        end if ;
        raise notice ' Total-Sum at Stock = %. article = %, embed =%, roll = % ', iStockID, iArticleID, rec1.to_embed , rec1.roll_out   ;

        actual_value = actual_value + rec1.to_embed - rec1.roll_out ;
        raise notice ' Total-Sum 0.1 = % ', actual_value ;

	
        FETCH NEXT from cur1 INTO rec1 ;

    END LOOP ;

	close cur1 ;

	raise notice ' Total-Sum = % ', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;
	


   END ;
    $$;


ALTER FUNCTION public.fct_get_reservedsum(iarticleid integer, istockid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_sales_of_order(integer, date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_sales_of_order(order_id integer, end_date date) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

       DECLARE

	sSql text ;
 	bOK bool ;
	this_day date;
	orderbook_month int;
	sum_sales float ;
 r record ;
     	BEGIN
	
	sSql = 'select sum(orderposition.amount *orderposition.price) as sum from orderposition, orderbook where   date(orderbook.orderedat) -  date(' || quote_literal(end_date) || ') >= 0  and orderbook.id = ' || order_id  || ' and orderposition.orderid = ' || order_id || ' ' || fct_getWhere(2,'orderposition.') ;

	-- raise notice ' sSql = % ', sSql ;
	execute sSql into sum_sales ;
	   
	 if sum_sales is null then
	    sum_sales = 0.00 ;
	 end if ;
	    
    	return sum_sales ;
        
    	END ;
    
     $$;


ALTER FUNCTION public.fct_get_sales_of_order(order_id integer, end_date date) OWNER TO cuon_admin;

--
-- Name: fct_get_sellingprice(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_sellingprice(iarticleid integer, ipricegroup integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

 
    DECLARE
        sSql    text ;
        fPrice float ;
        iAddressID integer ;
        recData record ;
        pricegroupID integer ;
        cur1 refcursor ;
	 
    BEGIN 
     fPrice = 0.00;
     sSql = 'select sellingprice'||iPricegroup||' as Price  from articles where id = ' || iArticleID ;
     raise notice 'sSql1 = %',sSql ;
     execute (sSql) into fPrice ;
     raise notice 'Price1 = %',fPrice ;
     IF fPrice IS NULL then
     	if iPricegroup > 1 then 
	   sSql = 'select sellingprice1 as Price  from articles where id = ' || iArticleID ;
           raise notice 'sSql2 = %',sSql ;
           
	   execute (sSql) into fPrice ;
	   raise notice 'Price2 = %',fPrice ;	     
        END IF;
      END IF;

      IF fPrice IS NULL then 
      	 fPrice = 0.00 ;
      END IF;

     return fPrice ;
    END ;
    $$;


ALTER FUNCTION public.fct_get_sellingprice(iarticleid integer, ipricegroup integer) OWNER TO cuon_admin;

--
-- Name: fct_get_statusbar(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_statusbar(imodul integer, iid integer) RETURNS text
    LANGUAGE plpgsql
    AS $$

    DECLARE
        sSql text ;  
        sValue text ;
        iClient int ;
      	sModul text ;
	r record;

   Begin
	 iClient = fct_getUserDataClient(  ) ;
	 sValue = 'NONE';
	 if iModul = 15300 then
	    sSql = ' select pr.name ||'','' ||  pa.name || '','' || mo.name ||  '' - ''  || mo.designation as sValue from sourcecode_projects as pr, sourcecode_parts as pa , sourcecode_module as mo, sourcecode_file as sf where sf.id = ' || iID || ' and mo.id = sf.modul_id and pa.id = mo.part_id and pr.id = pa.project_id' ;
	  
	elseif  iModul = 15200 then
	    sSql = ' select pr.name ||'','' ||  pa.name || '','' || pa.designation  as sValue from sourcecode_projects as pr, sourcecode_parts as pa , sourcecode_module as mo where mo.id = ' || iID || 'and  pa.id = mo.part_id and pr.id = pa.project_id' ;

	elseif  iModul = 15100 then
	    sSql = ' select pr.name ||'','' ||  pr.designation  as sValue from sourcecode_projects as pr, sourcecode_parts as pa where pa.id = ' || iID || 'and  pr.id = pa.project_id ' ;

	    
	end if ;

        raise notice 'sSql = %',sSql ;
	if sSql is not NULL then 
	        execute(sSql) into r ;
	        sValue := r.svalue ;
	end if ;
  

	return sValue ;
  END ;
    
     $$;


ALTER FUNCTION public.fct_get_statusbar(imodul integer, iid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_taxvat_for_article(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_taxvat_for_article(iarticleid integer) RETURNS public.return_misc3
    LANGUAGE plpgsql
    AS $$
 
    DECLARE
        sSql    text ;
        fTaxVat float ;
        iTaxVatArticle integer ;
        rData  record ;
        rValue return_misc3 ;
    BEGIN 
        fTaxVat := 0.00;
    
        select into rData tax_vat_id from articles where id = iArticleID ;
        IF rData.tax_vat_id IS NULL then
            iTaxVatArticle = 0;
        ELSE
            iTaxVatArticle = rData.tax_vat_id ;
        END IF ;
        
        IF iTaxVatArticle= 0 THEN
            select into rData  material_group.tax_vat from articles,material_group where articles.id = iArticleID and articles.material_group = material_group.id ;
            IF rData.tax_vat IS NULL then
                iTaxVatArticle = 0;
            ELSE
                iTaxVatArticle = rData.tax_vat ;
            END IF ;
        END IF ;
        IF iTaxVatArticle>0 then
            select into rData vat_value, vat_acct1 from tax_vat where id = iTaxVatArticle;
            fTaxVat = rData.vat_value ;
            if rData.vat_acct1 is null OR char_length(rData.vat_acct1) = 0 then 
                rValue.c = '38010' ;
            else 
                rValue.c = rData.vat_acct1 ;
            end if ;
        else
        
            fTaxVat = 0.00 ;
            rValue.c = '38010' ;
            
        END IF;

        rValue.a = iTaxVatArticle ;
        rValue.b = fTaxVat ;
        
        
        return rValue ;
    END ;
    $$;


ALTER FUNCTION public.fct_get_taxvat_for_article(iarticleid integer) OWNER TO cuon_admin;

--
-- Name: fct_get_totalsum(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_get_totalsum(iarticleid integer, istockid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
        rec1 record ;
	
    BEGIN
    actual_value = 0.00 ;

	sSql := 'select to_embed, roll_out, reserved_bit from stock_goods where stock_id = ' || iStockID || ' and article_id = ' || iArticleID  || ' ' ||  fct_getWhere(2,'') ;
	
    raise notice 'sSql = %', sSql ;
	OPEN cur1 FOR EXECUTE sSql ;
    FETCH cur1 INTO rec1 ;

    While FOUND LOOP


    	if rec1.to_embed IS NULL then
	       rec1.to_embed = 0.00;
        end if ;
	    if rec1.roll_out IS NULL then
	       rec1.roll_out = 0.00;
        end if ;
        if rec1.reserved_bit IS NULL then
	       rec1.reserved_bit = 0;
        end if ;

        raise notice ' Total-Sum at Stock = %. article = %, embed =%, roll = % ', iStockID, iArticleID, rec1.to_embed , rec1.roll_out   ;
        
        if rec1.reserved_bit = 0 then 
            actual_value = actual_value + rec1.to_embed - rec1.roll_out ;
        end if ;

        raise notice ' Total-Sum 0.1 = % ', actual_value ;

	
        FETCH NEXT from cur1 INTO rec1 ;

    END LOOP ;

    close cur1 ;

		
	raise notice ' Total-Sum = % ', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;
	


   END ;
    $$;


ALTER FUNCTION public.fct_get_totalsum(iarticleid integer, istockid integer) OWNER TO cuon_admin;

--
-- Name: fct_getaddress(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getaddress(iaddressid integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    recAddress  record;
    
    
    
    BEGIN
    sSql := 'select address, lastname, lastname2,firstname, street, zip, city,city as cityfield,  
        state, country from address 
        where id = ' || iAddressID  || ' ' || fct_getWhere(2,' ') ;
    for recAddress in execute sSql 
    LOOP 
        raise notice 'addresslastnmae = %',recAddress.lastname ;
        raise notice 'address = %',recAddress.address ;
        --raise notice 'cityfield = %',recAddress.cityfield ;
       
        if recAddress.country is NULL then
            recAddress.cityfield := recAddress.zip || ' ' || recAddress.city ;
        else 
            recAddress.cityfield := recAddress.country || '-' || recAddress.zip || ' ' || recAddress.city ;
        END IF ;
        --recAddress.country := fct_getChar(recAddress.country) ;
        raise notice 'country = %',recAddress.country ;
       raise notice 'cityfield = %',recAddress.cityfield ;
       
    END LOOP ;
    return recAddress  ;
       END ;
    $$;


ALTER FUNCTION public.fct_getaddress(iaddressid integer) OWNER TO cuon_admin;

--
-- Name: fct_getaddressfashion(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getaddressfashion(ifashionvalue integer, iaddressid integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    r record ;
    bOK bool ;
    
    
    BEGIN
    bOK := False ;
    sSql := ' select cb_fashion from addresses_misc where address_id = ' || iAddressID || fct_getWhere(2,' ') ;
    execute(sSql) into r ;

    if r.cb_fashion is not null AND r.cb_fashion > 0 then 
        bOK = True ;
    end if ;

    
    return bOK  ;
       END ;
    $$;


ALTER FUNCTION public.fct_getaddressfashion(ifashionvalue integer, iaddressid integer) OWNER TO cuon_admin;

--
-- Name: fct_getaddressfield(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getaddressfield(iaddressid integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    recAddress  record;
    
    
    
    BEGIN
    
     sSql := 'select id as address_id, address.address::text as address , address.firstname::text as firstname, address.lastname::text as lastname,
     address.lastname2::text as lastname2,  address.street::text as street, (address.zip  || '' '' ||  address.city)  as city ,
     (address.country  ||'' ''||  address.zip  || '' ''|| address.city)::text  as city_country , address.zip::text as zip,
     address.country::text as country,      address.city::text as city_alone,NULL::text as cityfield, NULL::text as first_last, NULL::text as last_first
     from  address  where id = ' || iAddressID  || ' ' || fct_getWhere(2,' ') ;
    for recAddress in execute sSql 
    LOOP 
        -- raise notice 'addresslastnmae = %',recAddress.lastname ;
        -- raise notice 'address = %',recAddress.address ;
        -- raise notice 'cityfield = %',recAddress.cityfield ;
       
        if recAddress.country is NULL then
            recAddress.cityfield := recAddress.zip || ' ' || recAddress.city_alone ;
        else 
            recAddress.cityfield := recAddress.country || '-' || recAddress.zip || ' ' || recAddress.city_alone ;
        END IF ;
        --recAddress.country := fct_getChar(recAddress.country) ;
       -- raise notice 'country = %',recAddress.country ;
       -- raise notice 'cityfield = %',recAddress.cityfield ;
       if recAddress.firstname is NULL then
            recAddress.first_last := recAddress.lastname ;
            recAddress.last_first := recAddress.lastname ;
        else
            recAddress.first_last := recAddress.firstname ||' ' || recAddress.lastname ;
            recAddress.last_first := recAddress.firstname ||' ' || recAddress.lastname ;
        END IF ;
       
    END LOOP ;
    return recAddress  ;
       END ;
    $$;


ALTER FUNCTION public.fct_getaddressfield(iaddressid integer) OWNER TO cuon_admin;

--
-- Name: fct_getarticlepartslistfororder(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getarticlepartslistfororder(orderid integer) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
 DECLARE
 
 
    sSql text ;
    r2 record ;
    rPositions record ;
    rArticlesPart record ;
    
    
    BEGIN
       
       sSql := 'select articleid  as article_id from orderposition where orderid = ' || OrderID || ' ' ||  fct_getWhere(2,' ') ;
       
       FOR r2 in execute(sSql)  LOOP
           
           
      
            
            return next r2 ;
            
        END LOOP ;
     
            
      
       
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getarticlepartslistfororder(orderid integer) OWNER TO cuon_admin;

--
-- Name: fct_getbarcode(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getbarcode(snumber text) RETURNS text
    LANGUAGE plpgsql
    AS $$


/* Fonction pour transformer une chaine de caractere en chaine code 128 en PL/pgsql 
Traduction de la fonction VB de Grandzebu par Nicolas Fanchamps en PL/SQL 
Portage de PL/SQL vers PL/pgsql (postgresql 7.4.2) par Gerald Salin, le 07/10/05 

PRE-REQUIS : installation du langage plpgsql dans la base ou sera utilisee la fonction
HOWTO : 
- j ai cree la fonction plSQL via l interface de creation de fonction de phpPgAdmin
- specifier le nom de la fonction
- 1 seul argument de type text
- 1 seul retour de type text
- langage plpgsql
- copier/coller tout ce code dans la partie definition
- valider

on peut appeler la fonction par la commande : select myfonction('texte a encoder')
*/

DECLARE
  ind integer;         /* Indice d avancement dans la chaine de caractere */
  chaine text; /*recuperation de l argument dans une variable*/
  longueur integer;    /* longueur de la chaine passee en argument */
  checksum integer;    /* Caractere de verification de la chaine codee */
  mini integer;        /* nbr de caracteres numeriques en suivant */
  dummy integer;       /* Traitement de 2 caracteres a la fois */
  tableB BOOLEAN;      /* Booleen pour verifier si on doit utiliser la table B du code 128 */
  code128 text;        /* code 128 de l argument */

BEGIN
  ind:=1; 
  chaine =  sNumber ;
 
  code128 := '';
  
  longueur := LENGTH(chaine); /* recupere la longueur de la chaine passee en argument */

 IF longueur < 1
    THEN
      RAISE EXCEPTION ' Argument Absent!!!';
    ELSE 
    FOR  ind  IN 1 .. longueur --test de la validite des caracteres composant l argument
      LOOP
        IF (ASCII(SUBSTR(chaine, ind, 1)) < 32) OR (ASCII(SUBSTR(chaine, ind, 1)) > 126)
          THEN
            RAISE EXCEPTION ' Argument invalide!!!';
        END IF;
      END LOOP;
  END IF;
  tableB := TRUE;
  WHILE ind <= longueur
    LOOP

      IF (tableB = TRUE)
        THEN
          --Voir si cest interessant de passer en table C
          --Oui pour 4 chiffres au debut ou a la fin, sinon pour 6 chiffres
          IF ((ind = 1) OR (ind+3 = longueur))
            THEN mini := 4;
          ELSE
            mini := 6;
          END IF;

          --TestNum : si les mini caracteres a partir de ind son numeriques, alors mini = 0
          mini := mini-1;
          IF ((ind + mini) <= longueur)
            THEN
              WHILE mini >= 0
              LOOP
                IF (ASCII(SUBSTR(chaine, ind+mini , 1)) < 48) OR (ASCII(SUBSTR(chaine, ind+mini, 1)) > 57)
                  THEN EXIT;
                END IF;
                mini := mini-1;
              END LOOP;
          END IF;

          --Si mini < 0 on passe en table C
          IF (mini < 0)
            THEN
              IF (ind = 1)
                THEN --Debuter sur la table C
                  code128 := CHR(210);
              ELSE --Commuter sur la table C
                code128 := code128 || CHR(204);
              END IF;
              tableB := FALSE;
          ELSE
            IF (ind = 1)
              THEN --Debuter sur la table B
                code128 := CHR(209);
            END IF;
          END IF;
 END IF;

      IF (tableB = FALSE)
        THEN --On est sur la table C, on va essayer de traiter 2 chiffres
          mini := 2;
          mini := mini-1;
          IF (ind + mini <= longueur)
            THEN
              WHILE mini >= 0
              LOOP
                
                IF (ASCII(SUBSTR(chaine, ind+mini , 1)) < 48) OR (ASCII(SUBSTR(chaine, ind+mini, 1)) > 57)
                  THEN EXIT;
                END IF;
                mini := mini-1;
              END LOOP;
          END IF;

          IF (mini < 0)
            THEN --OK Pour 2 chiffres, les traiter

              dummy := SUBSTR(chaine, ind, 2)::integer;
              IF (dummy < 95)
                THEN
                  dummy := dummy + 32;
              ELSE
                dummy := dummy + 100;
              END IF;
            code128 := code128 || CHR(dummy);
            ind := ind + 2;
          ELSE
            --On a pas deux chiffres, retourner en table B
            code128 := code128 || CHR(205);
            tableB := TRUE;
          END IF;

      END IF;

      IF (tableB = TRUE)
        THEN
          code128 := code128 || SUBSTR(chaine, ind, 1);
          ind := ind + 1;
      END IF;

    END LOOP;
    --Calcul de la clef de controle
    FOR ind IN 1 .. LENGTH(code128)
      LOOP
        dummy := ASCII(SUBSTR(code128, ind, 1));
        IF (dummy < 127)
          THEN
            dummy := dummy - 32;
        ELSE
          dummy := dummy - 100;
        END IF;

        IF (ind = 1)
          THEN
            checksum := dummy;
        END IF;

        checksum := mod(checksum + (ind-1) * dummy, 103);
      END LOOP;

      --Calcul du code ascii de la clef de controle
    IF (checksum < 95)
      THEN
        checksum := checksum + 32;
    ELSE
      checksum := checksum + 100;
    END IF;

    --Ajout de la clef et du STOP a la fin de la chaine codee
    code128 := code128 || CHR(checksum) || CHR(211);


RETURN code128;
END;

  $$;


ALTER FUNCTION public.fct_getbarcode(snumber text) OWNER TO cuon_admin;

--
-- Name: fct_getbarcodeforarticleid(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getbarcodeforarticleid(iid integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
	
	b1 text; 
    	sSql text;


    BEGIN

	sSql := 'select barcode from articles_barcode where article_id = ' || iID ;
	execute sSql into b1 ;
	if b1 is null then
	   b1 = '';
	end if ;

	 
    RETURN b1 ;
END;

  $$;


ALTER FUNCTION public.fct_getbarcodeforarticleid(iid integer) OWNER TO cuon_admin;

--
-- Name: fct_getbarcodeint(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getbarcodeint(iartikel integer) RETURNS text
    LANGUAGE plpgsql
    AS $$    
    DECLARE

	code128 text; 
    	chaine text;
    BEGIN
	code128 := '';
	chaine := to_char(iArtikel,'00000000000') ;

  	code128 =  fct_getBarcode(chaine);

    RETURN code128;
END;

  $$;


ALTER FUNCTION public.fct_getbarcodeint(iartikel integer) OWNER TO cuon_admin;

--
-- Name: fct_getbarcodetext(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getbarcodetext(sartikel text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
	
	code128 text; 
    	chaine text;
    BEGIN
	code128 := '';
	chaine = sArtikel ;
  	code128 =  fct_getBarcode(chaine);

    RETURN code128;
END;

  $$;


ALTER FUNCTION public.fct_getbarcodetext(sartikel text) OWNER TO cuon_admin;

--
-- Name: fct_getcdl(text[], text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getcdl(searchfields text[], stype text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE  
    rData record ;
    sSql text ;
    total_sum float ;
    totalPlus_sum float;
    totalMinus_sum float;
    book_uuid char(36);
    local_special_id int;
    dBegin date ;
    dEnd date ;

    SignFrom text;
    SignTo text ;

    NrFrom int ;
    NrTo int ;

    IdFrom int;
    IdTo int;
    
    
    BEGIN
	-- searchfields 
	-- 0, 1 = cash_date
	-- 2, 3 = cash sign
	-- 4, 5 = cash nr
	-- 6, 7 = id	

	-- sType :
	-- AllValues, AllDaily, AllUser, UserDaily

	total_sum := 0.0 ;
	totalPlus_sum  := 0.0 ;	  
	totalMinus_sum := 0.0 ;
	local_special_id := 0;
	raise notice ' searchfields = % % ',searchfields[1],searchfields[2] ;
    
 	select into dBegin fct_to_date (searchfields[1]) ;
	select into dEnd  fct_to_date(searchfields[2]);

    raise notice ' searchfields = % % ',searchfields[3],searchfields[4] ;

	select into SignFrom searchfields[3] ;
	select into SignTo  searchfields[4];

    raise notice ' searchfields = % % ',searchfields[5],searchfields[6] ;

    if char_length(searchfields[5]) > 0 and char_length(searchfields[6])  > 0 and searchfields[5] != 'NONE' and searchfields[6] != 'NONE'then 
    	select into NrFrom searchfields[5] ;
	    select into NrTo  searchfields[6];
    else
        NrFrom := 0;
        NrTo := 0;
    end if;

	raise notice ' searchfields = % % ',searchfields[7],searchfields[8] ;	

    if char_length(searchfields[7]) > 0 and char_length(searchfields[8])  > 0 and searchfields[7] != 'NONE' and searchfields[8] != 'NONE'then 

    	select into IdFrom searchfields[7] ;
	    select into IdTo  searchfields[8];
    else
        IdFrom = 0;
        IdTo = 0;
    end if ;
	

		sSql = ' select id, address_id, cash_desk_number,  order_id, cash_time,cast (cash_desk_user_short_cut as text), 

	case order_sum
	     when 0 then incomming_total  
	     else order_sum
	     end  as incomming_sum ,	     

	incomming_total, cash_date, cash_procedure , description, 
	case 
	     when address_id > 0 then (select lastname || '',  '' || city from address where id = address_id)
	     else '' '' 
	end as address ,

	0.0::float as r1, 0.0::float as r2, 0.0::float as r3, 0.0::float as r4, 1::int as special_id

	from cash_desk where cash_date between ' || quote_literal(dBegin) || ' and ' || quote_literal(dEnd) ;
	
	if ( char_length(SignFrom) > 0 AND char_length(SignTo) > 0 and SignFrom != 'NONE' and SignTo != 'NONE' ) then

	   sSql = sSql || ' AND cash_desk_user_short_cut between ' || quote_literal(SignFrom) || ' AND ' || quote_literal(SignTo) ;
	end if ;
	

	-- sType :
	-- AllValues, AllDaily, AllUser, UserDaily

 
	if sType = 'AllValues' then 
	      sSql = sSql || ' order by cash_date,  cash_time ' ;
	      book_uuid = fct_new_uuid();
	      insert into cash_desk_book_number (id, uuid,r_total_sum,r_total_plus_sum,r_total_minus_sum)values(nextval('cash_desk_book_number_id'),book_uuid,  total_sum, totalPlus_sum, totalMinus_sum);
	      select into local_special_id special_id from  cash_desk_book_number where uuid = book_uuid ;
	      
		
	elseif sType =  'AllDaily'  then
	     sSql = sSql || ' order by cash_date,  cash_time  ' ;

	elseif sType =  'AllUser'  then
	     sSql = sSql || ' order by  cash_desk_user_short_cut, cash_date,  cash_time ';

	elseif sType =  'UserDaily'  then
	     sSql = sSql  || ' order by  cash_desk_user_short_cut, cash_date,  cash_time ';

	end if ;

	 

	raise notice ' sSQl = % ', sSql ; 

	FOR rData in execute(sSql)  LOOP
           total_sum = total_sum + rData.incomming_sum ;
	   if rData.incomming_sum > 0.00 then 
	      totalPlus_sum = 	totalPlus_sum +  rData.incomming_sum ;
	   else 
	       totalMinus_sum = 	totalMinus_sum +  rData.incomming_sum ;
	   end if ;


	   if sType = 'AllValues' then 
	    
           elseif sType =  'AllDaily'  then


	   elseif sType =  'AllDaily'  then


	   elseif sType =  'AllDaily'  then


	   end if ;


	    -- raise notice 'sums %, % ',  total_sum , rData.incomming_sum ;
	     rData.r1 = total_sum ;
	     rData.r2 = totalPlus_sum ;
	     rData.r3 =  totalMinus_sum ;
	     if  local_special_id is null then
	     	    rData.special_id = 1;
	    else
		    
		    rData.special_id = local_special_id ;
	    end if ;
	     
	     return next rData ;
            
        END LOOP ;


	 

    END ;
    
     $$;


ALTER FUNCTION public.fct_getcdl(searchfields text[], stype text) OWNER TO cuon_admin;

--
-- Name: fct_getcdlminus(text[], text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getcdlminus(searchfields text[], stype text) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE  
    rData record ;
    sSql text ;
    total_sum float ;
    dBegin date ;
    dEnd date ;

    SignFrom text;
    SignTo text ;

    NrFrom int ;
    NrTo int ;

    IdFrom int;
    IdTo int;
    
    BEGIN
	-- searchfields 
	-- 0, 1 = cash_date
	

	-- sType :
	-- AllValues, AllDaily, AllUser, UserDaily

	total_sum := 0.0 ;
	raise notice ' searchfields = % % ',searchfields[1],searchfields[2] ;
	select into dBegin fct_to_date (searchfields[1]) ;
	select into dEnd  fct_to_date(searchfields[2]);

	select into SignFrom searchfields[3] ;
	select into SignTo  searchfields[4];

    raise notice ' searchfields = % % ',searchfields[5],searchfields[6] ;

    if char_length(searchfields[5]) > 0 and char_length(searchfields[6])  > 0 and searchfields[5] != 'NONE' and searchfields[6] != 'NONE' then 
    	select into NrFrom searchfields[5] ;
	    select into NrTo  searchfields[6];
    else
        NrFrom := 0;
        NrTo := 0;
    end if;

	raise notice ' searchfields = % % ',searchfields[7],searchfields[8] ;	

    if char_length(searchfields[7]) > 0 and char_length(searchfields[8])  > 0 and searchfields[7] != 'NONE' and searchfields[8] != 'NONE'then 

    	select into IdFrom searchfields[7] ;
	    select into IdTo  searchfields[8];
    else
        IdFrom = 0;
        IdTo = 0;
    end if ;



	sSql = ' select id, address_id, cash_desk_number,  order_id, cash_time,cast (cash_desk_user_short_cut as text), 

	case order_sum
	     when 0 then incomming_total  
	     else order_sum
	     end  as incomming_sum ,	     

	incomming_total, cash_date, cash_procedure , description, 
	case 
	     when address_id > 0 then (select lastname || '',  '' || city from address where id = address_id)
	     else '' '' 
	end as address ,

	0.0::float as r1, 0.0::float as r2, 0.0::float as r3, 0.0::float as r4 

	from cash_desk where cash_date between ' || quote_literal(dBegin) || ' and ' || quote_literal(dEnd) ||  ' and (order_sum < 0.00 or incomming_total < 0.00 ) ' ;
	if ( char_length(SignFrom) > 0 AND char_length(SignTo) > 0 and SignFrom != 'NONE' and SignTo != 'NONE' ) then

	   sSql = sSql || ' AND cash_desk_user_short_cut between ' || quote_literal(SignFrom) || ' AND ' || quote_literal(SignTo) ;
	end if ;


 
	if sType = 'AllValues' then 
	      sSql = sSql || ' order by cash_date,  cash_time ' ;
	elseif sType =  'AllDaily'  then
	     sSql = sSql || ' order by cash_date,  cash_time  ' ;

	elseif sType =  'AllDaily'  then
	     sSql = sSql || ' order by  cash_desk_user_short_cut, cash_date,  cash_time ';

	elseif sType =  'AllDaily'  then
	     sSql = sSql  || ' order by  cash_desk_user_short_cut, cash_date,  cash_time ';

	end if ;

	 

	raise notice ' sSQl = % ', sSql ; 

	FOR rData in execute(sSql)  LOOP
           total_sum = total_sum + rData.incomming_sum ;

	   if sType = 'AllValues' then 
	    
           elseif sType =  'AllDaily'  then


	   elseif sType =  'AllDaily'  then


	   elseif sType =  'AllDaily'  then


	   end if ;


	     raise notice 'sums %, % ',  total_sum , rData.incomming_sum ;
	     rData.r1 = total_sum ;
	     return next rData ;
            
        END LOOP ;


	 

    END ;
    
     $$;


ALTER FUNCTION public.fct_getcdlminus(searchfields text[], stype text) OWNER TO cuon_admin;

--
-- Name: fct_getcdlminusview(text[], text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getcdlminusview(searchfields text[], stype text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE  
    rData record ;
    sSql text ;
    total_sum float ;
    dBegin date ;
    dEnd date ;
    iOk int;
    

    BEGIN
    iOk = 1;


    sSql = 'create or replace view view_' || current_user || '_cdl_minus AS select * from fct_getCDLMinus(array [' || quote_literal(searchfields[1]) || ', ' || quote_literal(searchfields[2]) || ', ' ||  quote_literal(searchfields[3]) || ', ' || quote_literal(searchfields[4]) || ', ' ||  quote_literal(searchfields[5]) || ', ' || quote_literal(searchfields[6]) || ', ' ||  quote_literal(searchfields[7]) || ', ' || quote_literal(searchfields[8]) || '], ' || quote_literal(sType) || ')   as( id integer, address_id integer,  cash_desk_number integer,  order_id integer, cash_time time, cash_desk_user_short_cut text, outgoing_sum float,  outgoing_total float, cash_date date , cash_procedure integer  , description varchar(255),address text, sum1 float, sum2 float, sum3 float, sum4 float )' ;
    


    raise notice 'sSql view = %',sSql ;     
    execute sSql ;

    return iOk;

    END ;
    
     $$;


ALTER FUNCTION public.fct_getcdlminusview(searchfields text[], stype text) OWNER TO cuon_admin;

--
-- Name: fct_getcdlsum(text[]); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getcdlsum(searchfields text[]) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE  
    rData record ;
    rRow record ;
    sSql text ;
    total_sum float ;
    total_end_sum float ;
    curs1 refcursor;   
    dBegin date ;
    dEnd date;
    
    SignFrom text;
    SignTo text ;

    NrFrom int ;
    NrTo int ;

    IdFrom int;
    IdTo int;
    
     totalPlus_sum float;	
    totalMinus_sum float;
    BEGIN
	-- searchfields 
	-- 0, 1 = cash_date
	

	-- sType :
	-- AllValues, AllDaily, AllUser, UserDaily

	total_sum := 0.0 ;
	total_end_sum = 0.0;
	raise notice ' searchfields = % % ',searchfields[1],searchfields[2] ;
	select into dBegin fct_to_date (searchfields[1]) ;
	select into dEnd  fct_to_date(searchfields[2]);


	select into SignFrom searchfields[3] ;
	select into SignTo  searchfields[4];

    raise notice ' searchfields = % % ',searchfields[5],searchfields[6] ;

    if char_length(searchfields[5]) > 0 and char_length(searchfields[6])  > 0 and searchfields[5] != 'NONE' and searchfields[6] != 'NONE' then 
    	select into NrFrom searchfields[5] ;
	    select into NrTo  searchfields[6];
    else
        NrFrom := 0;
        NrTo := 0;
    end if;

	raise notice ' searchfields = % % ',searchfields[7],searchfields[8] ;	

    if char_length(searchfields[7]) > 0 and char_length(searchfields[8])  > 0 and searchfields[7] != 'NONE' and searchfields[8] != 'NONE' then 

    	select into IdFrom searchfields[7] ;
	    select into IdTo  searchfields[8];
    else
        IdFrom = 0;
        IdTo = 0;
    end if ;


	raise notice ' begin and end = % % ',dBegin,dEnd ;

	sSql := 'select cash_desk_number, 
	     case order_sum
		     when 0 then incomming_total  
	     	     else order_sum
		     end  as incomming_sum, 

        0.0::float as sum_end ,cash_date as cd_begin , cash_date as cd_end, cash_date,    
	    0.0::float as r1,  0.0::float as r2,  0.0::float as r3,  0.0::float as r4,  0.0::float as r5 from cash_desk where cash_date <= ' || quote_literal( dEnd)  || ' and  cash_desk_number = 0 ';

	if ( char_length(SignFrom) > 0 AND char_length(SignTo) > 0 and SignFrom != 'NONE' and SignTo != 'NONE' ) then

	   sSql = sSql || ' AND cash_desk_user_short_cut between ' || quote_literal(SignFrom) || ' AND ' || quote_literal(SignTo) ;
	end if ;


	sSql = sSql || ' order by cash_date,  cash_time ' ;

	-- OPEN curs1 FOR EXECUTE sCursor 	;

	-- fetch curs1 INTO rData ;      
	raise notice 'cursor = %', sSql ;

	    
	-- execute format (sSql) ;
	--  open curs1 ;
	OPEN curs1 FOR EXECUTE sSql ;
	fetch curs1 INTO rRow ;  
	rData = rRow ;
 	WHILE FOUND = true LOOP	 
   	    if rData.incomming_sum is null then
	        rData.incomming_sum = 0.00 ;
            end if ;
		
	   if rData.incomming_sum > 0.00 then 
	      totalPlus_sum = 	totalPlus_sum +  rData.incomming_sum ;
	   else 
	       totalMinus_sum = 	totalMinus_sum +  rData.incomming_sum ;
	   end if ;

	    if rRow.cash_date < dBegin then 
	    	    total_sum =   total_sum + rRow.incomming_sum ;
	    end if;
            total_end_sum =   total_end_sum + rRow.incomming_sum ;

            fetch curs1 INTO rRow ; 	      

	


	END LOOP;
	raise notice 'total sum = %, end_sum = %',total_sum, total_end_sum ;

	if total_sum is null then
	   total_sum = 0.00 ;
	end if ;
	   if total_end_sum is null then
	   total_end_sum = 0.00 ;
	end if ;
	raise notice 'total sum = %, end_sum = %',total_sum, total_end_sum ;	
	rData.incomming_sum = total_sum ;
	rData.sum_end = total_end_sum ;
	rData.r1 = totalPlus_sum ;
	rData.r2 = totalMinus_sum;
	

	
	return next rData ;
            
        


	 

    END ;
    
     $$;


ALTER FUNCTION public.fct_getcdlsum(searchfields text[]) OWNER TO cuon_admin;

--
-- Name: fct_getcdlsumview(text[]); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getcdlsumview(searchfields text[]) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE  
    rData record ;
    rRow record ;
    sSql text ;
    iOk int ;
    BEGIN
	iOk = 1 ;
	sSql := 'create or replace view view_' || current_user || '_cdl_sum AS select * from  fct_getCDLSum(array [' || quote_literal(searchfields[1]) || ', ' || quote_literal(searchfields[2]) || ']) as( cash_desk_number integer, sum_begin float,  sum_end float, cash_date_begin date , cash_date_end date, cash_date date, r1 float, r2 float, r3 float, r4 float, r5 float  ) ' ;
	raise notice 'sSql view = %',sSql ;     
	execute sSql ;


	 

	return iOk;
    END ;
    
     $$;


ALTER FUNCTION public.fct_getcdlsumview(searchfields text[]) OWNER TO cuon_admin;

--
-- Name: fct_getcdlview(text[], text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getcdlview(searchfields text[], stype text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE  
    rData record ;
    sSql text ;
    iOk int ;

    BEGIN
    iOk := 1;

    sSql = 'create or replace view view_' || current_user || '_cdl AS select * from fct_getCDL(array [' || quote_literal(searchfields[1]) || ', ' || quote_literal(searchfields[2]) || ', ' ||  quote_literal(searchfields[3]) || ', ' || quote_literal(searchfields[4]) || ', ' ||  quote_literal(searchfields[5]) || ', ' || quote_literal(searchfields[6]) || ', ' ||  quote_literal(searchfields[7]) || ', ' || quote_literal(searchfields[8]) || '], ' || quote_literal(sType) || ') as( id integer, address_id integer,  cash_desk_number integer,  order_id integer, cash_time time, cash_desk_user_short_cut text , incomming_sum float,  incomming_total float, cash_date date , cash_procedure integer  , description varchar(255),address text, sum1 float, sum2 float, sum3 float, sum4 float )   ' ;

    raise notice 'sSql view = %',sSql ;     
    execute sSql ;

    return iOK ;
    
    END ;
    
     $$;


ALTER FUNCTION public.fct_getcdlview(searchfields text[], stype text) OWNER TO cuon_admin;

--
-- Name: fct_getdays(date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getdays(ddate date) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
      days int := 0 ;
        
    BEGIN
        select into days date_part('days',now() - dDate)  ;
        
        if days is not null then 
            RETURN days ;
        else 
            RETURN 0 ;
        END IF ;
    END;
     
     $$;


ALTER FUNCTION public.fct_getdays(ddate date) OWNER TO cuon_admin;

--
-- Name: fct_getfirstdayofmonth(date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getfirstdayofmonth(ddate date) RETURNS date
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
        ret_val date ;
        firstDay date ;
        sDay date ;
        sSql text ;
    BEGIN
        sDay := ' 01-' || extract(month from dDate) ||'-' || extract(year from dDate)  ;
        sSql := 'SELECT date( ' || quote_literal(sDay) ||  ') ' ; 
        execute sSql into ret_val ;
        RETURN ret_val; 
   
    END;
     
     $$;


ALTER FUNCTION public.fct_getfirstdayofmonth(ddate date) OWNER TO cuon_admin;

--
-- Name: fct_getfirstdayofmonth(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getfirstdayofmonth(imonth integer, iyear integer) RETURNS date
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
        firstDay date ;
      
    BEGIN
        firstDay := date('01-' || iMonth || '-' || iYear ) ;
        RETURN firstDay ; 
    END; 
        $$;


ALTER FUNCTION public.fct_getfirstdayofmonth(imonth integer, iyear integer) OWNER TO cuon_admin;

--
-- Name: fct_getget_number(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getget_number(orderid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    iData int ;
    sSql text ;
    r2 record ;
    
    BEGIN
       iData := 0 ;
       sSql := 'select number as get_number from orderget where orderid = ' || OrderID || ' ' ||  fct_getWhere(2,' ') ;
       
       FOR r2 in execute(sSql)  LOOP
            
            if r2.get_number is not null then 
                iData := r2.get_number ;
            else
                iData := 0 ;
            END IF ;
            
             
        END LOOP ;
     
            
     return iData ; 
       
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getget_number(orderid integer) OWNER TO cuon_admin;

--
-- Name: fct_getgraveplantlistarticles(integer, text, text, text, text, text, text, text, text, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getgraveplantlistarticles(graveyard_id integer, grave_lastname_from text, grave_lastname_to text, esequentialnumberfrom text, esequentialnumberto text, dcontractbeginfrom text, dcontractbeginto text, dcontractendsfrom text, dcontractendsto text, contract integer, service integer, plantation integer, price integer, timetab integer, irows integer, iordersort integer) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE
        iClient int ;
        r record;
        searchsql text := '';
         additionalTables text := ' ' ;
        additionalWhere text  := ' ' ;
        additionalRows text  := ' ' ;
        GraveServiceNotesService int  := 40100  ;
        GraveServiceNotesSpring int  := 40101 ;
        GraveServiceNotesSummer int  := 40102 ;
        GraveServiceNotesAutumn int  := 40103 ;
        GraveServiceNotesWinter int  := 40104 ;
        GraveServiceNotesAnnual int  := 40105 ;
        GraveServiceNotesUnique int  := 40106 ;
        GraveServiceNotesHolliday int  := 40107 ;
         iGraveServiceID  int := 0 ;
         iRecordID int ;
        
    BEGIN
        IF timetab = 0 then 
            additionalTables  :=',grave_work_maintenance as gm, articles as ar  ' ;
             iGraveServiceID := GraveServiceNotesService ;
        ELSEIF timetab = 1 then 
            additionalTables  :=',grave_work_spring as gm, articles as ar  ' ;
              iGraveServiceID := GraveServiceNotesSpring ;
        ELSEIF timetab = 2 then 
            additionalTables  :=',grave_work_summer as gm, articles as ar  ' ;
              iGraveServiceID := GraveServiceNotesSummer ;
        ELSEIF timetab = 3 then 
            additionalTables  :=',grave_work_autumn as gm, articles as ar  ' ;
              iGraveServiceID := GraveServiceNotesAutumn ;
        ELSEIF timetab = 4 then 
            additionalTables  :=',grave_work_holiday as gm, articles as ar  ' ;
              iGraveServiceID := GraveServiceNotesHolliday ;
                       
        ELSEIF timetab = 5 then 
            additionalTables  :=',grave_work_winter as gm, articles as ar  ' ;
              iGraveServiceID := GraveServiceNotesWinter ;
        ELSEIF timetab = 6 then 
            additionalTables  :=',grave_work_year as gm, articles as ar  ' ;
             iGraveServiceID := GraveServiceNotesAnnual;       
        ELSEIF timetab = 7 then 
            additionalTables  :=',grave_work_single as gm, articles as ar  ' ;
              iGraveServiceID := GraveServiceNotesUnique ;
               
        END IF ;    
         
               
     
            
        additionalWhere  := ' and gm.grave_id = grave.id and  gm.article_id = ar.id ' || ' and gm.status != ''delete'' '    ;
        
        IF service > -1 THEN
            -- show the service graves
                IF timetab = 0 then 
                        additionalWhere := additionalWhere  || ' and gm.grave_service_id = ' || service ;
                ELSE 
                        additionalWhere := additionalWhere  || ' and gm.period_id = ' || service ; 
                END IF ;
        END IF ;
        
        additionalRows  := ', gm.article_id as service_article_id , ar.number as article_number, ar.designation as article_designation,gm.service_price as service_price, gm.service_count as service_count, gm.service_notes as article_notes,  (select fct_loadGraveServiceNote(gm.grave_id, '  ||  iGraveServiceID  || ' ) as service_notes ) , grave.common_notes as grave_notes ' ;
        
        searchsql := fct_getGravePlantListSQL(graveyard_id , grave_lastname_from , grave_lastname_to , eSequentialNumberFrom , eSequentialNumberTo , dContractBeginFrom , dContractBeginTo , dContractEndsFrom , dContractEndsTo ,contract,service, plantation, price,  iRows,additionalRows, additionalTables, additionalWhere)  
        ;
        -- searchsql := searchsql || ' ' ;
        
        
       
        searchsql := searchsql  ||  fct_getWhere(2,'graveyard.') || ' order by graveyard.id, grave.pos_number';
        raise notice 'SQL = %',searchsql ;
        
        for r in execute(searchsql)  LOOP 
            raise notice 'Article_number = %',r.article_designation ;
            return  next r; 
        END LOOP ;
        
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getgraveplantlistarticles(graveyard_id integer, grave_lastname_from text, grave_lastname_to text, esequentialnumberfrom text, esequentialnumberto text, dcontractbeginfrom text, dcontractbeginto text, dcontractendsfrom text, dcontractendsto text, contract integer, service integer, plantation integer, price integer, timetab integer, irows integer, iordersort integer) OWNER TO cuon_admin;

--
-- Name: fct_getgraveplantlistsql(integer, text, text, text, text, text, text, text, text, integer, integer, integer, integer, integer, text, text, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getgraveplantlistsql(graveyard_id integer, grave_lastname_from text, grave_lastname_to text, esequentialnumberfrom text, esequentialnumberto text, dcontractbeginfrom text, dcontractbeginto text, dcontractendsfrom text, dcontractendsto text, contract integer, service integer, plantation integer, price integer, irows integer, additionalrows text, additionaltables text, additionalwhere text) RETURNS text
    LANGUAGE plpgsql
    AS $$   
 DECLARE
        iClient int ;
       
        searchsql text := '';
   
        iIndex integer ;
        iIndex2 integer  ;
        newGravename text ;
        sSub1 text ;
        sSub2 text ;
        
    BEGIN
    -- graveyard.lastname 
        -- grab laufnummer
        
        
        searchsql := 'select graveyard.id as graveyard_id, grave.id as grave_id, graveyard.shortname , graveyard.designation , grave.firstname , grave.lastname, grave.pos_number, grave. contract_begins_at , grave.contract_ends_at , grave.detachment , grave.grave_number ' ;
         searchsql :=  searchsql  || additionalRows ;
        
        searchsql :=  searchsql  || 'from  graveyard, grave, address '  || additionalTables || ' where '  ;
        
        searchsql :=  searchsql  ||  ' grave.addressid = address.id and grave.graveyardid = graveyard.id and graveyard.id = ' || graveyard_id || ' '  ;
        searchsql := searchsql || ' and grave.status != ''delete'' ' ;
        searchsql :=  searchsql  || additionalWhere || ' ' ;
        
        IF graveyard_id > 0 THEN
        
            searchsql := searchsql  || ' and graveyard.id = ' || graveyard_id || ' ' ;
        END IF ;
            
        IF char_length(grave_lastname_from) > 0 AND char_length(grave_lastname_to) > 0 THEN 
            
            searchsql := searchsql  || ' and grave.lastname between ' || quote_literal(grave_lastname_from) || ' and ' || quote_literal(grave_lastname_to)   || ' ' ;
        
        END IF ;
         IF char_length(grave_lastname_from) > 0 AND char_length(grave_lastname_to) = 0 THEN 
         
            newGravename := grave_lastname_from ;
            iIndex := position('#!#' in newGravename) ;
            
            if iIndex > -1 then
                newGravename := overlay(newGravename placing '' from iIndex for 3) ;
                
            end if ;
            sSub1 := substring(newGravename from 0 for iIndex) ;
            
            iIndex2 := position('#!#' in newGravename) ;
            
            if iIndex2 > -1 then
                newGravename := overlay(newGravename placing '' from iIndex2 for 3) ;
                
            end if ;
            sSub2 := substring(newGravename from iIndex ) ;
            newGravename := sSub1 || quote_literal(sSub2) ;
            searchsql := searchsql  || ' and grave.lastname ' || newGravename || ' ' ;
        END IF ;
        
        IF char_length(eSequentialNumberFrom) > 0 AND char_length(eSequentialNumberTo) > 0 THEN 
           
            searchsql := searchsql  || ' and pos_number between ' || eSequentialNumberFrom || ' and ' || eSequentialNumberTo || ' ' ;
        END IF ;
        if price > -1 then 
            searchsql := searchsql  || ' and type_of_paid = ' || price  || ' ' ;
        end if ;
        
        IF char_length(dContractBeginFrom) > 0 AND char_length(dContractBeginTo) > 0 THEN 
            searchsql := searchsql  || ' and contract_begins_at between fct_to_date(' || quote_literal(dContractBeginFrom) || ') and fct_to_date(' || quote_literal(dContractBeginTo) ||') ' ;
        END IF ;
        
        IF char_length(dContractEndsFrom) > 0 AND char_length(dContractEndsTo) > 0 THEN 
            searchsql := searchsql  || ' and contract_ends_at between fct_to_date(' || quote_literal(dContractEndsFrom) || ') and fct_to_date(' || quote_literal(dContractEndsTo) ||') ' ;
        END IF ;
        

        IF contract = 1 THEN
            -- show only current running contracts
            
            searchsql := searchsql  || ' and (contract_ends_at  is  NULL or contract_ends_at > now() or contract_ends_at = ' || quote_literal('1900-01-01')  || ') ' ;
        END IF ;
        

        raise notice ' sql = %', searchsql ;
        
        
        return searchsql ;
         
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getgraveplantlistsql(graveyard_id integer, grave_lastname_from text, grave_lastname_to text, esequentialnumberfrom text, esequentialnumberto text, dcontractbeginfrom text, dcontractbeginto text, dcontractendsfrom text, dcontractendsto text, contract integer, service integer, plantation integer, price integer, irows integer, additionalrows text, additionaltables text, additionalwhere text) OWNER TO cuon_admin;

--
-- Name: fct_getgraveplantlistvalues(integer, text, text, text, text, text, text, text, text, integer, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getgraveplantlistvalues(graveyard_id integer, grave_lastname_from text, grave_lastname_to text, esequentialnumberfrom text, esequentialnumberto text, dcontractbeginfrom text, dcontractbeginto text, dcontractendsfrom text, dcontractendsto text, contract integer, service integer, plantation integer, price integer, timetab integer, irows integer, iordersort integer) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE
        iClient int ;
        r record;
        searchsql text := '';
        additionalTables text := ' ' ;
        additionalWhere text  := ' ' ;
         additionalRows text  := ' ' ;
    
    
    BEGIN
        searchsql := fct_getGravePlantListSQL(graveyard_id , grave_lastname_from , grave_lastname_to , eSequentialNumberFrom , eSequentialNumberTo , dContractBeginFrom , dContractBeginTo , dContractEndsFrom , dContractEndsTo ,contract,service, plantation, price, iRows,additionalRows,  additionalTables, additionalWhere) ;
        searchsql := searchsql  ||  fct_getWhere(2,'graveyard.') || ' order by  graveyard.shortname, grave.pos_number, grave.lastname, grave.firstname ';
         FOR r in execute(searchsql)  LOOP
         
        
            return next r ;

        END LOOP ;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getgraveplantlistvalues(graveyard_id integer, grave_lastname_from text, grave_lastname_to text, esequentialnumberfrom text, esequentialnumberto text, dcontractbeginfrom text, dcontractbeginto text, dcontractendsfrom text, dcontractendsto text, contract integer, service integer, plantation integer, price integer, timetab integer, irows integer, iordersort integer) OWNER TO cuon_admin;

--
-- Name: fct_getgravesforaddressid(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getgravesforaddressid(iaddressid integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
 DECLARE
     iClient int ;
    r record;
    searchsql text := '';

    BEGIN
       
         searchsql := 'select graveyard.shortname as graveyard ,grave.lastname as lastname ,grave.firstname as firstname, grave.id as gravevalue from graveyard, grave  where grave.addressid = ' || iAddressID ||  ' and graveyard.id = grave.graveyardid '  || fct_getWhere(2,'graveyard.') ;

       
        FOR r in execute(searchsql)  LOOP
         
        
         return next r.graveyard || ', ' || r.lastname || ', ' || r.firstname || '   ###' || r.gravevalue ;

        END LOOP ;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getgravesforaddressid(iaddressid integer) OWNER TO cuon_admin;

--
-- Name: fct_gethibernationforaddressid(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_gethibernationforaddressid(iaddressid integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $$
 DECLARE
     iClient int ;
    r record;
    searchsql text := ' ';

    BEGIN



        searchsql := 'select hibernation_number , sequence_of_stock, hibernation.id as hib_id from hibernation where addressnumber = ' || iAddressID ||  fct_getWhere(2,'') ;
        FOR r in execute(searchsql)  LOOP


            return next r.hibernation_number || ', ' || r.sequence_of_stock || '  ###' || r.hib_id ;

        END LOOP ;

    END ;



     $$;


ALTER FUNCTION public.fct_gethibernationforaddressid(iaddressid integer) OWNER TO cuon_admin;

--
-- Name: fct_getinpayment(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getinpayment(iinvoice integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
    fSum float ;
    sum_inpayment float ;
    sum_discount float ;
    iOrder integer ;
    sSql text := '';
    r record ;
    sNumber varchar(20) := '99999999999' ;
    BEGIN
        sSql = 'select sum(in_payment.inpayment)   as sum_inpayment from in_payment where   to_number(in_payment.invoice_number,' || quote_literal(sNumber)  || ') = '  || iInvoice || ' ' ||  fct_getWhere(2,' ') ;  
        -- RAISE NOTICE ' get residue for Invoice sql = %', sSql ;
        execute sSql into  sum_inpayment ;
        

        if sum_inpayment is null then
            sum_inpayment := 0.00 ;
        end if ;
         sSql = 'select sum(in_payment.cash_discount)   as sum_discount from in_payment where   to_number(in_payment.invoice_number,' || quote_literal(sNumber)  || ') = '  || iInvoice || ' ' ||  fct_getWhere(2,' ') ;  
        execute sSql into  sum_discount ;
        
        if sum_discount is null then
            sum_discount := 0.00 ;
        end if ;
        fSum := sum_inpayment + sum_discount ;
        
        return fSum ;
  
    END ;
    $$;


ALTER FUNCTION public.fct_getinpayment(iinvoice integer) OWNER TO cuon_admin;

--
-- Name: fct_getinpaymentmonth(integer, date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getinpaymentmonth(iinvoice integer, lastday date) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
    fSum float ;
    sum_inpayment float ;
    sum_discount float ;
    iOrder integer ;
    sSql text := '';
    r record ;
    sNumber varchar(20) := '99999999999' ;
    BEGIN
        sSql = 'select sum(in_payment.inpayment)   as sum_inpayment,   sum(in_payment.cash_discount)   as sum_discount from in_payment, list_of_invoices where   to_number(in_payment.invoice_number,' || quote_literal(sNumber)  || ') = '  || iInvoice || ' and list_of_invoices.invoice_number  =' || iInvoice || ' and  date_of_invoice <= ' || quote_literal(lastDay)  || ' and date_of_paid <=   ' || quote_literal(lastDay)  ||  ' ' ||  fct_getWhere(2,'in_payment.') ;  
        RAISE NOTICE ' get residue for Invoice sql = %', sSql ;
       -- execute sSql into  sum_inpayment ;
        

        -- if sum_inpayment is null then
        --     sum_inpayment := 0.00 ;
        -- end if ;
        --  sSql = 'select sum(in_payment.cash_discount)   as sum_discount from in_payment, list_of_invoices where   to_number(in_payment.invoice_number,' || quote_literal(sNumber)  || ') = '  || iInvoice || ' and  date_of_invoice <= ' || quote_literal(lastDay) ||  ' and date_of_paid <=   ' || quote_literal(lastDay)  || ' ' ||   fct_getWhere(2,'in_payment. ') ;
	--  raise notice ' sql = %', sSql ;
	 
        execute sSql into  sum_inpayment, sum_discount ;
        
        if sum_discount is null then
            sum_discount := 0.00 ;
        end if ;
	if sum_inpayment is null then
            sum_inpayment := 0.00 ;
        end if ;
        fSum := sum_inpayment + sum_discount ;
        
        return fSum ;
  
    END ;
    $$;


ALTER FUNCTION public.fct_getinpaymentmonth(iinvoice integer, lastday date) OWNER TO cuon_admin;

--
-- Name: fct_getinvoicegross(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getinvoicegross() RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE
    r1 record ;
    BEGIN 
    
        for i in 4614 .. 4714 LOOP 
            select into r1 list_of_invoices.invoice_number,  list_of_invoices.order_number, sum(amount*price) from orderposition, list_of_invoices where list_of_invoices.invoice_number = i and orderposition.orderid = list_of_invoices.order_number group by list_of_invoices.invoice_number, list_of_invoices.order_number ;
             
            return next r1 ;
        END LOOP ;
        
     
     
          
    END ;
    
     $$;


ALTER FUNCTION public.fct_getinvoicegross() OWNER TO cuon_admin;

--
-- Name: fct_getlastdayofmonth(date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getlastdayofmonth(ddate date) RETURNS date
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
        ret_val date ;
        firstDay date ;
        
    BEGIN
        
        SELECT date(substr(text(dDate + interval '1 month'),1,7)||'-01')-1 into ret_val; 
        
        RETURN ret_val; 
   
    END;
     
     $$;


ALTER FUNCTION public.fct_getlastdayofmonth(ddate date) OWNER TO cuon_admin;

--
-- Name: fct_getlastdayofmonth(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getlastdayofmonth(imonth integer, iyear integer) RETURNS date
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
        ret_val date ;
        firstDay date ;
        
    BEGIN
        firstDay := date('01-' || iMonth || '-' || iYear ) ;
        SELECT date(substr(text(firstDay + interval '1 month'),1,7)||'-01')-1 into ret_val; 
        
        RETURN ret_val; 
   
    END;
     
     $$;


ALTER FUNCTION public.fct_getlastdayofmonth(imonth integer, iyear integer) OWNER TO cuon_admin;

--
-- Name: fct_getlastname(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getlastname(iaddressid2 integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    recAddress  record;
    sLastname text ;
    
   
    iAddressID int ;

    BEGIN

    iAddressID = iAddressID2 ;
	
    if iAddressID is NULL then 
       iAddressID = 0;
    end if ;

    sSql := 'select lastname from address 
        where id = ' || iAddressID  || ' ' || fct_getWhere(2,' ') ;
    for recAddress in execute sSql 
    LOOP 
        raise notice 'addresslastnmae = %',recAddress.lastname ;
        
        if recAddress.lastname is NULL then
            sLastname := 'NONE'  ;
        else 
            sLastname := recAddress.lastname;
        END IF ;
        
       
    END LOOP ;
    return sLastname ;
       END ;
    $$;


ALTER FUNCTION public.fct_getlastname(iaddressid2 integer) OWNER TO cuon_admin;

--
-- Name: fct_getlastyear(date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getlastyear(ddate date) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
      iYear int ;

    BEGIN
        iYear := extract(year from dDate)   ;
	raise notice ' year = %, year -1 = % ', iYear, iYear -1 ;
        return iYear -1 ;
    END; 
        $$;


ALTER FUNCTION public.fct_getlastyear(ddate date) OWNER TO cuon_admin;

--
-- Name: fct_getopeninvoice(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getopeninvoice(iordernumber integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
    fSum float ;
    iOrder integer ;
    sSql text := '';
 BEGIN
     sSql := 'select  sum(list_of_invoices.total_amount ) -  fct_getOrderTotalSum(' || iOrderNumber || ') as residue from list_of_invoices where list_of_invoices.order_number = ' || iOrderNumber || ' ' ||  fct_getWhere(2,' ') ;
    -- RAISE NOTICE ' get residue for Invoice sql = %', sSql ;
    
    execute sSql into  fSum ;
   
    return fSum ;
    END ;
    $$;


ALTER FUNCTION public.fct_getopeninvoice(iordernumber integer) OWNER TO cuon_admin;

--
-- Name: fct_getordergifts(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getordergifts() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
    r1 record ;
    r2 record ;
    addArticleIDs text;
    iClient int ;
    aMArticles text[] ;
    i int ;
    sSql text ;
    sSql2 text ;
    bDoit bool ;
    iOrderType integer ;


    BEGIN  
        select allow_direct_debit into bDoit from addresses_misc  where address_id = NEW.addressnumber  ;
        raise notice ' allow direct debit = % ', bDoit ;
        if bDoit is not null AND bDoit = true then 
            iClient = fct_getUserDataClient(  ) ;
            addArticleIDs = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'order_add_position_gift_bank') ;
            iOrderType = NEW.order_type;
	    if iOrderType IS NULL then 
	       iOrderType = 1 ;
	    END IF ;
 	    if iOrderType = 1 or iOrderType = 8 or iOrderType = 4  then 
                aMArticles =  string_to_array(addArticleIDs, ',') ; 
                if array_length(aMArticles, 1) >= 1 then
                FOR i IN 1..array_length(aMArticles, 1) LOOP
                
                    sSql := 'select id from orderposition where orderid = ' || NEW.id || ' and articleid = ' || aMArticles[i]  ||  fct_getWhere(2,' ') ;
                    execute(sSql) into r1 ;
                    
                    IF r1.id is not null then 
                        raise notice ' successfull %',r1.id ;
                    ELSE 
                        raise notice ' successfull %',r1.id ;
                        raise notice ' We need to insert a line ';
                        sSql2 := 'select * from articles where id = ' || aMArticles[i]  || fct_getWhere(2,' ') ;
                        raise notice ' sSql2 = %', sSql2 ;
                        
                        execute(sSql2) into r2 ;
                        raise notice ' r2.id = %', r2.id ;
                        if r2.id is not null then 
                            insert into orderposition (id, uuid, orderid, articleid, amount,position,price, designation) values (nextval('orderposition_id'),fct_new_uuid() ,NEW.id, aMArticles[i]::INTEGER,1,0,r2.sellingprice1,quote_literal(' ') );
                        end if ;
                        
                        
                    END IF ;
                END LOOP; 

	      end if ;		

            END IF ;
        END IF ;
        
        RETURN NEW ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_getordergifts() OWNER TO cuon_admin;

--
-- Name: fct_getordertotalnetsum(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getordertotalnetsum(iorderid integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
DECLARE
    fSum     float ;

 BEGIN
    select fct_getOrderTotalSum(iOrderid,'t') into fsum ;
    return fSum ;
    END ;
    $$;


ALTER FUNCTION public.fct_getordertotalnetsum(iorderid integer) OWNER TO cuon_admin;

--
-- Name: fct_getordertotalsum(integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getordertotalsum(iorderid integer, stype text) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
    fSum     float ;
    sClient char ;
    cur1 refcursor ;
    sCursor text ;
    sSql text ; 
    fAmount     float;
    fPrice  float;
    fDiscount   float ;
    count   integer ;
    fTaxVat float ;
    iArticleID integer ;
    bNet  bool ;
    sDatabase return_text3;
    sDB text ;
    sDBPositions text ;
    vMisc return_misc3 ;
     
     
    BEGIN
    sDatabase = fct_get_database(sType);
    sDB = sDatabase.a ;
    sDBPositions = sDatabase.b ;
   
    
            
            
            
            
        sCursor := 'SELECT amount, price, discount, articleid, tax_vat FROM ' || sDBPositions || ' WHERE  orderid = '|| iOrderid || ' ' || fct_getWhere(2,' ')  ;
        fSum := 0.0 ;
        -- RAISE NOTICE 'sCursor = %', sCursor ;
        OPEN cur1 FOR EXECUTE sCursor ;
        FETCH cur1 INTO fAmount, fPrice, fDiscount,iArticleID, fTaxVat ;

        count := 0;

        WHILE FOUND LOOP
            RAISE NOTICE 'total sum for position , amount %, price % , discount %', fAmount,fPrice,fDiscount ;
            if fDiscount IS NULL then
                fDiscount := 0.0 ;
            end if ;
            if fTaxVat IS NULL then 
                fTaxVat:= 0.00;
            END IF;
            if fPrice IS NULL then
                fPrice := 0.0 ;
            end if ;
            
            vMisc := fct_get_taxvat_for_article(iArticleID);
            fTaxVat = vMisc.b ;
            
            -- now search for brutto/netto
            bNet := fct_get_net_for_article(iArticleID);
            raise notice ' order bNet value is %',bNet ;
            if fTaxVat > 0 THEN
                if bNet = true then 
                -- raise notice ' order calc as bnet is true';
                    fSum := fSum + ( fAmount * ( fPrice + (fPrice *fTaxVat/100) ) * (100 - fDiscount)/100 ) ;
                else 
                    fSum := fSum + ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                end if ;
                
            ELSE    
                fSum := fSum + ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                
            END IF ;
            FETCH cur1 INTO fAmount, fPrice, fDiscount ,iArticleID, fTaxVat;
    END LOOP ;
    close cur1 ;
    
    -- now get the whole discount 
    sSql := 'select discount from ' || sDB || ' where id = ' || iOrderid || ' ' || fct_getWhere(2,' ');
    execute sSql into fDiscount ;
    if fDiscount IS NULL then
            fDiscount := 0.0 ;
        end if ;
    fSum := fSum * ((100 - fDiscount)/100 )  ;
    
    return fSum ;
    END ;
    $$;


ALTER FUNCTION public.fct_getordertotalsum(iorderid integer, stype text) OWNER TO cuon_admin;

--
-- Name: fct_getpositionnetprice(integer, integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getpositionnetprice(iorderid integer, ipositionid integer, stype text) RETURNS record
    LANGUAGE plpgsql
    AS $$
    DECLARE
    r1 record ;
    addArticleIDs text;
    iClient int ;
    aMArticles text[] ;
    i int ;
    sSql text ;
    sSql2 text ;
    bDoit bool ;
    
   
    sDatabase return_text3;
    sDB text ;
    sDBPositions text ;
     sDBInvoice text ;
    BEGIN
    
    sDatabase = fct_get_database(sType);
    sDB = sDatabase.a ;
    sDBPositions = sDatabase.b ;
    sDBInvoice = sDatabase.c ;
    
 sSql2 := 'select ( select tax_vat_for_all_positions from ' || sDBInvoice || ' where ' || sDBInvoice || '.orderid  = ' || iOrderID || ' ) as   tax_vat_for_all_positions ,   
                    ' || sDBPositions || '.amount as amount,  ' || sDBPositions || '.position as position, ' || sDBPositions || '.price as price,  ' 
                    || sDBPositions || '.discount as discount, ' || sDBPositions || '.tax_vat as position_tax_vat, (select  material_group.tax_vat from material_group,articles where  articles.material_group = material_group.id and articles.id = ' || sDBPositions || '.articleid) as m_group_taxvat, 
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  articles.id = ' || sDBPositions || '.articleid)
                        when true then price when false then price / (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = ' || sDBPositions || '.articleid)) * 100  when NULL then 0.00
                    end  as end_price_netto,  
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  
                        articles.id = ' || sDBPositions || '.articleid)  when true then price /100 * (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = ' || sDBPositions || '.articleid)) 
                        when false then price when NULL then 0.00 
                    end as end_price_gross  
                    from  ' || sDBPositions || ', articles, ' || sDB || '  
                    where ' || sDB || '.id = ' || iOrderID  || ' and ' || sDBPositions || '.orderid = ' || sDB || '.id and articles.id = ' || sDBPositions || '.articleid and ' || sDBPositions || '.id = ' ||iPositionID || ' '  ||  fct_getWhere(2, sDBPositions || '.')  ;
                   
            
        execute (sSql2) into r1 ;
        raise notice 'sSql2 = % ergibt % ',sSql2, r1 ;
        return r1 ;
        
      END ;
    
     $$;


ALTER FUNCTION public.fct_getpositionnetprice(iorderid integer, ipositionid integer, stype text) OWNER TO cuon_admin;

--
-- Name: fct_getpositionsinglenetprice(integer, integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getpositionsinglenetprice(iorderid integer, ipositionid integer, stype text) RETURNS double precision
    LANGUAGE plpgsql
    AS $$

    DECLARE
        sSql text ;
        sSql2 text ;
        netPrice float ;
        r1 record ;
        
    BEGIN
  
        netPrice := 0.00 ;
        
        sSql := 'select *  from  fct_getPositionNetPrice(' || iOrderID || ', ' || iPositionID || ', ' || quote_literal(sType) || ') as (tax_vat_all_postion int, amount float , position int, price float, discount float, pos_tax_vat float, group_tax_vat int, net float , br float ) ' ;
        raise notice 'sSql = % ',sSql ;
       
        execute (sSql) into r1 ;
       
        if r1.net is not null then 
            netPrice := r1.net ;
        end if ;
       
        
        return netPrice ;
        
      END ;
    
     $$;


ALTER FUNCTION public.fct_getpositionsinglenetprice(iorderid integer, ipositionid integer, stype text) OWNER TO cuon_admin;

--
-- Name: fct_getreminder(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getreminder(idays integer) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
 DECLARE
     iClient int ;
    sSql text := '';
    r  record;
    r2 record ;
    
    BEGIN
       sSql := ' select total_amount,lastname, city, order_id, maturity,  residue , order_number, invoice_number,date_of_invoice, this_date from fct_getResidue() as (total_amount float, lastname varchar(50),  city varchar(50),  order_id integer,  maturity date,residue float,  order_number integer, invoice_number integer, date_of_invoice date, this_date date )  '  ;
       
        FOR r in execute(sSql)  LOOP
        
        IF r.this_date - r.maturity > iDays   THEN
            return next r;
        END IF ;
        
        END LOOP ;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getreminder(idays integer) OWNER TO cuon_admin;

--
-- Name: fct_getresidue(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getresidue() RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
 DECLARE
     iClient int ;
    searchsql text := '';
    r  record;
    r2 record ;
    sSql text := '' ;
    sSql2 text;
    BEGIN
       
        searchsql := 'select  list_of_invoices.order_number, fct_getResidueForInvoice(list_of_invoices.id) as residue, id  from list_of_invoices  where is_paid = false or is_paid is NULL '  || fct_getWhere(2,' ') || ' order by list_of_invoices.id' ;
        
        -- RAISE NOTICE ' get residue sql = %', searchsql ;

        FOR r in execute(searchsql)  LOOP
        
        IF r.residue > 0.01 or r.residue < -0.01 THEN
        
            
          
                sSql := 'select  list_of_invoices.total_amount as total_amount, address.lastname as lastname, address.city as city, orderbook.id as order_id, list_of_invoices.maturity as maturity, fct_getResidueForInvoice(list_of_invoices.id) as residue ,  list_of_invoices.order_number as order_number,  list_of_invoices.invoice_number as invoice_number, list_of_invoices.date_of_invoice as date_of_invoice, current_date  from list_of_invoices , orderbook, address  where list_of_invoices.id = ' ||  r.id  || ' and list_of_invoices.order_number = orderbook.id and address.id = orderbook.addressnumber order by list_of_invoices.maturity';
            FOR r2 in execute(sSql)  LOOP
                return NEXT r2 ;
            END LOOP ;
        ELSE
	   sSql2 = 'update list_of_invoices set is_paid = true where id = ' || r.id ;
	   raise notice ' sSql2 = %',sSql2 ;
	   execute(sSql2);
	
        END IF ;

        
        END LOOP ;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getresidue() OWNER TO cuon_admin;

--
-- Name: fct_getresidueforinvoice(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getresidueforinvoice(iinvoice integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
    fSum float ;
    iOrder integer ;
    sSql text := '';
 BEGIN
     sSql := 'select  list_of_invoices.total_amount  -  fct_getInpayment(list_of_invoices.invoice_number ) as residue from list_of_invoices where list_of_invoices.id = ' || iInvoice || ' ' ||  fct_getWhere(2,' ') ;
    -- RAISE NOTICE ' get residue for Invoice sql = %', sSql ;
    
    execute sSql into  fSum ;
   
    return fSum ;
    END ;
    $$;


ALTER FUNCTION public.fct_getresidueforinvoice(iinvoice integer) OWNER TO cuon_admin;

--
-- Name: fct_getresidueforinvoicemonth(integer, date); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getresidueforinvoicemonth(iinvoice integer, lastday date) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
    fSum float ;
    iOrder integer ;
    sSql text := '';
 BEGIN
     sSql := 'select  list_of_invoices.total_amount  -  fct_getInpaymentMonth(list_of_invoices.invoice_number, ' || quote_literal(lastDay) || ' ) as residue from list_of_invoices where list_of_invoices.id = ' || iInvoice ||  ' ' ||  fct_getWhere(2,' ') ;
    -- RAISE NOTICE ' get residue for Invoice sql = %', sSql ;
    
    execute sSql into  fSum ;
   
    return fSum ;
    END ;
    $$;


ALTER FUNCTION public.fct_getresidueforinvoicemonth(iinvoice integer, lastday date) OWNER TO cuon_admin;

--
-- Name: fct_getresidueformonth(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getresidueformonth(imonth integer, iyear integer) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
 DECLARE
     iClient int ;
    searchsql text := '';
    r1  record;
    r2 record ;
    sSql text := '' ;
    sSql2 text;
    sCursor text ;
    cur1 refcursor ;
    lastDay date;
    
    BEGIN

     lastDay := fct_getLastDayOfMonth(fct_getFirstDayOfMonth(iMonth, iYear) );


	sCursor := 'select  list_of_invoices.total_amount as total_amount, address.lastname as lastname, address.city as city, orderbook.id as order_id, list_of_invoices.maturity as maturity, fct_getResidueForInvoiceMonth(list_of_invoices.id, ' ||  quote_literal( lastDay)  || ') as residue ,  list_of_invoices.order_number as order_number,  list_of_invoices.invoice_number as invoice_number, list_of_invoices.date_of_invoice as date_of_invoice, current_date  from list_of_invoices , orderbook, address  where  date_of_invoice  <= ' || quote_literal( lastDay) || '  and list_of_invoices.order_number = orderbook.id and address.id = orderbook.addressnumber order by list_of_invoices.id';

	raise notice ' sCursor = %', sCursor ;
	
	OPEN cur1 FOR EXECUTE sCursor ;
        FETCH cur1 INTO r1 ;
	
	WHILE FOUND = true LOOP

	      	  if ( r1.date_of_invoice <=  lastDay and (r1.residue >= 0.01 or r1.residue <= -0.01)  )  then
		     return next r1 ;
		    end if ;
		    
	 FETCH cur1 INTO  r1 ;	
        END LOOP ;
       close cur1 ;
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getresidueformonth(imonth integer, iyear integer) OWNER TO cuon_admin;

--
-- Name: fct_getschedule1(text, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getschedule1(short_key text, days integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    sSql2    text ;

    recAddress  record;
    recSchedule record;
    
    
    BEGIN
	sSql := 'select '' ''::text as address, '' ''::text as firstname,'' ''::text as  lastname, '' ''::text as lastname2, '' ''::text as street,  '' ''::text as city,  '' ''::text as city_country,  '' ''::text as zip, '' ''::text as country,  '' ''::text as city_alone,  '' ''::text as cityfield, '' ''::text as first_last, '' ''::text as last_first,  address.id as address_id, schedul_date::text, dschedul_date, schedul_time_begin, '' ''::text as schedul_time_begin_text, partner_schedul.short_remark::text from partner_schedul,partner, address  where fct_getDays(fct_to_date(schedul_date))  = (-1 * ' || days || ') AND position(' || quote_literal(short_key) || 'in short_remark) > 0 AND partnerid = partner.id and address.id = addressid  ' ||  fct_getWhere(2,'partner_schedul.') ;
	raise notice 'sSql = %',sSql ;

	for recSchedule in execute sSql 
	LOOP
	
		if recSchedule.dschedul_date is NULL then 
		   recSchedule.dschedul_date = fct_to_date(recSchedule.schedul_date);
		END IF ;

		sSql2 = 'select *  from fct_getAddressField(' || recSchedule.address_id || ') as ( address_id int, address text , firstname text , lastname text,lastname2 text ,street text , city text , city_country text , zip text , country text , city_alone text ,cityfield text, first_last text , last_first text) ' ;
		raise notice 'sSql2 = %',sSql2 ;
		execute sSql2 into recAddress ;

		recSchedule.firstname = recAddress.firstname ;
		recSchedule.lastname = recAddress.lastname ;		   
		recSchedule.lastname2 = recAddress.lastname2 ;
		recSchedule.street = recAddress.street ;
		recSchedule.zip = recAddress.zip ;
		recSchedule.city = recAddress.city ;
		
		recSchedule.schedul_time_begin_text = fct_getTimeFromCheckbox(	recSchedule.schedul_time_begin) ;

		return recSchedule ;
	
	END LOOP;

	
	return recAddress;

	
      
	
       END ;
    $$;


ALTER FUNCTION public.fct_getschedule1(short_key text, days integer) OWNER TO cuon_admin;

--
-- Name: fct_getschedultime(integer, integer, character[], integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getschedultime(itimebegin integer, itimeend integer, atimes character[], recid integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    sTime varchar;
    BEGIN 
        sTime := aTimes[iTimeBegin] || ' - ' ||  aTimes[iTimeEnd]  ;
        
    return sTime ;
       END ;
    $$;


ALTER FUNCTION public.fct_getschedultime(itimebegin integer, itimeend integer, atimes character[], recid integer) OWNER TO cuon_admin;

--
-- Name: fct_getsellingpricesforarticle(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getsellingpricesforarticle(iarticleid integer, ipartid integer) RETURNS record
    LANGUAGE plpgsql
    AS $$

DECLARE
        sSql    text ;
        rData  record ;
        rData2  record ;
         
    BEGIN 
    
    sSql = 'select apl.quantities, sellingprice1, sellingprice2, sellingprice3, sellingprice4, 0.0::float as t1, 0.0::float as t2,0.0::float as t3,0.0::float as t4 from articles, articles_parts_list as apl  where articles.id = ' || iArticleID || ' and  articles.id = apl.part_id and apl.article_id = ' || iPartID || ' ' || fct_getWhere(2,'apl.') ; 
    execute(sSql) into rData ;
    
    if rData.sellingprice1 IS NULL then
        rData.sellingprice1 := 0.0::float  ;
    end if ;
    
    if rData.sellingprice2 IS NULL then
        rData.sellingprice2 := 0.0::float  ;
    end if ;
      if rData.sellingprice3 IS NULL then
        rData.sellingprice3 := 0.0::float  ;
    end if ;
      if rData.sellingprice4 IS NULL then
        rData.sellingprice4 := 0.0::float  ;
    end if ;
    
    rData.t1 =  rData.sellingprice1 * rData.quantities ;
    rData.t2 =  rData.sellingprice2 * rData.quantities ;
    rData.t3 =  rData.sellingprice3 * rData.quantities ;
    rData.t4 =  rData.sellingprice4 * rData.quantities ; 
  
    return  rData ;
    
    END ;
    
    $$;


ALTER FUNCTION public.fct_getsellingpricesforarticle(iarticleid integer, ipartid integer) OWNER TO cuon_admin;

--
-- Name: fct_getstattaxvat(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getstattaxvat() RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    DECLARE
    r1 record ;
    r2 record ;
    r3 record ;
    
    iMonth int ;
    iYear int ;
    sSql text ;
    sSql2 text ;
    invoice_netto float;
    invoice_taxvat float ;
    
    br1 float ;
    br0 float ;
    BEGIN
    
        br0 = 0.00 ;
        br1 = 0.00 ;
  
        FOR i IN 0 .. 1 LOOP
            
            iMonth := date_part('month',current_date) - i ;
            
            iYear := date_part('year',current_date) ;
            if iMonth < 1 then
                iMonth = iMonth + 12 ;
                iYear = iYear -1 ;
            end if ;
            
            
            FOR  r1 in select  id, vat_value, vat_name, vat_designation,0.00 as tax_vatSum, 0.00 as sum_price_netto, i as z1 from tax_vat  LOOP
    
                sSql := 'select li.invoice_number as invoice_number,  li.date_of_invoice as li_date, ' || i || ' as z1, li.order_number  as li_orderid from list_of_invoices  as li  where  date_part(''month'', li.date_of_invoice) = ' ||  iMonth  || ' and date_part(''year'', li.date_of_invoice) = ' || iYear ||  fct_getWhere(2,' ')   || ' order by li.invoice_number ' ; 
               
                FOR r2 in execute(sSql)  LOOP
                    invoice_taxvat := 0.00 ;
                    invoice_netto := 0.00 ;
               
                -- raise notice ' Invoice Number % Invoice Date % Order ID % ',r2.invoice_number, r2.li_date, r2.li_orderid ;

                    br0 :=   r1.tax_vatSum   +  r1.sum_price_netto ;
                     
                     
                    sSql2 := 'select ( select tax_vat_for_all_positions from orderinvoice where orderinvoice.orderid  = ' || r2.li_orderid  || ' ) as   tax_vat_for_all_positions ,   
                    orderposition.amount as amount,  orderposition.position as position, orderposition.price as price, 
                    orderposition.discount as discount, orderposition.tax_vat as position_tax_vat, 
                    (select  material_group.tax_vat from material_group,articles where  articles.material_group = material_group.id and articles.id = orderposition.articleid) as m_group_taxvat, 
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  articles.id = orderposition.articleid)
                        when true then price when false then price / (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = orderposition.articleid)) * 100  when NULL then 0.00
                    end  as end_price_netto,  
                    case 
                        ( select material_group.price_type_net from material_group, articles where  articles.material_group = material_group.id and  
                        articles.id = orderposition.articleid)  when true then price /100 * (100 + (select  tax_vat.vat_value from tax_vat,material_group,articles  
                        where  articles.material_group = material_group.id and material_group.tax_vat = tax_vat.id and articles.id = orderposition.articleid)) 
                        when false then price when NULL then 0.00 
                    end as end_price_gross  
                    from  orderposition, articles, orderbook  
                    where orderbook.id = ' || r2.li_orderid  || ' and orderposition.orderid = orderbook.id and articles.id = orderposition.articleid '  ||  fct_getWhere(2,'orderposition.')  ;
                   
                    FOR r3 in execute(sSql2) LOOP
                        IF r3.discount IS NULL THEN 
                            r3.discount := 0.00 ;
                        END IF ;
                        
                        IF r3.position_tax_vat IS NOT NULL and  r3.position_tax_vat > 0.00  and r1.vat_value = r3.position_tax_vat THEN
                             r1.tax_vatSum :=   r1.tax_vatSum +( (r3.end_price_netto -r3.discount)  * r3.amount * r3.position_tax_vat / 100 ) ;
                             r1.sum_price_netto:=   r1.sum_price_netto +( r3.end_price_netto  * r3.amount );
                             raise notice ' position taxvat = % ', r3.position_tax_vat ;
                        ELSEIF r3.tax_vat_for_all_positions IS NOT NULL and r3.tax_vat_for_all_positions > 0 and  r3.tax_vat_for_all_positions =  r1.id  THEN
                             r1.tax_vatSum :=   r1.tax_vatSum +  ( (r3.end_price_netto -r3.discount) * r3.amount * r1.vat_value /100 );
                             r1.sum_price_netto:=   r1.sum_price_netto + ( r3.end_price_netto  * r3.amount );
                             raise notice  'tax vatforalpositions '  ;
                      
                        ELSEIF  r3.m_group_taxvat IS NOT NULL and r3.m_group_taxvat > 0 and   r3.m_group_taxvat= r1.id and not (r3.tax_vat_for_all_positions IS NOT NULL and r3.tax_vat_for_all_positions > 0 ) THEN
                             r1.tax_vatSum :=   r1.tax_vatSum +( (r3.end_price_netto-r3.discount)  * r3.amount * r1.vat_value / 100 );
                             r1.sum_price_netto:=   r1.sum_price_netto + (r3.end_price_netto  * r3.amount );
                            raise notice ' materialgroup taxvat ' ;
                              
                       
                        END IF ;
                       
                         -- raise notice ' Orderid % Posion ID % TaxVat %  Menge % Netto % ', r2.li_orderid,r3.position,r1.vat_value,  r3.amount,r3.end_price_netto *  r3.amount ;
                    
                        
                        
                        
                    END LOOP ;
                      br1 :=  r1.tax_vatSum   +  r1.sum_price_netto ;
                      raise notice '  Invoice Number %           Br1 = % ',r2.invoice_number,  br1 - br0  ;
                     
                END LOOP ;
            RETURN NEXT r1;
        
            END LOOP ;
             -- raise notice ' Discount =   % ',  invoice_netto ;

        END LOOP;
    
     
       
    END ;
    
     $$;


ALTER FUNCTION public.fct_getstattaxvat() OWNER TO cuon_admin;

--
-- Name: fct_getsupply_number(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getsupply_number(orderid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    iData int ;
    sSql text ;
    r2 record ;
    
    BEGIN
       iData := 0 ;
       
       
       sSql := 'select delivery_number as supply_number from list_of_deliveries where order_number = ' || OrderID || ' ' ||  fct_getWhere(2,' ') ;
       raise notice ' SQL at fct_getSupply_number = % ', sSql ;
       FOR r2 in execute(sSql)  LOOP
           
            if r2.supply_number is not null then 
                iData := r2.supply_number ;
            else
                iData := 0 ;
            END IF ;
            raise notice 'iData = %',iData ;
             
        END LOOP ;
     
            
     return iData ; 
       
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getsupply_number(orderid integer) OWNER TO cuon_admin;

--
-- Name: fct_gettimefromcheckbox(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_gettimefromcheckbox(itime integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
      hours int;
      minutes int;
      sTime text;
       
    BEGIN
       
	hours = (iTime / 4);
	minutes = mod(iTime, 4) * 15;

	sTime = to_char(hours,'FM00') || ':' || to_char(minutes,'FM00') ;

       
        RETURN sTime ;
       
    END;
     
     $$;


ALTER FUNCTION public.fct_gettimefromcheckbox(itime integer) OWNER TO cuon_admin;

--
-- Name: fct_gettopidfororder(integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_gettopidfororder(orderbookid integer, stype text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE
 
    t1 integer ;
    sSql text ;
    r2 record ;
    sDatabase return_text3 ;
    sDBInvoice text ;
    
    topID integer ;
    
    BEGIN
    
        topID := 0;
        sDatabase = fct_get_database(sType);
        sDBInvoice = sDatabase.c ;
       
    
        sSql := 'select order_top from ' || sDBInvoice || ' where orderid = ' || OrderbookID  || ' ' ||  fct_getWhere(2,' ') ;
        raise notice 'sql = %', sSql ;
        execute(sSql) into r2;
        
        IF found AND r2.order_top is not NULL then 
            
            topID :=  r2.order_top ;
            
        END IF ;
        
        
    
        IF topID = 0 THEN 
        
            execute  'select addresses_misc.top_id as adr_top_id from addresses_misc,orderbook where addresses_misc.address_id = orderbook.addressnumber and orderbook.id = ' || OrderbookID || ' ' ||  fct_getWhere(2,'addresses_misc.') INTO  t1;
       
       
            
            raise notice 'top id adr = %', t1 ;
            
            if t1 is  not null then 
            
                if t1 > 0 then 
                    topID :=  t1;
                end if;
            end if ;
  
            
        
            
            
            
        end if ;
        
        return topID ;
        
            
      
       
    END ;
    
     $$;


ALTER FUNCTION public.fct_gettopidfororder(orderbookid integer, stype text) OWNER TO cuon_admin;

--
-- Name: fct_gettotalsellingpricesforarticle(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_gettotalsellingpricesforarticle(ipartid integer) RETURNS record
    LANGUAGE plpgsql
    AS $$

    DECLARE
        sSql    text ;
        rData  record ;
        rData2  record ;
        ft1 float ;
        ft2 float ;
        ft3 float ;
        ft4 float ;
        
    BEGIN 
     ft1 := 0.00 ;
     ft2 := 0.00 ;
     ft3 := 0.00 ;
     ft4 := 0.00 ;
     
     
    sSql := 'select  apl.quantities, sellingprice1, sellingprice2, sellingprice3, sellingprice4, 0.0::float as t1, 0.0::float as t2,0.0::float as t3,0.0::float as t4 from articles,  articles_parts_list as apl where articles.id = apl.part_id and apl.article_id = ' || iPartID || ' ' || fct_getWhere(2,'apl.') ;
    
    raise notice ' sSql = %', sSql ;
    
    FOR rData in execute(sSql) LOOP 
    
        if rData.sellingprice1 IS NOT NULL then
            raise notice 'sellingprice 1 = % , %  %', rData.sellingprice1, rData.quantities, rData.t1 ;
            ft1 :=  ft1 + (rData.sellingprice1 * rData.quantities );
             raise notice 'sellingprice 1 = % , % ', rData.sellingprice1, rData.t1 ;
        end if ;
        if rData.sellingprice2 IS NOT NULL then
            ft2 :=  ft2 + (rData.sellingprice2 * rData.quantities );
        end if ;
        if rData.sellingprice3 IS NOT NULL then
            ft3 :=  ft3 + (rData.sellingprice3 * rData.quantities );
        end if ;
        if rData.sellingprice4 IS NOT NULL then
            ft4 :=  ft4 + (rData.sellingprice4 * rData.quantities );
        end if ;
        
        
        
        
    END LOOP ;
    
        rData.t1 := ft1 ;
        rData.t2 := ft2 ;
        rData.t3 := ft3 ;
        rData.t4 := ft4 ;
        
        return  rData ;
    END ;
    
    $$;


ALTER FUNCTION public.fct_gettotalsellingpricesforarticle(ipartid integer) OWNER TO cuon_admin;

--
-- Name: fct_getunreckonedorder(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getunreckonedorder(orderid integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
        DECLARE
        iClient int ;
    sSql text := '';
    t1  text := '  -1 ' ;
    r record ;
    r2 record ;
    bInsert bool ;
    
    BEGIN
       
            bInsert = True ;
            sSql := 'select id from list_of_invoices where order_number =  ' || OrderID ||  ' ' ||  fct_getWhere(2,' ') ;
            raise notice 'sql  = %',sSql ;
            FOR r2 in execute(sSql)  LOOP
                raise notice 'id = %',r2.id ;
                if r2.id > 0 then
                    bInsert = False;
                end if ;
            END LOOP ;
            
        return bInsert ;
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_getunreckonedorder(orderid integer) OWNER TO cuon_admin;

--
-- Name: fct_getuserdataclient(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getuserdataclient() RETURNS integer
    LANGUAGE plpgsql
    AS $$
   -- get the client id
    DECLARE
    iClient    int ;
    sClient char ;
    BEGIN
        
        select into iClient current_client from cuon_user where username = CURRENT_USER ;
        -- raise notice 'Client id = %', iClient ;
	if iClient is NULL then 
	   iclient = 1;
	  End IF ;

        return iClient ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_getuserdataclient() OWNER TO cuon_admin;

--
-- Name: fct_getuserdatalocales(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getuserdatalocales() RETURNS text
    LANGUAGE plpgsql
    AS $$
   -- get the client id
    DECLARE
    sData text ;
    sClient char ;
    BEGIN
    
        select into sData user_locales from cuon_user where username = CURRENT_USER ;
        raise notice 'locales = %', sData ;
        return sData ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_getuserdatalocales() OWNER TO cuon_admin;

--
-- Name: fct_getuserdatanowhereclient(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getuserdatanowhereclient() RETURNS integer
    LANGUAGE plpgsql
    AS $$
       -- get the nowhereclient id
     DECLARE
    iClient    int ;
    sClient char ;
    BEGIN
    
        select into iClient no_where_client from cuon_user where username = CURRENT_USER ;
        -- raise notice 'NoWhereClient id = %', iClient ;
        return iClient ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_getuserdatanowhereclient() OWNER TO cuon_admin;

--
-- Name: fct_getuserexe(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getuserexe(dms_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
	sSuffix text;
	
	sPDFSuffix text ;
 	sExe text;
	sUserExe text ;
        vList text[]  ;
 	array_size integer ;

        BEGIN
		sUserExe := 'NONE';
		vList[1] :=  'exe_oowriter,sxw,sdw,odt,ott,doc,rtf,';
        	vList := array_append(vList,'exe_pdf,pdf,') ;

		array_size := array_length(vList,1) ;

    	     	sSql := 'select '','' || file_suffix || '','' from dms where id = ' || dms_id ;
		raise notice 'sSql = % ',sSql ;
	     	execute(sSql) into sSuffix ;

	        for ct in 1..array_size loop

		    if position(sSuffix in vlist[ct] ) >0 then 
		       raise notice 'suffix found %',sSuffix ;
		       sExe = substring( vlist[ct],0, position(',' in vlist[ct] ) );
		       raise notice 'exe found %',sExe ;	
		       EXIT ;
		    end if ;
      
		end loop;  
    
		sSql = 'select ' || sExe || ' from preferences where username = current_user and  is_standard_profile = ''t''  ';
		raise notice 'sSql = % ',sSql ;
		execute(sSql) into sUserExe ;
		return sUserExe ;
            
        END ;
    $$;


ALTER FUNCTION public.fct_getuserexe(dms_id integer) OWNER TO cuon_admin;

--
-- Name: fct_getuserstandardexe(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getuserstandardexe(ssuffix text) RETURNS text
    LANGUAGE plpgsql
    AS $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
	
	
	sPDFSuffix text ;
 	sExe text;
	sUserExe text ;
        vList text[]  ;
 	array_size integer ;

        BEGIN
		sUserExe := 'NONE';
		vList[1] :=  'exe_oowriter,sxw,sdw,odt,ott,doc,rtf,';
        	vList := array_append(vList,'exe_pdf,pdf,') ;

		array_size := array_length(vList,1) ;

    	 
	        for ct in 1..array_size loop

		    if position(sSuffix in vlist[ct] ) >0 then 
		       raise notice 'suffix found %',sSuffix ;
		       sExe = substring( vlist[ct],0, position(',' in vlist[ct] ) );
		       raise notice 'exe found %',sExe ;	
		       EXIT ;
		    end if ;
      
		end loop;  
    
		sSql = 'select ' || sExe || ' from preferences where username = current_user and  is_standard_profile = ''t''  ';
		raise notice 'sSql = % ',sSql ;
		execute(sSql) into sUserExe ;
		return sUserExe ;
            
        END ;
    $$;


ALTER FUNCTION public.fct_getuserstandardexe(ssuffix text) OWNER TO cuon_admin;

--
-- Name: fct_getvalueascurrency(double precision); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getvalueascurrency(value double precision) RETURNS text
    LANGUAGE plpgsql
    AS $$

    DECLARE
        sSql text ;  
        sValue text ;
        iClient int ;
        sCurrencySign text ;
        iCurrencyPrecision int ;
        fValue float ;
        sFormatter text ;
        
    BEGIN 
        iClient = fct_getUserDataClient(  ) ;
        sCurrencySign = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'CurrencySign') ; 
        iCurrencyPrecision = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'CurrencyPrecision')::int ;  
        if sCurrencySign is null then 
            sCurrencySign = 'EUR' ;
        end if ;
        if iCurrencyPrecision is null then 
            iCurrencyPrecision = 2 ;
        end if ;
        select into fValue  round(value::numeric,iCurrencyPrecision);
        -- raise notice 'Currency = % with %',sCurrencySign, iCurrencyPrecision ;
        if iCurrencyPrecision = 0 then 
            sFormatter = '99G999G999G999D' ;
        elseif iCurrencyPrecision = 2 then 
            sFormatter = '99G999G999G999D99' ;
        end if ;
            
        sValue = to_char(fValue,sFormatter) || ' ' || sCurrencySign ;
        if sValue is null then 
            sValue = '0' ;
        end if ;
        
        return sValue ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_getvalueascurrency(value double precision) OWNER TO cuon_admin;

--
-- Name: fct_getwhere(integer, character); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_getwhere(isingle integer, sprefix character) RETURNS text
    LANGUAGE plpgsql
    AS $$
    -- retuns the sWhere value
     DECLARE
    sWhere  text ;
    iClient int ;
    iNoWhereClient int ;

    BEGIN
        iClient := fct_getUserDataClient();
        iNoWhereClient := fct_getUserDataNoWhereClient();
        
    sWhere := ' ' ;
    if iNoWhereClient = '1'
    then
        if iSingle = 1
        then
             sWhere := 'WHERE ' ||  sPrefix  || 'client = ' || iClient || ' and ' || sPrefix  || 'status != ''delete'' ' ;
                    

        else 
            if iSingle = 2   then
                sWhere := 'AND ' ||  sPrefix  || 'client = ' || iClient || ' and ' || sPrefix  || 'status != ''delete'' ' ;
            END IF ;
        END IF ;
        
    END IF ;

  
    -- RAISE NOTICE ' sWhere  = %', sWhere ;
    RETURN sWhere ;
    END ;
     $$;


ALTER FUNCTION public.fct_getwhere(isingle integer, sprefix character) OWNER TO cuon_admin;

--
-- Name: fct_incomming_bank(character varying, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_incomming_bank(cuuid character varying, ibanknumber integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
 DECLARE
  
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    iNewAccountingID int ;
    sSql text ;
    sSql1 text ;
    
    sSql2 text ;
    account_revenue_id int;
    account_receivable_id int ;
    account_taxvat_id int ;
    account_revenue_net_id int := 0;
    
    
    BEGIN
        iClient = fct_getUserDataClient(  ) ;
        sSql := 'select id  from account_info where accounting_system_type = ''2400'' ' || fct_getWhere(2,' ')  ;
        execute(sSql) into account_receivable_id ;
           
       return new_transaction_uuid ;
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_incomming_bank(cuuid character varying, ibanknumber integer) OWNER TO cuon_admin;

--
-- Name: fct_insert(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values at an insert operation 
     
    BEGIN
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = 'insert' ;
        NEW.versions_uuid = fct_new_uuid()   ;
        NEW.versions_number = 1 ;
        -- RAISE NOTICE 'Name =  % ', TG_NAME ;
        
        IF TG_TABLE_NAME = 'list_of_invoices' THEN 
                NEW.last_transaction_uuid = fct_write_invoice(NEW.uuid) ;
               
              
            END IF ;
        RETURN NEW; 
    END;
     
    $$;


ALTER FUNCTION public.fct_insert() OWNER TO cuon_admin;

--
-- Name: fct_loadgraveservicenote(integer, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_loadgraveservicenote(igraveid integer, igraveserviceid integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
 DECLARE
  
    sNote text ;
    
    BEGIN
       select into sNote service_note from grave_service_notes where  grave_id = iGraveID and service_id = iGraveServiceID ;
       raise notice ' sNote = %', sNote ;
        if sNote is null then 
            sNote = ' ' ;
        -- BUG #3523746  Workaround comment out this elseif tree
        -- ELSEIF ascii(sNote) = 32 then 
        --    sNote = ' ' ;
            
        END IF ;
        raise notice ' sNote9 = %', sNote ;
        return sNote;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_loadgraveservicenote(igraveid integer, igraveserviceid integer) OWNER TO cuon_admin;

--
-- Name: fct_new_cash_desk_special_id(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_new_cash_desk_special_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
    
    this_id int ;
    old_id int ;
    iClient int;
    
    BEGIN
       
       iClient := 0;
       iClient := fct_getUserDataClient();
       select into old_id max(special_id) from cash_desk_book_number where client = iClient ;
       if old_id is NULL then
       	  old_id = 0;
       end if ;

      
       NEW.special_id = old_id + 1;
       NEW.real_timestamp = now() ;
       
       RETURN NEW ;
        
    END ;
    $$;


ALTER FUNCTION public.fct_new_cash_desk_special_id() OWNER TO cuon_admin;

--
-- Name: fct_new_uuid(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_new_uuid() RETURNS character
    LANGUAGE plpgsql
    AS $$
    DECLARE
    
    this_uuid char(36) ;
    new_md5 char(32) ;
    BEGIN
        SELECT into new_md5 md5(current_database()|| user ||current_timestamp ||random() ) ;
        -- 8 4 4 4 12
        this_uuid = substring(new_md5 from 1 for 8) || '-' || substring(new_md5 from 9 for 4) || '-' || substring(new_md5 from 13 for 4) || '-' || substring(new_md5 from 17 for 4) || '-' || substring(new_md5 from 21 for 12) ;
    
        --raise notice 'new uuid ', this_uuid ;
       
        
        RETURN this_uuid ;
        
    END ;
    $$;


ALTER FUNCTION public.fct_new_uuid() OWNER TO cuon_admin;

--
-- Name: fct_new_uuid_for_ticket_number(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_new_uuid_for_ticket_number() RETURNS opaque
    LANGUAGE plpgsql
    AS $$
    DECLARE
    
    this_uuid char(36) ;
    
    BEGIN
       NEW.ticket_number = fct_new_uuid() ;
       
       RETURN NEW ;
        
    END ;
    $$;


ALTER FUNCTION public.fct_new_uuid_for_ticket_number() OWNER TO cuon_admin;

--
-- Name: fct_null_uuid(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_null_uuid() RETURNS character
    LANGUAGE plpgsql
    AS $$
    DECLARE
    
    this_uuid char(36) ;
    new_md5 char(32) ;
    BEGIN
    
      this_uuid := '00000000-0000-0000-0000-000000000000' ;
    
      RETURN this_uuid ;
        
    END ;
    $$;


ALTER FUNCTION public.fct_null_uuid() OWNER TO cuon_admin;

--
-- Name: fct_orderbook_delete_invoice(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_orderbook_delete_invoice() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  remove all invoice for this deleted order
     DECLARE
    sSql text ;
    r2 record ;
    
    
    BEGIN
               sSql = 'select * from list_of_invoices where order_number = ' || OLD.id ;
               FOR r2 in execute(sSql)  LOOP
               
                -- raise notice ' delete this invoice : % ', r2.id ;
               END LOOP ;
               
                
               RETURN NEW; 
    END;
  
     $$;


ALTER FUNCTION public.fct_orderbook_delete_invoice() OWNER TO cuon_admin;

--
-- Name: fct_orderposition_insert(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_orderposition_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values to orderposition 
     
    
    BEGIN
               IF NEW.tax_vat is NULL THEN
                    NEW.tax_vat = 0.00 ;
               end if ;
               IF NEW.discount is NULL THEN
                    NEW.discount = 0.00 ;
               end if ;
               RETURN NEW; 
    END;
  
     $$;


ALTER FUNCTION public.fct_orderposition_insert() OWNER TO cuon_admin;

--
-- Name: fct_partner_schedul_change(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_partner_schedul_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    BEGIN
    
        if OLD.status is null then 
            OLD.status := 'insert' ;
            NEW.status := 'insert' ;
        end if ;
        
        if not OLD.status = 'delete' then 
            NEW.update_user_id = current_user   ;
            NEW.update_time = (select  now()) ;
            NEW.user_id = OLD.user_id;
            NEW.insert_time = OLD.insert_time ;
            NEW.status = 'update' ;
            if NEW.sep_info_3 = 2 then 
                NEW.sep_info_3 = 0 ;
            else  
                NEW.sep_info_3 = 5 ;
            end if ;
            RAISE NOTICE 'Name =  % ', TG_NAME ;
            RETURN NEW; 
      else 
            RETURN OLD;
      end if ;
    END;
    
     $$;


ALTER FUNCTION public.fct_partner_schedul_change() OWNER TO cuon_admin;

--
-- Name: fct_partner_schedul_insert(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_partner_schedul_insert() RETURNS opaque
    LANGUAGE plpgsql
    AS $$
    --  set default values to orderposition 
     BEGIN
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = 'insert' ;
        NEW.sep_info_3 = 5 ;
        RAISE NOTICE 'Name =  % ', TG_NAME ;
        RETURN NEW; 
    END;
    
  
     $$;


ALTER FUNCTION public.fct_partner_schedul_insert() OWNER TO cuon_admin;

--
-- Name: fct_pickup_schedul_delete(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_pickup_schedul_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values to orderposition 
   DECLARE   
      sSql text ;
      gets_partner_id int ;
      r1 record ;
      r2 record ;
      newUUID varchar(36);

   BEGIN
	if NEW.schedul_uuid is not null then 
	     sSql = 'delete from partner_schedul where uuid = '  || quote_literal( NEW.schedul_uuid)  ;
	     execute sSql ;
	end if ;

	return NEW ;
   END;
    
  
   $$;


ALTER FUNCTION public.fct_pickup_schedul_delete() OWNER TO cuon_admin;

--
-- Name: fct_pickup_schedul_insert(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_pickup_schedul_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values to orderposition 
   DECLARE   
      sSql text ;
      gets_partner_id int ;
      r1 record ;
      r2 record ;
      newUUID varchar(36);

   BEGIN
         gets_partner_id := 0 ;
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = 'insert' ;
        NEW.sep_info_3 = 5 ;
        RAISE NOTICE 'Name =  % ', TG_NAME ;
	if NEW.gets_staff_id is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_date is null then 
	   return NEW ;
	end if ;

	if NEW.gets_staff_time is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_id = 0 then 
	   return NEW ;
	end if ;

	if NEW.partnernumber is null or NEW.partnernumber < 1 then 

		sSql := 'select orderbook.customers_partner_id as partner_id, orderbook.addressnumber as address_id, orderbook.designation as designation from orderbook where orderbook.id = ' || NEW.orderid  ;
		RAISE NOTICE 'sSql0  =  % ', sSql ; 
		execute sSql into r1 ;
		RAISE NOTICE 'Vars0 = % % % ', r1.partner_id,r1.address_id, r1.designation ;
		if r1.partner_id is not null then
		   if r1.partner_id > 0 then 
	   	      gets_partner_id = r1.partner_id ;
                   end if ;
	         end if ;
	 	if r1.designation is null then 
		   r1.designation = 'PICKUP' ;
	      	end if ;
	RAISE NOTICE 'Vars = % % % ', r1.partner_id,r1.address_id, r1.designation ;

	if gets_partner_id = 0 then
	   	sSql := 'select min(partner.id) as partner_id from partner where partner.addressid = ' || r1.address_id || ' and status != ' || quote_literal('delete') ;
		RAISE NOTICE 'sSql1  =  % ', sSql ;
		execute sSql into r2 ;
		if r2.partner_id is not null then 
		   gets_partner_id = r2.partner_id ;
		end if ;
	end if ;    
	raise notice ' r1.addressid = %', r1.address_id;

	if gets_partner_id = 0 then
	   gets_partner_id =  fct_generatePartner(r1.address_id);
	end if ;

	else
	   gets_partner_id = NEW.partnernumber ;
	   sSql := 'select  orderbook.designation as designation from orderbook where orderbook.id = ' || NEW.orderid  ;
		RAISE NOTICE 'sSql0  =  % ', sSql ; 
		execute sSql into r1 ;
		if r1.designation is null then 
		   r1.designation = 'PICKUP' ;
	      	end if ;
	end if ;
	
	newUUID =  fct_new_uuid() ;	 
	NEW.schedul_uuid = newUUID ;

	   
	sSql := 'insert into partner_schedul (id, uuid,partnerid,schedul_staff_id, short_remark,dschedul_date,schedul_date, schedul_time_begin,dschedul_date_end,schedul_date_end, schedul_time_end ) values(nextval(' || quote_literal('partner_schedul_id') || '),' || quote_literal(newUUID) || ', ' || gets_partner_id || ', ' || NEW.gets_staff_id || ', ' || quote_literal( r1.designation ) || ', ' || quote_literal(NEW.gets_staff_date)|| ', ' || quote_literal(NEW.gets_staff_date) ||', ' || NEW.gets_staff_time  || ', ' || quote_literal(NEW.gets_staff_date)|| ', ' || quote_literal(NEW.gets_staff_date) ||', ' || NEW.gets_staff_time +1  || ' )';
	RAISE NOTICE 'sSql2  =  % ', sSql ;      
	execute(sSql);
        RETURN NEW; 
   END;
    
  
   $$;


ALTER FUNCTION public.fct_pickup_schedul_insert() OWNER TO cuon_admin;

--
-- Name: fct_pickup_schedul_update(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_pickup_schedul_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values to orderposition 
   DECLARE   
      sSql text ;
      gets_partner_id int ;
      r1 record ;
      r2 record ;
      newUUID varchar(36);

   BEGIN
	if NEW.gets_staff_id is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_date is null then 
	   return NEW ;
	end if ;

	if NEW.gets_staff_time is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_id = 0 then 
	   return NEW ;
	end if ;

   if NEW.schedul_uuid is not null then 
      gets_partner_id := 0 ;
	if NEW.partnernumber is null or NEW.partnernumber < 1 then 
	sSql := 'select orderbook.customers_partner_id as partner_id, orderbook.addressnumber as address_id, orderbook.designation as designation from orderbook where orderbook.id = ' || NEW.orderid  ;
	RAISE NOTICE 'sSql0  =  % ', sSql ; 
	execute sSql into r1 ;
	RAISE NOTICE 'Vars0 = % % % ', r1.partner_id,r1.address_id, r1.designation ;
	if r1.partner_id is not null then
	   if r1.partner_id > 0 then 
	      gets_partner_id = r1.partner_id ;
           end if ;
        end if ;

	if r1.designation is null then 
	   r1.designation = 'PICKUP' ;
	end if ;

	RAISE NOTICE 'Vars = % % % ', r1.partner_id,r1.address_id, r1.designation ;
	if gets_partner_id = 0 then
	   	sSql := 'select min(partner.id) as partner_id from partner where partner.addressid = ' || r1.address_id || ' and status != ' || quote_literal('delete') ;
		RAISE NOTICE 'sSql1  =  % ', sSql ;
		execute sSql into r2 ;
		if r2.partner_id is not null then 
		   gets_partner_id = r2.partner_id ;
		end if ;
	end if ;    

	if gets_partner_id = 0 then
	   gets_partner_id =  fct_generatePartner(r1.address_id);
	end if ;

	else
	   gets_partner_id = NEW.partnernumber ;
	   sSql := 'select  orderbook.designation as designation from orderbook where orderbook.id = ' || NEW.orderid  ;
		RAISE NOTICE 'sSql0  =  % ', sSql ; 
		execute sSql into r1 ;
		if r1.designation is null then 
		   r1.designation = 'PICKUP' ;
	      	end if ;
	end if ;
	
      sSql := 'update partner_schedul set  partnerid =  ' || gets_partner_id || ' ,schedul_staff_id = ' || NEW.gets_staff_id || ' , short_remark =  ' || quote_literal( r1.designation ) || ',dschedul_date =  ' || quote_literal(NEW.gets_staff_date)|| ' ,schedul_date = ' || quote_literal(NEW.gets_staff_date)|| ', schedul_time_begin = ' || NEW.gets_staff_time  || ',dschedul_date_end = ' || quote_literal(NEW.gets_staff_date)|| ',schedul_date_end = ' || quote_literal(NEW.gets_staff_date)|| ', schedul_time_end = ' || NEW.gets_staff_time + 1  || ' where uuid = ' || quote_literal( NEW.schedul_uuid)  ;
      execute sSql ;

      else 

      gets_partner_id := 0 ;
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = 'update' ;
        NEW.sep_info_3 = 5 ;
        RAISE NOTICE 'Name =  % ', TG_NAME ;
	if NEW.gets_staff_id is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_date is null then 
	   return NEW ;
	end if ;

	if NEW.gets_staff_time is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_id = 0 then 
	   return NEW ;
	end if ;

	 if NEW.schedul_uuid is not null then 
	sSql := 'select orderbook.customers_partner_id as partner_id, orderbook.addressnumber as address_id, orderbook.designation as designation from orderbook where orderbook.id = ' || NEW.orderid  ;
	RAISE NOTICE 'sSql0  =  % ', sSql ; 
	execute sSql into r1 ;
	RAISE NOTICE 'Vars0 = % % % ', r1.partner_id,r1.address_id, r1.designation ;
	if r1.partner_id is not null then
	   if r1.partner_id > 0 then 
	      gets_partner_id = r1.partner_id ;
           end if ;
        end if ;

	if r1.designation is null then 
	   r1.designation = 'PICKUP' ;
	end if ;

	RAISE NOTICE 'Vars = % % % ', r1.partner_id,r1.address_id, r1.designation ;
	if gets_partner_id = 0 then
	   	sSql := 'select min(partner.id) as partner_id from partner where partner.addressid = ' || r1.address_id || ' and status != ' || quote_literal('delete') ;
		RAISE NOTICE 'sSql1  =  % ', sSql ;
		execute sSql into r2 ;
		if r2.partner_id is not null then 
		   gets_partner_id = r2.partner_id ;
		end if ;
	end if ;    
	raise notice ' r1.addressid = %', r1.address_id;
	if gets_partner_id = 0 then
	   gets_partner_id =  fct_generatePartner(r1.address_id);
	end if ;
	
	else
	   gets_partner_id = NEW.partnernumber ;
	   sSql := 'select  orderbook.designation as designation from orderbook where orderbook.id = ' || NEW.orderid  ;
		RAISE NOTICE 'sSql0  =  % ', sSql ; 
		execute sSql into r1 ;
		if r1.designation is null then 
		   r1.designation = 'PICKUP' ;
	      	end if ;
	end if ;
	
	newUUID =  fct_new_uuid() ;	 
	NEW.schedul_uuid = newUUID ;

	   
	sSql := 'insert into partner_schedul (id, uuid,partnerid,schedul_staff_id, short_remark,dschedul_date,schedul_date, schedul_time_begin,dschedul_date_end,schedul_date_end, schedul_time_end ) values(nextval(' || quote_literal('partner_schedul_id') || '),' || quote_literal(newUUID) || ', ' || gets_partner_id || ', ' || NEW.gets_staff_id || ', ' || quote_literal( r1.designation ) || ', ' || quote_literal(NEW.gets_staff_date)|| ', ' || quote_literal(NEW.gets_staff_date) ||', ' || NEW.gets_staff_time  || ', ' || quote_literal(NEW.gets_staff_date)|| ', ' || quote_literal(NEW.gets_staff_date) ||', ' || NEW.gets_staff_time +1  || ' )';
	RAISE NOTICE 'sSql2  =  % ', sSql ;      
	execute(sSql);
        
       
       end if ;


       return NEW ;

   END;
    
  
   $$;


ALTER FUNCTION public.fct_pickup_schedul_update() OWNER TO cuon_admin;

--
-- Name: fct_resetinvoice(text, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_resetinvoice(sservice text, igraveid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    
    iClient int ;
    sSql text   ;
    
   
    r1 record ;
    r2 record ;
   last_position int ;
    grave_headline int ;
    grave_headline_designation text ;
    allInvoices int [] ;
    grave_part int ;
    
    BEGIN
       
        
        
        iClient = fct_getUserDataClient(  ) ;
        raise notice ' Client Number = %',iClient ;
       
        
    
        
        sSql := ' select grave.*, graveyard.shortname as graveyard_shortname from grave, graveyard ' ;
        
        IF sService = 'Service' THEN
            
            ALTER TABLE grave_work_maintenance DISABLE TRIGGER trg_updategrave_work_maintenance ;
            update grave_work_maintenance  set created_order = 0 where grave_id = iGraveID ;
            ALTER TABLE grave_work_maintenance  enABLE TRIGGER trg_updategrave_work_maintenance ;

        ELSEIF sService = 'Spring' THEN
            ALTER TABLE grave_work_spring DISABLE TRIGGER trg_updategrave_work_spring;
            update grave_work_spring set created_order = 0  where grave_id = iGraveID;
            ALTER TABLE grave_work_spring enABLE TRIGGER trg_updategrave_work_spring;


         ELSEIF sService = 'Summer' THEN
                        
            ALTER TABLE grave_work_summer DISABLE TRIGGER trg_updategrave_work_summer;
            update grave_work_summer set created_order = 0  where grave_id = iGraveID;
            ALTER TABLE grave_work_summer enABLE TRIGGER trg_updategrave_work_summer;

         ELSEIF sService = 'Autumn' THEN
            
            ALTER TABLE grave_work_autumn DISABLE TRIGGER trg_updategrave_work_autumn;
            update grave_work_autumn set created_order = 0  where grave_id = iGraveID;
            ALTER TABLE grave_work_autumn enABLE TRIGGER trg_updategrave_work_autumn;

        ELSEIF sService = 'Winter' THEN
            
            
            ALTER TABLE grave_work_winter DISABLE TRIGGER trg_updategrave_work_winter;
            update grave_work_winter set created_order = 0  where grave_id = iGraveID;
            ALTER TABLE grave_work_winter enABLE TRIGGER trg_updategrave_work_winter;

         ELSEIF sService = 'Holliday' THEN
           
            
            ALTER TABLE grave_work_holiday DISABLE TRIGGER trg_updategrave_work_holiday;
            update grave_work_holiday set created_order = 0  where grave_id = iGraveID;
            ALTER TABLE grave_work_holiday ENABLE TRIGGER trg_updategrave_work_holiday;
             
         ELSEIF sService = 'Unique' THEN
            -- unique must not to set clear, because this are standalone positions that must not set to reset ;
            
         ELSEIF sService = 'Yearly' THEN
            
            
            ALTER TABLE grave_work_year DISABLE TRIGGER trg_updategrave_work_year;
            update grave_work_year set created_order = 0  where grave_id = iGraveID;
            ALTER TABLE grave_work_year enABLE TRIGGER trg_updategrave_work_year;

        END IF ;
        
   


   -- for the next invoice ;
    ALTER TABLE grave DISABLE TRIGGER trg_updategrave;
    update grave set created_order = 0  where id = iGraveID;
    ALTER TABLE grave enABLE TRIGGER trg_updategrave;
          
        return  iGraveID;
       
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_resetinvoice(sservice text, igraveid integer) OWNER TO cuon_admin;

--
-- Name: fct_round_2_digits(double precision); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_round_2_digits(value double precision) RETURNS double precision
    LANGUAGE plpgsql
    AS $$ 
    DECLARE 
        retValue float ;
    BEGIN 
        select into retValue  round(value::numeric,2);
        return retValue ;
        
    END; 

    
    $$;


ALTER FUNCTION public.fct_round_2_digits(value double precision) OWNER TO cuon_admin;

--
-- Name: fct_savegraveservicenote(integer, integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_savegraveservicenote(igraveid integer, igraveserviceid integer, snote text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
  
    iRecordID int ;
    
    BEGIN
       select into iRecordID id from grave_service_notes where  grave_id = iGraveID and service_id = iGraveServiceID ;
        if iRecordID is null then 
            insert into grave_service_notes (id, grave_id, service_id, service_note) values (nextval('grave_service_notes_id'),iGraveID,iGraveServiceID,sNote) ;
            iRecordID = 0;
        ELSE
            update grave_service_notes set service_note = sNote where grave_id = iGraveID and service_id = iGraveServiceID ;
        END IF ;
        
        return iRecordID ;
        
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_savegraveservicenote(igraveid integer, igraveserviceid integer, snote text) OWNER TO cuon_admin;

--
-- Name: fct_set_block(text, integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_set_block(tablename text, id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    DECLARE

	ok bool ;
	sSql text ;
	var_id text ;
	val int ;
	found_data int ;
	r record ;
	
    BEGIN

    if id = 0 then
       ok = true ;
     else 
	ok = false ;
	found_data = 0 ;
	
	sSql := 'select  ' ;

	if tablename = 'orderbook' then
	   var_id =  'order_id' ;
	elseif  tablename = 'invoice' then
	  var_id =  'invoice_id' ;
	elseif  tablename = 'in_payment' then
	 var_id =  'inpayment_id' ;
	elseif  tablename = 'cash_desk' then
	 var_id =  'cashdesk_id' ;
		
	end if;

	sSql = sSql  || var_id ;
	sSql = sSql || ' as lock_id  from lock_process where ' || var_id || ' = '  || id ;

	raise notice ' sSql =%' , sSql ;
	execute (sSql) into r;
	raise notice ' r.lock_id = %',r.lock_id ;
	
	if r.lock_id is null   then

	   ok = true ;
	   select into val  nextval ('lock_process_id') ;
	   sSql := ' insert into lock_process (id, ' || var_id || ',user_id) values ( '  || val || ', ' || id ||',' ||   fct_getUserDataClient(  )  || ')' ;
	   
	   
	   	raise notice ' sSql =%' , sSql ;	
		execute sSql ;

	end if ;
     end if ;
     raise notice ' ok = %' , ok ;
    	 
     

	return ok ;
	
    END ;
    	
     $$;


ALTER FUNCTION public.fct_set_block(tablename text, id integer) OWNER TO cuon_admin;

--
-- Name: fct_set_top_for_cash_desk(integer); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_set_top_for_cash_desk(iorderid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE
    iOK integer ;
    iClientID integer ;
    top_id integer ;
    orderinvoiceid integer ;
    sSql text ;
    newID integer ;

    BEGIN
	select into iClientID * from  fct_getUserDataClient();
	raise notice 'Client ID = %', iClientID ;

	select into top_id * from fct_get_config_option(iClientID, 'clients.ini', 'CLIENT_' || iClientID, 'cash_desk_top' ) ;
	
	iOK := top_id;
	raise notice 'iOK = %', iOK ;
	-- update orderinvoice set 
	if iOK is not null then 
		sSql = 'select  id from orderinvoice where orderid = ' ||  iOrderid || ' ' ||  fct_getWhere(2,' ')  ;
	
		execute sSql into orderinvoiceid ;
	raise notice 'sSql = %, %',sSql,orderinvoiceid ;
		if orderinvoiceid is null then 
		        raise notice 'Not found, do an insert ' ;
			select nextval('orderinvoice_id') into newID ;
                        insert into orderinvoice (id,uuid,orderid, order_top) values (newID,fct_new_uuid(),iOrderID,top_id)  ;
		else
			update orderinvoice set order_top = top_id where id = orderinvoiceid ;
			
     		END IF ;

	ELSE 
	     iOK = 0;
	END IF ;

        return iOK  ;
    END ;
    $$;


ALTER FUNCTION public.fct_set_top_for_cash_desk(iorderid integer) OWNER TO cuon_admin;

--
-- Name: fct_setallactualstock(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_setallactualstock() RETURNS boolean
    LANGUAGE plpgsql
    AS $$

 
    DECLARE
        sSql    text ;
        sSql2  text;
	actual_value float ;
    fActual_reserved float ;
    fActual_netto float;

    fTotal_sum float;
    fTotal_reserved float;
    fTotal_netto float;


	stockID integer ;
	articleID integer ;
	iID integer ;
	iMaxID integer ;
	i integer ;
	j integer ;
    ok bool ;

	actual_id integer ;
	last_stockvalue  float ;
    last_reservedvalue float ;
    last_nettovalue float ;
    this_reserved_bit integer;

	toEmbed float ;
	rollOut float ;
 	cur1 refcursor ;
	newStockvalue float ;
	
    BEGIN
	ok := true ;
	select into stockID max(id) from stocks ;
	raise notice ' max StockID = % ', stockID ;

	select into articleID max(id) from articles ;
	raise notice ' max articleID = % ', articleID ;

	select into iMaxID max(id) from stock_goods ;
	raise notice ' max goods ID = % ', iMaxID ;

   
	FOR i IN 1..stockID LOOP

	    FOR j in 1..articleID LOOP
	    	-- RAISE NOTICE 'i,j = % %',i,j ;
    
            fTotal_sum = fct_get_TotalSum(  j  , i ) ;
            fTotal_reserved = fct_get_ReservedSum( j,i) ;
 
		    sSql = 'select id from stock_goods where stock_id = ' || i || ' and article_id = ' || j ||  ' ' ||  fct_getWhere(2,'') || ' order by date_of_change, id ';
		    OPEN cur1 FOR EXECUTE sSql ;
        	FETCH cur1 INTO actual_id ;
		    WHILE FOUND LOOP
		        select into last_stockvalue fct_get_lastStockValue(j,i,actual_id);
                select into last_reservedvalue fct_get_lastReservedValue(j,i,actual_id);

		        if last_stockvalue is null then
	   	      	    last_stockvalue := 0.00 ;
		        end if ;
		        select into toEmbed, rollOut to_embed,roll_out from stock_goods where id = actual_id ;
		        if toEmbed is null then
	   	      	    toEmbed := 0.00 ;
		        end if ;

		        if rollOut is null then
	   	      	    rollOut := 0.00 ;
		        end if ;

		        -- RAISE NOTICE 'last value = %, actual_id = %, to_embed = % ',last_stockvalue, actual_id, toEmbed ;
		        newStockvalue =  last_stockvalue + toEmbed - rollOut ;
		        -- RAISE NOTICE 'last value = %, actual_id = %, to_embed = %, newStock = % ',last_stockvalue, actual_id, toEmbed, newStockvalue ;
			
		        update stock_goods set actual_stock = newStockvalue where id = actual_id ;  

                sSql2 := 'select reserved_bit from stock_goods where id = ' || actual_id ;
                execute sSql2 into this_reserved_bit ;

                raise notice 'this_reserved_bit = %', this_reserved_bit ;
                if this_reserved_bit IS NULL THEN
                    this_reserved_bit = 0;
                END IF ;

                if this_reserved_bit = 1 then 
                    update stock_goods set actual_reserved = last_reservedvalue +  toEmbed - rollOut  where id = actual_id ;
                else
                    update stock_goods set actual_reserved = last_reservedvalue  where id = actual_id  ;
                end if ;
                update stock_goods set actual_reserved = last_reservedvalue where id = actual_id   ;
                fActual_netto = newStockvalue - last_reservedvalue ; 
                update stock_goods set actual_netto = fActual_netto where id = actual_id   ;

                update stock_goods set total_sum = fTotal_sum where  article_id = j;
                update stock_goods set reserved_sum = fTotal_reserved where  article_id = j;
                fTotal_netto = fTotal_sum - fTotal_reserved ;
                update stock_goods set netto_sum = fTotal_netto where  article_id = j;


		       FETCH cur1 INTO actual_id ;
		    END LOOP ;
		close cur1 ;

	    END LOOP;

	END LOOP;
	
    	return ok ;	



   END ;
    $$;


ALTER FUNCTION public.fct_setallactualstock() OWNER TO cuon_admin;

--
-- Name: fct_setdmspairedid(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_setdmspairedid() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values at a delete 
    -- if delete a record, dont realy delete, set status to delete 
     
    DECLARE
    
        f_upd    text;
        r1 record ;
        sSql text ;
        
    BEGIN
        if NEW.paired_id is not null then 
        
            if NEW.paired_id >  0 then 
           
                sSql := 'select file_format, file_suffix, document_rights_activated, document_rights_user_read,  document_rights_user_write ,  document_rights_user_execute ,  document_rights_group_read  , document_rights_group_write , document_rights_group_execute, document_rights_all_read  , document_rights_all_write , document_rights_all_execute , document_rights_user , document_rights_groups , dms_extract from dms where id = ' || NEW.paired_id ;
                execute(sSql) into r1 ;
                RAISE NOTICE ' paired id dms = %', r1 ;
                
                NEW.file_format := r1.file_format ;
                NEW.file_suffix := r1.file_suffix ;
                NEW.document_image := NULL ;
                RAISE NOTICE ' paired suffix2 = %', NEW.file_suffix ;
            end if ;
        end if ;
        
        RETURN NEW ;
    END;
     
     $$;


ALTER FUNCTION public.fct_setdmspairedid() OWNER TO cuon_admin;

--
-- Name: fct_seterp1(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_seterp1() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 
     DECLARE
    sSql    text ;
    sSql2 text ;
    sSql3 text ;
    r record ;
    bOK bool ;
    recAddress record ;
    recOrder record ;
    orderAge int ;
    iClient int;

    sum_sales float;
    iSum int ;

    iSum_1 int;
    iSum_2 int;
    iSum_3 int;
    iSum_4 int;
    iSum_5 int;
    

    erp_orderbook_1 text;
    erp_orderbook_2 text;
    erp_orderbook_3 text;
    erp_orderbook_4 text;
    erp_orderbook_5 text;

    erp_proposal_1 text;
    erp_proposal_2 text;
    erp_proposal_3 text;    
    erp_proposal_4 text;    
    erp_proposal_5 text;
    
erp_address_newer_then_1 text;
erp_address_newer_then_2 text;
erp_address_newer_then_3 text;
erp_address_newer_then_4 text;


erp_schedule_newer_then_1 text;
erp_schedule_newer_then_2 text;
erp_schedule_newer_then_3 text;
erp_schedule_newer_then_4 text;

erp_orderbook_1_age int ;
erp_orderbook_1_sales int ;
erp_orderbook_1_erp int ;


erp_orderbook_2_age int ;
erp_orderbook_2_sales int ;
erp_orderbook_2_erp int ;


erp_orderbook_3_age int ;
erp_orderbook_3_sales int ;
erp_orderbook_3_erp int ;


erp_orderbook_4_age int ;
erp_orderbook_4_sales int ;
erp_orderbook_4_erp int ;


erp_orderbook_5_age int ;
erp_orderbook_5_sales int ;
erp_orderbook_5_erp int ;


erp_address_newer_then_1_age int;
erp_address_newer_then_1_erp  int;

erp_address_newer_then_2_age int;
erp_address_newer_then_2_erp  int;

erp_address_newer_then_3_age int;
erp_address_newer_then_3_erp  int;

erp_address_newer_then_4_age int;
erp_address_newer_then_4_erp  int;



    BEGIN
    bOK = false ;
    -- first get the config options
    
     iClient = fct_getUserDataClient(  ) ;
     erp_orderbook_1 = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_orderbook_1' ) ;
     raise notice 'erp_orderbook_1 = % ',erp_orderbook_1 ;
     erp_orderbook_1_age = split_part(erp_orderbook_1,',',1) ;
     
     erp_orderbook_1_sales  = split_part(erp_orderbook_1,',',2) ;
     erp_orderbook_1_erp = split_part(erp_orderbook_1,',',3)   ;
     raise notice 'erp_orderbook_1 values = %, % , % ',erp_orderbook_1_age,erp_orderbook_1_sales,erp_orderbook_1_erp ;

     erp_orderbook_2 = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_orderbook_2' ) ;

     erp_orderbook_2_age = split_part(erp_orderbook_2,',',1) ;
     erp_orderbook_2_sales  = split_part(erp_orderbook_2,',',2) ;
     erp_orderbook_2_erp = split_part(erp_orderbook_2,',',3)   ;


     erp_orderbook_3 = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_orderbook_3' ) ;

     erp_orderbook_3_age = split_part(erp_orderbook_3,',',1) ;
     erp_orderbook_3_sales  = split_part(erp_orderbook_3,',',2) ;
     erp_orderbook_3_erp = split_part(erp_orderbook_3,',',3)   ;



     erp_orderbook_4 = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_orderbook_4' ) ;

     erp_orderbook_4_age = split_part(erp_orderbook_4,',',1) ;
     erp_orderbook_4_sales  = split_part(erp_orderbook_4,',',2) ;
     erp_orderbook_4_erp = split_part(erp_orderbook_4,',',3)   ;


     erp_orderbook_5 = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_orderbook_5' ) ;

     erp_orderbook_5_age = split_part(erp_orderbook_5,',',1) ;
     erp_orderbook_5_sales  = split_part(erp_orderbook_5,',',2) ;
     erp_orderbook_5_erp = split_part(erp_orderbook_5,',',3)   ;


     -- address
     erp_address_newer_then_1 =  fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_address_newer_then_1' ) ;
     erp_address_newer_then_1_age =  split_part(erp_address_newer_then_1,',',1) ;
     erp_address_newer_then_1_erp =  split_part(erp_address_newer_then_1,',',2) ;

     raise notice 'erp_address_newer_then_1 = % ',erp_address_newer_then_1 ;
   erp_address_newer_then_2 =  fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_address_newer_then_2' ) ;
     erp_address_newer_then_2_age =  split_part(erp_address_newer_then_2,',',1) ;
     erp_address_newer_then_2_erp =  split_part(erp_address_newer_then_2,',',2) ;

   erp_address_newer_then_3 =  fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_address_newer_then_3' ) ;
     erp_address_newer_then_3_age =  split_part(erp_address_newer_then_3,',',1) ;
     erp_address_newer_then_3_erp =  split_part(erp_address_newer_then_3,',',2) ;

         
   erp_address_newer_then_4 =  fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, 'erp_address_newer_then_4' ) ;
     erp_address_newer_then_4_age =  split_part(erp_address_newer_then_4,',',1) ;
     erp_address_newer_then_4_erp =  split_part(erp_address_newer_then_4,',',2) ;


     -- clear the cuon_erp1 table
     delete from cuon_erp1 ;
 
    sSql = 'select id,insert_time  from address ' ||  fct_getWhere(1,' ') ;
    -- raise notice 'ssql = % ',sSql ;
    for recAddress in execute sSql
	 LOOP
	 -- set all erp_newsletter to 0
	 insert into cuon_erp1 (id, address_id, erp_newsletter) values (nextval('cuon_erp1_id'),recAddress.id,0);
	  -- raise notice 'id from address for erp1 =  %',recAddress.id ;
	  sSql2 = 'select id from orderbook where addressnumber = ' || recAddress.id || ' ' || fct_getWhere(2,' ') ;
	  -- raise notice 'ssql = % ',sSql2 ;
	  iSum = 0;
	  iSum_1 = 0;
	   iSum_2 = 0 ;
         iSum_3 = 0;
         iSum_4 =0;
         iSum_5 = 0;
    
	  for recOrder in execute sSql2
	      LOOP
      	     	-- raise notice 'Age of order %, id =% ',orderAge, recOrder.id ;		      
		sSql3 = 'select  * from fct_check_age_of_order(' || recOrder.id || ') ';
		execute sSql3 into orderAge ;
		
		if orderAge <=  erp_orderbook_5_age then
		   -- get the sales from this order
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_1_age*30)));
		   -- raise notice ' sum sales 1 = % ', sum_sales ;

	           iSum_1 = iSum_1 +  sum_sales::int ;
	           raise notice ' iSum 1 = % ', iSum_1 ;
		   
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_2_age*30)));
		   -- raise notice ' sum sales 2 = % ', sum_sales ;
		  
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_3_age*30)));
		   -- raise notice ' sum sales 3 = % ', sum_sales ;
		
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_4_age*30)));

		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_5_age*30)));
		   -- raise notice ' sum sales 5 = % ', sum_sales ;
	
		   
		end if ;
		
		END LOOP ;
		 if iSum_1 > erp_orderbook_1_sales then
		      iSum = iSum +  erp_orderbook_1_erp::int ;
		      raise notice ' iSum 1 = % ', iSum_1 ;
		   
		   end if ;
		   
		-- raise notice ' diff date = %',date(recAddress.insert_time) - date(current_date - erp_address_newer_then_1_age*30) ;
		-- raise notice ' diff date 2 = %',date(recAddress.insert_time) - date(current_date - erp_address_newer_then_2_age*30) ;
		if ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_1_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_1_erp::int ;
		   -- raise notice ' insert time 1 = % ', iSum ;
		   
		elsif ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_2_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_2_erp::int ;
		   -- raise notice ' insert time 2 = % ', iSum ;
		elsif ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_3_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_3_erp::int ;
		   -- raise notice ' insert time 3 = % ', iSum ;
		   
		elsif ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_4_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_4_erp::int ;
		   -- raise notice ' insert time 4 = % ', iSum ;
                end if  ;


		-- update cuon_erp1
		update cuon_erp1 set erp_newsletter = iSum where address_id = recAddress.id ;
		
	 END LOOP;
	     
	     
    
    	 
    return bOK  ;
    END ;
    $$;


ALTER FUNCTION public.fct_seterp1() OWNER TO cuon_admin;

--
-- Name: fct_setgardenschedule(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_setgardenschedule() RETURNS integer
    LANGUAGE plpgsql
    AS $$
 DECLARE
    iClient int ;
    iOK int := 1 ;
    r record;
    searchsql text := '';
    sSql1 text ;
    GraveServiceNotesAnnual int = 40105 ;
    
    BEGIN
       
        

        searchsql := 'select * from grave_work_year ' || fct_getWhere(1,'');
        -- raise notice 'search SQL = %', searchsql ;
        FOR r in execute(searchsql)  LOOP
         
        
            -- do it
            -- raise notice ' r.uuid = %', r.uuid ;
            if r.uuid is not null then 
                sSql1 = ' select * from fct_createModulSchedule(' || GraveServiceNotesAnnual || ', ' || quote_literal(r.uuid) || ') ' ;
                -- raise notice 'sSql1 SQL = %', sSql1 ;
                execute(sSql1) into iOK;
                -- raise notice ' ---------------------------------------------------------------------Result = %', iOK ;
            end if ;
        END LOOP ;
        return iOK ;
    END ;
    

    
     $$;


ALTER FUNCTION public.fct_setgardenschedule() OWNER TO cuon_admin;

--
-- Name: fct_setnumberanddescription(integer, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_setnumberanddescription(iid integer, stype text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
 DECLARE
    sSql text ;
    sSql2 text ;
    defaultOrderNumber text ;
    defaultOrderDesignation text;
    sNumber text ;
    sDefault text ;
    
    r record ;
    bOK bool ;
    iClient int ;
    sDB text ;
    sDBPositions text ;
    vMisc return_misc3 ;
    sDatabase return_text3;
    z1 int ;
    i text ;
    sON text ;
    sc text ;
    newSeq int ;
    fieldNumber text ;
    fieldAddressid text ;
    colname		TEXT;
    colcontent	TEXT;
    colnames	TEXT[];
    coln		INT4;
    coli		INT4;
    
    BEGIN
        bOK := true ;
        iClient = fct_getUserDataClient(  ) ;
        sc := '_client_' || iClient ;
     
   
        sDatabase = fct_get_database(sType);
        sDB = sDatabase.a ;
        sDBPositions = sDatabase.b ;
      
        fieldNumber := 'number' ;
        fieldAddressid := 'addressnumber' ;
        
        if sType = 'Proposal' then 
            sNumber = 'proposal_number' ;
            sDefault = 'proposal_designation';
        elseif sType = 'Enquiry' then 
            sNumber = 'enquiry_number';
            sDefault = 'enquiry_designation';
        elseif sType = 'Project' then
            sNumber = 'project_number';
            sDefault = 'project_designation' ; 
            fieldNumber := 'name' ;
            fieldAddressid := 'customer_id' ;
        else
            sNumber = 'orderbook_number';
            sDefault = 'orderbook_designation';
                
        end if ;
        defaultOrderNumber = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, sNumber ) ; 
        defaultOrderDesignation = fct_get_config_option(iClient,'clients.ini', 'CLIENT_' || iClient, sDefault) ; 
raise notice 'options = %, %', defaultOrderNumber, defaultOrderDesignation ;
        if defaultOrderNumber is not null then 
            z1 = 1 ;
            sON := '' ;
            <<SplitNumber>> 
            LOOP 
                select into i split_part(defaultOrderNumber, ',', z1);
                    EXIT  SplitNumber  WHEN char_length(i) = 0  ;
                    raise notice 'i = % ', i ;
                    if i = '!id' then 
                        
                       sON :=  sON || iID ;
                        
                        raise notice 'sON = % ', sON ;
                
                   elseif i='!year'then
                        sON :=  sON || date_part('year', now()) ;
                    elseif i='!month'then 
                        sON    :=  sON || date_part('month', now()) ;
                    elseif i='!day' then 
                        sON :=  sON || date_part('day', now()) ;
                    elseif  substring(i from 1 for 4 ) = '!seq' then
                        if sType = 'Enquiry'then 
                            sSql = 'select nextval(' || quote_literal('numerical_enquiry_enquirynumber' || sc ) || ' )' ;
                        elseif sType = 'Proposal'then 
                            sSql = 'select nextval(' || quote_literal('numerical_proposal_proposalnumber' || sc ) || ' )' ;
                        elseif sType = 'Order'then 
                            sSql = 'select nextval(' || quote_literal('numerical_orderbook_ordernumber' || sc ) || ' )' ;
                        elseif sType = 'Project'then 
                            sSql = 'select nextval(' || quote_literal('numerical_project_projectnumber' || sc ) || ' )' ;
                        end if ;
                        
                        raise notice ' sSql = %' ,  sSql ;
                        
                        execute (sSql) into newSeq ;
                        
                        if newSeq is not null then 
                            
                            sON := sON || to_char(newSeq,'FM99990000') ;
                        end if ;
                        
                    else 
                        sON := sON ||  i ;
                        
                    raise notice 'sON = % ',  sON ;
                    end if ;
                    
                z1 := z1 + 1 ;
                
           
            
            END LOOP  SplitNumber ;
        end if ;
        
         if char_length(sON) > 30 then 
            sON = substring(sON from 1 for 30) ;
        end if ;
        sSql := 'update ' || sDB || ' set ' || fieldNumber || ' = ' || quote_literal(sON) ||  ' where id = ' || iID ;
         -- raise notice ' sSql = %' ,  sSql ;
        execute(sSql);
            
            
        if defaultOrderDesignation is not null then 
            sSql := 'select * from address where id = ( select ' || fieldAddressid || ' from ' || sDB  || ' where id = ' || iID || ')' ;
            raise notice ' sSql = %' ,  sSql ;
            execute (sSql) into r ;
            
            -- raise notice 'colnames %', r;
            
            z1 := 1 ;
            sON = '' ;
            <<SplitDesignation>> 
            LOOP 
                select into i split_part(defaultOrderDesignation, ',', z1);
                EXIT  SplitDesignation WHEN char_length(i) = 0  ;
                if substring(i from 1 for 1 ) = '!' then 
                    if i = '!id' then 
                        sON := sON || r.id ;
                    elseif i = '!address' then 
                        sON := sON || r.address ;
                    elseif i = '!lastname' then 
                        sON := sON || r.lastname ;   
                    elseif i = '!lastname2' then 
                        sON := sON || r.lastname2 ;
                    elseif i = '!firstname' then 
                        sON := sON || r.firstname ;   
                    elseif i = '!street' then 
                        sON := sON || r.street ;   
                    elseif i = '!zip' then 
                        sON := sON || r.zip ;   
                    elseif i = '!city' then 
                        sON := sON || r.city ;   
                         
                    end if ;  
                        
                        
                        
                else 
                        sON := sON ||  i ;
                end if ;
                 --  raise notice 'sON = % ',  sON ;
                 
                z1 := z1 + 1 ;
                
                
            END LOOP  SplitDesignation ;
        
        end if ;
                
             -- raise notice 'sON Last = % ',  sON ;
        if char_length(sON) > 50 then 
            sON = substring(sON from 1 for 50) ;
        end if ;
        
        sSql := 'update ' || sDB || ' set designation  = ' || quote_literal(sON) ||  ' where id = ' || iID ;
         -- raise notice ' sSql = %' ,  sSql ;
        execute(sSql);
            
        return bOK ;
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_setnumberanddescription(iid integer, stype text) OWNER TO cuon_admin;

--
-- Name: fct_setuserdata(character[]); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_setuserdata(dicuser character[]) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
    -- set the userdata for the current work
     
    DECLARE
    sSql text ;
    sName char(50) ;    
    sClient char(10) ;
    sNoWhereClient char(10) ;
    sLocales char(10) ;
    SQLDateFormat char(20) ;
    SQLTimeFormat char(20) ;
    SQLDateTimeFormat char(20) ;
    
    
    iNextID     int ;

    BEGIN
        
        sName := dicUser[1];
        sClient := dicUser[2];
        sNoWhereClient := dicUser[3];
        sLocales  := dicUser[4];
        SQLDateFormat  := dicUser[5];
        SQLTimeFormat := dicUser[6];
        SQLDateTimeFormat := dicUser[7];
        

        execute 'select * from  fct_checkUserProfile(' ||  quote_literal(sName) || ', ' ||  quote_literal(sClient)||')' ;
        execute 'delete from cuon_user where username = ' || quote_literal(sName )  ;
        select into iNextID nextval from  nextval('cuon_user_id'); 

	sSql = 'insert into  cuon_user (id,username, current_client, no_where_client ,user_locales, user_sql_date_format,user_sql_time_format,user_sql_date_time_format ) VALUES (' || iNextID || ', ' || quote_literal(sName) || ',' || quote_literal(sClient)  || ',' || quote_literal(sNoWhereClient) ||',' || quote_literal( sLocales) ||',' ||  quote_literal(SQLDateFormat) || ',' ||  quote_literal(SQLTimeFormat) ||',' || quote_literal( SQLDateTimeFormat) || ' )' ;
	raise notice 'sSql = %',sSql ;
	execute (sSql);
      

        return 1 ;
    END ;
    
     $$;


ALTER FUNCTION public.fct_setuserdata(dicuser character[]) OWNER TO cuon_admin;

--
-- Name: fct_stock_goods_insert(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_stock_goods_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

  DECLARE
    f_upd     varchar(400);
    v_delete varchar(20) ;
    v_actual_stock float;
    
    BEGIN

	v_actual_stock =  fct_get_lastStockValue( NEW.article_id,NEW.stock_id,NEW.id);
	v_actual_stock = v_actual_stock + to_embed - roll_out ;
	
        f_upd := '  update ' || TG_RELNAME || ' set actual_stock = ' || v_actual_stock || '  where  id = ' || NEW.id   ;

        -- RAISE NOTICE ' table-name =  % ', TG_RELNAME ;
        -- RAISE NOTICE ' sql =  % ',f_upd  ;
        -- RAISE NOTICE ' Name =  % ', TG_NAME ;
        execute f_upd ;
        RETURN NEW ;
    END;


     $$;


ALTER FUNCTION public.fct_stock_goods_insert() OWNER TO cuon_admin;

--
-- Name: fct_test_array(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_test_array() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  DECLARE
    bRet bool := True ;
    vList integer[]  ;
    array_size integer ;
    ct integer ;
    BEGIN
    vList[1] := 1 ;
    vList[2] := 7 ;
    vList[3] := 9 ;
    vList[4] := 11 ;

    array_size := 4;
    for ct in 1..array_size loop
      raise notice ' ct = %, array = % ', ct,vList[ct] ;
    end loop;
    return bRet ;
    END ;

    $$;


ALTER FUNCTION public.fct_test_array() OWNER TO cuon_admin;

--
-- Name: fct_to_date(text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_to_date(sdate text) RETURNS date
    LANGUAGE plpgsql
    AS $$
    
    DECLARE
        dDate date ;
        sDateFormat text ;
        
    BEGIN
        select into sDateFormat type_c from cuon_values  where name = 'SQLDateFormat'  ;
        
        select into dDate to_date(sDate,sDateFormat);
        RETURN dDate ;
    END;
     
     $$;


ALTER FUNCTION public.fct_to_date(sdate text) OWNER TO cuon_admin;

--
-- Name: fct_update(); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    --  set default values at an update operation 
     
    DECLARE
        history_table   varchar(400) ;
        cName           varchar(300) ;
        sText text ;
        ri RECORD;
        q record ;
	t TEXT;
    sSql text ;
    sSql2 text ;
        sExe text ;
        sExe2 text ;
        len integer ;
        
    BEGIN
        history_table := TG_TABLE_NAME || '_history' ;
        
       if NEW.uuid is NULL OR char_length(NEW.uuid ) <36 then 
            NEW.uuid := fct_new_uuid()   ;
        end if ;
        
        if OLD.versions_uuid is NULL OR char_length(OLD.versions_uuid ) <36 then 
            OLD.versions_uuid := fct_new_uuid()   ;
        end if ;  
        if OLD.versions_number is NULL OR OLD.versions_number  < 1 then 
                OLD.versions_number := 1 ;
        end if ; 
 
	if OLD.status is NULL then 
	   OLD.status = 'insert';
	end if ;
	-- raise notice 'Old.status = %', OLD.status ;
        if not OLD.status = 'delete' then 
        
            sExe := ' insert into ' || history_table || ' ( ' ;
            sExe2 := ' select  ' ;
            sSql := ' SELECT ordinal_position, column_name, data_type  FROM information_schema.columns   WHERE    table_schema = ' || quote_literal(TG_TABLE_SCHEMA) || '  AND table_name = ' || quote_literal(TG_TABLE_NAME) || ' ORDER BY ordinal_position ' ;
            
            
            FOR ri in execute(sSql)  
               
                LOOP
                    
                   
                    sExe := sExe || ri.column_name || ', ' ;
                    sExe2 := sExe2 || ri.column_name || ', ' ;
                  
                    
                END LOOP;
            len := length(sExe);    
            sExe := substring(sExe from 1 for (len-2) ) ;
            sExe := sExe || ' ) ' ;
            
            len := length(sExe2);    
            sExe2 := substring(sExe2 from 1 for (len-2) ) ;
            
            
            
            sExe := sExe || sExe2 || ' from ' || quote_ident(TG_TABLE_NAME) || ' where id = ' || OLD.id ;
            raise notice ' sExe = %', sExe ;
            execute sExe ;
            
            NEW.update_user_id := current_user   ;
            NEW.update_time := (select  now()) ;
            NEW.user_id := OLD.user_id;
            
            NEW.insert_time := OLD.insert_time ;
            
           
            -- NEW.status := OLD.status ;
           
            
            NEW.versions_number := OLD.versions_number+1 ;
            NEW.versions_uuid :=OLD.versions_uuid ;
            IF TG_TABLE_NAME = 'list_of_invoices' THEN 
                NEW.last_transaction_uuid = fct_write_invoice(NEW.uuid) ;
               
              
            END IF ;
            
            -- raise notice 'new = % %', NEW.uuid, NEW.status ;
            
            RETURN NEW; 
        else 
	     -- raise notice 'return old = %',OLD.status ;
            RETURN OLD;
       
       end if ;
      
    END;
    
  
    $$;


ALTER FUNCTION public.fct_update() OWNER TO cuon_admin;

--
-- Name: fct_write_config_value(integer, character varying, character varying, character varying, text); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_write_config_value(iclientid integer, sfile character varying, cpsection character varying, soptions character varying, svalue text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
    DECLARE 
     r1 record ;
     ok boolean ;
    BEGIN 
        ok = true ;
        select into r1  key_id  from cuon_config where client_id = iClientID and config_file = sFile   and  section = cpSection and  option = sOptions ;
        IF r1.key_id IS NULL THEN 
            insert into cuon_config (client_id, config_file , section , option, value ) values (iClientID, sFile,cpSection,sOptions, sValue );
        ELSE
            update cuon_config set client_id = iClientID, config_file = sFile, section = cpSection, option = sOptions,value = sValue  where key_id = r1.key_id ;
            
        END IF ;
        return ok ;

    END; 

    
    $$;


ALTER FUNCTION public.fct_write_config_value(iclientid integer, sfile character varying, cpsection character varying, soptions character varying, svalue text) OWNER TO cuon_admin;

--
-- Name: fct_write_invoice(character varying); Type: FUNCTION; Schema: public; Owner: cuon_admin
--

CREATE FUNCTION public.fct_write_invoice(cuuid character varying) RETURNS text
    LANGUAGE plpgsql
    AS $$
 DECLARE
    
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    iNewAccountingID int ;
    sSql text ;
    sSql1 text ;
    
    sSql2 text ;
    account_revenue_id int;
    account_receivable_id int ;
    account_taxvat_id int ;
    account_revenue_net_id int := 0;
    
    cur1 refcursor ;
     sCursor text ;
    r record ;
    r1 record ;
    r2 record ;
    old_transaction_uuid varchar(36) ;
    new_transaction_uuid varchar(36) ;
      fAmount     float;
    fPrice  float;
    fDiscount   float ;
    count   integer ;
    fTaxVat float ;
    iArticleID integer ;
    bNet  bool ;
    vMisc return_misc3 ;
    iTaxVatID int ;
    fSum float ;
    fSumNet float ;
    fSumTaxVat float ;
    
    BEGIN
        raise notice 'write invoice for uuid %', cUUID ;
        
        iClient = fct_getUserDataClient(  ) ;
        -- sSql := 'select id  from account_info where accounting_system_type = ''2400'' ' || fct_getWhere(2,' ')  ;
        -- execute(sSql) into account_receivable_id ;
        account_receivable_id = fct_get_account_id(2400) ;
        
        -- sSql := 'select id  from account_info where accounting_system_type = ''5000'' ' || fct_getWhere(2,' ')  ;
        -- execute(sSql) into account_revenue_id ;
        account_revenue_id =  fct_get_account_id(5000) ;
        raise notice 'account info id = %',account_receivable_id ;
        old_transaction_uuid := fct_null_uuid() ;
        new_transaction_uuid := fct_new_uuid() ;
        Begin 
            select into r * from list_of_invoices where uuid = cUUID ;
            if account_revenue_id  is not null  AND account_receivable_id is not null THEN 
                if r.last_transaction_uuid != fct_null_uuid() then
                    raise notice ' uuid = %',r.last_transaction_uuid ;
                   
                    -- delete old accountings
                    sSql2 := ' select * from account_system where transaction_uuid = ' || quote_literal(r.last_transaction_uuid) ;
                    raise notice ' sSql2 = %', sSql2 ;
                    
                    FOR r2 in execute(sSql2) 
                        LOOP
                            sSql := 'select * from fct_create_single_account(' || iClient || ', ' || quote_literal(r.invoice_number )|| ', ' || quote_literal('Delete ' || r2.document_designation )  || ', ' || quote_literal(r.date_of_invoice) || ', ' || r2.account_id || ', ' || r2.contra_account_id || ', ' || r2.amount * -1  || ', ' ||  r2.address_id || ', ' || quote_literal(new_transaction_uuid ) || ' )  ' ;
                            execute(sSql) into iNewAccountingID ;
                            raise notice 'new accounting id at delete = % ', iNewAccountingID ;
                           
                            
                        END LOOP ;
                end if ;
                new_transaction_uuid := fct_new_uuid() ;
                select into r1 * from orderbook where id = r.order_number ;
                
                -- revenue to accounts receivable, 
                
                -- raise notice 'r = %', r ;
                -- raise notice 'r1 = %', r1 ;
                
               
                -- Now check the positions and the tax vat
                sSql1 := 'select * from fct_create_single_account(' || iClient || ', ' || quote_literal(r.invoice_number )|| ', ' || quote_literal( 'Invoice ' || ', ' || r.invoice_number  || ' , ' || r.date_of_invoice ) || ', ' || quote_literal(r.date_of_invoice) || ', ' || account_receivable_id || ', ' || account_revenue_id || ', ' || r.total_amount || ', ' ||  r1.addressnumber || ', ' || quote_literal(new_transaction_uuid ) || ' )  ' ;
                execute(sSql1) into iNewAccountingID ;
                raise notice 'new accounting id = % ', iNewAccountingID ;
                
                sCursor := 'SELECT amount, price, discount, articleid, tax_vat FROM ORDERPOSITION WHERE  orderid = '|| r.order_number || ' ' || fct_getWhere(2,' ')  ;
            fSum := 0.0 ;
            -- RAISE NOTICE 'sCursor = %', sCursor ;
            OPEN cur1 FOR EXECUTE sCursor ;
            FETCH cur1 INTO fAmount, fPrice, fDiscount,iArticleID, fTaxVat ;
               WHILE FOUND LOOP
                    raise notice 'Cursor = %, %, %, %, % ',  fAmount, fPrice, fDiscount,iArticleID, fTaxVat ;
                    if fDiscount IS NULL then
                        fDiscount := 0.0 ;
                    end if ;
                    if fTaxVat IS NULL then 
                        fTaxVat:= 0.00;
                    END IF;
                    if fPrice IS NULL then
                        fPrice := 0.0 ;
                    end if ;
                    vMisc := fct_get_taxvat_for_article(iArticleID);
                    
                    fTaxVat = vMisc.b ;
                    iTaxVatID = vMisc.a ;
                    
                    -- now search for brutto/netto
                    bNet := fct_get_net_for_article(iArticleID);
                    raise notice ' order bNet value is %',bNet ;
                    if fTaxVat > 0 THEN
                        if bNet = true then 
                        -- raise notice ' order calc as bnet is true';
                            fSum :=  ( fAmount * ( fPrice + (fPrice *fTaxVat/100) ) * (100 - fDiscount)/100 ) ;
                            fSumNet := ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                            fSumTaxVat := fSum -fSumNet ;
                            
                        else 
                            fSum :=  ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                            fSumNet :=  ( fAmount * ( fPrice - (fPrice/(fTaxVat+100) *100) ) * (100 - fDiscount)/100 ) ;
                            fSumTaxVat := fSum -fSumNet ;
                        end if ;
                        
                    ELSE    
                        fSum := ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                        fSumNet := fSum ;
                        fSumTaxVat := 0.00 ;
                        
                    END IF ;
                    
                    
                    raise notice 'sum, net, taxvat, tax_vat_id, tax_vat_acct = %, %, % , %, % ', fSum, fSumNet, fSumTaxVat, iTaxVatID, vMisc.c ;
                    
                    -- now set the accounts for the taxvat:
                    if iTaxVatID is null then 
                        iTaxVatID = 0;
                    end if ;
                    
                    if iTaxVatID > 0 AND fSumTaxVat > 0.00  then 
                        
                        -- first the taxvat account id
                        sSql := 'select id  from account_info where accounting_system_type = ' || quote_literal(vMisc.c) || fct_getWhere(2,' ')  ;
                        raise notice 'sSql = % ',sSql ;
                        
                        execute(sSql) into account_taxvat_id ;
                         sSql := 'select id  from account_info where accounting_system_type = ' || quote_literal(vMisc.c::int +2000) || fct_getWhere(2,' ')  ;
                        raise notice 'sSql = % ',sSql ;
                        execute(sSql) into account_revenue_net_id ;
                        
                        
                        -- First book the revenue to taxvat 
                        
                        sSql1 := 'select * from fct_create_single_account(' || iClient || ', ' || quote_literal(r.invoice_number )|| ', ' || quote_literal( 'TaxVat ' || ', ' || r.invoice_number  || ' , ' || r.date_of_invoice ) || ', ' || quote_literal(r.date_of_invoice) || ', ' || account_revenue_id || ', ' || account_taxvat_id  || ', ' || fSumTaxVat || ', ' ||  r1.addressnumber || ', ' || quote_literal(new_transaction_uuid ) || ' )  ' ;
                        raise notice ' book the taxvat = % ', sSql1 ;
                        
                        execute(sSql1) into iNewAccountingID ;
                        raise notice 'new accounting id = % ', iNewAccountingID ;
                        
                      
                        
                    else
                      -- set all to the null taxvat account
                      
                       -- first the taxvat account id
                        sSql := 'select id  from account_info where accounting_system_type = ' || quote_literal('38010') || fct_getWhere(2,' ')  ;
                        raise notice 'sSql = % ',sSql ;
                        
                        execute(sSql) into account_taxvat_id ;
                         sSql := 'select id  from account_info where accounting_system_type = ' || quote_literal('43010') || fct_getWhere(2,' ')  ;
                        raise notice 'sSql = % ',sSql ;
                        execute(sSql) into account_revenue_net_id ;
                        
                        
                      
                    end if ;
                    
                    raise notice 'net values = %', account_revenue_net_id ;
                    
                    -- Then set the net to the new revenue id
                    sSql1 := 'select * from fct_create_single_account(' || iClient || ', ' || quote_literal(r.invoice_number )|| ', ' || quote_literal( 'Net Value ' || ', ' || r.invoice_number  || ' , ' || r.date_of_invoice ) || ', ' || quote_literal(r.date_of_invoice) || ', ' || account_revenue_id || ', ' || account_revenue_net_id  || ', ' || fSumNet || ', ' ||  r1.addressnumber || ', ' || quote_literal(new_transaction_uuid ) || ' )  ' ;
                    
                    raise notice ' book the net = % ', sSql1 ;
                    
                    execute(sSql1) into iNewAccountingID ;
                    raise notice 'new accounting id = % ', iNewAccountingID ;
                
                  
                    FETCH cur1 INTO fAmount, fPrice, fDiscount ,iArticleID, fTaxVat;
                END LOOP ;
                
    
            END IF ;
        exception when others then
          raise notice 'exception error ';
          raise notice 'new transaction uuid = %', new_transaction_uuid ;
          return new_transaction_uuid ;
          END ;
       
       return new_transaction_uuid ;
        
    END ;
    
     $$;


ALTER FUNCTION public.fct_write_invoice(cuuid character varying) OWNER TO cuon_admin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_standard; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_standard (
    id integer NOT NULL,
    user_id character(25),
    status character(25),
    insert_time timestamp without time zone,
    update_time timestamp without time zone,
    update_user_id character varying(25),
    client integer DEFAULT 1,
    sep_info_1 integer DEFAULT 0,
    sep_info_2 integer DEFAULT 0,
    sep_info_3 integer DEFAULT 0,
    versions_number integer DEFAULT 0,
    versions_uuid character varying(36) DEFAULT '0'::character varying,
    uuid character varying(36) DEFAULT '0'::character varying
);


ALTER TABLE public.t_standard OWNER TO cuon_admin;

--
-- Name: account_info; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_info (
    account_number character varying(10),
    type character varying(10),
    designation character varying(250),
    eg character varying(10),
    account_plan_number integer,
    defaults character varying(20),
    accounting_system_type character varying(10),
    special_id_1 integer DEFAULT 0,
    special_id_2 integer DEFAULT 0,
    special_id_3 integer DEFAULT 0,
    special_id_4 integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.account_info OWNER TO cuon_admin;

--
-- Name: account_info_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_info_history (
    account_number character varying(10),
    type character varying(10),
    designation character varying(250),
    eg character varying(10),
    account_plan_number integer,
    defaults character varying(20),
    accounting_system_type character varying(10),
    special_id_1 integer DEFAULT 0,
    special_id_2 integer DEFAULT 0,
    special_id_3 integer DEFAULT 0,
    special_id_4 integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.account_info_history OWNER TO cuon_admin;

--
-- Name: account_info_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_info_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_info_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_info_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.account_info_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_info_id OWNER TO cuon_admin;

--
-- Name: account_info_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_info ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_plan; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_plan (
    name character varying(50),
    designation character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.account_plan OWNER TO cuon_admin;

--
-- Name: account_plan_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_plan_history (
    name character varying(50),
    designation character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.account_plan_history OWNER TO cuon_admin;

--
-- Name: account_plan_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_plan_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_plan_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_plan_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.account_plan_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_plan_id OWNER TO cuon_admin;

--
-- Name: account_plan_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_plan ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_plan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_sentence; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_sentence (
    document_number1 integer,
    document_number2 character varying(50),
    designation character varying(70),
    short_key character varying(10),
    automatic_calc boolean,
    accounting_date date,
    value_d1 numeric(10,2),
    value_c1 numeric(10,2),
    account_1 character varying(10),
    value_d2 numeric(10,2),
    value_c2 numeric(10,2),
    account_2 character varying(10),
    value_d3 numeric(10,2),
    value_c3 numeric(10,2),
    account_3 character varying(10),
    value_d4 numeric(10,2),
    value_c4 numeric(10,2),
    account_4 character varying(10),
    value_d5 numeric(10,2),
    value_c5 numeric(10,2),
    account_5 character varying(10),
    value_d6 numeric(10,2),
    value_c6 numeric(10,2),
    account_6 character varying(10),
    value_d7 numeric(10,2),
    value_c7 numeric(10,2),
    account_7 character varying(10),
    address_id integer,
    debitnr character varying(20),
    kreditnr character varying(20)
)
INHERITS (public.t_standard);


ALTER TABLE public.account_sentence OWNER TO cuon_admin;

--
-- Name: account_sentence_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_sentence_history (
    document_number1 integer,
    document_number2 character varying(50),
    designation character varying(70),
    short_key character varying(10),
    automatic_calc boolean,
    accounting_date date,
    value_d1 numeric(10,2),
    value_c1 numeric(10,2),
    account_1 character varying(10),
    value_d2 numeric(10,2),
    value_c2 numeric(10,2),
    account_2 character varying(10),
    value_d3 numeric(10,2),
    value_c3 numeric(10,2),
    account_3 character varying(10),
    value_d4 numeric(10,2),
    value_c4 numeric(10,2),
    account_4 character varying(10),
    value_d5 numeric(10,2),
    value_c5 numeric(10,2),
    account_5 character varying(10),
    value_d6 numeric(10,2),
    value_c6 numeric(10,2),
    account_6 character varying(10),
    value_d7 numeric(10,2),
    value_c7 numeric(10,2),
    account_7 character varying(10),
    address_id integer,
    debitnr character varying(20),
    kreditnr character varying(20)
)
INHERITS (public.t_standard);


ALTER TABLE public.account_sentence_history OWNER TO cuon_admin;

--
-- Name: account_sentence_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_sentence_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_sentence_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_sentence_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.account_sentence_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_sentence_id OWNER TO cuon_admin;

--
-- Name: account_sentence_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_sentence ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_sentence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_system; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_system (
    document_number character varying(36),
    document_designation character varying(255),
    account_id integer,
    contra_account_id integer,
    amount numeric(12,2),
    address_id integer,
    actual_date date,
    transaction_uuid character varying(36)
)
INHERITS (public.t_standard);


ALTER TABLE public.account_system OWNER TO cuon_admin;

--
-- Name: account_system_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.account_system_history (
    document_number character varying(36),
    document_designation character varying(255),
    account_id integer,
    contra_account_id integer,
    amount numeric(12,2),
    address_id integer,
    actual_date date,
    transaction_uuid character varying(36)
)
INHERITS (public.t_standard);


ALTER TABLE public.account_system_history OWNER TO cuon_admin;

--
-- Name: account_system_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_system_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_system_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_system_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.account_system_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_system_id OWNER TO cuon_admin;

--
-- Name: account_system_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.account_system ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_address; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_address (
    address character varying(30),
    lastname character varying(50),
    lastname2 character varying(50),
    firstname character varying(50),
    street character varying(50),
    country character varying(6),
    state character varying(6),
    zip character varying(10),
    city character varying(50),
    phone character varying(25),
    fax character varying(25),
    email character varying(100),
    zip_for_postbox character varying(10),
    postbox character varying(20),
    suburb character varying(50),
    state_full character varying(50),
    letter_address character varying(120),
    phone_handy character varying(35),
    email_noification character varying(35),
    newsletter character varying(17000),
    homepage_url character varying(250),
    status_info character varying(250),
    sip character varying(225),
    skype character varying(225),
    letter_address2 character varying(225),
    letter_address3 character varying(225),
    letter_address4 character varying(225),
    letter_address5 character varying(225)
)
INHERITS (public.t_standard);


ALTER TABLE public.t_address OWNER TO cuon_admin;

--
-- Name: address; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.address (
    webshop_address_id integer,
    webshop_zone_id integer,
    webshop_gender character varying(1),
    webshop_customers_id integer,
    caller_id integer,
    rep_id integer,
    salesman_id integer
)
INHERITS (public.t_address);


ALTER TABLE public.address OWNER TO cuon_admin;

--
-- Name: address_bank; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.address_bank (
    address_id integer,
    bank_id integer,
    depositor character varying(80),
    account_number character varying(40),
    iban character varying(80),
    bank_ranking integer DEFAULT 1
)
INHERITS (public.t_standard);


ALTER TABLE public.address_bank OWNER TO cuon_admin;

--
-- Name: address_bank_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.address_bank_history (
    address_id integer,
    bank_id integer,
    depositor character varying(80),
    account_number character varying(40),
    iban character varying(80),
    bank_ranking integer DEFAULT 1
)
INHERITS (public.t_standard);


ALTER TABLE public.address_bank_history OWNER TO cuon_admin;

--
-- Name: address_bank_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.address_bank_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.address_bank_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: address_bank_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.address_bank_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_bank_id OWNER TO cuon_admin;

--
-- Name: address_bank_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.address_bank ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.address_bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: address_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.address_history (
    webshop_address_id integer,
    webshop_zone_id integer,
    webshop_gender character varying(1),
    webshop_customers_id integer,
    caller_id integer,
    rep_id integer,
    salesman_id integer
)
INHERITS (public.t_address);


ALTER TABLE public.address_history OWNER TO cuon_admin;

--
-- Name: address_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.address_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.address_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: address_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.address_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id OWNER TO cuon_admin;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.address ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: address_notes; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.address_notes (
    address_id integer,
    notes_misc text,
    notes_contacter text,
    notes_representant text,
    notes_salesman text,
    notes_organisation text
)
INHERITS (public.t_standard);


ALTER TABLE public.address_notes OWNER TO cuon_admin;

--
-- Name: address_notes_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.address_notes_history (
    address_id integer,
    notes_misc text,
    notes_contacter text,
    notes_representant text,
    notes_salesman text,
    notes_organisation text
)
INHERITS (public.t_standard);


ALTER TABLE public.address_notes_history OWNER TO cuon_admin;

--
-- Name: address_notes_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.address_notes_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.address_notes_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: address_notes_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.address_notes_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_notes_id OWNER TO cuon_admin;

--
-- Name: address_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.address_notes ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.address_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: addresses_misc; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.addresses_misc (
    address_id integer,
    line character(35),
    fashion character(35),
    taxnumber character(35),
    to_calc_tax boolean,
    price_group character(35),
    address_number character(55),
    is_webshop boolean,
    update_from_webshop boolean,
    top_id integer,
    date_of_launch date,
    legal_form integer,
    turnover integer,
    count_of_employees integer,
    trade integer,
    cb_fashion integer,
    additional_emails text,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    allow_direct_debit boolean
)
INHERITS (public.t_standard);


ALTER TABLE public.addresses_misc OWNER TO cuon_admin;

--
-- Name: addresses_misc_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.addresses_misc_history (
    address_id integer,
    line character(35),
    fashion character(35),
    taxnumber character(35),
    to_calc_tax boolean,
    price_group character(35),
    address_number character(55),
    is_webshop boolean,
    update_from_webshop boolean,
    top_id integer,
    date_of_launch date,
    legal_form integer,
    turnover integer,
    count_of_employees integer,
    trade integer,
    cb_fashion integer,
    additional_emails text,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    allow_direct_debit boolean
)
INHERITS (public.t_standard);


ALTER TABLE public.addresses_misc_history OWNER TO cuon_admin;

--
-- Name: addresses_misc_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.addresses_misc_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.addresses_misc_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: addresses_misc_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.addresses_misc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addresses_misc_id OWNER TO cuon_admin;

--
-- Name: addresses_misc_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.addresses_misc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.addresses_misc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: administrative_district; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.administrative_district (
    state_id integer,
    name character varying(160),
    ar_short character varying(6)
)
INHERITS (public.t_standard);


ALTER TABLE public.administrative_district OWNER TO cuon_admin;

--
-- Name: administrative_district_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.administrative_district_history (
    state_id integer,
    name character varying(160),
    ar_short character varying(6)
)
INHERITS (public.t_standard);


ALTER TABLE public.administrative_district_history OWNER TO cuon_admin;

--
-- Name: administrative_district_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.administrative_district_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administrative_district_id OWNER TO cuon_admin;

--
-- Name: articles; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles (
    number character varying(150),
    designation character varying(250),
    wrapping character varying(15),
    quantumperwrap double precision,
    unit character varying(15),
    manufactor_id integer,
    weight double precision,
    sellingprice1 double precision,
    sellingprice2 double precision,
    sellingprice3 double precision,
    sellingprice4 double precision,
    tax_vat character varying(20),
    material_group integer,
    associated_with integer,
    associated_id integer,
    tax_vat_id integer,
    articles_notes text,
    norm character varying(100),
    drawing_number character varying(100),
    material character varying(100),
    min_stock double precision DEFAULT 1.00,
    min_order_stock double precision DEFAULT 1.00,
    prefered_stock_place_id integer,
    allow_minus_values boolean DEFAULT false
)
INHERITS (public.t_standard);


ALTER TABLE public.articles OWNER TO cuon_admin;

--
-- Name: articles_barcode; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_barcode (
    barcode character varying(250),
    article_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_barcode OWNER TO cuon_admin;

--
-- Name: articles_barcode_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_barcode_history (
    barcode character varying(250),
    article_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_barcode_history OWNER TO cuon_admin;

--
-- Name: articles_barcode_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_barcode_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_barcode_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_barcode_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_barcode_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_barcode_id OWNER TO cuon_admin;

--
-- Name: articles_barcode_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_barcode ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_barcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_customers; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_customers (
    article_id integer,
    customers_id integer,
    customers_article_number character varying(100),
    customers_article_designation character varying(255)
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_customers OWNER TO cuon_admin;

--
-- Name: articles_customers_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_customers_history (
    article_id integer,
    customers_id integer,
    customers_article_number character varying(100),
    customers_article_designation character varying(255)
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_customers_history OWNER TO cuon_admin;

--
-- Name: articles_customers_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_customers_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_customers_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_customers_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_customers_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_customers_id OWNER TO cuon_admin;

--
-- Name: articles_customers_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_customers ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_history (
    number character varying(150),
    designation character varying(250),
    wrapping character varying(15),
    quantumperwrap double precision,
    unit character varying(15),
    manufactor_id integer,
    weight double precision,
    sellingprice1 double precision,
    sellingprice2 double precision,
    sellingprice3 double precision,
    sellingprice4 double precision,
    tax_vat character varying(20),
    material_group integer,
    associated_with integer,
    associated_id integer,
    tax_vat_id integer,
    articles_notes text,
    norm character varying(100),
    drawing_number character varying(100),
    material character varying(100),
    min_stock double precision DEFAULT 1.00,
    min_order_stock double precision DEFAULT 1.00,
    prefered_stock_place_id integer,
    allow_minus_values boolean DEFAULT false
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_history OWNER TO cuon_admin;

--
-- Name: articles_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_id OWNER TO cuon_admin;

--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_parts_list; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_parts_list (
    article_id integer,
    part_id integer,
    quantities double precision,
    designation character varying(60),
    position_number character varying(30),
    calc_percent double precision,
    calc_absolute double precision,
    parts_note text
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_parts_list OWNER TO cuon_admin;

--
-- Name: articles_parts_list_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_parts_list_history (
    article_id integer,
    part_id integer,
    quantities double precision,
    designation character varying(60),
    position_number character varying(30),
    calc_percent double precision,
    calc_absolute double precision,
    parts_note text
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_parts_list_history OWNER TO cuon_admin;

--
-- Name: articles_parts_list_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_parts_list_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_parts_list_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_parts_list_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_parts_list_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_parts_list_id OWNER TO cuon_admin;

--
-- Name: articles_parts_list_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_parts_list ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_parts_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_purchase; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_purchase (
    articles_number character varying(30),
    designation character varying(60),
    addressid integer,
    unitprice double precision,
    discount double precision,
    minquantum double precision,
    vendorsnumber character varying(35),
    vendorsdesignation character varying(55),
    last_date date,
    articles_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_purchase OWNER TO cuon_admin;

--
-- Name: articles_purchase_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_purchase_history (
    articles_number character varying(30),
    designation character varying(60),
    addressid integer,
    unitprice double precision,
    discount double precision,
    minquantum double precision,
    vendorsnumber character varying(35),
    vendorsdesignation character varying(55),
    last_date date,
    articles_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_purchase_history OWNER TO cuon_admin;

--
-- Name: articles_purchase_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_purchase_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_purchase_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_purchase_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_purchase_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_purchase_id OWNER TO cuon_admin;

--
-- Name: articles_purchase_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_purchase ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_purchase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_sales; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_sales (
    articles_number character varying(30),
    sellingprice1 double precision,
    sellingprice2 double precision,
    sellingprice3 double precision,
    sellingprice4 double precision,
    designation character varying(30),
    tax_vat character varying(20),
    tax_vat2 integer,
    pic_dms_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_sales OWNER TO cuon_admin;

--
-- Name: articles_sales_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_sales_history (
    articles_number character varying(30),
    sellingprice1 double precision,
    sellingprice2 double precision,
    sellingprice3 double precision,
    sellingprice4 double precision,
    designation character varying(30),
    tax_vat character varying(20),
    tax_vat2 integer,
    pic_dms_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_sales_history OWNER TO cuon_admin;

--
-- Name: articles_sales_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_sales_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_sales_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_sales_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_sales_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_sales_id OWNER TO cuon_admin;

--
-- Name: articles_sales_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_sales ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_sales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_stock; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_stock (
    articles_number character varying(30),
    minimum_stock double precision DEFAULT 0,
    maximum_stock double precision DEFAULT 0,
    is_check_stock boolean,
    articles_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_stock OWNER TO cuon_admin;

--
-- Name: articles_stock_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_stock_history (
    articles_number character varying(30),
    minimum_stock double precision DEFAULT 0,
    maximum_stock double precision DEFAULT 0,
    is_check_stock boolean,
    articles_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_stock_history OWNER TO cuon_admin;

--
-- Name: articles_stock_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_stock_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_stock_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_stock_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_stock_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_stock_id OWNER TO cuon_admin;

--
-- Name: articles_stock_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_stock ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_webshop; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_webshop (
    articles_number character varying(30),
    is_webshop_stock boolean,
    is_webshop_article boolean,
    pricegroup integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_webshop OWNER TO cuon_admin;

--
-- Name: articles_webshop_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.articles_webshop_history (
    articles_number character varying(30),
    is_webshop_stock boolean,
    is_webshop_article boolean,
    pricegroup integer
)
INHERITS (public.t_standard);


ALTER TABLE public.articles_webshop_history OWNER TO cuon_admin;

--
-- Name: articles_webshop_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_webshop_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_webshop_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: articles_webshop_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.articles_webshop_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_webshop_id OWNER TO cuon_admin;

--
-- Name: articles_webshop_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.articles_webshop ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.articles_webshop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: bank; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.bank (
    address_id integer,
    bcn character varying(35),
    short_designation character varying(255),
    pan character varying(70),
    bic character varying(70)
)
INHERITS (public.t_standard);


ALTER TABLE public.bank OWNER TO cuon_admin;

--
-- Name: bank_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.bank_history (
    address_id integer,
    bcn character varying(35),
    short_designation character varying(255),
    pan character varying(70),
    bic character varying(70)
)
INHERITS (public.t_standard);


ALTER TABLE public.bank_history OWNER TO cuon_admin;

--
-- Name: bank_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.bank_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.bank_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: bank_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.bank_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_id OWNER TO cuon_admin;

--
-- Name: bank_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.bank ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.bank_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: biblio; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.biblio (
    title character varying(250),
    designation character varying(250),
    author character varying(250),
    publisher character varying(150),
    series character varying(150),
    year integer,
    month integer,
    isbn character varying(70),
    description text,
    page integer,
    bib_type character varying(30)
)
INHERITS (public.t_standard);


ALTER TABLE public.biblio OWNER TO cuon_admin;

--
-- Name: biblio_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.biblio_history (
    title character varying(250),
    designation character varying(250),
    author character varying(250),
    publisher character varying(150),
    series character varying(150),
    year integer,
    month integer,
    isbn character varying(70),
    description text,
    page integer,
    bib_type character varying(30)
)
INHERITS (public.t_standard);


ALTER TABLE public.biblio_history OWNER TO cuon_admin;

--
-- Name: biblio_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.biblio_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.biblio_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: biblio_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.biblio_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.biblio_id OWNER TO cuon_admin;

--
-- Name: biblio_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.biblio ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.biblio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany (
    botany_name character varying(160),
    genus_id integer,
    local_name character varying(160),
    description character varying(240),
    habitat character varying(200),
    article_id integer,
    tips text
)
INHERITS (public.t_standard);


ALTER TABLE public.botany OWNER TO cuon_admin;

--
-- Name: botany_class; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_class (
    name character varying(240),
    description character varying(240),
    divisio_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_class OWNER TO cuon_admin;

--
-- Name: botany_class_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_class_history (
    name character varying(240),
    description character varying(240),
    divisio_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_class_history OWNER TO cuon_admin;

--
-- Name: botany_class_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_class_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_class_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_class_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_class_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_class_id OWNER TO cuon_admin;

--
-- Name: botany_class_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_class ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_divisio; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_divisio (
    name character varying(240),
    description character varying(240),
    local_name character varying(160),
    kingdom_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_divisio OWNER TO cuon_admin;

--
-- Name: botany_divisio_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_divisio_history (
    name character varying(240),
    description character varying(240),
    local_name character varying(160),
    kingdom_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_divisio_history OWNER TO cuon_admin;

--
-- Name: botany_divisio_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_divisio_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_divisio_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_divisio_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_divisio_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_divisio_id OWNER TO cuon_admin;

--
-- Name: botany_divisio_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_divisio ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_divisio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_family; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_family (
    name character varying(240),
    description character varying(240),
    ordo_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_family OWNER TO cuon_admin;

--
-- Name: botany_family_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_family_history (
    name character varying(240),
    description character varying(240),
    ordo_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_family_history OWNER TO cuon_admin;

--
-- Name: botany_family_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_family_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_family_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_family_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_family_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_family_id OWNER TO cuon_admin;

--
-- Name: botany_family_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_family ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_family_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_genus; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_genus (
    name character varying(240),
    description character varying(240),
    family_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_genus OWNER TO cuon_admin;

--
-- Name: botany_genus_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_genus_history (
    name character varying(240),
    description character varying(240),
    family_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_genus_history OWNER TO cuon_admin;

--
-- Name: botany_genus_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_genus_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_genus_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_genus_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_genus_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_genus_id OWNER TO cuon_admin;

--
-- Name: botany_genus_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_genus ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_genus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_history (
    botany_name character varying(160),
    genus_id integer,
    local_name character varying(160),
    description character varying(240),
    habitat character varying(200),
    article_id integer,
    tips text
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_history OWNER TO cuon_admin;

--
-- Name: botany_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_id OWNER TO cuon_admin;

--
-- Name: botany_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_kingdom; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_kingdom (
    name character varying(240),
    description character varying(240)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_kingdom OWNER TO cuon_admin;

--
-- Name: botany_kingdom_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_kingdom_history (
    name character varying(240),
    description character varying(240)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_kingdom_history OWNER TO cuon_admin;

--
-- Name: botany_kingdom_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_kingdom_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_kingdom_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_kingdom_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_kingdom_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_kingdom_id OWNER TO cuon_admin;

--
-- Name: botany_kingdom_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_kingdom ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_kingdom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_ordo; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_ordo (
    name character varying(240),
    description character varying(240),
    class_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_ordo OWNER TO cuon_admin;

--
-- Name: botany_ordo_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.botany_ordo_history (
    name character varying(240),
    description character varying(240),
    class_id integer,
    local_name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.botany_ordo_history OWNER TO cuon_admin;

--
-- Name: botany_ordo_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_ordo_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_ordo_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: botany_ordo_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.botany_ordo_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.botany_ordo_id OWNER TO cuon_admin;

--
-- Name: botany_ordo_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.botany_ordo ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.botany_ordo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cash_desk; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cash_desk (
    cash_desk_number integer DEFAULT 0,
    order_id integer,
    cash_time time without time zone,
    cash_desk_user_short_cut character varying(4),
    order_sum double precision,
    incomming_total double precision,
    cash_date date,
    cash_procedure integer DEFAULT 0,
    description character varying(255),
    address_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.cash_desk OWNER TO cuon_admin;

--
-- Name: cash_desk_book_number; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cash_desk_book_number (
    special_id integer,
    real_timestamp timestamp without time zone,
    r_total_sum double precision,
    r_total_plus_sum double precision,
    r_total_minus_sum double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.cash_desk_book_number OWNER TO cuon_admin;

--
-- Name: cash_desk_book_number_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cash_desk_book_number_history (
    special_id integer,
    real_timestamp timestamp without time zone,
    r_total_sum double precision,
    r_total_plus_sum double precision,
    r_total_minus_sum double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.cash_desk_book_number_history OWNER TO cuon_admin;

--
-- Name: cash_desk_book_number_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cash_desk_book_number_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cash_desk_book_number_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cash_desk_book_number_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cash_desk_book_number_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cash_desk_book_number_id OWNER TO cuon_admin;

--
-- Name: cash_desk_book_number_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cash_desk_book_number ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cash_desk_book_number_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cash_desk_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cash_desk_history (
    cash_desk_number integer DEFAULT 0,
    order_id integer,
    cash_time time without time zone,
    cash_desk_user_short_cut character varying(4),
    order_sum double precision,
    incomming_total double precision,
    cash_date date,
    cash_procedure integer DEFAULT 0,
    description character varying(255),
    address_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.cash_desk_history OWNER TO cuon_admin;

--
-- Name: cash_desk_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cash_desk_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cash_desk_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cash_desk_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cash_desk_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cash_desk_id OWNER TO cuon_admin;

--
-- Name: cash_desk_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cash_desk ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cash_desk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: city; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.city (
    country_id integer,
    state_id integer,
    ar_id integer,
    district_id integer,
    city character varying(65),
    longitude double precision,
    latitude double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.city OWNER TO cuon_admin;

--
-- Name: city_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.city_history (
    country_id integer,
    state_id integer,
    ar_id integer,
    district_id integer,
    city character varying(65),
    longitude double precision,
    latitude double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.city_history OWNER TO cuon_admin;

--
-- Name: city_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.city_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.city_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: city_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.city_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.city_id OWNER TO cuon_admin;

--
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.city ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: clients; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.clients (
    name character varying(50),
    designation character varying(250),
    address_id integer,
    client_number character varying(25),
    account_plan_number integer,
    client_uuid character(36),
    client_schedul_time text[]
)
INHERITS (public.t_standard);


ALTER TABLE public.clients OWNER TO cuon_admin;

--
-- Name: clients_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.clients_history (
    name character varying(50),
    designation character varying(250),
    address_id integer,
    client_number character varying(25),
    account_plan_number integer,
    client_uuid character(36),
    client_schedul_time text[]
)
INHERITS (public.t_standard);


ALTER TABLE public.clients_history OWNER TO cuon_admin;

--
-- Name: clients_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.clients_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.clients_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: clients_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.clients_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id OWNER TO cuon_admin;

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.clients ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_partner_schedul; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_partner_schedul (
    partnerid integer,
    schedul_date character varying(10),
    schedul_time_begin integer,
    schedul_time_end integer,
    priority integer DEFAULT 0,
    process_status integer DEFAULT 1,
    notes text,
    short_remark character varying(60),
    alarm_notify boolean,
    alarm_time integer,
    alarm_days integer,
    alarm_hours integer,
    alarm_minutes integer,
    schedul_staff_id integer DEFAULT 1,
    schedul_date_end character varying(10),
    dschedul_date date,
    dschedul_date_end date,
    begin_end_text character varying(50),
    begin_text character varying(5),
    end_text character varying(5)
)
INHERITS (public.t_standard);


ALTER TABLE public.t_partner_schedul OWNER TO cuon_admin;

--
-- Name: contact; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.contact (
    address_id integer,
    contacter_id integer
)
INHERITS (public.t_partner_schedul);


ALTER TABLE public.contact OWNER TO cuon_admin;

--
-- Name: contact_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.contact_history (
    address_id integer,
    contacter_id integer
)
INHERITS (public.t_partner_schedul);


ALTER TABLE public.contact_history OWNER TO cuon_admin;

--
-- Name: contact_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.contact_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.contact_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: contact_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.contact_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_id OWNER TO cuon_admin;

--
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.contact ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: country; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.country (
    planet_id integer,
    name character varying(100),
    short_name character varying(8),
    name2 character varying(100),
    iso_code_2 character varying(2),
    iso_code_3 character varying(3),
    format_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.country OWNER TO cuon_admin;

--
-- Name: country_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.country_history (
    planet_id integer,
    name character varying(100),
    short_name character varying(8),
    name2 character varying(100),
    iso_code_2 character varying(2),
    iso_code_3 character varying(3),
    format_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.country_history OWNER TO cuon_admin;

--
-- Name: country_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.country_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.country_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: country_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.country_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id OWNER TO cuon_admin;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.country ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon (
    skey character varying(255) NOT NULL,
    svalue text NOT NULL,
    uuid character varying(36)
);


ALTER TABLE public.cuon OWNER TO cuon_admin;

--
-- Name: cuon_clients; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_clients (
    name character varying(50),
    version character varying(50),
    clientdata text,
    cpu character varying(20),
    clientid character varying(120)
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_clients OWNER TO cuon_admin;

--
-- Name: cuon_clients_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_clients_history (
    name character varying(50),
    version character varying(50),
    clientdata text,
    cpu character varying(20),
    clientid character varying(120)
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_clients_history OWNER TO cuon_admin;

--
-- Name: cuon_clients_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_clients_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_clients_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_clients_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_clients_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_clients_id OWNER TO cuon_admin;

--
-- Name: cuon_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_clients ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_config; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_config (
    client_id integer DEFAULT 0,
    config_file character varying(200),
    section character varying(250),
    option character varying(250),
    value text,
    key_id integer NOT NULL
);


ALTER TABLE public.cuon_config OWNER TO cuon_admin;

--
-- Name: cuon_config_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_config_history (
    client_id integer DEFAULT 0,
    config_file character varying(200),
    section character varying(250),
    option character varying(250),
    value text,
    key_id integer NOT NULL
);


ALTER TABLE public.cuon_config_history OWNER TO cuon_admin;

--
-- Name: cuon_config_history_key_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_config_history_key_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_config_history_key_id_seq OWNER TO cuon_admin;

--
-- Name: cuon_config_history_key_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cuon_admin
--

ALTER SEQUENCE public.cuon_config_history_key_id_seq OWNED BY public.cuon_config_history.key_id;


--
-- Name: cuon_config_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_config_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_config_id OWNER TO cuon_admin;

--
-- Name: cuon_config_key_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_config_key_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_config_key_id_seq OWNER TO cuon_admin;

--
-- Name: cuon_config_key_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cuon_admin
--

ALTER SEQUENCE public.cuon_config_key_id_seq OWNED BY public.cuon_config.key_id;


--
-- Name: cuon_erp1; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_erp1 (
    address_id integer NOT NULL,
    erp_newsletter integer
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_erp1 OWNER TO cuon_admin;

--
-- Name: cuon_erp1_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_erp1_history (
    address_id integer NOT NULL,
    erp_newsletter integer
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_erp1_history OWNER TO cuon_admin;

--
-- Name: cuon_erp1_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_erp1_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_erp1_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_erp1_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_erp1_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_erp1_id OWNER TO cuon_admin;

--
-- Name: cuon_erp1_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_erp1 ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_erp1_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_user; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_user (
    username character varying(50),
    current_client integer,
    no_where_client integer,
    user_locales character varying(10),
    user_sql_date_format character varying(20),
    user_sql_time_format character varying(20),
    user_sql_date_time_format character varying(20)
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_user OWNER TO cuon_admin;

--
-- Name: cuon_user_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_user_history (
    username character varying(50),
    current_client integer,
    no_where_client integer,
    user_locales character varying(10),
    user_sql_date_format character varying(20),
    user_sql_time_format character varying(20),
    user_sql_date_time_format character varying(20)
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_user_history OWNER TO cuon_admin;

--
-- Name: cuon_user_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_user_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_user_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_user_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_user_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_user_id OWNER TO cuon_admin;

--
-- Name: cuon_user_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_user ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_values; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_values (
    name character varying(50),
    type_c character varying(150),
    type_i integer,
    type_f double precision,
    type_d date
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_values OWNER TO cuon_admin;

--
-- Name: cuon_values_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.cuon_values_history (
    name character varying(50),
    type_c character varying(150),
    type_i integer,
    type_f double precision,
    type_d date
)
INHERITS (public.t_standard);


ALTER TABLE public.cuon_values_history OWNER TO cuon_admin;

--
-- Name: cuon_values_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_values_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_values_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: cuon_values_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.cuon_values_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuon_values_id OWNER TO cuon_admin;

--
-- Name: cuon_values_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.cuon_values ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuon_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: district; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.district (
    ar_id integer,
    name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.district OWNER TO cuon_admin;

--
-- Name: district_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.district_history (
    ar_id integer,
    name character varying(160)
)
INHERITS (public.t_standard);


ALTER TABLE public.district_history OWNER TO cuon_admin;

--
-- Name: district_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.district_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.district_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: district_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.district_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.district_id OWNER TO cuon_admin;

--
-- Name: district_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.district ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.district_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: dms; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.dms (
    title character varying(85),
    category character varying(85),
    size_x integer,
    size_y integer,
    document_image text,
    sub1 character varying(85),
    sub2 character varying(85),
    sub3 character varying(85),
    sub4 character varying(85),
    sub5 character varying(85),
    search1 character varying(250),
    search2 character varying(250),
    search3 character varying(250),
    search4 character varying(250),
    file_format character varying(50),
    insert_from character varying(50),
    insert_at_date timestamp without time zone,
    insert_from_module integer DEFAULT 1,
    file_suffix character varying(50),
    document_date date,
    document_rights_activated boolean,
    document_rights_user_read boolean,
    document_rights_user_write boolean,
    document_rights_user_execute boolean,
    document_rights_group_read boolean,
    document_rights_group_write boolean,
    document_rights_group_execute boolean,
    document_rights_all_read boolean,
    document_rights_all_write boolean,
    document_rights_all_execute boolean,
    document_rights_user character varying(250),
    document_rights_groups character varying(250),
    dms_extract text,
    paired_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.dms OWNER TO cuon_admin;

--
-- Name: dms_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.dms_history (
    title character varying(85),
    category character varying(85),
    size_x integer,
    size_y integer,
    document_image text,
    sub1 character varying(85),
    sub2 character varying(85),
    sub3 character varying(85),
    sub4 character varying(85),
    sub5 character varying(85),
    search1 character varying(250),
    search2 character varying(250),
    search3 character varying(250),
    search4 character varying(250),
    file_format character varying(50),
    insert_from character varying(50),
    insert_at_date timestamp without time zone,
    insert_from_module integer DEFAULT 1,
    file_suffix character varying(50),
    document_date date,
    document_rights_activated boolean,
    document_rights_user_read boolean,
    document_rights_user_write boolean,
    document_rights_user_execute boolean,
    document_rights_group_read boolean,
    document_rights_group_write boolean,
    document_rights_group_execute boolean,
    document_rights_all_read boolean,
    document_rights_all_write boolean,
    document_rights_all_execute boolean,
    document_rights_user character varying(250),
    document_rights_groups character varying(250),
    dms_extract text,
    paired_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.dms_history OWNER TO cuon_admin;

--
-- Name: dms_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.dms_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.dms_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: dms_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.dms_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dms_id OWNER TO cuon_admin;

--
-- Name: dms_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.dms ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.dms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiry; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiry (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_number character varying(60),
    enquiry_from date,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60)
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiry OWNER TO cuon_admin;

--
-- Name: enquiry_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiry_history (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_number character varying(60),
    enquiry_from date,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60)
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiry_history OWNER TO cuon_admin;

--
-- Name: enquiry_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiry_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiry_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiry_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.enquiry_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enquiry_id OWNER TO cuon_admin;

--
-- Name: enquiry_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiry ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiryget; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiryget (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    number character varying(25),
    partnernumber integer,
    loadreference character varying(20),
    pickup_at date,
    remark_to_pickup_at character varying(100),
    forwarding_agency_number integer,
    contact_person_number integer,
    pickup_note_number character(20),
    pickup_note_date date,
    pickup_note_status character(40),
    orderid integer
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiryget OWNER TO cuon_admin;

--
-- Name: enquiryget_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiryget_history (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    number character varying(25),
    partnernumber integer,
    loadreference character varying(20),
    pickup_at date,
    remark_to_pickup_at character varying(100),
    forwarding_agency_number integer,
    contact_person_number integer,
    pickup_note_number character(20),
    pickup_note_date date,
    pickup_note_status character(40),
    orderid integer
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiryget_history OWNER TO cuon_admin;

--
-- Name: enquiryget_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiryget_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiryget_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiryget_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.enquiryget_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enquiryget_id OWNER TO cuon_admin;

--
-- Name: enquiryget_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiryget ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiryget_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiryinvoice; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiryinvoice (
    orderid integer,
    order_top integer,
    tax_vat_for_all_positions integer DEFAULT 0,
    different_billing_address_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiryinvoice OWNER TO cuon_admin;

--
-- Name: enquiryinvoice_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiryinvoice_history (
    orderid integer,
    order_top integer,
    tax_vat_for_all_positions integer DEFAULT 0,
    different_billing_address_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiryinvoice_history OWNER TO cuon_admin;

--
-- Name: enquiryinvoice_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiryinvoice_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiryinvoice_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiryinvoice_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.enquiryinvoice_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enquiryinvoice_id OWNER TO cuon_admin;

--
-- Name: enquiryinvoice_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiryinvoice ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiryinvoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquirymisc; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquirymisc (
    orderid integer,
    retry boolean,
    monthly boolean,
    quarterly boolean,
    semestral boolean,
    yearly boolean,
    daily boolean,
    weekly boolean,
    custom_retry_days integer,
    create_get boolean,
    create_supply boolean,
    create_invoice boolean,
    part_of_order boolean,
    part_of_order_number integer,
    original_order_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.enquirymisc OWNER TO cuon_admin;

--
-- Name: enquirymisc_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquirymisc_history (
    orderid integer,
    retry boolean,
    monthly boolean,
    quarterly boolean,
    semestral boolean,
    yearly boolean,
    daily boolean,
    weekly boolean,
    custom_retry_days integer,
    create_get boolean,
    create_supply boolean,
    create_invoice boolean,
    part_of_order boolean,
    part_of_order_number integer,
    original_order_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.enquirymisc_history OWNER TO cuon_admin;

--
-- Name: enquirymisc_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquirymisc_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquirymisc_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquirymisc_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.enquirymisc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enquirymisc_id OWNER TO cuon_admin;

--
-- Name: enquirymisc_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquirymisc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquirymisc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiryposition; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiryposition (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiryposition OWNER TO cuon_admin;

--
-- Name: enquiryposition_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquiryposition_history (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.enquiryposition_history OWNER TO cuon_admin;

--
-- Name: enquiryposition_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiryposition_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiryposition_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquiryposition_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.enquiryposition_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enquiryposition_id OWNER TO cuon_admin;

--
-- Name: enquiryposition_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquiryposition ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquiryposition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquirysupply; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquirysupply (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    orderid integer,
    number character varying(25)
)
INHERITS (public.t_standard);


ALTER TABLE public.enquirysupply OWNER TO cuon_admin;

--
-- Name: enquirysupply_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.enquirysupply_history (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    orderid integer,
    number character varying(25)
)
INHERITS (public.t_standard);


ALTER TABLE public.enquirysupply_history OWNER TO cuon_admin;

--
-- Name: enquirysupply_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquirysupply_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquirysupply_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: enquirysupply_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.enquirysupply_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enquirysupply_id OWNER TO cuon_admin;

--
-- Name: enquirysupply_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.enquirysupply ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.enquirysupply_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ext_two; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.ext_two (
    d1 character varying(30)
)
INHERITS (public.t_standard);


ALTER TABLE public.ext_two OWNER TO cuon_admin;

--
-- Name: ext_two_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.ext_two_history (
    d1 character varying(30)
)
INHERITS (public.t_standard);


ALTER TABLE public.ext_two_history OWNER TO cuon_admin;

--
-- Name: ext_two_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.ext_two_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ext_two_id OWNER TO cuon_admin;

--
-- Name: grave; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave (
    lastname character varying(150),
    firstname character varying(150),
    addressid integer DEFAULT 0,
    graveyardid integer DEFAULT 0,
    pos_number integer DEFAULT 0,
    contract_begins_at date,
    contract_ends_at date,
    detachment character varying(150),
    grave_number character varying(150),
    type_of_grave integer,
    type_of_paid integer,
    percents integer,
    contract_id character varying(255),
    common_notes text,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    created_order integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.grave OWNER TO cuon_admin;

--
-- Name: grave_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_history (
    lastname character varying(150),
    firstname character varying(150),
    addressid integer DEFAULT 0,
    graveyardid integer DEFAULT 0,
    pos_number integer DEFAULT 0,
    contract_begins_at date,
    contract_ends_at date,
    detachment character varying(150),
    grave_number character varying(150),
    type_of_grave integer,
    type_of_paid integer,
    percents integer,
    contract_id character varying(255),
    common_notes text,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    created_order integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_history OWNER TO cuon_admin;

--
-- Name: grave_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_id OWNER TO cuon_admin;

--
-- Name: grave_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_invoice_info; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_invoice_info (
    grave_id integer,
    address_id integer,
    top_id integer,
    part double precision,
    invoice_begin date,
    invoice_end date
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_invoice_info OWNER TO cuon_admin;

--
-- Name: grave_invoice_info_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_invoice_info_history (
    grave_id integer,
    address_id integer,
    top_id integer,
    part double precision,
    invoice_begin date,
    invoice_end date
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_invoice_info_history OWNER TO cuon_admin;

--
-- Name: grave_invoice_info_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_invoice_info_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_invoice_info_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_invoice_info_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_invoice_info_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_invoice_info_id OWNER TO cuon_admin;

--
-- Name: grave_invoice_info_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_invoice_info ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_invoice_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_service_notes; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_service_notes (
    grave_id integer,
    service_id integer DEFAULT 0,
    service_note text
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_service_notes OWNER TO cuon_admin;

--
-- Name: grave_service_notes_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_service_notes_history (
    grave_id integer,
    service_id integer DEFAULT 0,
    service_note text
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_service_notes_history OWNER TO cuon_admin;

--
-- Name: grave_service_notes_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_service_notes_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_service_notes_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_service_notes_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_service_notes_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_service_notes_id OWNER TO cuon_admin;

--
-- Name: grave_service_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_service_notes ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_service_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_grave_work; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_grave_work (
    grave_id integer,
    service_count double precision,
    service_designation character varying(255),
    service_price double precision,
    service_date date,
    service_notes text,
    article_id integer,
    period_id integer,
    services text,
    automatic_price integer DEFAULT 1,
    created_order integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.t_grave_work OWNER TO cuon_admin;

--
-- Name: grave_work_autumn; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_autumn (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_autumn OWNER TO cuon_admin;

--
-- Name: grave_work_autumn_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_autumn_history (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_autumn_history OWNER TO cuon_admin;

--
-- Name: grave_work_autumn_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_autumn_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_autumn_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_autumn_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_autumn_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_autumn_id OWNER TO cuon_admin;

--
-- Name: grave_work_autumn_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_autumn ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_autumn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_holiday; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_holiday (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_holiday OWNER TO cuon_admin;

--
-- Name: grave_work_holiday_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_holiday_history (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_holiday_history OWNER TO cuon_admin;

--
-- Name: grave_work_holiday_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_holiday_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_holiday_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_holiday_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_holiday_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_holiday_id OWNER TO cuon_admin;

--
-- Name: grave_work_holiday_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_holiday ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_holiday_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_maintenance; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_maintenance (
    grave_id integer,
    period_id integer,
    grave_service_id integer,
    service_price double precision,
    hints_for_service text,
    additional_services text,
    services text,
    service_count double precision,
    service_designation character varying(255),
    service_date date,
    service_notes text,
    article_id integer DEFAULT 0,
    automatic_price integer DEFAULT 1,
    created_order integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_work_maintenance OWNER TO cuon_admin;

--
-- Name: grave_work_maintenance_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_maintenance_history (
    grave_id integer,
    period_id integer,
    grave_service_id integer,
    service_price double precision,
    hints_for_service text,
    additional_services text,
    services text,
    service_count double precision,
    service_designation character varying(255),
    service_date date,
    service_notes text,
    article_id integer DEFAULT 0,
    automatic_price integer DEFAULT 1,
    created_order integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.grave_work_maintenance_history OWNER TO cuon_admin;

--
-- Name: grave_work_maintenance_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_maintenance_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_maintenance_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_maintenance_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_maintenance_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_maintenance_id OWNER TO cuon_admin;

--
-- Name: grave_work_maintenance_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_maintenance ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_maintenance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_single; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_single (
    unique_date date
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_single OWNER TO cuon_admin;

--
-- Name: grave_work_single_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_single_history (
    unique_date date
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_single_history OWNER TO cuon_admin;

--
-- Name: grave_work_single_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_single_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_single_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_single_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_single_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_single_id OWNER TO cuon_admin;

--
-- Name: grave_work_single_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_single ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_single_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_spring; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_spring (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_spring OWNER TO cuon_admin;

--
-- Name: grave_work_spring_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_spring_history (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_spring_history OWNER TO cuon_admin;

--
-- Name: grave_work_spring_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_spring_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_spring_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_spring_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_spring_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_spring_id OWNER TO cuon_admin;

--
-- Name: grave_work_spring_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_spring ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_spring_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_summer; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_summer (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_summer OWNER TO cuon_admin;

--
-- Name: grave_work_summer_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_summer_history (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_summer_history OWNER TO cuon_admin;

--
-- Name: grave_work_summer_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_summer_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_summer_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_summer_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_summer_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_summer_id OWNER TO cuon_admin;

--
-- Name: grave_work_summer_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_summer ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_summer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_winter; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_winter (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_winter OWNER TO cuon_admin;

--
-- Name: grave_work_winter_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_winter_history (
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_winter_history OWNER TO cuon_admin;

--
-- Name: grave_work_winter_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_winter_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_winter_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_winter_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_winter_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_winter_id OWNER TO cuon_admin;

--
-- Name: grave_work_winter_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_winter ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_winter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_year; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_year (
    year_date date,
    annual_day integer,
    annual_month integer
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_year OWNER TO cuon_admin;

--
-- Name: grave_work_year_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.grave_work_year_history (
    year_date date,
    annual_day integer,
    annual_month integer
)
INHERITS (public.t_grave_work);


ALTER TABLE public.grave_work_year_history OWNER TO cuon_admin;

--
-- Name: grave_work_year_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_year_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_year_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: grave_work_year_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.grave_work_year_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grave_work_year_id OWNER TO cuon_admin;

--
-- Name: grave_work_year_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.grave_work_year ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.grave_work_year_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: graveyard; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.graveyard (
    shortname character varying(50),
    designation character varying(250),
    addressid integer
)
INHERITS (public.t_standard);


ALTER TABLE public.graveyard OWNER TO cuon_admin;

--
-- Name: graveyard_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.graveyard_history (
    shortname character varying(50),
    designation character varying(250),
    addressid integer
)
INHERITS (public.t_standard);


ALTER TABLE public.graveyard_history OWNER TO cuon_admin;

--
-- Name: graveyard_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.graveyard_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.graveyard_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: graveyard_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.graveyard_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.graveyard_id OWNER TO cuon_admin;

--
-- Name: graveyard_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.graveyard ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.graveyard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: hibernation; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.hibernation (
    hibernation_number integer,
    addressnumber integer,
    begin_staff_number integer,
    ends_staff_number integer,
    begin_working_time double precision,
    ends_working_time double precision,
    begin_notes text,
    ends_notes text,
    begin_date date,
    end_date date,
    check_begin_delivered boolean,
    check_ends_fetched boolean,
    check_begin_fetched boolean,
    check_ends_delivered boolean,
    status_ready boolean,
    status_invoice_printed boolean,
    sequence_of_stock integer,
    invoice_repeat boolean
)
INHERITS (public.t_standard);


ALTER TABLE public.hibernation OWNER TO cuon_admin;

--
-- Name: hibernation_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.hibernation_history (
    hibernation_number integer,
    addressnumber integer,
    begin_staff_number integer,
    ends_staff_number integer,
    begin_working_time double precision,
    ends_working_time double precision,
    begin_notes text,
    ends_notes text,
    begin_date date,
    end_date date,
    check_begin_delivered boolean,
    check_ends_fetched boolean,
    check_begin_fetched boolean,
    check_ends_delivered boolean,
    status_ready boolean,
    status_invoice_printed boolean,
    sequence_of_stock integer,
    invoice_repeat boolean
)
INHERITS (public.t_standard);


ALTER TABLE public.hibernation_history OWNER TO cuon_admin;

--
-- Name: hibernation_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.hibernation_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.hibernation_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: hibernation_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.hibernation_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernation_id OWNER TO cuon_admin;

--
-- Name: hibernation_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.hibernation ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.hibernation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: hibernation_plant; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.hibernation_plant (
    hibernation_number integer,
    plant_number integer,
    botany_number integer,
    plant_status integer,
    plant_notice character varying(80),
    vermin character varying(60),
    diameter double precision,
    price_last_year double precision,
    price double precision,
    add_earth double precision,
    add_pot double precision,
    add_material double precision,
    add_misc double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.hibernation_plant OWNER TO cuon_admin;

--
-- Name: hibernation_plant_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.hibernation_plant_history (
    hibernation_number integer,
    plant_number integer,
    botany_number integer,
    plant_status integer,
    plant_notice character varying(80),
    vermin character varying(60),
    diameter double precision,
    price_last_year double precision,
    price double precision,
    add_earth double precision,
    add_pot double precision,
    add_material double precision,
    add_misc double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.hibernation_plant_history OWNER TO cuon_admin;

--
-- Name: hibernation_plant_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.hibernation_plant_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.hibernation_plant_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: hibernation_plant_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.hibernation_plant_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernation_plant_id OWNER TO cuon_admin;

--
-- Name: hibernation_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.hibernation_plant ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.hibernation_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: in_payment; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.in_payment (
    invoice_number character varying(140),
    cash_discount double precision,
    inpayment double precision,
    date_of_paid date,
    account_id integer,
    account_type integer,
    credit_card_number character varying(110),
    credit_card_type character varying(110),
    order_id integer,
    money_cash boolean,
    money_credit_card boolean,
    money_offset boolean,
    money_transfer boolean,
    money_direct_debit boolean,
    bank_ranking integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.in_payment OWNER TO cuon_admin;

--
-- Name: in_payment_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.in_payment_history (
    invoice_number character varying(140),
    cash_discount double precision,
    inpayment double precision,
    date_of_paid date,
    account_id integer,
    account_type integer,
    credit_card_number character varying(110),
    credit_card_type character varying(110),
    order_id integer,
    money_cash boolean,
    money_credit_card boolean,
    money_offset boolean,
    money_transfer boolean,
    money_direct_debit boolean,
    bank_ranking integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.in_payment_history OWNER TO cuon_admin;

--
-- Name: in_payment_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.in_payment_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.in_payment_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: in_payment_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.in_payment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.in_payment_id OWNER TO cuon_admin;

--
-- Name: in_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.in_payment ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.in_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_deliveries; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_deliveries (
    delivery_number integer,
    order_number integer,
    date_of_delivery date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_deliveries OWNER TO cuon_admin;

--
-- Name: list_of_deliveries_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_deliveries_history (
    delivery_number integer,
    order_number integer,
    date_of_delivery date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_deliveries_history OWNER TO cuon_admin;

--
-- Name: list_of_deliveries_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_deliveries_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_deliveries_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_deliveries_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.list_of_deliveries_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_of_deliveries_id OWNER TO cuon_admin;

--
-- Name: list_of_deliveries_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_deliveries ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_deliveries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_hibernation_incoming; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_hibernation_incoming (
    incoming_number integer,
    order_number integer,
    date_of_incoming date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_hibernation_incoming OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_incoming_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_hibernation_incoming_history (
    incoming_number integer,
    order_number integer,
    date_of_incoming date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_hibernation_incoming_history OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_incoming_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_hibernation_incoming_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_hibernation_incoming_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_hibernation_incoming_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.list_of_hibernation_incoming_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_of_hibernation_incoming_id OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_incoming_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_hibernation_incoming ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_hibernation_incoming_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_hibernation_outgoing; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_hibernation_outgoing (
    outgoing_number integer,
    order_number integer,
    date_of_outgoing date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_hibernation_outgoing OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_outgoing_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_hibernation_outgoing_history (
    outgoing_number integer,
    order_number integer,
    date_of_outgoing date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_hibernation_outgoing_history OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_outgoing_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_hibernation_outgoing_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_hibernation_outgoing_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_hibernation_outgoing_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.list_of_hibernation_outgoing_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_of_hibernation_outgoing_id OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_outgoing_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_hibernation_outgoing ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_hibernation_outgoing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_hibernation_pickup; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_hibernation_pickup (
    pickup_number integer,
    order_number integer,
    date_of_pickup date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_hibernation_pickup OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_pickup_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_hibernation_pickup_history (
    pickup_number integer,
    order_number integer,
    date_of_pickup date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_hibernation_pickup_history OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_pickup_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_hibernation_pickup_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_hibernation_pickup_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_hibernation_pickup_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.list_of_hibernation_pickup_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_of_hibernation_pickup_id OWNER TO cuon_admin;

--
-- Name: list_of_hibernation_pickup_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_hibernation_pickup ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_hibernation_pickup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_invoices; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_invoices (
    invoice_number integer,
    order_number integer,
    total_amount double precision,
    date_of_invoice date,
    maturity date,
    last_transaction_uuid character varying(36),
    is_paid boolean DEFAULT false
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_invoices OWNER TO cuon_admin;

--
-- Name: list_of_invoices_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_invoices_history (
    invoice_number integer,
    order_number integer,
    total_amount double precision,
    date_of_invoice date,
    maturity date,
    last_transaction_uuid character varying(36),
    is_paid boolean DEFAULT false
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_invoices_history OWNER TO cuon_admin;

--
-- Name: list_of_invoices_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_invoices_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_invoices_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_invoices_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.list_of_invoices_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_of_invoices_id OWNER TO cuon_admin;

--
-- Name: list_of_invoices_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_invoices ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_invoices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_pickups; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_pickups (
    pickup_number integer,
    order_number integer,
    date_of_pickup date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_pickups OWNER TO cuon_admin;

--
-- Name: list_of_pickups_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.list_of_pickups_history (
    pickup_number integer,
    order_number integer,
    date_of_pickup date
)
INHERITS (public.t_standard);


ALTER TABLE public.list_of_pickups_history OWNER TO cuon_admin;

--
-- Name: list_of_pickups_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_pickups_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_pickups_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: list_of_pickups_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.list_of_pickups_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_of_pickups_id OWNER TO cuon_admin;

--
-- Name: list_of_pickups_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.list_of_pickups ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.list_of_pickups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: lock_process; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.lock_process (
    lock_user integer,
    order_id integer,
    invoice_id integer,
    inpayment_id integer,
    cashdesk_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.lock_process OWNER TO cuon_admin;

--
-- Name: lock_process_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.lock_process_history (
    lock_user integer,
    order_id integer,
    invoice_id integer,
    inpayment_id integer,
    cashdesk_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.lock_process_history OWNER TO cuon_admin;

--
-- Name: lock_process_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.lock_process_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.lock_process_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: lock_process_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.lock_process_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lock_process_id OWNER TO cuon_admin;

--
-- Name: lock_process_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.lock_process ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.lock_process_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: logistics; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.logistics (
    address_id integer,
    designation character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.logistics OWNER TO cuon_admin;

--
-- Name: logistics_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.logistics_history (
    address_id integer,
    designation character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.logistics_history OWNER TO cuon_admin;

--
-- Name: logistics_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.logistics_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.logistics_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: logistics_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.logistics_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logistics_id OWNER TO cuon_admin;

--
-- Name: logistics_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.logistics ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.logistics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: material_group; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.material_group (
    name character varying(30),
    designation character varying(250),
    tax_vat integer,
    price_type_net boolean,
    price_type_gross boolean
)
INHERITS (public.t_standard);


ALTER TABLE public.material_group OWNER TO cuon_admin;

--
-- Name: material_group_accounts; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.material_group_accounts (
    material_group_id integer,
    contra_account_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.material_group_accounts OWNER TO cuon_admin;

--
-- Name: material_group_accounts_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.material_group_accounts_history (
    material_group_id integer,
    contra_account_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.material_group_accounts_history OWNER TO cuon_admin;

--
-- Name: material_group_accounts_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.material_group_accounts_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.material_group_accounts_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: material_group_accounts_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.material_group_accounts_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.material_group_accounts_id OWNER TO cuon_admin;

--
-- Name: material_group_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.material_group_accounts ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.material_group_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: material_group_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.material_group_history (
    name character varying(30),
    designation character varying(250),
    tax_vat integer,
    price_type_net boolean,
    price_type_gross boolean
)
INHERITS (public.t_standard);


ALTER TABLE public.material_group_history OWNER TO cuon_admin;

--
-- Name: material_group_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.material_group_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.material_group_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: material_group_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.material_group_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.material_group_id OWNER TO cuon_admin;

--
-- Name: material_group_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.material_group ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.material_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: mindmap; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.mindmap (
    header character varying(250),
    designation text,
    process_status integer,
    begin_at date,
    end_at date
)
INHERITS (public.t_standard);


ALTER TABLE public.mindmap OWNER TO cuon_admin;

--
-- Name: mindmap_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.mindmap_history (
    header character varying(250),
    designation text,
    process_status integer,
    begin_at date,
    end_at date
)
INHERITS (public.t_standard);


ALTER TABLE public.mindmap_history OWNER TO cuon_admin;

--
-- Name: mindmap_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.mindmap_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.mindmap_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: mindmap_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.mindmap_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mindmap_id OWNER TO cuon_admin;

--
-- Name: mindmap_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.mindmap ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.mindmap_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: misc_data; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.misc_data (
    cdata01 character varying(254),
    cdata02 character varying(254),
    cdata03 character varying(254),
    cdata04 character varying(254),
    cdata05 character varying(254),
    cdata06 character varying(254),
    cdata07 character varying(254),
    cdata08 character varying(254),
    cdata09 character varying(254),
    cdata10 character varying(254),
    idata01 integer,
    idata02 integer,
    idata03 integer,
    idata04 integer,
    idata05 integer,
    idata06 integer,
    idata07 integer,
    idata08 integer,
    idata09 integer,
    idata10 integer,
    fdata01 double precision,
    fdata02 double precision,
    fdata03 double precision,
    fdata04 double precision,
    fdata05 double precision,
    fdata06 double precision,
    fdata07 double precision,
    fdata08 double precision,
    fdata09 double precision,
    fdata10 double precision,
    ddata01 date,
    ddata02 date,
    ddata03 date,
    ddata04 date,
    ddata05 date,
    ddata06 date,
    ddata07 date,
    ddata08 date,
    ddata09 date,
    ddata10 date
)
INHERITS (public.t_standard);


ALTER TABLE public.misc_data OWNER TO cuon_admin;

--
-- Name: misc_data_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.misc_data_history (
    cdata01 character varying(254),
    cdata02 character varying(254),
    cdata03 character varying(254),
    cdata04 character varying(254),
    cdata05 character varying(254),
    cdata06 character varying(254),
    cdata07 character varying(254),
    cdata08 character varying(254),
    cdata09 character varying(254),
    cdata10 character varying(254),
    idata01 integer,
    idata02 integer,
    idata03 integer,
    idata04 integer,
    idata05 integer,
    idata06 integer,
    idata07 integer,
    idata08 integer,
    idata09 integer,
    idata10 integer,
    fdata01 double precision,
    fdata02 double precision,
    fdata03 double precision,
    fdata04 double precision,
    fdata05 double precision,
    fdata06 double precision,
    fdata07 double precision,
    fdata08 double precision,
    fdata09 double precision,
    fdata10 double precision,
    ddata01 date,
    ddata02 date,
    ddata03 date,
    ddata04 date,
    ddata05 date,
    ddata06 date,
    ddata07 date,
    ddata08 date,
    ddata09 date,
    ddata10 date
)
INHERITS (public.t_standard);


ALTER TABLE public.misc_data_history OWNER TO cuon_admin;

--
-- Name: misc_data_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.misc_data_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.misc_data_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: misc_data_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.misc_data_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.misc_data_id OWNER TO cuon_admin;

--
-- Name: misc_data_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.misc_data ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.misc_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderbook; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderbook (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_id integer DEFAULT 0,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_from date,
    processed_at date,
    enquiry_number character varying(120) DEFAULT 0,
    enquiry_id integer DEFAULT 0,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60),
    cash_desk_user_shortkey character varying(4),
    order_created_by integer DEFAULT 1,
    order_type integer DEFAULT 1
)
INHERITS (public.t_standard);


ALTER TABLE public.orderbook OWNER TO cuon_admin;

--
-- Name: orderbook_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderbook_history (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_id integer DEFAULT 0,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_from date,
    processed_at date,
    enquiry_number character varying(120) DEFAULT 0,
    enquiry_id integer DEFAULT 0,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60),
    cash_desk_user_shortkey character varying(4),
    order_created_by integer DEFAULT 1,
    order_type integer DEFAULT 1
)
INHERITS (public.t_standard);


ALTER TABLE public.orderbook_history OWNER TO cuon_admin;

--
-- Name: orderbook_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderbook_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderbook_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderbook_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.orderbook_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderbook_id OWNER TO cuon_admin;

--
-- Name: orderbook_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderbook ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderget; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderget (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    number character varying(25),
    partnernumber integer,
    loadreference character varying(20),
    pickup_at date,
    remark_to_pickup_at character varying(100),
    forwarding_agency_number integer,
    contact_person_number integer,
    pickup_note_number character(20),
    pickup_note_date date,
    pickup_note_status character(40),
    orderid integer,
    gets_staff_date date,
    gets_staff_time integer,
    gets_staff_id integer,
    schedul_uuid character varying(36)
)
INHERITS (public.t_standard);


ALTER TABLE public.orderget OWNER TO cuon_admin;

--
-- Name: orderget_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderget_history (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    number character varying(25),
    partnernumber integer,
    loadreference character varying(20),
    pickup_at date,
    remark_to_pickup_at character varying(100),
    forwarding_agency_number integer,
    contact_person_number integer,
    pickup_note_number character(20),
    pickup_note_date date,
    pickup_note_status character(40),
    orderid integer,
    gets_staff_date date,
    gets_staff_time integer,
    gets_staff_id integer,
    schedul_uuid character varying(36)
)
INHERITS (public.t_standard);


ALTER TABLE public.orderget_history OWNER TO cuon_admin;

--
-- Name: orderget_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderget_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderget_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderget_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.orderget_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderget_id OWNER TO cuon_admin;

--
-- Name: orderget_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderget ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderget_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderinvoice; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderinvoice (
    orderid integer,
    order_top integer,
    tax_vat_for_all_positions integer DEFAULT 0,
    different_billing_address_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.orderinvoice OWNER TO cuon_admin;

--
-- Name: orderinvoice_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderinvoice_history (
    orderid integer,
    order_top integer,
    tax_vat_for_all_positions integer DEFAULT 0,
    different_billing_address_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.orderinvoice_history OWNER TO cuon_admin;

--
-- Name: orderinvoice_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderinvoice_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderinvoice_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderinvoice_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.orderinvoice_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderinvoice_id OWNER TO cuon_admin;

--
-- Name: orderinvoice_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderinvoice ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderinvoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ordermisc; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.ordermisc (
    orderid integer,
    retry boolean,
    monthly boolean,
    quarterly boolean,
    semestral boolean,
    yearly boolean,
    daily boolean,
    weekly boolean,
    custom_retry_days integer,
    create_get boolean,
    create_supply boolean,
    create_invoice boolean,
    part_of_order boolean,
    part_of_order_number integer,
    original_order_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.ordermisc OWNER TO cuon_admin;

--
-- Name: ordermisc_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.ordermisc_history (
    orderid integer,
    retry boolean,
    monthly boolean,
    quarterly boolean,
    semestral boolean,
    yearly boolean,
    daily boolean,
    weekly boolean,
    custom_retry_days integer,
    create_get boolean,
    create_supply boolean,
    create_invoice boolean,
    part_of_order boolean,
    part_of_order_number integer,
    original_order_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.ordermisc_history OWNER TO cuon_admin;

--
-- Name: ordermisc_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.ordermisc_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ordermisc_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ordermisc_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.ordermisc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ordermisc_id OWNER TO cuon_admin;

--
-- Name: ordermisc_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.ordermisc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ordermisc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderposition; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderposition (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.orderposition OWNER TO cuon_admin;

--
-- Name: orderposition_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.orderposition_history (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.orderposition_history OWNER TO cuon_admin;

--
-- Name: orderposition_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderposition_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderposition_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: orderposition_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.orderposition_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderposition_id OWNER TO cuon_admin;

--
-- Name: orderposition_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.orderposition ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.orderposition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ordersupply; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.ordersupply (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    orderid integer,
    number character varying(25)
)
INHERITS (public.t_standard);


ALTER TABLE public.ordersupply OWNER TO cuon_admin;

--
-- Name: ordersupply_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.ordersupply_history (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    orderid integer,
    number character varying(25)
)
INHERITS (public.t_standard);


ALTER TABLE public.ordersupply_history OWNER TO cuon_admin;

--
-- Name: ordersupply_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.ordersupply_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ordersupply_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: ordersupply_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.ordersupply_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ordersupply_id OWNER TO cuon_admin;

--
-- Name: ordersupply_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.ordersupply ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.ordersupply_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: partner; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.partner (
    addressid integer,
    titular character varying(30),
    phone1 character varying(25),
    phone2 character varying(25),
    homepage character varying(100),
    faxprivat character varying(25),
    birthday date,
    business_function character varying(225),
    job character varying(225),
    department character varying(225),
    additional_emails text
)
INHERITS (public.t_address);


ALTER TABLE public.partner OWNER TO cuon_admin;

--
-- Name: partner_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.partner_history (
    addressid integer,
    titular character varying(30),
    phone1 character varying(25),
    phone2 character varying(25),
    homepage character varying(100),
    faxprivat character varying(25),
    birthday date,
    business_function character varying(225),
    job character varying(225),
    department character varying(225),
    additional_emails text
)
INHERITS (public.t_address);


ALTER TABLE public.partner_history OWNER TO cuon_admin;

--
-- Name: partner_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.partner_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.partner_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: partner_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.partner_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.partner_id OWNER TO cuon_admin;

--
-- Name: partner_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.partner ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.partner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: partner_schedul; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.partner_schedul (
    schedul_uuid character varying(36) DEFAULT '0'::character varying,
    schedul_insert_from integer DEFAULT 0
)
INHERITS (public.t_partner_schedul);


ALTER TABLE public.partner_schedul OWNER TO cuon_admin;

--
-- Name: partner_schedul_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.partner_schedul_history (
    schedul_uuid character varying(36) DEFAULT '0'::character varying,
    schedul_insert_from integer DEFAULT 0
)
INHERITS (public.t_partner_schedul);


ALTER TABLE public.partner_schedul_history OWNER TO cuon_admin;

--
-- Name: partner_schedul_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.partner_schedul_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.partner_schedul_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: partner_schedul_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.partner_schedul_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.partner_schedul_id OWNER TO cuon_admin;

--
-- Name: partner_schedul_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.partner_schedul ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.partner_schedul_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: planet; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.planet (
    name character varying(100),
    name2 character varying(100),
    orbit double precision,
    diameter double precision,
    solar_system_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.planet OWNER TO cuon_admin;

--
-- Name: planet_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.planet_history (
    name character varying(100),
    name2 character varying(100),
    orbit double precision,
    diameter double precision,
    solar_system_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.planet_history OWNER TO cuon_admin;

--
-- Name: planet_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.planet_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.planet_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: planet_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.planet_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planet_id OWNER TO cuon_admin;

--
-- Name: planet_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.planet ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.planet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: precurementposition; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.precurementposition (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.precurementposition OWNER TO cuon_admin;

--
-- Name: precurementposition_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.precurementposition_history (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.precurementposition_history OWNER TO cuon_admin;

--
-- Name: precurementposition_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.precurementposition_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.precurementposition_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: precurementposition_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.precurementposition_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.precurementposition_id OWNER TO cuon_admin;

--
-- Name: precurementposition_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.precurementposition ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.precurementposition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: preferences; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.preferences (
    username character varying(60),
    userdescription character varying(60),
    locales character varying(10),
    encoding character varying(15),
    profile_name character varying(25),
    description character varying(30),
    printing_system character varying(20),
    path_to_report_invoices character varying(255),
    path_to_report_address_lists character varying(255),
    path_to_docs_invoices character varying(255),
    path_to_docs_address_lists character varying(255),
    path_to_report_supply character varying(255),
    path_to_report_pickup character varying(255),
    path_to_docs_supply character varying(255),
    path_to_docs_pickup character varying(255),
    is_standard_profile boolean,
    scanner_device character varying(120),
    scanner_mode character varying(40),
    scanner_resolution integer,
    scanner_depth integer,
    scanner_brx double precision,
    scanner_bry double precision,
    scanner_brightness double precision,
    scanner_white_level double precision,
    scanner_contrast double precision,
    exe_oowriter character varying(255),
    exe_oocalc character varying(255),
    exe_oodraw character varying(255),
    exe_ooimpress character varying(255),
    exe_music character varying(255),
    exe_ogg character varying(255),
    exe_wav character varying(255),
    exe_pdf character varying(255),
    exe_tex character varying(255),
    exe_ltx character varying(255),
    exe_txt character varying(255),
    exe_image character varying(255),
    exe_flowchart character varying(255),
    exe_googleearth character varying(255),
    exe_internet character varying(255),
    exe_html character varying(255),
    email_user_address character varying(255),
    email_user_host character varying(255),
    email_user_port integer,
    email_user_loginname character varying(255),
    email_user_password character varying(255),
    email_user_signatur text,
    color_bg character varying(40),
    color_fg character varying(40),
    color_duty_bg character varying(40),
    color_duty_fg character varying(40),
    exe_print_pickup character varying(255),
    exe_print_supply character varying(255),
    exe_print_invoice character varying(255),
    exe_print_newsletter character varying(255),
    exe_python character varying(255),
    exe_mindmap character varying(255),
    time_offset character varying(10),
    email_ext_prg character varying(255),
    twitter_user_name character varying(255),
    twitter_user_password character varying(255),
    std_text_chat character varying(255),
    std_3d_chat character varying(255),
    std_email_prg character varying(255),
    user_win_max boolean DEFAULT false,
    facebook character varying(255),
    user_check_imap boolean DEFAULT false,
    user_email_ssl boolean DEFAULT false,
    user_imap_email_ssl boolean DEFAULT false,
    email_user_imap_host character varying(255),
    email_user_imap_port integer,
    email_user_imap_loginname character varying(255),
    email_user_imap_password character varying(255),
    email_user_imap_crypt integer,
    email_user_crypt integer,
    user_show_news boolean DEFAULT true,
    exe_cad character varying(255),
    user_mail_pickup boolean DEFAULT true,
    user_mail_supply boolean DEFAULT true,
    user_mail_invoice boolean DEFAULT true,
    user_grid integer DEFAULT 0,
    grid_user_name character(200) DEFAULT 0,
    grid_user_password character(24) DEFAULT 0,
    exe_print_cash1 character varying(255),
    exe_print_cash2 character varying(255),
    exe_print_cash3 character varying(255)
)
INHERITS (public.t_standard);


ALTER TABLE public.preferences OWNER TO cuon_admin;

--
-- Name: preferences_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.preferences_history (
    username character varying(60),
    userdescription character varying(60),
    locales character varying(10),
    encoding character varying(15),
    profile_name character varying(25),
    description character varying(30),
    printing_system character varying(20),
    path_to_report_invoices character varying(255),
    path_to_report_address_lists character varying(255),
    path_to_docs_invoices character varying(255),
    path_to_docs_address_lists character varying(255),
    path_to_report_supply character varying(255),
    path_to_report_pickup character varying(255),
    path_to_docs_supply character varying(255),
    path_to_docs_pickup character varying(255),
    is_standard_profile boolean,
    scanner_device character varying(120),
    scanner_mode character varying(40),
    scanner_resolution integer,
    scanner_depth integer,
    scanner_brx double precision,
    scanner_bry double precision,
    scanner_brightness double precision,
    scanner_white_level double precision,
    scanner_contrast double precision,
    exe_oowriter character varying(255),
    exe_oocalc character varying(255),
    exe_oodraw character varying(255),
    exe_ooimpress character varying(255),
    exe_music character varying(255),
    exe_ogg character varying(255),
    exe_wav character varying(255),
    exe_pdf character varying(255),
    exe_tex character varying(255),
    exe_ltx character varying(255),
    exe_txt character varying(255),
    exe_image character varying(255),
    exe_flowchart character varying(255),
    exe_googleearth character varying(255),
    exe_internet character varying(255),
    exe_html character varying(255),
    email_user_address character varying(255),
    email_user_host character varying(255),
    email_user_port integer,
    email_user_loginname character varying(255),
    email_user_password character varying(255),
    email_user_signatur text,
    color_bg character varying(40),
    color_fg character varying(40),
    color_duty_bg character varying(40),
    color_duty_fg character varying(40),
    exe_print_pickup character varying(255),
    exe_print_supply character varying(255),
    exe_print_invoice character varying(255),
    exe_print_newsletter character varying(255),
    exe_python character varying(255),
    exe_mindmap character varying(255),
    time_offset character varying(10),
    email_ext_prg character varying(255),
    twitter_user_name character varying(255),
    twitter_user_password character varying(255),
    std_text_chat character varying(255),
    std_3d_chat character varying(255),
    std_email_prg character varying(255),
    user_win_max boolean DEFAULT false,
    facebook character varying(255),
    user_check_imap boolean DEFAULT false,
    user_email_ssl boolean DEFAULT false,
    user_imap_email_ssl boolean DEFAULT false,
    email_user_imap_host character varying(255),
    email_user_imap_port integer,
    email_user_imap_loginname character varying(255),
    email_user_imap_password character varying(255),
    email_user_imap_crypt integer,
    email_user_crypt integer,
    user_show_news boolean DEFAULT true,
    exe_cad character varying(255),
    user_mail_pickup boolean DEFAULT true,
    user_mail_supply boolean DEFAULT true,
    user_mail_invoice boolean DEFAULT true,
    user_grid integer DEFAULT 0,
    grid_user_name character(200) DEFAULT 0,
    grid_user_password character(24) DEFAULT 0,
    exe_print_cash1 character varying(255),
    exe_print_cash2 character varying(255),
    exe_print_cash3 character varying(255)
)
INHERITS (public.t_standard);


ALTER TABLE public.preferences_history OWNER TO cuon_admin;

--
-- Name: preferences_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.preferences_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.preferences_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: preferences_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.preferences_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.preferences_id OWNER TO cuon_admin;

--
-- Name: preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.preferences ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: procurement; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.procurement (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_from date,
    processed_at date,
    enquiry_number character varying(120) DEFAULT 0,
    enquiry_id integer DEFAULT 0,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60)
)
INHERITS (public.t_standard);


ALTER TABLE public.procurement OWNER TO cuon_admin;

--
-- Name: procurement_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.procurement_history (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_from date,
    processed_at date,
    enquiry_number character varying(120) DEFAULT 0,
    enquiry_id integer DEFAULT 0,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60)
)
INHERITS (public.t_standard);


ALTER TABLE public.procurement_history OWNER TO cuon_admin;

--
-- Name: procurement_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.procurement_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.procurement_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: procurement_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.procurement_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.procurement_id OWNER TO cuon_admin;

--
-- Name: procurement_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.procurement ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.procurement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_phases; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_phases (
    project_id integer,
    name character varying(140),
    designation character varying(140),
    time_in_days integer,
    starts_at date,
    ends_at date,
    phase_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_phases OWNER TO cuon_admin;

--
-- Name: project_phases_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_phases_history (
    project_id integer,
    name character varying(140),
    designation character varying(140),
    time_in_days integer,
    starts_at date,
    ends_at date,
    phase_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_phases_history OWNER TO cuon_admin;

--
-- Name: project_phases_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_phases_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_phases_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_phases_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.project_phases_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_phases_id OWNER TO cuon_admin;

--
-- Name: project_phases_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_phases ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_phases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_task_material_res; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_task_material_res (
    task_id integer,
    article_id integer,
    get_at date,
    unit character varying(30),
    planed_amount double precision,
    real_amount double precision,
    material_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_task_material_res OWNER TO cuon_admin;

--
-- Name: project_task_material_res_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_task_material_res_history (
    task_id integer,
    article_id integer,
    get_at date,
    unit character varying(30),
    planed_amount double precision,
    real_amount double precision,
    material_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_task_material_res_history OWNER TO cuon_admin;

--
-- Name: project_task_material_res_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_task_material_res_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_task_material_res_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_task_material_res_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.project_task_material_res_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_task_material_res_id OWNER TO cuon_admin;

--
-- Name: project_task_material_res_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_task_material_res ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_task_material_res_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_task_staff_res; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_task_staff_res (
    task_id integer,
    staff_id integer,
    planed_working_day date,
    real_working_day date,
    planed_time_begin character varying(10),
    planed_time_ends character varying(10),
    real_time_begin character varying(10),
    real_time_ends character varying(10),
    staff_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_task_staff_res OWNER TO cuon_admin;

--
-- Name: project_task_staff_res_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_task_staff_res_history (
    task_id integer,
    staff_id integer,
    planed_working_day date,
    real_working_day date,
    planed_time_begin character varying(10),
    planed_time_ends character varying(10),
    real_time_begin character varying(10),
    real_time_ends character varying(10),
    staff_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_task_staff_res_history OWNER TO cuon_admin;

--
-- Name: project_task_staff_res_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_task_staff_res_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_task_staff_res_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_task_staff_res_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.project_task_staff_res_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_task_staff_res_id OWNER TO cuon_admin;

--
-- Name: project_task_staff_res_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_task_staff_res ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_task_staff_res_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_tasks; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_tasks (
    phase_id integer,
    name character varying(140),
    designation character varying(140),
    time_in_days integer,
    starts_at date,
    ends_at date,
    task_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_tasks OWNER TO cuon_admin;

--
-- Name: project_tasks_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.project_tasks_history (
    phase_id integer,
    name character varying(140),
    designation character varying(140),
    time_in_days integer,
    starts_at date,
    ends_at date,
    task_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.project_tasks_history OWNER TO cuon_admin;

--
-- Name: project_tasks_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_tasks_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_tasks_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: project_tasks_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.project_tasks_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_tasks_id OWNER TO cuon_admin;

--
-- Name: project_tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.project_tasks ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: projects; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.projects (
    name character varying(140),
    designation character varying(140),
    customer_id integer,
    project_time_in_days integer,
    project_starts_at date,
    project_ends_at date,
    modul_project_number integer,
    modul_number integer,
    project_status integer,
    internal_project_number character varying(140),
    partner_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.projects OWNER TO cuon_admin;

--
-- Name: projects_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.projects_history (
    name character varying(140),
    designation character varying(140),
    customer_id integer,
    project_time_in_days integer,
    project_starts_at date,
    project_ends_at date,
    modul_project_number integer,
    modul_number integer,
    project_status integer,
    internal_project_number character varying(140),
    partner_id integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.projects_history OWNER TO cuon_admin;

--
-- Name: projects_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.projects_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.projects_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: projects_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.projects_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id OWNER TO cuon_admin;

--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.projects ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposal; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposal (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_from date,
    processed_at date,
    enquiry_number character varying(120) DEFAULT 0,
    enquiry_id integer DEFAULT 0,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60)
)
INHERITS (public.t_standard);


ALTER TABLE public.proposal OWNER TO cuon_admin;

--
-- Name: proposal_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposal_history (
    number character varying(30),
    designation character varying(50),
    orderedat date,
    deliveredat date,
    addressnumber integer,
    packing_cost double precision DEFAULT 0,
    postage_cost double precision DEFAULT 0,
    misc_cost double precision DEFAULT 0,
    build_retry boolean,
    type_retry integer DEFAULT 0,
    supply_retry boolean,
    gets_retry boolean,
    invoice_retry boolean,
    custom_retry_days integer DEFAULT 0,
    modul_number integer DEFAULT 0,
    modul_order_number integer DEFAULT 0,
    discount double precision DEFAULT 0,
    ready_for_invoice boolean,
    process_status integer DEFAULT 500,
    proposal_number integer DEFAULT 0,
    customers_ordernumber character varying(120) DEFAULT 0,
    customers_partner_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    staff_id integer DEFAULT 0,
    pricegroup1 boolean,
    pricegroup2 boolean,
    pricegroup3 boolean,
    pricegroup4 boolean,
    pricegroup_none boolean,
    discount_value double precision DEFAULT 0,
    from_modul integer DEFAULT 0,
    from_modul_id integer DEFAULT 0,
    enquiry_from date,
    processed_at date,
    enquiry_number character varying(120) DEFAULT 0,
    enquiry_id integer DEFAULT 0,
    answered_at date,
    desired_date text,
    customer_enquiry_number character varying(60)
)
INHERITS (public.t_standard);


ALTER TABLE public.proposal_history OWNER TO cuon_admin;

--
-- Name: proposal_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposal_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposal_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposal_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.proposal_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposal_id OWNER TO cuon_admin;

--
-- Name: proposal_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposal ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalget; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalget (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    number character varying(25),
    partnernumber integer,
    loadreference character varying(20),
    pickup_at date,
    remark_to_pickup_at character varying(100),
    forwarding_agency_number integer,
    contact_person_number integer,
    pickup_note_number character(20),
    pickup_note_date date,
    pickup_note_status character(40),
    orderid integer
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalget OWNER TO cuon_admin;

--
-- Name: proposalget_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalget_history (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    number character varying(25),
    partnernumber integer,
    loadreference character varying(20),
    pickup_at date,
    remark_to_pickup_at character varying(100),
    forwarding_agency_number integer,
    contact_person_number integer,
    pickup_note_number character(20),
    pickup_note_date date,
    pickup_note_status character(40),
    orderid integer
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalget_history OWNER TO cuon_admin;

--
-- Name: proposalget_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalget_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalget_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalget_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.proposalget_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposalget_id OWNER TO cuon_admin;

--
-- Name: proposalget_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalget ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalget_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalinvoice; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalinvoice (
    orderid integer,
    order_top integer,
    tax_vat_for_all_positions integer DEFAULT 0,
    different_billing_address_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalinvoice OWNER TO cuon_admin;

--
-- Name: proposalinvoice_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalinvoice_history (
    orderid integer,
    order_top integer,
    tax_vat_for_all_positions integer DEFAULT 0,
    different_billing_address_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalinvoice_history OWNER TO cuon_admin;

--
-- Name: proposalinvoice_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalinvoice_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalinvoice_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalinvoice_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.proposalinvoice_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposalinvoice_id OWNER TO cuon_admin;

--
-- Name: proposalinvoice_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalinvoice ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalinvoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalmisc; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalmisc (
    orderid integer,
    retry boolean,
    monthly boolean,
    quarterly boolean,
    semestral boolean,
    yearly boolean,
    daily boolean,
    weekly boolean,
    custom_retry_days integer,
    create_get boolean,
    create_supply boolean,
    create_invoice boolean,
    part_of_order boolean,
    part_of_order_number integer,
    original_order_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalmisc OWNER TO cuon_admin;

--
-- Name: proposalmisc_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalmisc_history (
    orderid integer,
    retry boolean,
    monthly boolean,
    quarterly boolean,
    semestral boolean,
    yearly boolean,
    daily boolean,
    weekly boolean,
    custom_retry_days integer,
    create_get boolean,
    create_supply boolean,
    create_invoice boolean,
    part_of_order boolean,
    part_of_order_number integer,
    original_order_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalmisc_history OWNER TO cuon_admin;

--
-- Name: proposalmisc_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalmisc_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalmisc_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalmisc_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.proposalmisc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposalmisc_id OWNER TO cuon_admin;

--
-- Name: proposalmisc_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalmisc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalmisc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalposition; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalposition (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalposition OWNER TO cuon_admin;

--
-- Name: proposalposition_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalposition_history (
    orderid integer,
    articleid integer,
    designation character(120),
    amount double precision,
    "position" integer,
    price double precision,
    tax_vat double precision,
    discount double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalposition_history OWNER TO cuon_admin;

--
-- Name: proposalposition_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalposition_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalposition_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalposition_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.proposalposition_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposalposition_id OWNER TO cuon_admin;

--
-- Name: proposalposition_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalposition ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalposition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalsupply; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalsupply (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    orderid integer,
    number character varying(25)
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalsupply OWNER TO cuon_admin;

--
-- Name: proposalsupply_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.proposalsupply_history (
    ordernumber character varying(30),
    addressnumber integer,
    designation character varying(50),
    orderid integer,
    number character varying(25)
)
INHERITS (public.t_standard);


ALTER TABLE public.proposalsupply_history OWNER TO cuon_admin;

--
-- Name: proposalsupply_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalsupply_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalsupply_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: proposalsupply_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.proposalsupply_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proposalsupply_id OWNER TO cuon_admin;

--
-- Name: proposalsupply_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.proposalsupply ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.proposalsupply_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_sourcecode; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_sourcecode (
    name character varying(140),
    designation character varying(140),
    partner_id integer DEFAULT 0,
    starts_at date,
    ends_at date,
    time_in_days integer,
    process_status integer,
    prg_source_suffix character varying(250),
    prg_source_editor character varying(250),
    prg_gui_suffix character varying(250),
    prg_gui_editor character varying(250),
    prg_ini_suffix character varying(250),
    prg_ini_editor character varying(250),
    prg_field_suffix character varying(250),
    prg_field_editor character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.t_sourcecode OWNER TO cuon_admin;

--
-- Name: sourcecode_file; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_file (
    modul_id integer,
    file_folder character varying(250),
    file_name character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_file OWNER TO cuon_admin;

--
-- Name: sourcecode_file_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_file_history (
    modul_id integer,
    file_folder character varying(250),
    file_name character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_file_history OWNER TO cuon_admin;

--
-- Name: sourcecode_file_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_file_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_file_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_file_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.sourcecode_file_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourcecode_file_id OWNER TO cuon_admin;

--
-- Name: sourcecode_file_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_file ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_modul_material_res; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_modul_material_res (
    modul_id integer,
    article_id integer,
    get_at date,
    unit character varying(30),
    planed_amount double precision,
    real_amount double precision,
    material_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.sourcecode_modul_material_res OWNER TO cuon_admin;

--
-- Name: sourcecode_modul_material_res_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_modul_material_res_history (
    modul_id integer,
    article_id integer,
    get_at date,
    unit character varying(30),
    planed_amount double precision,
    real_amount double precision,
    material_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.sourcecode_modul_material_res_history OWNER TO cuon_admin;

--
-- Name: sourcecode_modul_material_res_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_modul_material_res_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_modul_material_res_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_modul_material_res_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.sourcecode_modul_material_res_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourcecode_modul_material_res_id OWNER TO cuon_admin;

--
-- Name: sourcecode_modul_material_res_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_modul_material_res ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_modul_material_res_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_module; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_module (
    part_id integer,
    modul_folder character varying(250),
    file_name character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_module OWNER TO cuon_admin;

--
-- Name: sourcecode_module_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_module_history (
    part_id integer,
    modul_folder character varying(250),
    file_name character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_module_history OWNER TO cuon_admin;

--
-- Name: sourcecode_module_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_module_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_module_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_module_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.sourcecode_module_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourcecode_module_id OWNER TO cuon_admin;

--
-- Name: sourcecode_module_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_module ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_module_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_module_staff_res; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_module_staff_res (
    modul_id integer,
    staff_id integer,
    planed_working_day date,
    real_working_day date,
    planed_time_begin character varying(10),
    planed_time_ends character varying(10),
    real_time_begin character varying(10),
    real_time_ends character varying(10),
    staff_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.sourcecode_module_staff_res OWNER TO cuon_admin;

--
-- Name: sourcecode_module_staff_res_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_module_staff_res_history (
    modul_id integer,
    staff_id integer,
    planed_working_day date,
    real_working_day date,
    planed_time_begin character varying(10),
    planed_time_ends character varying(10),
    real_time_begin character varying(10),
    real_time_ends character varying(10),
    staff_status integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.sourcecode_module_staff_res_history OWNER TO cuon_admin;

--
-- Name: sourcecode_module_staff_res_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_module_staff_res_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_module_staff_res_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_module_staff_res_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.sourcecode_module_staff_res_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourcecode_module_staff_res_id OWNER TO cuon_admin;

--
-- Name: sourcecode_module_staff_res_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_module_staff_res ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_module_staff_res_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_parts; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_parts (
    project_id integer,
    part_folder character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_parts OWNER TO cuon_admin;

--
-- Name: sourcecode_parts_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_parts_history (
    project_id integer,
    part_folder character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_parts_history OWNER TO cuon_admin;

--
-- Name: sourcecode_parts_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_parts_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_parts_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_parts_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.sourcecode_parts_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourcecode_parts_id OWNER TO cuon_admin;

--
-- Name: sourcecode_parts_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_parts ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_parts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_projects; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_projects (
    customer_id integer,
    modul_project_number integer,
    modul_number integer,
    internal_project_number character varying(140),
    project_folder character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_projects OWNER TO cuon_admin;

--
-- Name: sourcecode_projects_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.sourcecode_projects_history (
    customer_id integer,
    modul_project_number integer,
    modul_number integer,
    internal_project_number character varying(140),
    project_folder character varying(250)
)
INHERITS (public.t_sourcecode);


ALTER TABLE public.sourcecode_projects_history OWNER TO cuon_admin;

--
-- Name: sourcecode_projects_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_projects_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_projects_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sourcecode_projects_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.sourcecode_projects_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sourcecode_projects_id OWNER TO cuon_admin;

--
-- Name: sourcecode_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.sourcecode_projects ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sourcecode_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff (
    titular character varying(30),
    phone1 character varying(25),
    phone2 character varying(25),
    homepage character varying(100),
    faxprivat character varying(25),
    birthday date,
    staff_number character varying(55),
    cuon_username character varying(55),
    letter_phrase_1 character varying(155),
    letter_phrase_2 character varying(155),
    my_sign_1 character varying(155),
    signature_1 text,
    signature_2 text,
    signature_graphic_1 integer,
    signature_graphic_2 integer
)
INHERITS (public.t_address);


ALTER TABLE public.staff OWNER TO cuon_admin;

--
-- Name: staff_disease; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_disease (
    staff_id integer,
    name character varying(50),
    designation character varying(250),
    disease_from date,
    disease_to date,
    days double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_disease OWNER TO cuon_admin;

--
-- Name: staff_disease_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_disease_history (
    staff_id integer,
    name character varying(50),
    designation character varying(250),
    disease_from date,
    disease_to date,
    days double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_disease_history OWNER TO cuon_admin;

--
-- Name: staff_disease_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_disease_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_disease_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_disease_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.staff_disease_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_disease_id OWNER TO cuon_admin;

--
-- Name: staff_disease_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_disease ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_disease_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_fee; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_fee (
    staff_id integer,
    working_time_from character varying(5),
    working_time_to character varying(5),
    fee_per_hour double precision,
    fee_per_hour_calc double precision,
    fee_per_hour_invoice double precision,
    fee_per_overtime double precision,
    fee_per_overtime_calc double precision,
    fee_per_overtime_invoice double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_fee OWNER TO cuon_admin;

--
-- Name: staff_fee_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_fee_history (
    staff_id integer,
    working_time_from character varying(5),
    working_time_to character varying(5),
    fee_per_hour double precision,
    fee_per_hour_calc double precision,
    fee_per_hour_invoice double precision,
    fee_per_overtime double precision,
    fee_per_overtime_calc double precision,
    fee_per_overtime_invoice double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_fee_history OWNER TO cuon_admin;

--
-- Name: staff_fee_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_fee_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_fee_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_fee_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.staff_fee_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_fee_id OWNER TO cuon_admin;

--
-- Name: staff_fee_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_fee ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_fee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_history (
    titular character varying(30),
    phone1 character varying(25),
    phone2 character varying(25),
    homepage character varying(100),
    faxprivat character varying(25),
    birthday date,
    staff_number character varying(55),
    cuon_username character varying(55),
    letter_phrase_1 character varying(155),
    letter_phrase_2 character varying(155),
    my_sign_1 character varying(155),
    signature_1 text,
    signature_2 text,
    signature_graphic_1 integer,
    signature_graphic_2 integer
)
INHERITS (public.t_address);


ALTER TABLE public.staff_history OWNER TO cuon_admin;

--
-- Name: staff_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.staff_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_id OWNER TO cuon_admin;

--
-- Name: staff_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_misc; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_misc (
    staff_id integer,
    holyday_code integer
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_misc OWNER TO cuon_admin;

--
-- Name: staff_misc_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_misc_history (
    staff_id integer,
    holyday_code integer
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_misc_history OWNER TO cuon_admin;

--
-- Name: staff_misc_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_misc_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_misc_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_misc_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.staff_misc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_misc_id OWNER TO cuon_admin;

--
-- Name: staff_misc_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_misc ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_misc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_vacation; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_vacation (
    staff_id integer,
    name character varying(50),
    designation character varying(250),
    vacation_from date,
    vacation_to date,
    days double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_vacation OWNER TO cuon_admin;

--
-- Name: staff_vacation_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.staff_vacation_history (
    staff_id integer,
    name character varying(50),
    designation character varying(250),
    vacation_from date,
    vacation_to date,
    days double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.staff_vacation_history OWNER TO cuon_admin;

--
-- Name: staff_vacation_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_vacation_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_vacation_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: staff_vacation_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.staff_vacation_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.staff_vacation_id OWNER TO cuon_admin;

--
-- Name: staff_vacation_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.staff_vacation ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.staff_vacation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: states; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.states (
    country_id integer,
    name character varying(160),
    state_short character varying(6)
)
INHERITS (public.t_standard);


ALTER TABLE public.states OWNER TO cuon_admin;

--
-- Name: states_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.states_history (
    country_id integer,
    name character varying(160),
    state_short character varying(6)
)
INHERITS (public.t_standard);


ALTER TABLE public.states_history OWNER TO cuon_admin;

--
-- Name: states_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.states_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.states_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: states_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.states_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.states_id OWNER TO cuon_admin;

--
-- Name: states_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.states ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.states_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: stock_goods; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.stock_goods (
    stock_id integer,
    article_id integer,
    to_embed double precision,
    roll_out double precision,
    designation character varying(50),
    date_of_change timestamp without time zone,
    reference_number character varying(30),
    actual_stock double precision,
    delivery_id integer,
    stock_place_id integer,
    stock_place_info character varying(250),
    single_price double precision,
    process_status integer,
    real_place character varying(250),
    special_flag_bit integer,
    reserved_bit integer DEFAULT 0,
    booking_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    order_id integer DEFAULT 0,
    actual_reserved double precision,
    actual_netto double precision,
    total_sum double precision,
    reserved_sum double precision,
    netto_sum double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.stock_goods OWNER TO cuon_admin;

--
-- Name: stock_goods_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.stock_goods_history (
    stock_id integer,
    article_id integer,
    to_embed double precision,
    roll_out double precision,
    designation character varying(50),
    date_of_change timestamp without time zone,
    reference_number character varying(30),
    actual_stock double precision,
    delivery_id integer,
    stock_place_id integer,
    stock_place_info character varying(250),
    single_price double precision,
    process_status integer,
    real_place character varying(250),
    special_flag_bit integer,
    reserved_bit integer DEFAULT 0,
    booking_id integer DEFAULT 0,
    project_id integer DEFAULT 0,
    order_id integer DEFAULT 0,
    actual_reserved double precision,
    actual_netto double precision,
    total_sum double precision,
    reserved_sum double precision,
    netto_sum double precision
)
INHERITS (public.t_standard);


ALTER TABLE public.stock_goods_history OWNER TO cuon_admin;

--
-- Name: stock_goods_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.stock_goods_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.stock_goods_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: stock_goods_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.stock_goods_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_goods_id OWNER TO cuon_admin;

--
-- Name: stock_goods_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.stock_goods ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.stock_goods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: stocks; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.stocks (
    name character varying(35),
    designation character varying(125),
    address character varying(255),
    phone character varying(50),
    cost_center_a character varying(10),
    cost_center_b character varying(10),
    cost_center_c character varying(10),
    cost_center_d character varying(10)
)
INHERITS (public.t_standard);


ALTER TABLE public.stocks OWNER TO cuon_admin;

--
-- Name: stocks_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.stocks_history (
    name character varying(35),
    designation character varying(125),
    address character varying(255),
    phone character varying(50),
    cost_center_a character varying(10),
    cost_center_b character varying(10),
    cost_center_c character varying(10),
    cost_center_d character varying(10)
)
INHERITS (public.t_standard);


ALTER TABLE public.stocks_history OWNER TO cuon_admin;

--
-- Name: stocks_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.stocks_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.stocks_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: stocks_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.stocks_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stocks_id OWNER TO cuon_admin;

--
-- Name: stocks_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.stocks ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.stocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: support_project; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.support_project (
    designation character varying(250),
    support_project_number character varying(100),
    project_id integer,
    is_public integer
)
INHERITS (public.t_standard);


ALTER TABLE public.support_project OWNER TO cuon_admin;

--
-- Name: support_project_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.support_project_history (
    designation character varying(250),
    support_project_number character varying(100),
    project_id integer,
    is_public integer
)
INHERITS (public.t_standard);


ALTER TABLE public.support_project_history OWNER TO cuon_admin;

--
-- Name: support_project_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.support_project_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.support_project_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: support_project_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.support_project_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.support_project_id OWNER TO cuon_admin;

--
-- Name: support_project_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.support_project ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.support_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: support_ticket; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.support_ticket (
    ticket_number character varying(36),
    designation text,
    support_project_id integer,
    status_id integer,
    severity_id integer,
    priority_id integer,
    reproduce_id integer,
    reported_by_id integer,
    reported_day date,
    reported_time time without time zone,
    solved_day date,
    solved_time time without time zone,
    platform_id integer,
    note text,
    short_designation character varying(255),
    reported_by_partner_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.support_ticket OWNER TO cuon_admin;

--
-- Name: support_ticket_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.support_ticket_history (
    ticket_number character varying(36),
    designation text,
    support_project_id integer,
    status_id integer,
    severity_id integer,
    priority_id integer,
    reproduce_id integer,
    reported_by_id integer,
    reported_day date,
    reported_time time without time zone,
    solved_day date,
    solved_time time without time zone,
    platform_id integer,
    note text,
    short_designation character varying(255),
    reported_by_partner_id integer
)
INHERITS (public.t_standard);


ALTER TABLE public.support_ticket_history OWNER TO cuon_admin;

--
-- Name: support_ticket_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.support_ticket_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.support_ticket_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: support_ticket_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.support_ticket_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.support_ticket_id OWNER TO cuon_admin;

--
-- Name: support_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.support_ticket ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.support_ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_address_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_address_history (
    address character varying(30),
    lastname character varying(50),
    lastname2 character varying(50),
    firstname character varying(50),
    street character varying(50),
    country character varying(6),
    state character varying(6),
    zip character varying(10),
    city character varying(50),
    phone character varying(25),
    fax character varying(25),
    email character varying(100),
    zip_for_postbox character varying(10),
    postbox character varying(20),
    suburb character varying(50),
    state_full character varying(50),
    letter_address character varying(120),
    phone_handy character varying(35),
    email_noification character varying(35),
    newsletter character varying(17000),
    homepage_url character varying(250),
    status_info character varying(250),
    sip character varying(225),
    skype character varying(225),
    letter_address2 character varying(225),
    letter_address3 character varying(225),
    letter_address4 character varying(225),
    letter_address5 character varying(225)
)
INHERITS (public.t_standard);


ALTER TABLE public.t_address_history OWNER TO cuon_admin;

--
-- Name: t_address_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_address_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_address_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_address_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.t_address_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_address_id OWNER TO cuon_admin;

--
-- Name: t_address_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_address ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_grave_work_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_grave_work_history (
    grave_id integer,
    service_count double precision,
    service_designation character varying(255),
    service_price double precision,
    service_date date,
    service_notes text,
    article_id integer,
    period_id integer,
    services text,
    automatic_price integer DEFAULT 1,
    created_order integer DEFAULT 0
)
INHERITS (public.t_standard);


ALTER TABLE public.t_grave_work_history OWNER TO cuon_admin;

--
-- Name: t_grave_work_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_grave_work_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_grave_work_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_grave_work_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.t_grave_work_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_grave_work_id OWNER TO cuon_admin;

--
-- Name: t_grave_work_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_grave_work ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_grave_work_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_partner_schedul_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_partner_schedul_history (
    partnerid integer,
    schedul_date character varying(10),
    schedul_time_begin integer,
    schedul_time_end integer,
    priority integer DEFAULT 0,
    process_status integer DEFAULT 1,
    notes text,
    short_remark character varying(60),
    alarm_notify boolean,
    alarm_time integer,
    alarm_days integer,
    alarm_hours integer,
    alarm_minutes integer,
    schedul_staff_id integer DEFAULT 1,
    schedul_date_end character varying(10),
    dschedul_date date,
    dschedul_date_end date,
    begin_end_text character varying(50),
    begin_text character varying(5),
    end_text character varying(5)
)
INHERITS (public.t_standard);


ALTER TABLE public.t_partner_schedul_history OWNER TO cuon_admin;

--
-- Name: t_partner_schedul_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_partner_schedul_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_partner_schedul_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_partner_schedul_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.t_partner_schedul_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_partner_schedul_id OWNER TO cuon_admin;

--
-- Name: t_partner_schedul_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_partner_schedul ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_partner_schedul_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_sourcecode_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_sourcecode_history (
    name character varying(140),
    designation character varying(140),
    partner_id integer DEFAULT 0,
    starts_at date,
    ends_at date,
    time_in_days integer,
    process_status integer,
    prg_source_suffix character varying(250),
    prg_source_editor character varying(250),
    prg_gui_suffix character varying(250),
    prg_gui_editor character varying(250),
    prg_ini_suffix character varying(250),
    prg_ini_editor character varying(250),
    prg_field_suffix character varying(250),
    prg_field_editor character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.t_sourcecode_history OWNER TO cuon_admin;

--
-- Name: t_sourcecode_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_sourcecode_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_sourcecode_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_sourcecode_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.t_sourcecode_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_sourcecode_id OWNER TO cuon_admin;

--
-- Name: t_sourcecode_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_sourcecode ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_sourcecode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_standard_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.t_standard_history (
    id integer NOT NULL,
    user_id character(25),
    status character(25),
    insert_time timestamp without time zone,
    update_time timestamp without time zone,
    update_user_id character varying(25),
    client integer DEFAULT 1,
    sep_info_1 integer DEFAULT 0,
    sep_info_2 integer DEFAULT 0,
    sep_info_3 integer DEFAULT 0,
    versions_number integer DEFAULT 0,
    versions_uuid character varying(36) DEFAULT '0'::character varying,
    uuid character varying(36) DEFAULT '0'::character varying
);


ALTER TABLE public.t_standard_history OWNER TO cuon_admin;

--
-- Name: t_standard_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_standard_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_standard_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: t_standard_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.t_standard_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_standard_id OWNER TO cuon_admin;

--
-- Name: t_standard_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.t_standard ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.t_standard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tax_vat; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.tax_vat (
    vat_name character varying(20),
    vat_designation character varying(60),
    vat_value double precision,
    vat_type character varying(10),
    vat_country character varying(10),
    vat_def character varying(10),
    vat_coefficient double precision,
    vat_acct1 character varying(10),
    vat_acct2 character varying(10),
    logo text
)
INHERITS (public.t_standard);


ALTER TABLE public.tax_vat OWNER TO cuon_admin;

--
-- Name: tax_vat_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.tax_vat_history (
    vat_name character varying(20),
    vat_designation character varying(60),
    vat_value double precision,
    vat_type character varying(10),
    vat_country character varying(10),
    vat_def character varying(10),
    vat_coefficient double precision,
    vat_acct1 character varying(10),
    vat_acct2 character varying(10),
    logo text
)
INHERITS (public.t_standard);


ALTER TABLE public.tax_vat_history OWNER TO cuon_admin;

--
-- Name: tax_vat_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.tax_vat_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tax_vat_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tax_vat_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.tax_vat_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tax_vat_id OWNER TO cuon_admin;

--
-- Name: tax_vat_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.tax_vat ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tax_vat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: terms_of_payment; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.terms_of_payment (
    number character varying(50),
    designation character varying(250),
    short_expression character varying(900),
    term_of_payment text,
    value1 character varying(200),
    value2 character varying(200),
    value3 character varying(200),
    value4 character varying(200),
    value5 character varying(200),
    value6 character varying(200),
    value7 character varying(200),
    value8 character varying(200),
    value9 character varying(200),
    value10 character varying(200),
    days integer,
    delay integer
)
INHERITS (public.t_standard);


ALTER TABLE public.terms_of_payment OWNER TO cuon_admin;

--
-- Name: terms_of_payment_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.terms_of_payment_history (
    number character varying(50),
    designation character varying(250),
    short_expression character varying(900),
    term_of_payment text,
    value1 character varying(200),
    value2 character varying(200),
    value3 character varying(200),
    value4 character varying(200),
    value5 character varying(200),
    value6 character varying(200),
    value7 character varying(200),
    value8 character varying(200),
    value9 character varying(200),
    value10 character varying(200),
    days integer,
    delay integer
)
INHERITS (public.t_standard);


ALTER TABLE public.terms_of_payment_history OWNER TO cuon_admin;

--
-- Name: terms_of_payment_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.terms_of_payment_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.terms_of_payment_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: terms_of_payment_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.terms_of_payment_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.terms_of_payment_id OWNER TO cuon_admin;

--
-- Name: terms_of_payment_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.terms_of_payment ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.terms_of_payment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: user_info; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.user_info (
    real_name character varying(254),
    designation character varying(254),
    title character varying(254)
)
INHERITS (public.t_standard);


ALTER TABLE public.user_info OWNER TO cuon_admin;

--
-- Name: user_info_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.user_info_history (
    real_name character varying(254),
    designation character varying(254),
    title character varying(254)
)
INHERITS (public.t_standard);


ALTER TABLE public.user_info_history OWNER TO cuon_admin;

--
-- Name: user_info_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.user_info_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.user_info_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: user_info_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.user_info_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_info_id OWNER TO cuon_admin;

--
-- Name: user_info_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.user_info ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.user_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: usergroups; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.usergroups (
    group_name character varying(55),
    group_designation character varying(254)
)
INHERITS (public.t_standard);


ALTER TABLE public.usergroups OWNER TO cuon_admin;

--
-- Name: usergroups_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.usergroups_history (
    group_name character varying(55),
    group_designation character varying(254)
)
INHERITS (public.t_standard);


ALTER TABLE public.usergroups_history OWNER TO cuon_admin;

--
-- Name: usergroups_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.usergroups_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usergroups_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: usergroups_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.usergroups_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usergroups_id OWNER TO cuon_admin;

--
-- Name: usergroups_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.usergroups ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.usergroups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: userlogin; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.userlogin (
    username character varying(16),
    lastname character varying(30),
    firstname character varying(30)
)
INHERITS (public.t_standard);


ALTER TABLE public.userlogin OWNER TO cuon_admin;

--
-- Name: userlogin_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.userlogin_history (
    username character varying(16),
    lastname character varying(30),
    firstname character varying(30)
)
INHERITS (public.t_standard);


ALTER TABLE public.userlogin_history OWNER TO cuon_admin;

--
-- Name: userlogin_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.userlogin_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.userlogin_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: userlogin_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.userlogin_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.userlogin_id OWNER TO cuon_admin;

--
-- Name: userlogin_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.userlogin ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.userlogin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: web2; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.web2 (
    name character(50),
    designation character(250),
    type integer,
    data text,
    linked_keys text,
    root_keys text,
    c_type_value character varying(60),
    save_to_dir character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.web2 OWNER TO cuon_admin;

--
-- Name: web2_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.web2_history (
    name character(50),
    designation character(250),
    type integer,
    data text,
    linked_keys text,
    root_keys text,
    c_type_value character varying(60),
    save_to_dir character varying(250)
)
INHERITS (public.t_standard);


ALTER TABLE public.web2_history OWNER TO cuon_admin;

--
-- Name: web2_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.web2_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.web2_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: web2_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.web2_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web2_id OWNER TO cuon_admin;

--
-- Name: web2_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.web2 ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.web2_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: xmlvalues; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.xmlvalues (
    name character(50),
    description character(250),
    text text
)
INHERITS (public.t_standard);


ALTER TABLE public.xmlvalues OWNER TO cuon_admin;

--
-- Name: xmlvalues_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.xmlvalues_history (
    name character(50),
    description character(250),
    text text
)
INHERITS (public.t_standard);


ALTER TABLE public.xmlvalues_history OWNER TO cuon_admin;

--
-- Name: xmlvalues_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.xmlvalues_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.xmlvalues_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: xmlvalues_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.xmlvalues_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xmlvalues_id OWNER TO cuon_admin;

--
-- Name: xmlvalues_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.xmlvalues ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.xmlvalues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: zipcodes; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.zipcodes (
    city_id integer,
    zipcode character varying(20)
)
INHERITS (public.t_standard);


ALTER TABLE public.zipcodes OWNER TO cuon_admin;

--
-- Name: zipcodes_history; Type: TABLE; Schema: public; Owner: cuon_admin
--

CREATE TABLE public.zipcodes_history (
    city_id integer,
    zipcode character varying(20)
)
INHERITS (public.t_standard);


ALTER TABLE public.zipcodes_history OWNER TO cuon_admin;

--
-- Name: zipcodes_history_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.zipcodes_history ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.zipcodes_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: zipcodes_id; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

CREATE SEQUENCE public.zipcodes_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zipcodes_id OWNER TO cuon_admin;

--
-- Name: zipcodes_id_seq; Type: SEQUENCE; Schema: public; Owner: cuon_admin
--

ALTER TABLE public.zipcodes ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.zipcodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: account_info client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_info sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_info sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_info sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_info versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_info versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_info uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_info_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_info_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_info_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_info_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_info_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_info_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_info_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_info_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_plan client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_plan sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_plan sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_plan sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_plan versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_plan versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_plan uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_plan_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_plan_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_plan_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_plan_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_plan_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_plan_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_plan_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_plan_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_sentence client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_sentence sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_sentence sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_sentence sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_sentence versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_sentence versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_sentence uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_sentence_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_sentence_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_sentence_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_sentence_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_sentence_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_sentence_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_sentence_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_sentence_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_system client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_system sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_system sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_system sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_system versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_system versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_system uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: account_system_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: account_system_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: account_system_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: account_system_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: account_system_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: account_system_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: account_system_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.account_system_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN client SET DEFAULT 1;


--
-- Name: address sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: address sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: address sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: address versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: address versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: address uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address_bank client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN client SET DEFAULT 1;


--
-- Name: address_bank sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: address_bank sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: address_bank sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: address_bank versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: address_bank versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: address_bank uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address_bank_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: address_bank_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: address_bank_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: address_bank_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: address_bank_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: address_bank_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: address_bank_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: address_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: address_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: address_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: address_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: address_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: address_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address_notes client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN client SET DEFAULT 1;


--
-- Name: address_notes sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: address_notes sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: address_notes sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: address_notes versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: address_notes versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: address_notes uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address_notes_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: address_notes_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: address_notes_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: address_notes_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: address_notes_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: address_notes_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: address_notes_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: addresses_misc client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN client SET DEFAULT 1;


--
-- Name: addresses_misc sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: addresses_misc sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: addresses_misc sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: addresses_misc versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: addresses_misc versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: addresses_misc uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: addresses_misc_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: addresses_misc_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: addresses_misc_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: addresses_misc_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: addresses_misc_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: addresses_misc_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: addresses_misc_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: administrative_district client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN client SET DEFAULT 1;


--
-- Name: administrative_district sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: administrative_district sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: administrative_district sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: administrative_district versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: administrative_district versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: administrative_district uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: administrative_district_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: administrative_district_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: administrative_district_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: administrative_district_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: administrative_district_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: administrative_district_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: administrative_district_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.administrative_district_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_barcode client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_barcode sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_barcode sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_barcode sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_barcode versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_barcode versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_barcode uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_barcode_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_barcode_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_barcode_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_barcode_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_barcode_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_barcode_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_barcode_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_barcode_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_customers client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_customers sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_customers sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_customers sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_customers versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_customers versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_customers uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_customers_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_customers_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_customers_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_customers_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_customers_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_customers_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_customers_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_customers_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_parts_list client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_parts_list sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_parts_list sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_parts_list sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_parts_list versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_parts_list versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_parts_list uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_parts_list_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_parts_list_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_parts_list_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_parts_list_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_parts_list_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_parts_list_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_parts_list_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_parts_list_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_purchase client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_purchase sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_purchase sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_purchase sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_purchase versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_purchase versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_purchase uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_purchase_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_purchase_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_purchase_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_purchase_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_purchase_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_purchase_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_purchase_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_purchase_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_sales client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_sales sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_sales sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_sales sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_sales versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_sales versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_sales uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_sales_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_sales_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_sales_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_sales_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_sales_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_sales_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_sales_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_sales_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_stock client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_stock sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_stock sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_stock sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_stock versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_stock versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_stock uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_stock_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_stock_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_stock_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_stock_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_stock_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_stock_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_stock_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_webshop client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_webshop sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_webshop sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_webshop sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_webshop versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_webshop versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_webshop uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_webshop_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: articles_webshop_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: articles_webshop_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: articles_webshop_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: articles_webshop_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: articles_webshop_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: articles_webshop_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_webshop_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: bank client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN client SET DEFAULT 1;


--
-- Name: bank sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: bank sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: bank sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: bank versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: bank versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: bank uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: bank_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: bank_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: bank_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: bank_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: bank_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: bank_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: bank_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.bank_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: biblio client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN client SET DEFAULT 1;


--
-- Name: biblio sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: biblio sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: biblio sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: biblio versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: biblio versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: biblio uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: biblio_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: biblio_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: biblio_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: biblio_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: biblio_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: biblio_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: biblio_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.biblio_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_class client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_class sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_class sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_class sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_class versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_class versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_class uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_class_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_class_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_class_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_class_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_class_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_class_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_class_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_class_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_divisio client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_divisio sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_divisio sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_divisio sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_divisio versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_divisio versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_divisio uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_divisio_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_divisio_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_divisio_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_divisio_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_divisio_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_divisio_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_divisio_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_divisio_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_family client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_family sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_family sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_family sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_family versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_family versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_family uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_family_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_family_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_family_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_family_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_family_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_family_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_family_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_family_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_genus client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_genus sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_genus sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_genus sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_genus versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_genus versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_genus uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_genus_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_genus_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_genus_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_genus_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_genus_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_genus_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_genus_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_genus_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_kingdom client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_kingdom sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_kingdom sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_kingdom sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_kingdom versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_kingdom versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_kingdom uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_kingdom_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_kingdom_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_kingdom_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_kingdom_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_kingdom_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_kingdom_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_kingdom_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_kingdom_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_ordo client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_ordo sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_ordo sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_ordo sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_ordo versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_ordo versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_ordo uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_ordo_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: botany_ordo_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: botany_ordo_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: botany_ordo_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: botany_ordo_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: botany_ordo_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: botany_ordo_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.botany_ordo_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cash_desk sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cash_desk sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cash_desk sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cash_desk versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cash_desk versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk_book_number client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cash_desk_book_number sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cash_desk_book_number sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cash_desk_book_number sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cash_desk_book_number versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cash_desk_book_number versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk_book_number uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk_book_number_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cash_desk_book_number_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cash_desk_book_number_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cash_desk_book_number_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cash_desk_book_number_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cash_desk_book_number_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk_book_number_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_book_number_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cash_desk_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cash_desk_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cash_desk_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cash_desk_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cash_desk_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cash_desk_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cash_desk_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: city client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN client SET DEFAULT 1;


--
-- Name: city sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: city sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: city sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: city versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: city versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: city uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: city_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: city_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: city_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: city_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: city_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: city_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: city_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.city_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: clients client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN client SET DEFAULT 1;


--
-- Name: clients sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: clients sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: clients sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: clients versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: clients versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: clients uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: clients_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: clients_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: clients_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: clients_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: clients_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: clients_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: clients_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.clients_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: contact client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN client SET DEFAULT 1;


--
-- Name: contact sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: contact sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: contact sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: contact versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: contact versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: contact uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: contact priority; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN priority SET DEFAULT 0;


--
-- Name: contact process_status; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN process_status SET DEFAULT 1;


--
-- Name: contact schedul_staff_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact ALTER COLUMN schedul_staff_id SET DEFAULT 1;


--
-- Name: contact_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: contact_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: contact_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: contact_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: contact_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: contact_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: contact_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: contact_history priority; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN priority SET DEFAULT 0;


--
-- Name: contact_history process_status; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN process_status SET DEFAULT 1;


--
-- Name: contact_history schedul_staff_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.contact_history ALTER COLUMN schedul_staff_id SET DEFAULT 1;


--
-- Name: country client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN client SET DEFAULT 1;


--
-- Name: country sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: country sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: country sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: country versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: country versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: country uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: country_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: country_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: country_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: country_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: country_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: country_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: country_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.country_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_clients client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_clients sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_clients sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_clients sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_clients versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_clients versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_clients uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_clients_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_clients_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_clients_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_clients_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_clients_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_clients_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_clients_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_clients_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_config key_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_config ALTER COLUMN key_id SET DEFAULT nextval('public.cuon_config_key_id_seq'::regclass);


--
-- Name: cuon_config_history key_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_config_history ALTER COLUMN key_id SET DEFAULT nextval('public.cuon_config_history_key_id_seq'::regclass);


--
-- Name: cuon_erp1 client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_erp1 sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_erp1 sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_erp1 sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_erp1 versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_erp1 versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_erp1 uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1 ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_erp1_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_erp1_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_erp1_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_erp1_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_erp1_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_erp1_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_erp1_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_user client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_user sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_user sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_user sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_user versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_user versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_user uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_user_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_user_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_user_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_user_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_user_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_user_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_user_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_user_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_values client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_values sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_values sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_values sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_values versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_values versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_values uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_values_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: cuon_values_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: cuon_values_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: cuon_values_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: cuon_values_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: cuon_values_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: cuon_values_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_values_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: district client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN client SET DEFAULT 1;


--
-- Name: district sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: district sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: district sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: district versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: district versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: district uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: district_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: district_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: district_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: district_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: district_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: district_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: district_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.district_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: dms client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN client SET DEFAULT 1;


--
-- Name: dms sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: dms sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: dms sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: dms versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: dms versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: dms uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: dms_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: dms_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: dms_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: dms_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: dms_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: dms_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: dms_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiry client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiry sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiry sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiry sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiry versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiry versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiry uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiry_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiry_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiry_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiry_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiry_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiry_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiry_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiry_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryget client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiryget sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiryget sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiryget sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiryget versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiryget versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryget uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryget_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiryget_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiryget_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiryget_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiryget_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiryget_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryget_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryget_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryinvoice client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiryinvoice sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiryinvoice sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiryinvoice sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiryinvoice versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiryinvoice versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryinvoice uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryinvoice_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiryinvoice_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiryinvoice_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiryinvoice_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiryinvoice_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiryinvoice_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryinvoice_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryinvoice_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirymisc client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquirymisc sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquirymisc sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquirymisc sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquirymisc versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquirymisc versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirymisc uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirymisc_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquirymisc_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquirymisc_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquirymisc_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquirymisc_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquirymisc_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirymisc_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirymisc_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryposition client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiryposition sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiryposition sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiryposition sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiryposition versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiryposition versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryposition uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryposition_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquiryposition_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquiryposition_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquiryposition_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquiryposition_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquiryposition_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquiryposition_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquiryposition_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirysupply client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquirysupply sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquirysupply sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquirysupply sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquirysupply versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquirysupply versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirysupply uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirysupply_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: enquirysupply_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: enquirysupply_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: enquirysupply_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: enquirysupply_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: enquirysupply_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: enquirysupply_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.enquirysupply_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: ext_two client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN client SET DEFAULT 1;


--
-- Name: ext_two sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: ext_two sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: ext_two sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: ext_two versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: ext_two versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: ext_two uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: ext_two_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: ext_two_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: ext_two_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: ext_two_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: ext_two_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: ext_two_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: ext_two_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ext_two_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_invoice_info client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_invoice_info sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_invoice_info sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_invoice_info sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_invoice_info versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_invoice_info versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_invoice_info uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_invoice_info_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_invoice_info_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_invoice_info_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_invoice_info_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_invoice_info_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_invoice_info_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_invoice_info_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_invoice_info_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_service_notes client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_service_notes sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_service_notes sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_service_notes sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_service_notes versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_service_notes versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_service_notes uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_service_notes_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_service_notes_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_service_notes_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_service_notes_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_service_notes_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_service_notes_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_service_notes_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_service_notes_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_autumn client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_autumn sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_autumn sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_autumn sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_autumn versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_autumn versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_autumn uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_autumn automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_autumn created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_autumn_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_autumn_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_autumn_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_autumn_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_autumn_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_autumn_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_autumn_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_autumn_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_autumn_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_autumn_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_holiday client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_holiday sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_holiday sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_holiday sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_holiday versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_holiday versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_holiday uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_holiday automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_holiday created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_holiday_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_holiday_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_holiday_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_holiday_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_holiday_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_holiday_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_holiday_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_holiday_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_holiday_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_holiday_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_maintenance client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_maintenance sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_maintenance sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_maintenance sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_maintenance versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_maintenance versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_maintenance uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_maintenance_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_maintenance_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_maintenance_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_maintenance_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_maintenance_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_maintenance_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_maintenance_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_maintenance_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_single client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_single sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_single sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_single sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_single versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_single versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_single uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_single automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_single created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_single_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_single_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_single_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_single_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_single_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_single_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_single_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_single_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_single_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_single_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_spring client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_spring sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_spring sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_spring sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_spring versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_spring versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_spring uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_spring automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_spring created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_spring_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_spring_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_spring_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_spring_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_spring_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_spring_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_spring_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_spring_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_spring_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_spring_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_summer client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_summer sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_summer sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_summer sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_summer versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_summer versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_summer uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_summer automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_summer created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_summer_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_summer_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_summer_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_summer_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_summer_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_summer_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_summer_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_summer_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_summer_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_summer_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_winter client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_winter sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_winter sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_winter sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_winter versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_winter versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_winter uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_winter automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_winter created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_winter_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_winter_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_winter_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_winter_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_winter_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_winter_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_winter_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_winter_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_winter_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_winter_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_year client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_year sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_year sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_year sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_year versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_year versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_year uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_year automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_year created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: grave_work_year_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: grave_work_year_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: grave_work_year_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: grave_work_year_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: grave_work_year_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: grave_work_year_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_year_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: grave_work_year_history automatic_price; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN automatic_price SET DEFAULT 1;


--
-- Name: grave_work_year_history created_order; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.grave_work_year_history ALTER COLUMN created_order SET DEFAULT 0;


--
-- Name: graveyard client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN client SET DEFAULT 1;


--
-- Name: graveyard sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: graveyard sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: graveyard sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: graveyard versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: graveyard versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: graveyard uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: graveyard_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: graveyard_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: graveyard_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: graveyard_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: graveyard_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: graveyard_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: graveyard_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.graveyard_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN client SET DEFAULT 1;


--
-- Name: hibernation sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: hibernation sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: hibernation sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: hibernation versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: hibernation versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: hibernation_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: hibernation_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: hibernation_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: hibernation_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: hibernation_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation_plant client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN client SET DEFAULT 1;


--
-- Name: hibernation_plant sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: hibernation_plant sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: hibernation_plant sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: hibernation_plant versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: hibernation_plant versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation_plant uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation_plant_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: hibernation_plant_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: hibernation_plant_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: hibernation_plant_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: hibernation_plant_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: hibernation_plant_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: hibernation_plant_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation_plant_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: in_payment client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN client SET DEFAULT 1;


--
-- Name: in_payment sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: in_payment sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: in_payment sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: in_payment versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: in_payment versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: in_payment uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: in_payment_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: in_payment_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: in_payment_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: in_payment_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: in_payment_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: in_payment_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: in_payment_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.in_payment_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_deliveries client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_deliveries sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_deliveries sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_deliveries sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_deliveries versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_deliveries versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_deliveries uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_deliveries_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_deliveries_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_deliveries_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_deliveries_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_deliveries_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_deliveries_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_deliveries_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_deliveries_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_incoming client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_hibernation_incoming sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_incoming uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_incoming_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_hibernation_incoming_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_hibernation_incoming_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_incoming_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_incoming_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_outgoing client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_hibernation_outgoing sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_outgoing uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_outgoing_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_hibernation_outgoing_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_hibernation_outgoing_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_outgoing_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_outgoing_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_pickup client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_hibernation_pickup sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_pickup uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_pickup_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_hibernation_pickup_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_hibernation_pickup_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_hibernation_pickup_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_hibernation_pickup_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_invoices client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_invoices sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_invoices sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_invoices sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_invoices versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_invoices versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_invoices uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_invoices_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_invoices_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_invoices_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_invoices_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_invoices_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_invoices_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_invoices_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_invoices_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_pickups client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_pickups sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_pickups sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_pickups sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_pickups versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_pickups versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_pickups uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_pickups_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: list_of_pickups_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: list_of_pickups_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: list_of_pickups_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: list_of_pickups_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: list_of_pickups_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: list_of_pickups_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.list_of_pickups_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: lock_process client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN client SET DEFAULT 1;


--
-- Name: lock_process sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: lock_process sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: lock_process sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: lock_process versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: lock_process versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: lock_process uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: lock_process_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: lock_process_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: lock_process_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: lock_process_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: lock_process_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: lock_process_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: lock_process_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.lock_process_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: logistics client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN client SET DEFAULT 1;


--
-- Name: logistics sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: logistics sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: logistics sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: logistics versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: logistics versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: logistics uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: logistics_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: logistics_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: logistics_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: logistics_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: logistics_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: logistics_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: logistics_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.logistics_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN client SET DEFAULT 1;


--
-- Name: material_group sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: material_group sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: material_group sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: material_group versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: material_group versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group_accounts client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN client SET DEFAULT 1;


--
-- Name: material_group_accounts sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: material_group_accounts sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: material_group_accounts sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: material_group_accounts versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: material_group_accounts versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group_accounts uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group_accounts_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: material_group_accounts_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: material_group_accounts_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: material_group_accounts_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: material_group_accounts_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: material_group_accounts_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group_accounts_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_accounts_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: material_group_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: material_group_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: material_group_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: material_group_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: material_group_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: material_group_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.material_group_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: mindmap client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN client SET DEFAULT 1;


--
-- Name: mindmap sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: mindmap sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: mindmap sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: mindmap versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: mindmap versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: mindmap uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: mindmap_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: mindmap_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: mindmap_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: mindmap_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: mindmap_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: mindmap_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: mindmap_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.mindmap_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: misc_data client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN client SET DEFAULT 1;


--
-- Name: misc_data sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: misc_data sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: misc_data sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: misc_data versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: misc_data versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: misc_data uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: misc_data_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: misc_data_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: misc_data_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: misc_data_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: misc_data_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: misc_data_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: misc_data_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.misc_data_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderbook client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderbook sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderbook sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderbook sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderbook versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderbook versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderbook uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderbook_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderbook_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderbook_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderbook_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderbook_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderbook_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderbook_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderbook_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderget client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderget sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderget sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderget sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderget versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderget versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderget uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderget_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderget_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderget_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderget_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderget_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderget_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderget_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderget_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderinvoice client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderinvoice sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderinvoice sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderinvoice sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderinvoice versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderinvoice versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderinvoice uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderinvoice_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderinvoice_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderinvoice_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderinvoice_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderinvoice_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderinvoice_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderinvoice_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderinvoice_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: ordermisc client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN client SET DEFAULT 1;


--
-- Name: ordermisc sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: ordermisc sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: ordermisc sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: ordermisc versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: ordermisc versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: ordermisc uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: ordermisc_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: ordermisc_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: ordermisc_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: ordermisc_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: ordermisc_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: ordermisc_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: ordermisc_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordermisc_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderposition client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderposition sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderposition sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderposition sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderposition versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderposition versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderposition uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: orderposition_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: orderposition_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: orderposition_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: orderposition_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: orderposition_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: orderposition_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: orderposition_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: ordersupply client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN client SET DEFAULT 1;


--
-- Name: ordersupply sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: ordersupply sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: ordersupply sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: ordersupply versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: ordersupply versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: ordersupply uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: ordersupply_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: ordersupply_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: ordersupply_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: ordersupply_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: ordersupply_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: ordersupply_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: ordersupply_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.ordersupply_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: partner client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN client SET DEFAULT 1;


--
-- Name: partner sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: partner sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: partner sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: partner versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: partner versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: partner uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: partner_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: partner_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: partner_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: partner_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: partner_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_schedul client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN client SET DEFAULT 1;


--
-- Name: partner_schedul sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: partner_schedul sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: partner_schedul sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: partner_schedul versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: partner_schedul versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_schedul uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_schedul priority; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN priority SET DEFAULT 0;


--
-- Name: partner_schedul process_status; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN process_status SET DEFAULT 1;


--
-- Name: partner_schedul schedul_staff_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul ALTER COLUMN schedul_staff_id SET DEFAULT 1;


--
-- Name: partner_schedul_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: partner_schedul_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: partner_schedul_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: partner_schedul_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: partner_schedul_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: partner_schedul_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_schedul_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: partner_schedul_history priority; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN priority SET DEFAULT 0;


--
-- Name: partner_schedul_history process_status; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN process_status SET DEFAULT 1;


--
-- Name: partner_schedul_history schedul_staff_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner_schedul_history ALTER COLUMN schedul_staff_id SET DEFAULT 1;


--
-- Name: planet client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN client SET DEFAULT 1;


--
-- Name: planet sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: planet sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: planet sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: planet versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: planet versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: planet uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: planet_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: planet_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: planet_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: planet_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: planet_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: planet_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: planet_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.planet_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: precurementposition client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN client SET DEFAULT 1;


--
-- Name: precurementposition sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: precurementposition sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: precurementposition sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: precurementposition versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: precurementposition versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: precurementposition uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: precurementposition_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: precurementposition_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: precurementposition_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: precurementposition_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: precurementposition_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: precurementposition_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: precurementposition_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.precurementposition_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: preferences client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN client SET DEFAULT 1;


--
-- Name: preferences sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: preferences sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: preferences sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: preferences versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: preferences versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: preferences uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: preferences_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: preferences_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: preferences_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: preferences_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: preferences_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: preferences_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: preferences_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.preferences_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: procurement client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN client SET DEFAULT 1;


--
-- Name: procurement sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: procurement sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: procurement sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: procurement versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: procurement versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: procurement uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: procurement_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: procurement_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: procurement_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: procurement_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: procurement_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: procurement_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: procurement_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.procurement_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_phases client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_phases sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_phases sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_phases sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_phases versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_phases versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_phases uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_phases_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_phases_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_phases_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_phases_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_phases_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_phases_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_phases_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_phases_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_material_res client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_task_material_res sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_task_material_res sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_task_material_res sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_task_material_res versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_task_material_res versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_material_res uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_material_res_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_task_material_res_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_task_material_res_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_task_material_res_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_task_material_res_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_task_material_res_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_material_res_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_material_res_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_staff_res client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_task_staff_res sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_task_staff_res sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_task_staff_res sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_task_staff_res versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_task_staff_res versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_staff_res uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_staff_res_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_task_staff_res_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_task_staff_res_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_task_staff_res_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_task_staff_res_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_task_staff_res_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_task_staff_res_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_task_staff_res_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_tasks client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_tasks sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_tasks sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_tasks sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_tasks versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_tasks versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_tasks uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: project_tasks_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: project_tasks_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: project_tasks_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: project_tasks_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: project_tasks_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: project_tasks_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: project_tasks_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.project_tasks_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: projects client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN client SET DEFAULT 1;


--
-- Name: projects sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: projects sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: projects sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: projects versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: projects versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: projects uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: projects_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: projects_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: projects_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: projects_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: projects_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: projects_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: projects_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.projects_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposal client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposal sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposal sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposal sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposal versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposal versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposal uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposal_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposal_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposal_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposal_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposal_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposal_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposal_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposal_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalget client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalget sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalget sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalget sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalget versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalget versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalget uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalget_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalget_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalget_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalget_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalget_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalget_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalget_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalget_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalinvoice client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalinvoice sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalinvoice sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalinvoice sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalinvoice versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalinvoice versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalinvoice uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalinvoice_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalinvoice_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalinvoice_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalinvoice_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalinvoice_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalinvoice_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalinvoice_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalinvoice_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalmisc client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalmisc sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalmisc sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalmisc sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalmisc versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalmisc versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalmisc uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalmisc_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalmisc_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalmisc_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalmisc_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalmisc_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalmisc_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalmisc_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalmisc_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalposition client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalposition sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalposition sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalposition sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalposition versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalposition versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalposition uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalposition_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalposition_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalposition_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalposition_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalposition_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalposition_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalposition_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalposition_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalsupply client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalsupply sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalsupply sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalsupply sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalsupply versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalsupply versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalsupply uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalsupply_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: proposalsupply_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: proposalsupply_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: proposalsupply_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: proposalsupply_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: proposalsupply_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: proposalsupply_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.proposalsupply_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_file client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_file sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_file sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_file sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_file versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_file versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_file uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_file partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_file_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_file_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_file_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_file_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_file_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_file_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_file_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_file_history partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_file_history ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_modul_material_res sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_modul_material_res uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_modul_material_res_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_modul_material_res_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_modul_material_res_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_modul_material_res_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_modul_material_res_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_module sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_module sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_module sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_module versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_module versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_module_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_module_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_module_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_module_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_module_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_module_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module_history partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_history ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_module_staff_res sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module_staff_res uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module_staff_res_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_module_staff_res_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_module_staff_res_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_module_staff_res_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_module_staff_res_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_parts client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_parts sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_parts sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_parts sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_parts versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_parts versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_parts uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_parts partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_parts_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_parts_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_parts_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_parts_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_parts_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_parts_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_parts_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_parts_history partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_parts_history ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_projects client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_projects sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_projects sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_projects sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_projects versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_projects versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_projects uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_projects partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: sourcecode_projects_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: sourcecode_projects_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: sourcecode_projects_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: sourcecode_projects_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: sourcecode_projects_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: sourcecode_projects_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_projects_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: sourcecode_projects_history partner_id; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.sourcecode_projects_history ALTER COLUMN partner_id SET DEFAULT 0;


--
-- Name: staff client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_disease client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_disease sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_disease sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_disease sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_disease versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_disease versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_disease uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_disease_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_disease_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_disease_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_disease_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_disease_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_disease_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_disease_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_fee client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_fee sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_fee sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_fee sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_fee versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_fee versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_fee uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_fee_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_fee_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_fee_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_fee_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_fee_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_fee_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_fee_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_misc client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_misc sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_misc sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_misc sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_misc versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_misc versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_misc uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_misc_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_misc_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_misc_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_misc_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_misc_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_misc_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_misc_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_vacation client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_vacation sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_vacation sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_vacation sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_vacation versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_vacation versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_vacation uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_vacation_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: staff_vacation_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: staff_vacation_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: staff_vacation_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: staff_vacation_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: staff_vacation_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: staff_vacation_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: states client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN client SET DEFAULT 1;


--
-- Name: states sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: states sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: states sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: states versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: states versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: states uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: states_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: states_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: states_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: states_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: states_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: states_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: states_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.states_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: stock_goods client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN client SET DEFAULT 1;


--
-- Name: stock_goods sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: stock_goods sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: stock_goods sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: stock_goods versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: stock_goods versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: stock_goods uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: stock_goods_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: stock_goods_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: stock_goods_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: stock_goods_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: stock_goods_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: stock_goods_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: stock_goods_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stock_goods_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: stocks client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN client SET DEFAULT 1;


--
-- Name: stocks sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: stocks sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: stocks sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: stocks versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: stocks versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: stocks uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: stocks_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: stocks_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: stocks_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: stocks_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: stocks_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: stocks_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: stocks_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.stocks_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: support_project client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN client SET DEFAULT 1;


--
-- Name: support_project sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: support_project sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: support_project sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: support_project versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: support_project versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: support_project uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: support_project_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: support_project_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: support_project_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: support_project_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: support_project_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: support_project_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: support_project_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_project_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: support_ticket client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN client SET DEFAULT 1;


--
-- Name: support_ticket sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: support_ticket sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: support_ticket sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: support_ticket versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: support_ticket versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: support_ticket uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: support_ticket_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: support_ticket_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: support_ticket_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: support_ticket_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: support_ticket_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: support_ticket_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: support_ticket_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.support_ticket_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_address client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_address sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_address sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_address sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_address versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_address versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_address uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_address_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_address_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_address_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_address_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_address_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_address_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_address_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_address_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_grave_work client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_grave_work sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_grave_work sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_grave_work sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_grave_work versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_grave_work versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_grave_work uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_grave_work_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_grave_work_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_grave_work_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_grave_work_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_grave_work_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_grave_work_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_grave_work_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_grave_work_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_partner_schedul client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_partner_schedul sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_partner_schedul sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_partner_schedul sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_partner_schedul versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_partner_schedul versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_partner_schedul uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_partner_schedul_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_partner_schedul_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_partner_schedul_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_partner_schedul_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_partner_schedul_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_partner_schedul_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_partner_schedul_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_partner_schedul_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_sourcecode client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_sourcecode sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_sourcecode sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_sourcecode sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_sourcecode versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_sourcecode versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_sourcecode uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: t_sourcecode_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: t_sourcecode_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: t_sourcecode_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: t_sourcecode_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: t_sourcecode_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: t_sourcecode_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: t_sourcecode_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_sourcecode_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: tax_vat client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN client SET DEFAULT 1;


--
-- Name: tax_vat sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: tax_vat sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: tax_vat sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: tax_vat versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: tax_vat versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: tax_vat uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: tax_vat_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: tax_vat_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: tax_vat_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: tax_vat_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: tax_vat_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: tax_vat_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: tax_vat_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.tax_vat_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: terms_of_payment client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN client SET DEFAULT 1;


--
-- Name: terms_of_payment sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: terms_of_payment sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: terms_of_payment sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: terms_of_payment versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: terms_of_payment versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: terms_of_payment uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: terms_of_payment_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: terms_of_payment_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: terms_of_payment_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: terms_of_payment_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: terms_of_payment_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: terms_of_payment_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: terms_of_payment_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.terms_of_payment_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: user_info client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN client SET DEFAULT 1;


--
-- Name: user_info sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: user_info sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: user_info sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: user_info versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: user_info versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: user_info uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: user_info_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: user_info_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: user_info_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: user_info_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: user_info_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: user_info_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: user_info_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.user_info_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: usergroups client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN client SET DEFAULT 1;


--
-- Name: usergroups sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: usergroups sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: usergroups sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: usergroups versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: usergroups versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: usergroups uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: usergroups_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: usergroups_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: usergroups_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: usergroups_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: usergroups_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: usergroups_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: usergroups_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.usergroups_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: userlogin client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN client SET DEFAULT 1;


--
-- Name: userlogin sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: userlogin sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: userlogin sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: userlogin versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: userlogin versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: userlogin uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: userlogin_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: userlogin_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: userlogin_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: userlogin_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: userlogin_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: userlogin_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: userlogin_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.userlogin_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: web2 client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN client SET DEFAULT 1;


--
-- Name: web2 sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: web2 sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: web2 sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: web2 versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: web2 versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: web2 uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2 ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: web2_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: web2_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: web2_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: web2_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: web2_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: web2_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: web2_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.web2_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: xmlvalues client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN client SET DEFAULT 1;


--
-- Name: xmlvalues sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: xmlvalues sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: xmlvalues sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: xmlvalues versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: xmlvalues versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: xmlvalues uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: xmlvalues_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: xmlvalues_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: xmlvalues_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: xmlvalues_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: xmlvalues_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: xmlvalues_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: xmlvalues_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.xmlvalues_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: zipcodes client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN client SET DEFAULT 1;


--
-- Name: zipcodes sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: zipcodes sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: zipcodes sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: zipcodes versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: zipcodes versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: zipcodes uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: zipcodes_history client; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN client SET DEFAULT 1;


--
-- Name: zipcodes_history sep_info_1; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN sep_info_1 SET DEFAULT 0;


--
-- Name: zipcodes_history sep_info_2; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN sep_info_2 SET DEFAULT 0;


--
-- Name: zipcodes_history sep_info_3; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN sep_info_3 SET DEFAULT 0;


--
-- Name: zipcodes_history versions_number; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN versions_number SET DEFAULT 0;


--
-- Name: zipcodes_history versions_uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN versions_uuid SET DEFAULT '0'::character varying;


--
-- Name: zipcodes_history uuid; Type: DEFAULT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.zipcodes_history ALTER COLUMN uuid SET DEFAULT '0'::character varying;


--
-- Name: address_bank address_bank_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank
    ADD CONSTRAINT address_bank_pkey PRIMARY KEY (id);


--
-- Name: address_notes address_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_notes
    ADD CONSTRAINT address_notes_pkey PRIMARY KEY (id);


--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: addresses_misc addresses_misc_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.addresses_misc
    ADD CONSTRAINT addresses_misc_pkey PRIMARY KEY (id);


--
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- Name: articles_stock articles_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.articles_stock
    ADD CONSTRAINT articles_stock_pkey PRIMARY KEY (id);


--
-- Name: cuon_erp1_history cuon_erp1_history_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1_history
    ADD CONSTRAINT cuon_erp1_history_pkey PRIMARY KEY (address_id);


--
-- Name: cuon_erp1 cuon_erp1_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon_erp1
    ADD CONSTRAINT cuon_erp1_pkey PRIMARY KEY (address_id);


--
-- Name: cuon cuon_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.cuon
    ADD CONSTRAINT cuon_pkey PRIMARY KEY (skey);


--
-- Name: dms dms_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.dms
    ADD CONSTRAINT dms_pkey PRIMARY KEY (id);


--
-- Name: partner partner_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner
    ADD CONSTRAINT partner_pkey PRIMARY KEY (id);


--
-- Name: staff_disease staff_disease_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease
    ADD CONSTRAINT staff_disease_pkey PRIMARY KEY (id);


--
-- Name: staff_fee staff_fee_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee
    ADD CONSTRAINT staff_fee_pkey PRIMARY KEY (id);


--
-- Name: staff_misc staff_misc_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc
    ADD CONSTRAINT staff_misc_pkey PRIMARY KEY (id);


--
-- Name: staff staff_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff
    ADD CONSTRAINT staff_pkey PRIMARY KEY (id);


--
-- Name: staff_vacation staff_vacation_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation
    ADD CONSTRAINT staff_vacation_pkey PRIMARY KEY (id);


--
-- Name: t_standard_history t_standard_history_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_standard_history
    ADD CONSTRAINT t_standard_history_pkey PRIMARY KEY (id);


--
-- Name: t_standard t_standard_pkey; Type: CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.t_standard
    ADD CONSTRAINT t_standard_pkey PRIMARY KEY (id);


--
-- Name: address_history_idx_version; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_history_idx_version ON public.address_history USING btree (versions_uuid);


--
-- Name: address_idx_caller_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_caller_id ON public.address USING btree (caller_id);


--
-- Name: address_idx_firstname; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_firstname ON public.address USING btree (firstname);


--
-- Name: address_idx_gin_city; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_gin_city ON public.address USING gin (city public.gin_trgm_ops);


--
-- Name: address_idx_gin_firstname; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_gin_firstname ON public.address USING gin (firstname public.gin_trgm_ops);


--
-- Name: address_idx_gin_lastname; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_gin_lastname ON public.address USING gin (lastname public.gin_trgm_ops);


--
-- Name: address_idx_gin_lastname2; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_gin_lastname2 ON public.address USING gin (lastname2 public.gin_trgm_ops);


--
-- Name: address_idx_gin_newsletter; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_gin_newsletter ON public.address USING gin (newsletter public.gin_trgm_ops);


--
-- Name: address_idx_gin_zip; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_gin_zip ON public.address USING gin (zip public.gin_trgm_ops);


--
-- Name: address_idx_lastname; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_lastname ON public.address USING btree (lastname);


--
-- Name: address_idx_newsletter; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_newsletter ON public.address USING btree (newsletter);


--
-- Name: address_idx_phone; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_phone ON public.address USING btree (phone);


--
-- Name: address_idx_rep_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_rep_id ON public.address USING btree (rep_id);


--
-- Name: address_idx_salesman_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_salesman_id ON public.address USING btree (salesman_id);


--
-- Name: address_idx_status_client; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_status_client ON public.address USING btree (status, client);


--
-- Name: address_idx_status_info; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_status_info ON public.address USING btree (status_info);


--
-- Name: address_idx_uuid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_idx_uuid ON public.address USING btree (uuid);


--
-- Name: address_notes_idx_address_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_notes_idx_address_id ON public.address_notes USING btree (address_id);


--
-- Name: address_notes_idx_status_client; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_notes_idx_status_client ON public.address_notes USING btree (status, client);


--
-- Name: address_notes_idx_uuid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX address_notes_idx_uuid ON public.address_notes USING btree (uuid);


--
-- Name: articles_history_idx_version; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX articles_history_idx_version ON public.articles_history USING btree (versions_uuid);


--
-- Name: cuon_user_idx_name; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX cuon_user_idx_name ON public.cuon_user USING btree (username);


--
-- Name: dms_history_idx_version; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_history_idx_version ON public.dms_history USING btree (versions_uuid);


--
-- Name: dms_idx_gin_search1; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_search1 ON public.dms USING gin (search1 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_search2; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_search2 ON public.dms USING gin (search2 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_search3; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_search3 ON public.dms USING gin (search3 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_search4; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_search4 ON public.dms USING gin (search4 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_sub1; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_sub1 ON public.dms USING gin (sub1 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_sub2; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_sub2 ON public.dms USING gin (sub2 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_sub3; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_sub3 ON public.dms USING gin (sub3 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_sub4; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_sub4 ON public.dms USING gin (sub4 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_sub5; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_sub5 ON public.dms USING gin (sub5 public.gin_trgm_ops);


--
-- Name: dms_idx_gin_title1; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_gin_title1 ON public.dms USING gin (title public.gin_trgm_ops);


--
-- Name: dms_idx_insert_from_module; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX dms_idx_insert_from_module ON public.dms USING btree (insert_from_module);


--
-- Name: enquiry_idx_gin_designation; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX enquiry_idx_gin_designation ON public.enquiry USING gin (designation public.gin_trgm_ops);


--
-- Name: enquiry_idx_gin_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX enquiry_idx_gin_number ON public.enquiry USING gin (number public.gin_trgm_ops);


--
-- Name: hibernation_idx_all1; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX hibernation_idx_all1 ON public.hibernation USING btree (sequence_of_stock, hibernation_number, client, status);


--
-- Name: hibernation_plant_idx_all1; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX hibernation_plant_idx_all1 ON public.hibernation_plant USING btree (plant_number, diameter, client, status);


--
-- Name: in_payment_idx_date_of_paid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX in_payment_idx_date_of_paid ON public.in_payment USING btree (date_of_paid);


--
-- Name: in_payment_idx_inpayment; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX in_payment_idx_inpayment ON public.in_payment USING btree (inpayment);


--
-- Name: in_payment_idx_invoice_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX in_payment_idx_invoice_number ON public.in_payment USING btree (invoice_number);


--
-- Name: in_payment_idx_order_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX in_payment_idx_order_id ON public.in_payment USING btree (order_id);


--
-- Name: index_address_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_address_id ON public.address USING btree (id);


--
-- Name: index_addresses_misc_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_addresses_misc_id ON public.addresses_misc USING btree (id);


--
-- Name: index_administrative_district_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_administrative_district_id ON public.administrative_district USING btree (id);


--
-- Name: index_articles_barcode_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_barcode_id ON public.articles_barcode USING btree (id);


--
-- Name: index_articles_customers_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_customers_id ON public.articles_customers USING btree (id);


--
-- Name: index_articles_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_id ON public.articles USING btree (id);


--
-- Name: index_articles_parts_list_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_parts_list_id ON public.articles_parts_list USING btree (id);


--
-- Name: index_articles_purchase_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_purchase_id ON public.articles_purchase USING btree (id);


--
-- Name: index_articles_sales_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_sales_id ON public.articles_sales USING btree (id);


--
-- Name: index_articles_webshop_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_articles_webshop_id ON public.articles_webshop USING btree (id);


--
-- Name: index_bank_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_bank_id ON public.bank USING btree (id);


--
-- Name: index_cash_desk_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_cash_desk_id ON public.cash_desk USING btree (id);


--
-- Name: index_contact_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_contact_id ON public.contact USING btree (id);


--
-- Name: index_cuon_clients_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_cuon_clients_id ON public.cuon_clients USING btree (id);


--
-- Name: index_dms_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_dms_id ON public.dms USING btree (id);


--
-- Name: index_ext_two_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE UNIQUE INDEX index_ext_two_id ON public.ext_two USING btree (id);


--
-- Name: list_of_invoices_idx_date_of_invoice; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX list_of_invoices_idx_date_of_invoice ON public.list_of_invoices USING btree (date_of_invoice);


--
-- Name: list_of_invoices_idx_invoice_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX list_of_invoices_idx_invoice_number ON public.list_of_invoices USING btree (invoice_number);


--
-- Name: list_of_invoices_idx_maturity; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX list_of_invoices_idx_maturity ON public.list_of_invoices USING btree (maturity);


--
-- Name: list_of_invoices_idx_order_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX list_of_invoices_idx_order_number ON public.list_of_invoices USING btree (order_number);


--
-- Name: list_of_invoices_idx_total_amount; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX list_of_invoices_idx_total_amount ON public.list_of_invoices USING btree (total_amount);


--
-- Name: material_group_idx_name; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX material_group_idx_name ON public.material_group USING btree (name);


--
-- Name: orderbook_idx_gin_designation; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderbook_idx_gin_designation ON public.orderbook USING gin (designation public.gin_trgm_ops);


--
-- Name: orderbook_idx_gin_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderbook_idx_gin_number ON public.orderbook USING gin (number public.gin_trgm_ops);


--
-- Name: orderbook_idx_modul_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderbook_idx_modul_number ON public.orderbook USING btree (modul_number);


--
-- Name: orderbook_idx_modul_order_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderbook_idx_modul_order_number ON public.orderbook USING btree (modul_order_number);


--
-- Name: orderbook_idx_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderbook_idx_number ON public.orderbook USING btree (number);


--
-- Name: orderinvoice_idx_order_top; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderinvoice_idx_order_top ON public.orderinvoice USING btree (order_top);


--
-- Name: orderinvoice_idx_orderid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderinvoice_idx_orderid ON public.orderinvoice USING btree (orderid);


--
-- Name: orderposition_idx_articleid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderposition_idx_articleid ON public.orderposition USING btree (articleid);


--
-- Name: orderposition_idx_orderid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX orderposition_idx_orderid ON public.orderposition USING btree (orderid);


--
-- Name: partner_idx_addressid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_idx_addressid ON public.partner USING btree (addressid);


--
-- Name: partner_idx_status_client; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_idx_status_client ON public.partner USING btree (status, client);


--
-- Name: partner_idx_uuid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_idx_uuid ON public.partner USING btree (uuid);


--
-- Name: partner_schedul_idx_date; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_date ON public.partner_schedul USING btree (schedul_date);


--
-- Name: partner_schedul_idx_partnerid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_partnerid ON public.partner_schedul USING btree (partnerid);


--
-- Name: partner_schedul_idx_process_status; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_process_status ON public.partner_schedul USING btree (process_status);


--
-- Name: partner_schedul_idx_schedul_insert_time_day; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_schedul_insert_time_day ON public.partner_schedul USING btree (date_part('day'::text, insert_time));


--
-- Name: partner_schedul_idx_schedul_insert_time_month; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_schedul_insert_time_month ON public.partner_schedul USING btree (date_part('month'::text, insert_time));


--
-- Name: partner_schedul_idx_schedul_insert_time_quarter; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_schedul_insert_time_quarter ON public.partner_schedul USING btree (date_part('quarter'::text, insert_time));


--
-- Name: partner_schedul_idx_schedul_insert_time_week; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_schedul_insert_time_week ON public.partner_schedul USING btree (date_part('week'::text, insert_time));


--
-- Name: partner_schedul_idx_schedul_insert_time_year; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_schedul_insert_time_year ON public.partner_schedul USING btree (date_part('year'::text, insert_time));


--
-- Name: partner_schedul_idx_schedul_staff_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_schedul_staff_id ON public.partner_schedul USING btree (schedul_staff_id);


--
-- Name: partner_schedul_idx_sep_info_3; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_sep_info_3 ON public.partner_schedul USING btree (sep_info_3);


--
-- Name: partner_schedul_idx_status_client; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_status_client ON public.partner_schedul USING btree (status, client);


--
-- Name: partner_schedul_idx_user_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_user_id ON public.partner_schedul USING btree (user_id);


--
-- Name: partner_schedul_idx_uuid; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX partner_schedul_idx_uuid ON public.partner_schedul USING btree (uuid);


--
-- Name: project_phases_idx_project_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX project_phases_idx_project_id ON public.project_phases USING btree (project_id);


--
-- Name: project_task_material_res_idx_task_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX project_task_material_res_idx_task_id ON public.project_task_material_res USING btree (task_id);


--
-- Name: project_task_staff_res_idx_task_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX project_task_staff_res_idx_task_id ON public.project_task_staff_res USING btree (task_id);


--
-- Name: project_tasks_idx_phase_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX project_tasks_idx_phase_id ON public.project_tasks USING btree (phase_id);


--
-- Name: projects_idx_all1; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_all1 ON public.projects USING btree (name, designation, client, status);


--
-- Name: projects_idx_customer_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_customer_id ON public.projects USING btree (customer_id);


--
-- Name: projects_idx_gin_designation; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_gin_designation ON public.projects USING gin (designation public.gin_trgm_ops);


--
-- Name: projects_idx_gin_internal_project_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_gin_internal_project_number ON public.projects USING gin (internal_project_number public.gin_trgm_ops);


--
-- Name: projects_idx_gin_name; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_gin_name ON public.projects USING gin (name public.gin_trgm_ops);


--
-- Name: projects_idx_modul_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_modul_number ON public.projects USING btree (modul_number);


--
-- Name: projects_idx_modul_project_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX projects_idx_modul_project_number ON public.projects USING btree (modul_project_number);


--
-- Name: proposal_idx_gin_designation; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX proposal_idx_gin_designation ON public.proposal USING gin (designation public.gin_trgm_ops);


--
-- Name: proposal_idx_gin_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX proposal_idx_gin_number ON public.proposal USING gin (number public.gin_trgm_ops);


--
-- Name: staff_idx_cuon_username; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX staff_idx_cuon_username ON public.staff USING btree (cuon_username);


--
-- Name: support_ticket_number; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX support_ticket_number ON public.support_ticket USING btree (ticket_number);


--
-- Name: support_ticket_platform_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX support_ticket_platform_id ON public.support_ticket USING btree (platform_id);


--
-- Name: support_ticket_priority_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX support_ticket_priority_id ON public.support_ticket USING btree (priority_id);


--
-- Name: support_ticket_project_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX support_ticket_project_id ON public.support_ticket USING btree (support_project_id);


--
-- Name: support_ticket_status_id; Type: INDEX; Schema: public; Owner: cuon_admin
--

CREATE INDEX support_ticket_status_id ON public.support_ticket USING btree (status_id);


--
-- Name: orderget trg_delete_pick_schedul; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_delete_pick_schedul BEFORE DELETE ON public.orderget FOR EACH ROW EXECUTE PROCEDURE public.fct_pickup_schedul_delete();


--
-- Name: account_info trg_deleteaccount_info; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaccount_info BEFORE DELETE ON public.account_info FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: account_plan trg_deleteaccount_plan; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaccount_plan BEFORE DELETE ON public.account_plan FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: account_sentence trg_deleteaccount_sentence; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaccount_sentence BEFORE DELETE ON public.account_sentence FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: account_system trg_deleteaccount_system; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaccount_system BEFORE DELETE ON public.account_system FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: address trg_deleteaddress; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaddress BEFORE DELETE ON public.address FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: address_bank trg_deleteaddress_bank; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaddress_bank BEFORE DELETE ON public.address_bank FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: address_notes trg_deleteaddress_notes; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteaddress_notes BEFORE DELETE ON public.address_notes FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles trg_deletearticles; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles BEFORE DELETE ON public.articles FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles_barcode trg_deletearticles_barcode; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles_barcode BEFORE DELETE ON public.articles_barcode FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles_parts_list trg_deletearticles_parts_list; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles_parts_list BEFORE DELETE ON public.articles_parts_list FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles_purchase trg_deletearticles_purchase; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles_purchase BEFORE DELETE ON public.articles_purchase FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles_sales trg_deletearticles_sales; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles_sales BEFORE DELETE ON public.articles_sales FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles_stock trg_deletearticles_stock; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles_stock BEFORE DELETE ON public.articles_stock FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: articles_webshop trg_deletearticles_webshop; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletearticles_webshop BEFORE DELETE ON public.articles_webshop FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: bank trg_deletebank; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebank BEFORE DELETE ON public.bank FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: biblio trg_deletebiblio; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebiblio BEFORE DELETE ON public.biblio FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany trg_deletebotany; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany BEFORE DELETE ON public.botany FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany_class trg_deletebotany_class; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany_class BEFORE DELETE ON public.botany_class FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany_divisio trg_deletebotany_divisio; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany_divisio BEFORE DELETE ON public.botany_divisio FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany_family trg_deletebotany_family; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany_family BEFORE DELETE ON public.botany_family FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany_genus trg_deletebotany_genus; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany_genus BEFORE DELETE ON public.botany_genus FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany_kingdom trg_deletebotany_kingdom; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany_kingdom BEFORE DELETE ON public.botany_kingdom FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: botany_ordo trg_deletebotany_ordo; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletebotany_ordo BEFORE DELETE ON public.botany_ordo FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: cash_desk trg_deletecash_desk; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletecash_desk BEFORE DELETE ON public.cash_desk FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: cash_desk_book_number trg_deletecash_desk_book_number; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletecash_desk_book_number BEFORE DELETE ON public.cash_desk_book_number FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: clients trg_deleteclients; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteclients BEFORE DELETE ON public.clients FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: dms trg_deletedms; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletedms BEFORE DELETE ON public.dms FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: enquiry trg_deleteenquiry; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteenquiry BEFORE DELETE ON public.enquiry FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: enquiryget trg_deleteenquiryget; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteenquiryget BEFORE DELETE ON public.enquiryget FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: enquiryinvoice trg_deleteenquiryinvoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteenquiryinvoice BEFORE DELETE ON public.enquiryinvoice FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: enquirymisc trg_deleteenquirymisc; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteenquirymisc BEFORE DELETE ON public.enquirymisc FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: enquiryposition trg_deleteenquiryposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteenquiryposition BEFORE DELETE ON public.enquiryposition FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: enquirysupply trg_deleteenquirysupply; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteenquirysupply BEFORE DELETE ON public.enquirysupply FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave trg_deletegrave; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave BEFORE DELETE ON public.grave FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_service_notes trg_deletegrave_service_notes; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_service_notes BEFORE DELETE ON public.grave_service_notes FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_autumn trg_deletegrave_work_autumn; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_autumn BEFORE DELETE ON public.grave_work_autumn FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_holiday trg_deletegrave_work_holiday; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_holiday BEFORE DELETE ON public.grave_work_holiday FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_maintenance trg_deletegrave_work_maintenance; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_maintenance BEFORE DELETE ON public.grave_work_maintenance FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_single trg_deletegrave_work_single; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_single BEFORE DELETE ON public.grave_work_single FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_spring trg_deletegrave_work_spring; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_spring BEFORE DELETE ON public.grave_work_spring FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_summer trg_deletegrave_work_summer; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_summer BEFORE DELETE ON public.grave_work_summer FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_winter trg_deletegrave_work_winter; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_winter BEFORE DELETE ON public.grave_work_winter FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: grave_work_year trg_deletegrave_work_year; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegrave_work_year BEFORE DELETE ON public.grave_work_year FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: graveyard trg_deletegraveyard; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletegraveyard BEFORE DELETE ON public.graveyard FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: hibernation trg_deletehibernation; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletehibernation BEFORE DELETE ON public.hibernation FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: hibernation_plant trg_deletehibernation_plant; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletehibernation_plant BEFORE DELETE ON public.hibernation_plant FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: in_payment trg_deletein_payment; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletein_payment BEFORE DELETE ON public.in_payment FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: list_of_deliveries trg_deletelist_of_deliveries; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletelist_of_deliveries BEFORE DELETE ON public.list_of_deliveries FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: list_of_invoices trg_deletelist_of_invoices; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletelist_of_invoices BEFORE DELETE ON public.list_of_invoices FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: orderbook trg_deleteorderbook; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteorderbook BEFORE DELETE ON public.orderbook FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: orderinvoice trg_deleteorderinvoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteorderinvoice BEFORE DELETE ON public.orderinvoice FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: orderposition trg_deleteorderposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteorderposition BEFORE DELETE ON public.orderposition FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: partner trg_deletepartner; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletepartner BEFORE DELETE ON public.partner FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: project_phases trg_deleteproject_phases; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproject_phases BEFORE DELETE ON public.project_phases FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: project_task_material_res trg_deleteproject_task_material_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproject_task_material_res BEFORE DELETE ON public.project_task_material_res FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: project_tasks trg_deleteproject_tasks; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproject_tasks BEFORE DELETE ON public.project_tasks FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: projects trg_deleteprojects; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteprojects BEFORE DELETE ON public.projects FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: proposal trg_deleteproposal; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproposal BEFORE DELETE ON public.proposal FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: proposalget trg_deleteproposalget; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproposalget BEFORE DELETE ON public.proposalget FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: proposalmisc trg_deleteproposalmisc; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproposalmisc BEFORE DELETE ON public.proposalmisc FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: proposalposition trg_deleteproposalposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproposalposition BEFORE DELETE ON public.proposalposition FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: proposalsupply trg_deleteproposalsupply; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deleteproposalsupply BEFORE DELETE ON public.proposalsupply FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: sourcecode_file trg_deletesourcecode_file; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesourcecode_file BEFORE DELETE ON public.sourcecode_file FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: sourcecode_modul_material_res trg_deletesourcecode_modul_material_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesourcecode_modul_material_res BEFORE DELETE ON public.sourcecode_modul_material_res FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: sourcecode_module trg_deletesourcecode_module; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesourcecode_module BEFORE DELETE ON public.sourcecode_module FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: sourcecode_module_staff_res trg_deletesourcecode_module_staff_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesourcecode_module_staff_res BEFORE DELETE ON public.sourcecode_module_staff_res FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: sourcecode_parts trg_deletesourcecode_parts; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesourcecode_parts BEFORE DELETE ON public.sourcecode_parts FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: sourcecode_projects trg_deletesourcecode_projects; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesourcecode_projects BEFORE DELETE ON public.sourcecode_projects FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: staff trg_deletestaff; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletestaff BEFORE DELETE ON public.staff FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: stock_goods trg_deletestock_goods; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletestock_goods BEFORE DELETE ON public.stock_goods FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: support_project trg_deletesupport_project; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesupport_project BEFORE DELETE ON public.support_project FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: support_ticket trg_deletesupport_ticket; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_deletesupport_ticket BEFORE DELETE ON public.support_ticket FOR EACH ROW EXECUTE PROCEDURE public.fct_delete();


--
-- Name: cuon trg_insert_cuon; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insert_cuon BEFORE INSERT ON public.cuon FOR EACH ROW EXECUTE PROCEDURE public.fct_cuon_insert();


--
-- Name: dms trg_insert_dms_pairedid; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insert_dms_pairedid BEFORE INSERT ON public.dms FOR EACH ROW EXECUTE PROCEDURE public.fct_setdmspairedid();


--
-- Name: lock_process trg_insert_lock_process; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insert_lock_process BEFORE INSERT ON public.lock_process FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: orderbook trg_insert_orderbook_gift1; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insert_orderbook_gift1 BEFORE INSERT ON public.orderbook FOR EACH ROW EXECUTE PROCEDURE public.fct_getordergifts();


--
-- Name: partner_schedul trg_insert_partner_schedul; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insert_partner_schedul BEFORE INSERT ON public.partner_schedul FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: orderget trg_insert_pick_schedul; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insert_pick_schedul BEFORE INSERT ON public.orderget FOR EACH ROW EXECUTE PROCEDURE public.fct_pickup_schedul_insert();


--
-- Name: account_info trg_insertaccount_info; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaccount_info BEFORE INSERT ON public.account_info FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: account_plan trg_insertaccount_plan; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaccount_plan BEFORE INSERT ON public.account_plan FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: account_sentence trg_insertaccount_sentence; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaccount_sentence BEFORE INSERT ON public.account_sentence FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: account_system trg_insertaccount_system; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaccount_system BEFORE INSERT ON public.account_system FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: address trg_insertaddress; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaddress BEFORE INSERT ON public.address FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: address_bank trg_insertaddress_bank; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaddress_bank BEFORE INSERT ON public.address_bank FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: address_notes trg_insertaddress_notes; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertaddress_notes BEFORE INSERT ON public.address_notes FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles trg_insertarticles; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles BEFORE INSERT ON public.articles FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles_barcode trg_insertarticles_barcode; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles_barcode BEFORE INSERT ON public.articles_barcode FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles_parts_list trg_insertarticles_parts_list; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles_parts_list BEFORE INSERT ON public.articles_parts_list FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles_purchase trg_insertarticles_purchase; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles_purchase BEFORE INSERT ON public.articles_purchase FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles_sales trg_insertarticles_sales; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles_sales BEFORE INSERT ON public.articles_sales FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles_stock trg_insertarticles_stock; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles_stock BEFORE INSERT ON public.articles_stock FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: articles_webshop trg_insertarticles_webshop; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertarticles_webshop BEFORE INSERT ON public.articles_webshop FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: bank trg_insertbank; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbank BEFORE INSERT ON public.bank FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: biblio trg_insertbiblio; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbiblio BEFORE INSERT ON public.biblio FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany trg_insertbotany; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany BEFORE INSERT ON public.botany FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany_class trg_insertbotany_class; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany_class BEFORE INSERT ON public.botany_class FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany_divisio trg_insertbotany_divisio; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany_divisio BEFORE INSERT ON public.botany_divisio FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany_family trg_insertbotany_family; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany_family BEFORE INSERT ON public.botany_family FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany_genus trg_insertbotany_genus; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany_genus BEFORE INSERT ON public.botany_genus FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany_kingdom trg_insertbotany_kingdom; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany_kingdom BEFORE INSERT ON public.botany_kingdom FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: botany_ordo trg_insertbotany_ordo; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertbotany_ordo BEFORE INSERT ON public.botany_ordo FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: cash_desk trg_insertcash_desk; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertcash_desk BEFORE INSERT ON public.cash_desk FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: cash_desk_book_number trg_insertcash_desk_book_number; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertcash_desk_book_number BEFORE INSERT ON public.cash_desk_book_number FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: clients trg_insertclients; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertclients BEFORE INSERT ON public.clients FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: dms trg_insertdms; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertdms BEFORE INSERT ON public.dms FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: enquiry trg_insertenquiry; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertenquiry BEFORE INSERT ON public.enquiry FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: enquiryget trg_insertenquiryget; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertenquiryget BEFORE INSERT ON public.enquiryget FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: enquiryinvoice trg_insertenquiryinvoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertenquiryinvoice BEFORE INSERT ON public.enquiryinvoice FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: enquirymisc trg_insertenquirymisc; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertenquirymisc BEFORE INSERT ON public.enquirymisc FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: enquiryposition trg_insertenquiryposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertenquiryposition BEFORE INSERT ON public.enquiryposition FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: enquirysupply trg_insertenquirysupply; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertenquirysupply BEFORE INSERT ON public.enquirysupply FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave trg_insertgrave; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave BEFORE INSERT ON public.grave FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_service_notes trg_insertgrave_service_notes; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_service_notes BEFORE INSERT ON public.grave_service_notes FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_autumn trg_insertgrave_work_autumn; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_autumn BEFORE INSERT ON public.grave_work_autumn FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_holiday trg_insertgrave_work_holiday; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_holiday BEFORE INSERT ON public.grave_work_holiday FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_maintenance trg_insertgrave_work_maintenance; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_maintenance BEFORE INSERT ON public.grave_work_maintenance FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_single trg_insertgrave_work_single; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_single BEFORE INSERT ON public.grave_work_single FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_spring trg_insertgrave_work_spring; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_spring BEFORE INSERT ON public.grave_work_spring FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_summer trg_insertgrave_work_summer; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_summer BEFORE INSERT ON public.grave_work_summer FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_winter trg_insertgrave_work_winter; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_winter BEFORE INSERT ON public.grave_work_winter FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: grave_work_year trg_insertgrave_work_year; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgrave_work_year BEFORE INSERT ON public.grave_work_year FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: graveyard trg_insertgraveyard; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertgraveyard BEFORE INSERT ON public.graveyard FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: hibernation trg_inserthibernation; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_inserthibernation BEFORE INSERT ON public.hibernation FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: hibernation_plant trg_inserthibernation_plant; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_inserthibernation_plant BEFORE INSERT ON public.hibernation_plant FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: in_payment trg_insertin_payment; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertin_payment BEFORE INSERT ON public.in_payment FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: list_of_deliveries trg_insertlist_of_deliveries; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertlist_of_deliveries BEFORE INSERT ON public.list_of_deliveries FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: list_of_invoices trg_insertlist_of_invoices; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertlist_of_invoices BEFORE INSERT ON public.list_of_invoices FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: orderbook trg_insertorderbook; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertorderbook BEFORE INSERT ON public.orderbook FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: orderinvoice trg_insertorderinvoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertorderinvoice BEFORE INSERT ON public.orderinvoice FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: orderposition trg_insertorderposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertorderposition BEFORE INSERT ON public.orderposition FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: partner trg_insertpartner; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertpartner BEFORE INSERT ON public.partner FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: project_phases trg_insertproject_phases; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproject_phases BEFORE INSERT ON public.project_phases FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: project_task_material_res trg_insertproject_task_material_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproject_task_material_res BEFORE INSERT ON public.project_task_material_res FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: project_tasks trg_insertproject_tasks; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproject_tasks BEFORE INSERT ON public.project_tasks FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: projects trg_insertprojects; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertprojects BEFORE INSERT ON public.projects FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: proposal trg_insertproposal; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproposal BEFORE INSERT ON public.proposal FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: proposalget trg_insertproposalget; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproposalget BEFORE INSERT ON public.proposalget FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: proposalmisc trg_insertproposalmisc; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproposalmisc BEFORE INSERT ON public.proposalmisc FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: proposalposition trg_insertproposalposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproposalposition BEFORE INSERT ON public.proposalposition FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: proposalsupply trg_insertproposalsupply; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertproposalsupply BEFORE INSERT ON public.proposalsupply FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: sourcecode_file trg_insertsourcecode_file; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsourcecode_file BEFORE INSERT ON public.sourcecode_file FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: sourcecode_modul_material_res trg_insertsourcecode_modul_material_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsourcecode_modul_material_res BEFORE INSERT ON public.sourcecode_modul_material_res FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: sourcecode_module trg_insertsourcecode_module; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsourcecode_module BEFORE INSERT ON public.sourcecode_module FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: sourcecode_module_staff_res trg_insertsourcecode_module_staff_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsourcecode_module_staff_res BEFORE INSERT ON public.sourcecode_module_staff_res FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: sourcecode_parts trg_insertsourcecode_parts; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsourcecode_parts BEFORE INSERT ON public.sourcecode_parts FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: sourcecode_projects trg_insertsourcecode_projects; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsourcecode_projects BEFORE INSERT ON public.sourcecode_projects FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: staff trg_insertstaff; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertstaff BEFORE INSERT ON public.staff FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: stock_goods trg_insertstock_goods; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertstock_goods BEFORE INSERT ON public.stock_goods FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: support_project trg_insertsupport_project; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsupport_project BEFORE INSERT ON public.support_project FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: support_ticket trg_insertsupport_ticket; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertsupport_ticket BEFORE INSERT ON public.support_ticket FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: cash_desk trg_insertz1_cashdesk; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertz1_cashdesk BEFORE INSERT ON public.cash_desk FOR EACH ROW EXECUTE PROCEDURE public.fct_beforecashdesk();


--
-- Name: in_payment trg_insertz1_inpayment; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_insertz1_inpayment BEFORE INSERT ON public.in_payment FOR EACH ROW EXECUTE PROCEDURE public.fct_beforeinpayment();


--
-- Name: orderposition trg_new_orderpositon; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_new_orderpositon BEFORE INSERT ON public.orderposition FOR EACH ROW EXECUTE PROCEDURE public.fct_orderposition_insert();


--
-- Name: stock_goods trg_new_stock_goods; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_new_stock_goods BEFORE INSERT ON public.stock_goods FOR EACH ROW EXECUTE PROCEDURE public.fct_stock_goods_insert();


--
-- Name: orderbook trg_orderbook_delete_invoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_orderbook_delete_invoice AFTER DELETE ON public.orderbook FOR EACH ROW EXECUTE PROCEDURE public.fct_orderbook_delete_invoice();


--
-- Name: cash_desk_book_number trg_t_insert_cash_desk_book_number_; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_t_insert_cash_desk_book_number_ BEFORE INSERT ON public.cash_desk_book_number FOR EACH ROW EXECUTE PROCEDURE public.fct_insert();


--
-- Name: cash_desk_book_number trg_t_new_cash_desk_special_id_; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_t_new_cash_desk_special_id_ BEFORE INSERT ON public.cash_desk_book_number FOR EACH ROW EXECUTE PROCEDURE public.fct_new_cash_desk_special_id();


--
-- Name: cuon trg_update_cuon; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_update_cuon BEFORE UPDATE ON public.cuon FOR EACH ROW EXECUTE PROCEDURE public.fct_cuon_insert();


--
-- Name: dms trg_update_dms_pairedid; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_update_dms_pairedid BEFORE UPDATE ON public.dms FOR EACH ROW EXECUTE PROCEDURE public.fct_setdmspairedid();


--
-- Name: orderbook trg_update_orderbook_gift1; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_update_orderbook_gift1 BEFORE UPDATE ON public.orderbook FOR EACH ROW EXECUTE PROCEDURE public.fct_getordergifts();


--
-- Name: partner_schedul trg_update_partner_schedul; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_update_partner_schedul BEFORE UPDATE ON public.partner_schedul FOR EACH ROW EXECUTE PROCEDURE public.fct_partner_schedul_change();


--
-- Name: orderget trg_update_pick_schedul; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_update_pick_schedul BEFORE UPDATE ON public.orderget FOR EACH ROW EXECUTE PROCEDURE public.fct_pickup_schedul_update();


--
-- Name: account_info trg_updateaccount_info; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaccount_info BEFORE UPDATE ON public.account_info FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: account_plan trg_updateaccount_plan; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaccount_plan BEFORE UPDATE ON public.account_plan FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: account_sentence trg_updateaccount_sentence; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaccount_sentence BEFORE UPDATE ON public.account_sentence FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: account_system trg_updateaccount_system; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaccount_system BEFORE UPDATE ON public.account_system FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: address trg_updateaddress; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaddress BEFORE UPDATE ON public.address FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: address_bank trg_updateaddress_bank; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaddress_bank BEFORE UPDATE ON public.address_bank FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: address_notes trg_updateaddress_notes; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateaddress_notes BEFORE UPDATE ON public.address_notes FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles trg_updatearticles; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles BEFORE UPDATE ON public.articles FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles_barcode trg_updatearticles_barcode; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles_barcode BEFORE UPDATE ON public.articles_barcode FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles_parts_list trg_updatearticles_parts_list; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles_parts_list BEFORE UPDATE ON public.articles_parts_list FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles_purchase trg_updatearticles_purchase; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles_purchase BEFORE UPDATE ON public.articles_purchase FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles_sales trg_updatearticles_sales; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles_sales BEFORE UPDATE ON public.articles_sales FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles_stock trg_updatearticles_stock; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles_stock BEFORE UPDATE ON public.articles_stock FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: articles_webshop trg_updatearticles_webshop; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatearticles_webshop BEFORE UPDATE ON public.articles_webshop FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: bank trg_updatebank; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebank BEFORE UPDATE ON public.bank FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: biblio trg_updatebiblio; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebiblio BEFORE UPDATE ON public.biblio FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany trg_updatebotany; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany BEFORE UPDATE ON public.botany FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany_class trg_updatebotany_class; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany_class BEFORE UPDATE ON public.botany_class FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany_divisio trg_updatebotany_divisio; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany_divisio BEFORE UPDATE ON public.botany_divisio FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany_family trg_updatebotany_family; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany_family BEFORE UPDATE ON public.botany_family FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany_genus trg_updatebotany_genus; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany_genus BEFORE UPDATE ON public.botany_genus FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany_kingdom trg_updatebotany_kingdom; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany_kingdom BEFORE UPDATE ON public.botany_kingdom FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: botany_ordo trg_updatebotany_ordo; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatebotany_ordo BEFORE UPDATE ON public.botany_ordo FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: cash_desk trg_updatecash_desk; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatecash_desk BEFORE UPDATE ON public.cash_desk FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: cash_desk_book_number trg_updatecash_desk_book_number; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatecash_desk_book_number BEFORE UPDATE ON public.cash_desk_book_number FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: clients trg_updateclients; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateclients BEFORE UPDATE ON public.clients FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: dms trg_updatedms; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatedms BEFORE UPDATE ON public.dms FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: enquiry trg_updateenquiry; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateenquiry BEFORE UPDATE ON public.enquiry FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: enquiryget trg_updateenquiryget; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateenquiryget BEFORE UPDATE ON public.enquiryget FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: enquiryinvoice trg_updateenquiryinvoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateenquiryinvoice BEFORE UPDATE ON public.enquiryinvoice FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: enquirymisc trg_updateenquirymisc; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateenquirymisc BEFORE UPDATE ON public.enquirymisc FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: enquiryposition trg_updateenquiryposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateenquiryposition BEFORE UPDATE ON public.enquiryposition FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: enquirysupply trg_updateenquirysupply; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateenquirysupply BEFORE UPDATE ON public.enquirysupply FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave trg_updategrave; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave BEFORE UPDATE ON public.grave FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_service_notes trg_updategrave_service_notes; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_service_notes BEFORE UPDATE ON public.grave_service_notes FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_autumn trg_updategrave_work_autumn; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_autumn BEFORE UPDATE ON public.grave_work_autumn FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_holiday trg_updategrave_work_holiday; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_holiday BEFORE UPDATE ON public.grave_work_holiday FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_maintenance trg_updategrave_work_maintenance; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_maintenance BEFORE UPDATE ON public.grave_work_maintenance FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_single trg_updategrave_work_single; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_single BEFORE UPDATE ON public.grave_work_single FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_spring trg_updategrave_work_spring; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_spring BEFORE UPDATE ON public.grave_work_spring FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_summer trg_updategrave_work_summer; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_summer BEFORE UPDATE ON public.grave_work_summer FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_winter trg_updategrave_work_winter; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_winter BEFORE UPDATE ON public.grave_work_winter FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: grave_work_year trg_updategrave_work_year; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategrave_work_year BEFORE UPDATE ON public.grave_work_year FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: graveyard trg_updategraveyard; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updategraveyard BEFORE UPDATE ON public.graveyard FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: hibernation trg_updatehibernation; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatehibernation BEFORE UPDATE ON public.hibernation FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: hibernation_plant trg_updatehibernation_plant; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatehibernation_plant BEFORE UPDATE ON public.hibernation_plant FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: in_payment trg_updatein_payment; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatein_payment BEFORE UPDATE ON public.in_payment FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: list_of_deliveries trg_updatelist_of_deliveries; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatelist_of_deliveries BEFORE UPDATE ON public.list_of_deliveries FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: list_of_invoices trg_updatelist_of_invoices; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatelist_of_invoices BEFORE UPDATE ON public.list_of_invoices FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: orderbook trg_updateorderbook; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateorderbook BEFORE UPDATE ON public.orderbook FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: orderinvoice trg_updateorderinvoice; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateorderinvoice BEFORE UPDATE ON public.orderinvoice FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: orderposition trg_updateorderposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateorderposition BEFORE UPDATE ON public.orderposition FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: partner trg_updatepartner; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatepartner BEFORE UPDATE ON public.partner FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: project_phases trg_updateproject_phases; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproject_phases BEFORE UPDATE ON public.project_phases FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: project_task_material_res trg_updateproject_task_material_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproject_task_material_res BEFORE UPDATE ON public.project_task_material_res FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: project_tasks trg_updateproject_tasks; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproject_tasks BEFORE UPDATE ON public.project_tasks FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: projects trg_updateprojects; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateprojects BEFORE UPDATE ON public.projects FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: proposal trg_updateproposal; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproposal BEFORE UPDATE ON public.proposal FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: proposalget trg_updateproposalget; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproposalget BEFORE UPDATE ON public.proposalget FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: proposalmisc trg_updateproposalmisc; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproposalmisc BEFORE UPDATE ON public.proposalmisc FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: proposalposition trg_updateproposalposition; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproposalposition BEFORE UPDATE ON public.proposalposition FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: proposalsupply trg_updateproposalsupply; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updateproposalsupply BEFORE UPDATE ON public.proposalsupply FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: sourcecode_file trg_updatesourcecode_file; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesourcecode_file BEFORE UPDATE ON public.sourcecode_file FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: sourcecode_modul_material_res trg_updatesourcecode_modul_material_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesourcecode_modul_material_res BEFORE UPDATE ON public.sourcecode_modul_material_res FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: sourcecode_module trg_updatesourcecode_module; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesourcecode_module BEFORE UPDATE ON public.sourcecode_module FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: sourcecode_module_staff_res trg_updatesourcecode_module_staff_res; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesourcecode_module_staff_res BEFORE UPDATE ON public.sourcecode_module_staff_res FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: sourcecode_parts trg_updatesourcecode_parts; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesourcecode_parts BEFORE UPDATE ON public.sourcecode_parts FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: sourcecode_projects trg_updatesourcecode_projects; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesourcecode_projects BEFORE UPDATE ON public.sourcecode_projects FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: staff trg_updatestaff; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatestaff BEFORE UPDATE ON public.staff FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: stock_goods trg_updatestock_goods; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatestock_goods BEFORE UPDATE ON public.stock_goods FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: support_project trg_updatesupport_project; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesupport_project BEFORE UPDATE ON public.support_project FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: support_ticket trg_updatesupport_ticket; Type: TRIGGER; Schema: public; Owner: cuon_admin
--

CREATE TRIGGER trg_updatesupport_ticket BEFORE UPDATE ON public.support_ticket FOR EACH ROW EXECUTE PROCEDURE public.fct_update();


--
-- Name: hibernation f_HibernationAddressID; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.hibernation
    ADD CONSTRAINT "f_HibernationAddressID" FOREIGN KEY (addressnumber) REFERENCES public.address(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: orderposition f_OrderpositionArticleid; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.orderposition
    ADD CONSTRAINT "f_OrderpositionArticleid" FOREIGN KEY (articleid) REFERENCES public.articles(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: partner f_PartnerAddress; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.partner
    ADD CONSTRAINT "f_PartnerAddress" FOREIGN KEY (addressid) REFERENCES public.address(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: address_bank fk_addressbank_address; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.address_bank
    ADD CONSTRAINT fk_addressbank_address FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- Name: staff_disease fk_staffdisease_staff; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_disease
    ADD CONSTRAINT fk_staffdisease_staff FOREIGN KEY (staff_id) REFERENCES public.staff(id);


--
-- Name: staff_fee fk_stafffee_staff; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_fee
    ADD CONSTRAINT fk_stafffee_staff FOREIGN KEY (staff_id) REFERENCES public.staff(id);


--
-- Name: staff_misc fk_staffmisc_staff; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_misc
    ADD CONSTRAINT fk_staffmisc_staff FOREIGN KEY (staff_id) REFERENCES public.staff(id);


--
-- Name: staff_vacation fk_staffvacation_staff; Type: FK CONSTRAINT; Schema: public; Owner: cuon_admin
--

ALTER TABLE ONLY public.staff_vacation
    ADD CONSTRAINT fk_staffvacation_staff FOREIGN KEY (staff_id) REFERENCES public.staff(id);


--
-- Name: TABLE account_info; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.account_info TO cuon_all;


--
-- Name: TABLE account_info_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.account_info_history TO cuon_all;


--
-- Name: SEQUENCE account_info_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.account_info_id TO cuon_all;


--
-- Name: TABLE account_plan; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.account_plan TO cuon_all;


--
-- Name: TABLE account_plan_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.account_plan_history TO cuon_all;


--
-- Name: SEQUENCE account_plan_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.account_plan_id TO cuon_all;


--
-- Name: TABLE account_sentence; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.account_sentence TO cuon_all;


--
-- Name: TABLE account_sentence_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.account_sentence_history TO cuon_all;


--
-- Name: SEQUENCE account_sentence_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.account_sentence_id TO cuon_all;


--
-- Name: TABLE address; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.address TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.address TO cuon_all;


--
-- Name: TABLE address_bank; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.address_bank TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.address_bank TO cuon_all;


--
-- Name: TABLE address_bank_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.address_bank_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.address_bank_history TO cuon_all;


--
-- Name: SEQUENCE address_bank_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.address_bank_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.address_bank_id TO cuon_all;


--
-- Name: TABLE address_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.address_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.address_history TO cuon_all;


--
-- Name: SEQUENCE address_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.address_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.address_id TO cuon_all;


--
-- Name: TABLE address_notes; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.address_notes TO cuon_all;


--
-- Name: TABLE address_notes_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.address_notes_history TO cuon_all;


--
-- Name: SEQUENCE address_notes_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.address_notes_id TO cuon_all;


--
-- Name: TABLE addresses_misc; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.addresses_misc TO cuon_all;


--
-- Name: TABLE addresses_misc_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.addresses_misc_history TO cuon_all;


--
-- Name: SEQUENCE addresses_misc_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.addresses_misc_id TO cuon_all;


--
-- Name: TABLE administrative_district; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.administrative_district TO cuon_all;


--
-- Name: TABLE administrative_district_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.administrative_district_history TO cuon_all;


--
-- Name: SEQUENCE administrative_district_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.administrative_district_id TO cuon_all;


--
-- Name: TABLE articles; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles TO cuon_all;


--
-- Name: TABLE articles_barcode; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_barcode TO cuon_all;


--
-- Name: TABLE articles_barcode_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_barcode_history TO cuon_all;


--
-- Name: SEQUENCE articles_barcode_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_barcode_id TO cuon_all;


--
-- Name: TABLE articles_customers; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_customers TO cuon_all;


--
-- Name: TABLE articles_customers_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_customers_history TO cuon_all;


--
-- Name: SEQUENCE articles_customers_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_customers_id TO cuon_all;


--
-- Name: TABLE articles_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_history TO cuon_all;


--
-- Name: SEQUENCE articles_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_id TO cuon_all;


--
-- Name: TABLE articles_parts_list; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_parts_list TO cuon_all;


--
-- Name: TABLE articles_parts_list_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_parts_list_history TO cuon_all;


--
-- Name: SEQUENCE articles_parts_list_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_parts_list_id TO cuon_all;


--
-- Name: TABLE articles_purchase; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_purchase TO cuon_all;


--
-- Name: TABLE articles_purchase_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_purchase_history TO cuon_all;


--
-- Name: SEQUENCE articles_purchase_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_purchase_id TO cuon_all;


--
-- Name: TABLE articles_sales; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_sales TO cuon_all;


--
-- Name: TABLE articles_sales_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_sales_history TO cuon_all;


--
-- Name: SEQUENCE articles_sales_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_sales_id TO cuon_all;


--
-- Name: TABLE articles_stock; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_stock TO cuon_all;


--
-- Name: TABLE articles_stock_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_stock_history TO cuon_all;


--
-- Name: SEQUENCE articles_stock_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_stock_id TO cuon_all;


--
-- Name: TABLE articles_webshop; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_webshop TO cuon_all;


--
-- Name: TABLE articles_webshop_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.articles_webshop_history TO cuon_all;


--
-- Name: SEQUENCE articles_webshop_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.articles_webshop_id TO cuon_all;


--
-- Name: TABLE bank; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.bank TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.bank TO cuon_all;


--
-- Name: TABLE bank_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.bank_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.bank_history TO cuon_all;


--
-- Name: SEQUENCE bank_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.bank_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.bank_id TO cuon_all;


--
-- Name: TABLE biblio; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.biblio TO cuon_all;


--
-- Name: TABLE biblio_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.biblio_history TO cuon_all;


--
-- Name: SEQUENCE biblio_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.biblio_id TO cuon_all;


--
-- Name: TABLE botany; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany TO cuon_all;


--
-- Name: TABLE botany_class; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_class TO cuon_all;


--
-- Name: TABLE botany_class_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_class_history TO cuon_all;


--
-- Name: SEQUENCE botany_class_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.botany_class_id TO cuon_all;


--
-- Name: TABLE botany_divisio; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_divisio TO cuon_all;


--
-- Name: TABLE botany_divisio_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_divisio_history TO cuon_all;


--
-- Name: SEQUENCE botany_divisio_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.botany_divisio_id TO cuon_all;


--
-- Name: TABLE botany_family; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_family TO cuon_all;


--
-- Name: TABLE botany_family_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_family_history TO cuon_all;


--
-- Name: SEQUENCE botany_family_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.botany_family_id TO cuon_all;


--
-- Name: TABLE botany_genus; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_genus TO cuon_all;


--
-- Name: TABLE botany_genus_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_genus_history TO cuon_all;


--
-- Name: SEQUENCE botany_genus_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.botany_genus_id TO cuon_all;


--
-- Name: TABLE botany_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_history TO cuon_all;


--
-- Name: SEQUENCE botany_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.botany_id TO cuon_all;


--
-- Name: TABLE botany_ordo; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_ordo TO cuon_all;


--
-- Name: TABLE botany_ordo_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.botany_ordo_history TO cuon_all;


--
-- Name: SEQUENCE botany_ordo_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.botany_ordo_id TO cuon_all;


--
-- Name: TABLE cash_desk; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT ON TABLE public.cash_desk TO cuon_all;


--
-- Name: TABLE cash_desk_book_number; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.cash_desk_book_number TO cuon_all;


--
-- Name: TABLE cash_desk_book_number_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.cash_desk_book_number_history TO cuon_all;


--
-- Name: SEQUENCE cash_desk_book_number_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.cash_desk_book_number_id TO cuon_all;


--
-- Name: TABLE cash_desk_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT ON TABLE public.cash_desk_history TO cuon_all;


--
-- Name: SEQUENCE cash_desk_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.cash_desk_id TO cuon_all;
GRANT SELECT,UPDATE ON SEQUENCE public.cash_desk_id TO PUBLIC;


--
-- Name: TABLE city; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.city TO cuon_all;


--
-- Name: TABLE city_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.city_history TO cuon_all;


--
-- Name: SEQUENCE city_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.city_id TO cuon_all;


--
-- Name: TABLE clients; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.clients TO cuon_all;


--
-- Name: TABLE clients_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.clients_history TO cuon_all;


--
-- Name: SEQUENCE clients_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.clients_id TO cuon_all;


--
-- Name: TABLE contact; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.contact TO cuon_all;


--
-- Name: TABLE contact_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.contact_history TO cuon_all;


--
-- Name: SEQUENCE contact_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.contact_id TO cuon_all;


--
-- Name: TABLE country; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.country TO cuon_all;


--
-- Name: TABLE country_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.country_history TO cuon_all;


--
-- Name: SEQUENCE country_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.country_id TO cuon_all;


--
-- Name: TABLE cuon; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT ALL ON TABLE public.cuon TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon TO zope_rights;


--
-- Name: TABLE cuon_clients; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_clients TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_clients TO cuon_all;


--
-- Name: TABLE cuon_clients_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_clients_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_clients_history TO cuon_all;


--
-- Name: SEQUENCE cuon_clients_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.cuon_clients_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.cuon_clients_id TO cuon_all;


--
-- Name: TABLE cuon_config; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.cuon_config TO zope_rights;
GRANT SELECT ON TABLE public.cuon_config TO cuon_all;


--
-- Name: TABLE cuon_config_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.cuon_config_history TO zope_rights;
GRANT SELECT ON TABLE public.cuon_config_history TO cuon_all;


--
-- Name: SEQUENCE cuon_config_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.cuon_config_id TO zope_rights;
GRANT SELECT ON SEQUENCE public.cuon_config_id TO cuon_all;


--
-- Name: TABLE cuon_user; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_user TO cuon_all;


--
-- Name: TABLE cuon_user_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_user_history TO cuon_all;


--
-- Name: SEQUENCE cuon_user_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.cuon_user_id TO cuon_all;


--
-- Name: TABLE cuon_values; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_values TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_values TO cuon_all;


--
-- Name: TABLE cuon_values_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_values_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.cuon_values_history TO cuon_all;


--
-- Name: SEQUENCE cuon_values_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.cuon_values_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.cuon_values_id TO cuon_all;


--
-- Name: TABLE district; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.district TO cuon_all;


--
-- Name: TABLE district_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.district_history TO cuon_all;


--
-- Name: SEQUENCE district_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.district_id TO cuon_all;


--
-- Name: TABLE dms; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.dms TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.dms TO cuon_all;


--
-- Name: TABLE dms_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.dms_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.dms_history TO cuon_all;


--
-- Name: SEQUENCE dms_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.dms_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.dms_id TO cuon_all;


--
-- Name: TABLE enquiry; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiry TO cuon_all;


--
-- Name: TABLE enquiry_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiry_history TO cuon_all;


--
-- Name: SEQUENCE enquiry_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.enquiry_id TO cuon_all;


--
-- Name: TABLE enquiryget; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiryget TO cuon_all;


--
-- Name: TABLE enquiryget_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiryget_history TO cuon_all;


--
-- Name: SEQUENCE enquiryget_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.enquiryget_id TO cuon_all;


--
-- Name: TABLE enquiryinvoice; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiryinvoice TO cuon_all;


--
-- Name: TABLE enquiryinvoice_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiryinvoice_history TO cuon_all;


--
-- Name: SEQUENCE enquiryinvoice_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.enquiryinvoice_id TO cuon_all;


--
-- Name: TABLE enquiryposition; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiryposition TO cuon_all;


--
-- Name: TABLE enquiryposition_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquiryposition_history TO cuon_all;


--
-- Name: SEQUENCE enquiryposition_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.enquiryposition_id TO cuon_all;


--
-- Name: TABLE enquirysupply; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquirysupply TO cuon_all;


--
-- Name: TABLE enquirysupply_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.enquirysupply_history TO cuon_all;


--
-- Name: SEQUENCE enquirysupply_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.enquirysupply_id TO cuon_all;


--
-- Name: TABLE grave; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave TO cuon_all;


--
-- Name: TABLE grave_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_history TO cuon_all;


--
-- Name: SEQUENCE grave_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_id TO cuon_all;


--
-- Name: TABLE grave_invoice_info; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_invoice_info TO cuon_all;


--
-- Name: TABLE grave_invoice_info_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_invoice_info_history TO cuon_all;


--
-- Name: SEQUENCE grave_invoice_info_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_invoice_info_id TO cuon_all;


--
-- Name: TABLE grave_service_notes; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_service_notes TO cuon_all;


--
-- Name: TABLE grave_service_notes_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_service_notes_history TO cuon_all;


--
-- Name: SEQUENCE grave_service_notes_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_service_notes_id TO cuon_all;


--
-- Name: TABLE grave_work_autumn; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_autumn TO cuon_all;


--
-- Name: TABLE grave_work_autumn_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_autumn_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_autumn_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_autumn_id TO cuon_all;


--
-- Name: TABLE grave_work_holiday; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_holiday TO cuon_all;


--
-- Name: TABLE grave_work_holiday_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_holiday_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_holiday_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_holiday_id TO cuon_all;


--
-- Name: TABLE grave_work_maintenance; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_maintenance TO cuon_all;


--
-- Name: TABLE grave_work_maintenance_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_maintenance_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_maintenance_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_maintenance_id TO cuon_all;


--
-- Name: TABLE grave_work_single; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_single TO cuon_all;


--
-- Name: TABLE grave_work_single_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_single_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_single_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_single_id TO cuon_all;


--
-- Name: TABLE grave_work_spring; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_spring TO cuon_all;


--
-- Name: TABLE grave_work_spring_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_spring_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_spring_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_spring_id TO cuon_all;


--
-- Name: TABLE grave_work_summer; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_summer TO cuon_all;


--
-- Name: TABLE grave_work_summer_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_summer_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_summer_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_summer_id TO cuon_all;


--
-- Name: TABLE grave_work_winter; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_winter TO cuon_all;


--
-- Name: TABLE grave_work_winter_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_winter_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_winter_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_winter_id TO cuon_all;


--
-- Name: TABLE grave_work_year; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_year TO cuon_all;


--
-- Name: TABLE grave_work_year_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.grave_work_year_history TO cuon_all;


--
-- Name: SEQUENCE grave_work_year_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.grave_work_year_id TO cuon_all;


--
-- Name: TABLE graveyard; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.graveyard TO cuon_all;


--
-- Name: TABLE graveyard_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.graveyard_history TO cuon_all;


--
-- Name: SEQUENCE graveyard_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.graveyard_id TO cuon_all;


--
-- Name: TABLE hibernation; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.hibernation TO cuon_all;


--
-- Name: TABLE hibernation_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.hibernation_history TO cuon_all;


--
-- Name: SEQUENCE hibernation_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.hibernation_id TO cuon_all;


--
-- Name: TABLE hibernation_plant; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.hibernation_plant TO cuon_all;


--
-- Name: TABLE hibernation_plant_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.hibernation_plant_history TO cuon_all;


--
-- Name: SEQUENCE hibernation_plant_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.hibernation_plant_id TO cuon_all;


--
-- Name: TABLE in_payment; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.in_payment TO cuon_all;


--
-- Name: TABLE in_payment_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.in_payment_history TO cuon_all;


--
-- Name: SEQUENCE in_payment_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.in_payment_id TO cuon_all;


--
-- Name: TABLE list_of_deliveries; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_deliveries TO cuon_all;


--
-- Name: TABLE list_of_deliveries_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_deliveries_history TO cuon_all;


--
-- Name: SEQUENCE list_of_deliveries_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.list_of_deliveries_id TO cuon_all;


--
-- Name: TABLE list_of_hibernation_incoming; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_hibernation_incoming TO cuon_all;


--
-- Name: TABLE list_of_hibernation_incoming_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_hibernation_incoming_history TO cuon_all;


--
-- Name: SEQUENCE list_of_hibernation_incoming_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.list_of_hibernation_incoming_id TO cuon_all;


--
-- Name: TABLE list_of_hibernation_outgoing; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_hibernation_outgoing TO cuon_all;


--
-- Name: TABLE list_of_hibernation_outgoing_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_hibernation_outgoing_history TO cuon_all;


--
-- Name: SEQUENCE list_of_hibernation_outgoing_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.list_of_hibernation_outgoing_id TO cuon_all;


--
-- Name: TABLE list_of_hibernation_pickup; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_hibernation_pickup TO cuon_all;


--
-- Name: TABLE list_of_hibernation_pickup_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_hibernation_pickup_history TO cuon_all;


--
-- Name: SEQUENCE list_of_hibernation_pickup_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.list_of_hibernation_pickup_id TO cuon_all;


--
-- Name: TABLE list_of_invoices; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_invoices TO cuon_all;


--
-- Name: TABLE list_of_invoices_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_invoices_history TO cuon_all;


--
-- Name: SEQUENCE list_of_invoices_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.list_of_invoices_id TO cuon_all;


--
-- Name: TABLE list_of_pickups; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_pickups TO cuon_all;


--
-- Name: TABLE list_of_pickups_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.list_of_pickups_history TO cuon_all;


--
-- Name: SEQUENCE list_of_pickups_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.list_of_pickups_id TO cuon_all;


--
-- Name: TABLE material_group; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.material_group TO cuon_all;


--
-- Name: TABLE material_group_accounts; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.material_group_accounts TO cuon_all;


--
-- Name: TABLE material_group_accounts_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.material_group_accounts_history TO cuon_all;


--
-- Name: SEQUENCE material_group_accounts_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.material_group_accounts_id TO cuon_all;


--
-- Name: TABLE material_group_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.material_group_history TO cuon_all;


--
-- Name: SEQUENCE material_group_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.material_group_id TO cuon_all;


--
-- Name: TABLE mindmap; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.mindmap TO cuon_all;


--
-- Name: TABLE mindmap_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.mindmap_history TO cuon_all;


--
-- Name: SEQUENCE mindmap_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.mindmap_id TO cuon_all;


--
-- Name: TABLE misc_data; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.misc_data TO cuon_all;


--
-- Name: TABLE misc_data_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.misc_data_history TO cuon_all;


--
-- Name: SEQUENCE misc_data_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.misc_data_id TO cuon_all;


--
-- Name: TABLE orderbook; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderbook TO cuon_all;


--
-- Name: TABLE orderbook_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderbook_history TO cuon_all;


--
-- Name: SEQUENCE orderbook_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.orderbook_id TO cuon_all;


--
-- Name: TABLE orderget; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderget TO cuon_all;


--
-- Name: TABLE orderget_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderget_history TO cuon_all;


--
-- Name: SEQUENCE orderget_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.orderget_id TO cuon_all;


--
-- Name: TABLE orderinvoice; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderinvoice TO cuon_all;


--
-- Name: TABLE orderinvoice_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderinvoice_history TO cuon_all;


--
-- Name: SEQUENCE orderinvoice_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.orderinvoice_id TO cuon_all;


--
-- Name: TABLE orderposition; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderposition TO cuon_all;


--
-- Name: TABLE orderposition_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.orderposition_history TO cuon_all;


--
-- Name: SEQUENCE orderposition_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.orderposition_id TO cuon_all;


--
-- Name: TABLE ordersupply; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.ordersupply TO cuon_all;


--
-- Name: TABLE ordersupply_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.ordersupply_history TO cuon_all;


--
-- Name: SEQUENCE ordersupply_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.ordersupply_id TO cuon_all;


--
-- Name: TABLE partner; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.partner TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.partner TO cuon_all;


--
-- Name: TABLE partner_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.partner_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.partner_history TO cuon_all;


--
-- Name: SEQUENCE partner_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.partner_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.partner_id TO cuon_all;


--
-- Name: TABLE partner_schedul; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON TABLE public.partner_schedul TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.partner_schedul TO cuon_all;


--
-- Name: TABLE partner_schedul_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON TABLE public.partner_schedul_history TO zope_rights;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.partner_schedul_history TO cuon_all;


--
-- Name: SEQUENCE partner_schedul_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.partner_schedul_id TO zope_rights;
GRANT SELECT,UPDATE ON SEQUENCE public.partner_schedul_id TO cuon_all;


--
-- Name: TABLE planet; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.planet TO cuon_all;


--
-- Name: TABLE planet_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.planet_history TO cuon_all;


--
-- Name: SEQUENCE planet_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.planet_id TO cuon_all;


--
-- Name: TABLE preferences; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.preferences TO cuon_all;


--
-- Name: TABLE preferences_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.preferences_history TO cuon_all;


--
-- Name: SEQUENCE preferences_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.preferences_id TO cuon_all;


--
-- Name: TABLE project_phases; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_phases TO cuon_all;


--
-- Name: TABLE project_phases_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_phases_history TO cuon_all;


--
-- Name: SEQUENCE project_phases_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.project_phases_id TO cuon_all;


--
-- Name: TABLE project_task_material_res; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_task_material_res TO cuon_all;


--
-- Name: TABLE project_task_material_res_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_task_material_res_history TO cuon_all;


--
-- Name: SEQUENCE project_task_material_res_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.project_task_material_res_id TO cuon_all;


--
-- Name: TABLE project_task_staff_res; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_task_staff_res TO cuon_all;


--
-- Name: TABLE project_task_staff_res_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_task_staff_res_history TO cuon_all;


--
-- Name: SEQUENCE project_task_staff_res_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.project_task_staff_res_id TO cuon_all;


--
-- Name: TABLE project_tasks; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_tasks TO cuon_all;


--
-- Name: TABLE project_tasks_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.project_tasks_history TO cuon_all;


--
-- Name: SEQUENCE project_tasks_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.project_tasks_id TO cuon_all;


--
-- Name: TABLE projects; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.projects TO cuon_all;


--
-- Name: TABLE projects_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.projects_history TO cuon_all;


--
-- Name: SEQUENCE projects_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.projects_id TO cuon_all;


--
-- Name: TABLE proposal; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposal TO cuon_all;


--
-- Name: TABLE proposal_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposal_history TO cuon_all;


--
-- Name: SEQUENCE proposal_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.proposal_id TO cuon_all;


--
-- Name: TABLE proposalget; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalget TO cuon_all;


--
-- Name: TABLE proposalget_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalget_history TO cuon_all;


--
-- Name: SEQUENCE proposalget_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.proposalget_id TO cuon_all;


--
-- Name: TABLE proposalinvoice; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalinvoice TO cuon_all;


--
-- Name: TABLE proposalinvoice_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalinvoice_history TO cuon_all;


--
-- Name: SEQUENCE proposalinvoice_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.proposalinvoice_id TO cuon_all;


--
-- Name: TABLE proposalposition; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalposition TO cuon_all;


--
-- Name: TABLE proposalposition_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalposition_history TO cuon_all;


--
-- Name: SEQUENCE proposalposition_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.proposalposition_id TO cuon_all;


--
-- Name: TABLE proposalsupply; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalsupply TO cuon_all;


--
-- Name: TABLE proposalsupply_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.proposalsupply_history TO cuon_all;


--
-- Name: SEQUENCE proposalsupply_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.proposalsupply_id TO cuon_all;


--
-- Name: TABLE sourcecode_file; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_file TO cuon_all;


--
-- Name: TABLE sourcecode_file_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_file_history TO cuon_all;


--
-- Name: SEQUENCE sourcecode_file_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.sourcecode_file_id TO cuon_all;


--
-- Name: TABLE sourcecode_modul_material_res; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_modul_material_res TO cuon_all;


--
-- Name: TABLE sourcecode_modul_material_res_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_modul_material_res_history TO cuon_all;


--
-- Name: SEQUENCE sourcecode_modul_material_res_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.sourcecode_modul_material_res_id TO cuon_all;


--
-- Name: TABLE sourcecode_module; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_module TO cuon_all;


--
-- Name: TABLE sourcecode_module_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_module_history TO cuon_all;


--
-- Name: SEQUENCE sourcecode_module_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.sourcecode_module_id TO cuon_all;


--
-- Name: TABLE sourcecode_parts; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_parts TO cuon_all;


--
-- Name: TABLE sourcecode_parts_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_parts_history TO cuon_all;


--
-- Name: SEQUENCE sourcecode_parts_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.sourcecode_parts_id TO cuon_all;


--
-- Name: TABLE sourcecode_projects; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_projects TO cuon_all;


--
-- Name: TABLE sourcecode_projects_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sourcecode_projects_history TO cuon_all;


--
-- Name: SEQUENCE sourcecode_projects_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.sourcecode_projects_id TO cuon_all;


--
-- Name: TABLE staff; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.staff TO zope_rights;
GRANT SELECT ON TABLE public.staff TO cuon_all;


--
-- Name: TABLE staff_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.staff_history TO zope_rights;
GRANT SELECT ON TABLE public.staff_history TO cuon_all;


--
-- Name: SEQUENCE staff_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.staff_id TO zope_rights;
GRANT SELECT ON SEQUENCE public.staff_id TO cuon_all;


--
-- Name: TABLE states; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.states TO cuon_all;


--
-- Name: TABLE states_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.states_history TO cuon_all;


--
-- Name: SEQUENCE states_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.states_id TO cuon_all;


--
-- Name: TABLE stock_goods; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.stock_goods TO cuon_all;


--
-- Name: TABLE stock_goods_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.stock_goods_history TO cuon_all;


--
-- Name: SEQUENCE stock_goods_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.stock_goods_id TO cuon_all;


--
-- Name: TABLE stocks; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.stocks TO cuon_all;


--
-- Name: TABLE stocks_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.stocks_history TO cuon_all;


--
-- Name: SEQUENCE stocks_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.stocks_id TO cuon_all;


--
-- Name: TABLE support_project; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.support_project TO cuon_all;
GRANT SELECT,INSERT ON TABLE public.support_project TO cuon_supportticketweb;


--
-- Name: TABLE support_project_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.support_project_history TO cuon_all;
GRANT SELECT,INSERT ON TABLE public.support_project_history TO cuon_supportticketweb;


--
-- Name: SEQUENCE support_project_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.support_project_id TO cuon_all;
GRANT SELECT ON SEQUENCE public.support_project_id TO cuon_supportticketweb;


--
-- Name: TABLE support_ticket; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.support_ticket TO cuon_all;
GRANT SELECT,INSERT ON TABLE public.support_ticket TO cuon_supportticketweb;


--
-- Name: TABLE support_ticket_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.support_ticket_history TO cuon_all;
GRANT SELECT,INSERT ON TABLE public.support_ticket_history TO cuon_supportticketweb;


--
-- Name: SEQUENCE support_ticket_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.support_ticket_id TO cuon_all;
GRANT SELECT ON SEQUENCE public.support_ticket_id TO cuon_supportticketweb;


--
-- Name: TABLE tax_vat; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.tax_vat TO cuon_all;


--
-- Name: TABLE tax_vat_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.tax_vat_history TO cuon_all;


--
-- Name: SEQUENCE tax_vat_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.tax_vat_id TO cuon_all;


--
-- Name: TABLE terms_of_payment; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.terms_of_payment TO cuon_all;


--
-- Name: TABLE terms_of_payment_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.terms_of_payment_history TO cuon_all;


--
-- Name: SEQUENCE terms_of_payment_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.terms_of_payment_id TO cuon_all;


--
-- Name: TABLE user_info; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_info TO cuon_all;


--
-- Name: TABLE user_info_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_info_history TO cuon_all;


--
-- Name: SEQUENCE user_info_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.user_info_id TO cuon_all;


--
-- Name: TABLE usergroups; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.usergroups TO cuon_all;


--
-- Name: TABLE usergroups_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.usergroups_history TO cuon_all;


--
-- Name: SEQUENCE usergroups_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.usergroups_id TO cuon_all;


--
-- Name: TABLE userlogin; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.userlogin TO cuon_all;


--
-- Name: TABLE userlogin_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.userlogin_history TO cuon_all;


--
-- Name: SEQUENCE userlogin_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.userlogin_id TO cuon_all;


--
-- Name: TABLE web2; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.web2 TO zope_rights;


--
-- Name: TABLE web2_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON TABLE public.web2_history TO zope_rights;


--
-- Name: SEQUENCE web2_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT ON SEQUENCE public.web2_id TO zope_rights;


--
-- Name: TABLE xmlvalues; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.xmlvalues TO cuon_all;


--
-- Name: TABLE xmlvalues_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.xmlvalues_history TO cuon_all;


--
-- Name: SEQUENCE xmlvalues_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.xmlvalues_id TO cuon_all;


--
-- Name: TABLE zipcodes; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.zipcodes TO cuon_all;


--
-- Name: TABLE zipcodes_history; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.zipcodes_history TO cuon_all;


--
-- Name: SEQUENCE zipcodes_id; Type: ACL; Schema: public; Owner: cuon_admin
--

GRANT SELECT,UPDATE ON SEQUENCE public.zipcodes_id TO cuon_all;


--
-- PostgreSQL database dump complete
--

