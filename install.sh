#!/bin/bash



# install postgres modules

cd DATABASE
psql -Ucuon_admin cuon < cuon_schema.sql

cd sql

psql -Ucuon_admin cuon <  basics.sql
psql -Ucuon_admin cuon <  it.sql
psql -Ucuon_admin cuon <  address.sql
psql -Ucuon_admin cuon <  user.sql
psql -Ucuon_admin cuon <  databases.sql
psql -Ucuon_admin cuon <  garden.sql
psql -Ucuon_admin cuon <  invoice.sql
psql -Ucuon_admin cuon <  order.sql
psql -Ucuon_admin cuon <  accounting_inpayment.sql
psql -Ucuon_admin cuon <  accounting.sql
psql -Ucuon_admin cuon <  accounting_order.sql
psql -Ucuon_admin cuon <  dms.sql
psql -Ucuon_admin cuon <  graves.sql
psql -Ucuon_admin cuon <  address.sql
psql -Ucuon_admin cuon <  articles.sql

echo " select * from fct_create_allindex() ;" | psql -Ucuon_admin cuon

echo " select * from fct_create_alltrigger() ;" | psql -Ucuon_admin cuon
