<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'insert_time') ?>

    <?= $form->field($model, 'update_time') ?>

    <?php // echo $form->field($model, 'update_user_id') ?>

    <?php // echo $form->field($model, 'client') ?>

    <?php // echo $form->field($model, 'sep_info_1') ?>

    <?php // echo $form->field($model, 'sep_info_2') ?>

    <?php // echo $form->field($model, 'sep_info_3') ?>

    <?php // echo $form->field($model, 'versions_number') ?>

    <?php // echo $form->field($model, 'versions_uuid') ?>

    <?php // echo $form->field($model, 'uuid') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'lastname') ?>

    <?php // echo $form->field($model, 'lastname2') ?>

    <?php // echo $form->field($model, 'firstname') ?>

    <?php // echo $form->field($model, 'street') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'zip') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'zip_for_postbox') ?>

    <?php // echo $form->field($model, 'postbox') ?>

    <?php // echo $form->field($model, 'suburb') ?>

    <?php // echo $form->field($model, 'state_full') ?>

    <?php // echo $form->field($model, 'letter_address') ?>

    <?php // echo $form->field($model, 'phone_handy') ?>

    <?php // echo $form->field($model, 'email_noification') ?>

    <?php // echo $form->field($model, 'newsletter') ?>

    <?php // echo $form->field($model, 'homepage_url') ?>

    <?php // echo $form->field($model, 'status_info') ?>

    <?php // echo $form->field($model, 'sip') ?>

    <?php // echo $form->field($model, 'skype') ?>

    <?php // echo $form->field($model, 'letter_address2') ?>

    <?php // echo $form->field($model, 'letter_address3') ?>

    <?php // echo $form->field($model, 'letter_address4') ?>

    <?php // echo $form->field($model, 'letter_address5') ?>

    <?php // echo $form->field($model, 'webshop_address_id') ?>

    <?php // echo $form->field($model, 'webshop_zone_id') ?>

    <?php // echo $form->field($model, 'webshop_gender') ?>

    <?php // echo $form->field($model, 'webshop_customers_id') ?>

    <?php // echo $form->field($model, 'caller_id') ?>

    <?php // echo $form->field($model, 'rep_id') ?>

    <?php // echo $form->field($model, 'salesman_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
