<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\BaseHtml;


/* @var $this yii\web\View */
/* @var $model app\models\Address */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="address-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>


   <?=  Html::button('Account', array('onclick' => 'js:document.location.href="index.php?r=addressbank/index&address_id='.$model->id.'"')) ?>
   <?=  Html::button('Partner', array('onclick' => 'js:document.location.href="index.php?r=partner/index&address_id='.$model->id.'"')) ?>
   <?=  Html::button('Misc', array('onclick' => 'js:document.location.href="index.php?r=addressesmisc/index&address_id='.$model->id.'"')) ?>


        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            /* 'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            'update_user_id',
            'client',
            'sep_info_1',
            'sep_info_2',
            'sep_info_3',
            'versions_number',
            'versions_uuid',
            'uuid', */
            'address',
            'lastname',
            'lastname2',
            'firstname',
            'street',
            'country',
            'state',
            'zip',
            'city',
            'phone',
            'fax',
            'email:email',
            'zip_for_postbox',
            'postbox',
            'suburb',
            'state_full',
            'letter_address',
            'phone_handy',
            'email_noification:email',
            'newsletter',
            'homepage_url:url',
            'status_info',
            'sip',
            'skype',
            'letter_address2',
            'letter_address3',
            'letter_address4',
            'letter_address5',
            'webshop_address_id',
            'webshop_zone_id',
            'webshop_gender',
            'webshop_customers_id',
            'caller_id',
            'rep_id',
            'salesman_id',
        ],
    ]) ?>

</div>
