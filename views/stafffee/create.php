<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StaffFee */

$this->title = Yii::t('app', 'Create Staff Fee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff Fees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-fee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
