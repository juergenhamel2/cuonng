<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddressesmiscSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Addressesmiscs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addressesmisc-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Addressesmisc'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            //'address_id',
            //'line',
            //'fashion',
            //'taxnumber',
            //'to_calc_tax:boolean',
            //'price_group',
            //'address_number',
            //'is_webshop:boolean',
            //'update_from_webshop:boolean',
            //'top_id',
            //'date_of_launch',
            //'legal_form',
            //'turnover',
            //'count_of_employees',
            //'trade',
            //'cb_fashion',
            //'additional_emails:ntext',
            //'pricegroup1:boolean',
            //'pricegroup2:boolean',
            //'pricegroup3:boolean',
            //'pricegroup4:boolean',
            //'pricegroup_none:boolean',
            //'allow_direct_debit:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
