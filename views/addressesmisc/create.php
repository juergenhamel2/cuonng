<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Addressesmisc */

$this->title = Yii::t('app', 'Create Addressesmisc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addressesmiscs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addressesmisc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
