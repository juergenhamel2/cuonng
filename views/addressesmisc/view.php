<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Addressesmisc */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addressesmiscs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="addressesmisc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            'update_user_id',
            'client',
            'sep_info_1',
            'sep_info_2',
            'sep_info_3',
            'versions_number',
            'versions_uuid',
            'uuid',
            'address_id',
            'line',
            'fashion',
            'taxnumber',
            'to_calc_tax:boolean',
            'price_group',
            'address_number',
            'is_webshop:boolean',
            'update_from_webshop:boolean',
            'top_id',
            'date_of_launch',
            'legal_form',
            'turnover',
            'count_of_employees',
            'trade',
            'cb_fashion',
            'additional_emails:ntext',
            'pricegroup1:boolean',
            'pricegroup2:boolean',
            'pricegroup3:boolean',
            'pricegroup4:boolean',
            'pricegroup_none:boolean',
            'allow_direct_debit:boolean',
        ],
    ]) ?>

</div>
