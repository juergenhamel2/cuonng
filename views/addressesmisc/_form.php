<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Addressesmisc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addressesmisc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insert_time')->textInput() ?>

    <?= $form->field($model, 'update_time')->textInput() ?>

    <?= $form->field($model, 'update_user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client')->textInput() ?>

    <?= $form->field($model, 'sep_info_1')->textInput() ?>

    <?= $form->field($model, 'sep_info_2')->textInput() ?>

    <?= $form->field($model, 'sep_info_3')->textInput() ?>

    <?= $form->field($model, 'versions_number')->textInput() ?>

    <?= $form->field($model, 'versions_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_id')->textInput() ?>

    <?= $form->field($model, 'line')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fashion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taxnumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_calc_tax')->checkbox() ?>

    <?= $form->field($model, 'price_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_webshop')->checkbox() ?>

    <?= $form->field($model, 'update_from_webshop')->checkbox() ?>

    <?= $form->field($model, 'top_id')->textInput() ?>

    <?= $form->field($model, 'date_of_launch')->textInput() ?>

    <?= $form->field($model, 'legal_form')->textInput() ?>

    <?= $form->field($model, 'turnover')->textInput() ?>

    <?= $form->field($model, 'count_of_employees')->textInput() ?>

    <?= $form->field($model, 'trade')->textInput() ?>

    <?= $form->field($model, 'cb_fashion')->textInput() ?>

    <?= $form->field($model, 'additional_emails')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pricegroup1')->checkbox() ?>

    <?= $form->field($model, 'pricegroup2')->checkbox() ?>

    <?= $form->field($model, 'pricegroup3')->checkbox() ?>

    <?= $form->field($model, 'pricegroup4')->checkbox() ?>

    <?= $form->field($model, 'pricegroup_none')->checkbox() ?>

    <?= $form->field($model, 'allow_direct_debit')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
