<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressesmiscSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addressesmisc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'insert_time') ?>

    <?= $form->field($model, 'update_time') ?>

    <?php // echo $form->field($model, 'update_user_id') ?>

    <?php // echo $form->field($model, 'client') ?>

    <?php // echo $form->field($model, 'sep_info_1') ?>

    <?php // echo $form->field($model, 'sep_info_2') ?>

    <?php // echo $form->field($model, 'sep_info_3') ?>

    <?php // echo $form->field($model, 'versions_number') ?>

    <?php // echo $form->field($model, 'versions_uuid') ?>

    <?php // echo $form->field($model, 'uuid') ?>

    <?php // echo $form->field($model, 'address_id') ?>

    <?php // echo $form->field($model, 'line') ?>

    <?php // echo $form->field($model, 'fashion') ?>

    <?php // echo $form->field($model, 'taxnumber') ?>

    <?php // echo $form->field($model, 'to_calc_tax')->checkbox() ?>

    <?php // echo $form->field($model, 'price_group') ?>

    <?php // echo $form->field($model, 'address_number') ?>

    <?php // echo $form->field($model, 'is_webshop')->checkbox() ?>

    <?php // echo $form->field($model, 'update_from_webshop')->checkbox() ?>

    <?php // echo $form->field($model, 'top_id') ?>

    <?php // echo $form->field($model, 'date_of_launch') ?>

    <?php // echo $form->field($model, 'legal_form') ?>

    <?php // echo $form->field($model, 'turnover') ?>

    <?php // echo $form->field($model, 'count_of_employees') ?>

    <?php // echo $form->field($model, 'trade') ?>

    <?php // echo $form->field($model, 'cb_fashion') ?>

    <?php // echo $form->field($model, 'additional_emails') ?>

    <?php // echo $form->field($model, 'pricegroup1')->checkbox() ?>

    <?php // echo $form->field($model, 'pricegroup2')->checkbox() ?>

    <?php // echo $form->field($model, 'pricegroup3')->checkbox() ?>

    <?php // echo $form->field($model, 'pricegroup4')->checkbox() ?>

    <?php // echo $form->field($model, 'pricegroup_none')->checkbox() ?>

    <?php // echo $form->field($model, 'allow_direct_debit')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
