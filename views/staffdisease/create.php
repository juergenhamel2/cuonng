<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StaffDisease */

$this->title = Yii::t('app', 'Create Staff Disease');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff Diseases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-disease-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
