<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StaffDiseaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staff-disease-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'insert_time') ?>

    <?= $form->field($model, 'update_time') ?>

    <?php // echo $form->field($model, 'update_user_id') ?>

    <?php // echo $form->field($model, 'client') ?>

    <?php // echo $form->field($model, 'sep_info_1') ?>

    <?php // echo $form->field($model, 'sep_info_2') ?>

    <?php // echo $form->field($model, 'sep_info_3') ?>

    <?php // echo $form->field($model, 'versions_number') ?>

    <?php // echo $form->field($model, 'versions_uuid') ?>

    <?php // echo $form->field($model, 'uuid') ?>

    <?php // echo $form->field($model, 'staff_id') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'designation') ?>

    <?php // echo $form->field($model, 'disease_from') ?>

    <?php // echo $form->field($model, 'disease_to') ?>

    <?php // echo $form->field($model, 'days') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
