<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staff-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insert_time')->textInput() ?>

    <?= $form->field($model, 'update_time')->textInput() ?>

    <?= $form->field($model, 'update_user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client')->textInput() ?>

    <?= $form->field($model, 'sep_info_1')->textInput() ?>

    <?= $form->field($model, 'sep_info_2')->textInput() ?>

    <?= $form->field($model, 'sep_info_3')->textInput() ?>

    <?= $form->field($model, 'versions_number')->textInput() ?>

    <?= $form->field($model, 'versions_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip_for_postbox')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postbox')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'suburb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state_full')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_handy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_noification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'newsletter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'homepage_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'skype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_address2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_address3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_address4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_address5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'titular')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'homepage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'faxprivat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday')->textInput() ?>

    <?= $form->field($model, 'staff_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cuon_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_phrase_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'letter_phrase_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'my_sign_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'signature_1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'signature_2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'signature_graphic_1')->textInput() ?>

    <?= $form->field($model, 'signature_graphic_2')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
