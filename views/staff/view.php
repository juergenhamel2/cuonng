<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="staff-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

 <?=  Html::button('Fee', array('onclick' => 'js:document.location.href="index.php?r=stafffee/index&staff_id='.$model->id.'"')) ?>


        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            'update_user_id',
            'client',
            'sep_info_1',
            'sep_info_2',
            'sep_info_3',
            'versions_number',
            'versions_uuid',
            'uuid',
            'address',
            'lastname',
            'lastname2',
            'firstname',
            'street',
            'country',
            'state',
            'zip',
            'city',
            'phone',
            'fax',
            'email:email',
            'zip_for_postbox',
            'postbox',
            'suburb',
            'state_full',
            'letter_address',
            'phone_handy',
            'email_noification:email',
            'newsletter',
            'homepage_url:url',
            'status_info',
            'sip',
            'skype',
            'letter_address2',
            'letter_address3',
            'letter_address4',
            'letter_address5',
            'titular',
            'phone1',
            'phone2',
            'homepage',
            'faxprivat',
            'birthday',
            'staff_number',
            'cuon_username',
            'letter_phrase_1',
            'letter_phrase_2',
            'my_sign_1',
            'signature_1:ntext',
            'signature_2:ntext',
            'signature_graphic_1',
            'signature_graphic_2',
        ],
    ]) ?>

</div>
