<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StaffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Staff');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Staff'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            //'address',
            //'lastname',
            //'lastname2',
            //'firstname',
            //'street',
            //'country',
            //'state',
            //'zip',
            //'city',
            //'phone',
            //'fax',
            //'email:email',
            //'zip_for_postbox',
            //'postbox',
            //'suburb',
            //'state_full',
            //'letter_address',
            //'phone_handy',
            //'email_noification:email',
            //'newsletter',
            //'homepage_url:url',
            //'status_info',
            //'sip',
            //'skype',
            //'letter_address2',
            //'letter_address3',
            //'letter_address4',
            //'letter_address5',
            //'titular',
            //'phone1',
            //'phone2',
            //'homepage',
            //'faxprivat',
            //'birthday',
            //'staff_number',
            //'cuon_username',
            //'letter_phrase_1',
            //'letter_phrase_2',
            //'my_sign_1',
            //'signature_1:ntext',
            //'signature_2:ntext',
            //'signature_graphic_1',
            //'signature_graphic_2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
