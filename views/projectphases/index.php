<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectPhasesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$request = Yii::$app->request;
$this->title = Yii::t('app', 'Project Phases');
$this->params['breadcrumbs'][] = $this->title;
$project_id = $request->get('project_id');;

?>
<div class="project-phases-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Project Phases'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            //'project_id',
            //'name',
            //'designation',
            //'time_in_days:datetime',
            //'starts_at',
            //'ends_at',
            //'phase_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
