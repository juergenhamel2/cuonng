<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectPhases */

$this->title = Yii::t('app', 'Create Project Phases');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Phases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-phases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
