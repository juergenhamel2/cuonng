<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=  Html::button('Phases', array('onclick' => 'js:document.location.href="index.php?r=projectphases/index&project_id='.$model->id.'"')) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            'update_user_id',
            'client',
            'sep_info_1',
            'sep_info_2',
            'sep_info_3',
            'versions_number',
            'versions_uuid',
            'uuid',
            'name',
            'designation',
            'customer_id',
            'project_time_in_days:datetime',
            'project_starts_at',
            'project_ends_at',
            'modul_project_number',
            'modul_number',
            'project_status',
            'internal_project_number',
            'partner_id',
        ],
    ]) ?>

</div>
