<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php //$form->field($model, 'id') ?>

    <?php // $form->field($model, 'user_id') ?>

    <?php // $form->field($model, 'status') ?>

    <?php //$form->field($model, 'insert_time') ?>

    <?php // $form->field($model, 'update_time') ?>

    <?php // echo $form->field($model, 'update_user_id') ?>

    <?php // echo $form->field($model, 'client') ?>

    <?php // echo $form->field($model, 'sep_info_1') ?>

    <?php // echo $form->field($model, 'sep_info_2') ?>

    <?php // echo $form->field($model, 'sep_info_3') ?>

    <?php // echo $form->field($model, 'versions_number') ?>

    <?php // echo $form->field($model, 'versions_uuid') ?>

    <?php // echo $form->field($model, 'uuid') ?>

    <?php= echo $form->field($model, 'name') ?>

    <?php= echo $form->field($model, 'designation') ?>

    <?php // echo $form->field($model, 'customer_id') ?>

    <?php // echo $form->field($model, 'project_time_in_days') ?>

    <?php // echo $form->field($model, 'project_starts_at') ?>

    <?php // echo $form->field($model, 'project_ends_at') ?>

    <?php // echo $form->field($model, 'modul_project_number') ?>

    <?php // echo $form->field($model, 'modul_number') ?>

    <?php // echo $form->field($model, 'project_status') ?>

    <?php // echo $form->field($model, 'internal_project_number') ?>

    <?php // echo $form->field($model, 'partner_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
