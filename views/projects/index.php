<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Projects'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            // 'user_id',
            // 'status',
            // 'insert_time',
            // 'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            'name',
            'designation',
            //'customer_id',
            //'project_time_in_days:datetime',
            //'project_starts_at',
            //'project_ends_at',
            //'modul_project_number',
            //'modul_number',
            //'project_status',
            //'internal_project_number',
            //'partner_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
