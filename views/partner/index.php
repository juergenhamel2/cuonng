<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Partners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Partner'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            //'address',
            //'lastname',
            //'lastname2',
            //'firstname',
            //'street',
            //'country',
            //'state',
            //'zip',
            //'city',
            //'phone',
            //'fax',
            //'email:email',
            //'zip_for_postbox',
            //'postbox',
            //'suburb',
            //'state_full',
            //'letter_address',
            //'phone_handy',
            //'email_noification:email',
            //'newsletter',
            //'homepage_url:url',
            //'status_info',
            //'sip',
            //'skype',
            //'letter_address2',
            //'letter_address3',
            //'letter_address4',
            //'letter_address5',
            //'addressid',
            //'titular',
            //'phone1',
            //'phone2',
            //'homepage',
            //'faxprivat',
            //'birthday',
            //'business_function',
            //'job',
            //'department',
            //'additional_emails:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
