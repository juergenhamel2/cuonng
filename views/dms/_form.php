<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dms-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insert_time')->textInput() ?>

    <?= $form->field($model, 'update_time')->textInput() ?>

    <?= $form->field($model, 'update_user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client')->textInput() ?>

    <?= $form->field($model, 'sep_info_1')->textInput() ?>

    <?= $form->field($model, 'sep_info_2')->textInput() ?>

    <?= $form->field($model, 'sep_info_3')->textInput() ?>

    <?= $form->field($model, 'versions_number')->textInput() ?>

    <?= $form->field($model, 'versions_uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size_x')->textInput() ?>

    <?= $form->field($model, 'size_y')->textInput() ?>

    <?= $form->field($model, 'uploadpath')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'document_image')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sub1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'search1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'search2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'search3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'search4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_format')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insert_from')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insert_at_date')->textInput() ?>

    <?= $form->field($model, 'insert_from_module')->textInput() ?>

    <?= $form->field($model, 'file_suffix')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_date')->textInput() ?>

    <?= $form->field($model, 'document_rights_activated')->checkbox() ?>

    <?= $form->field($model, 'document_rights_user_read')->checkbox() ?>

    <?= $form->field($model, 'document_rights_user_write')->checkbox() ?>

    <?= $form->field($model, 'document_rights_user_execute')->checkbox() ?>

    <?= $form->field($model, 'document_rights_group_read')->checkbox() ?>

    <?= $form->field($model, 'document_rights_group_write')->checkbox() ?>

    <?= $form->field($model, 'document_rights_group_execute')->checkbox() ?>

    <?= $form->field($model, 'document_rights_all_read')->checkbox() ?>

    <?= $form->field($model, 'document_rights_all_write')->checkbox() ?>

    <?= $form->field($model, 'document_rights_all_execute')->checkbox() ?>

    <?= $form->field($model, 'document_rights_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_rights_groups')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dms_extract')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'paired_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
