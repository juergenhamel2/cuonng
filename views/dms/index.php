<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'dms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dms-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Dms'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            // 'user_id',
            // 'status',
            // 'insert_time',
            // 'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            'title',
            'category',
            //'size_x',
            //'size_y',
            //'document_image:ntext',
            'sub1',
            'sub2',
            'sub3',
            //'sub4',
            //'sub5',
            //'search1',
            //'search2',
            //'search3',
            //'search4',
            //'file_format',
            //'insert_from',
            //'insert_at_date',
            //'insert_from_module',
            //'file_suffix',
            //'document_date',
            //'document_rights_activated:boolean',
            //'document_rights_user_read:boolean',
            //'document_rights_user_write:boolean',
            //'document_rights_user_execute:boolean',
            //'document_rights_group_read:boolean',
            //'document_rights_group_write:boolean',
            //'document_rights_group_execute:boolean',
            //'document_rights_all_read:boolean',
            //'document_rights_all_write:boolean',
            //'document_rights_all_execute:boolean',
            //'document_rights_user',
            //'document_rights_groups',
            //'dms_extract:ntext',
            //'paired_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
