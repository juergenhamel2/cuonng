<?php
   use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\helpers\Url ;
  use yii\web\UrlManager ;
use dms\upload;

?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])?>
<?= $form->field($model, 'image')->fileInput() ?>
<!-- <?= Html::a(Yii::t('app', 'Submit'), ['upload'], ['class' => 'btn btn-primary']) ?> -->
 <?= Html::submitButton('Submit',  ['upload'], ['class' => 'btn btn-primary']) ?>
    

<?php ActiveForm::end() ?>
