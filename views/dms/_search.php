<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DmsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dms-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?php // echo  $form->field($model, 'user_id') ?>

    <?php // echo  $form->field($model, 'status') ?>

    <?php // echo  $form->field($model, 'insert_time') ?>

    <?php // echo  $form->field($model, 'update_time') ?>

    <?php // echo $form->field($model, 'update_user_id') ?>

    <?php // echo $form->field($model, 'client') ?>

    <?php // echo $form->field($model, 'sep_info_1') ?>

    <?php // echo $form->field($model, 'sep_info_2') ?>

    <?php // echo $form->field($model, 'sep_info_3') ?>

    <?php // echo $form->field($model, 'versions_number') ?>

    <?php // echo $form->field($model, 'versions_uuid') ?>

    <?php // echo $form->field($model, 'uuid') ?>

    <?= $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'size_x') ?>

    <?php // echo $form->field($model, 'size_y') ?>

    <?php // echo $form->field($model, 'document_image') ?>

    <?= $form->field($model, 'sub1') ?>

    <?php // echo $form->field($model, 'sub2') ?>

    <?php // echo $form->field($model, 'sub3') ?>

    <?php // echo $form->field($model, 'sub4') ?>

    <?php // echo $form->field($model, 'sub5') ?>

    <?= $form->field($model, 'search1') ?>

    <?php // echo $form->field($model, 'search2') ?>

    <?php // echo $form->field($model, 'search3') ?>

    <?php // echo $form->field($model, 'search4') ?>

    <?php // echo $form->field($model, 'file_format') ?>

    <?php // echo $form->field($model, 'insert_from') ?>

    <?php // echo $form->field($model, 'insert_at_date') ?>

    <?php // echo $form->field($model, 'insert_from_module') ?>

    <?php // echo $form->field($model, 'file_suffix') ?>

    <?php // echo $form->field($model, 'document_date') ?>

    <?php // echo $form->field($model, 'document_rights_activated')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_user_read')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_user_write')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_user_execute')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_group_read')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_group_write')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_group_execute')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_all_read')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_all_write')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_all_execute')->checkbox() ?>

    <?php // echo $form->field($model, 'document_rights_user') ?>

    <?php // echo $form->field($model, 'document_rights_groups') ?>

    <?php // echo $form->field($model, 'dms_extract') ?>

    <?php // echo $form->field($model, 'paired_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
