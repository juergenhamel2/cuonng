<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dms */

$this->title = Yii::t('app', 'Update Dms: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dms-update">

    <h1><?= Html::encode($this->title) ?></h1>
  <h2><?= Html::encode("test for update") ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
