<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url ;
  use yii\web\UrlManager ;


/* @var $this yii\web\View */
/* @var $model app\models\Dms */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="dms-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Upload'), ['uploadfile', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>



  </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'user_id',
            // 'status',
            // 'insert_time',
            // 'update_time',
            // 'update_user_id',
            // 'client',
            // 'sep_info_1',
            // 'sep_info_2',
            // 'sep_info_3',
            // 'versions_number',
            // 'versions_uuid',
            // 'uuid',
            'title',
            'category',
            'size_x',
            'size_y',
            'uploadpath',
            //'document_image:ntext',
            'sub1',
            'sub2',
            'sub3',
            'sub4',
            'sub5',
            'search1',
            'search2',
            'search3',
            'search4',
            'file_format',
            'insert_from',
            'insert_at_date',
            'insert_from_module',
            'file_suffix',
            'document_date',
            'document_rights_activated:boolean',
            'document_rights_user_read:boolean',
            'document_rights_user_write:boolean',
            'document_rights_user_execute:boolean',
            'document_rights_group_read:boolean',
            'document_rights_group_write:boolean',
            'document_rights_group_execute:boolean',
            'document_rights_all_read:boolean',
            'document_rights_all_write:boolean',
            'document_rights_all_execute:boolean',
            'document_rights_user',
            'document_rights_groups',
            'dms_extract:ntext',
            'paired_id',


        ],
    ]) ?>

</div>
