<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dms */

$this->title = Yii::t('app', 'Create Dms');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dms-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
