<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StaffVacation */

$this->title = Yii::t('app', 'Create Staff Vacation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff Vacations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-vacation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
