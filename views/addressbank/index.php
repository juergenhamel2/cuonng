<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddressBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$request = Yii::$app->request;

$this->title = Yii::t('app', 'Address Banks');
$this->params['breadcrumbs'][] = $this->title;
$address_id = $request->get('address_id');;

?>
<div class="address-bank-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h1><?= Html::encode($address_id) ?></h1> 
    <p>
        <?= Html::a(Yii::t('app', 'Create Address Bank'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'status',
            'insert_time',
            'update_time',
            //'update_user_id',
            //'client',
            //'sep_info_1',
            //'sep_info_2',
            //'sep_info_3',
            //'versions_number',
            //'versions_uuid',
            //'uuid',
            'address_id',
            'bank_id',
            //'depositor',
            //'account_number',
            //'iban',
            //'bank_ranking',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
