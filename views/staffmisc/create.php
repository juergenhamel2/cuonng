<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StaffMisc */

$this->title = Yii::t('app', 'Create Staff Misc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff Miscs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-misc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
