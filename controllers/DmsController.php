<?php

namespace app\controllers;

use Yii;
use app\models\Dms;
use app\models\DmsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadImageForm;
/**
 * DmsController implements the CRUD actions for Dms model.
 */
class DmsController extends Controller
{


  public function actionUpload() {

 Yii::trace('rzyy - DmsController actionUpload upload file' ,__METHOD__);

   return $this->render('upload', ['model' => $model]);
 }


  public function actionUploadfile() {
 $model = new UploadImageForm();
 Yii::trace('rzyy -DmsController actionUploadfile upload file' ,__METHOD__);
   if (Yii::$app->request->isPost) {
Yii::trace('rzyy -DmsController actionUploadfile isPost is true' ,__METHOD__);
   $test = Yii::$app->request;
   Yii::trace('rzyy -DmsController actionUploadfile image: ' .   json_encode($test->resolve()),__METHOD__);
   // $model->image->saveAs('../uploads/' . $model->image->baseName . '.' . $modl->image->extension);



   }
   return $this->render('upload', ['model' => $model]);
 }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dms models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DmsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dms model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dms();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Dms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Deletes an existing Dms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dms::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
