<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dms;

/**
 * DmsSearch represents the model behind the search form of `app\models\Dms`.
 */
class DmsSearch extends Dms
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'size_x', 'size_y', 'insert_from_module', 'paired_id'], 'integer'],
            [['user_id', 'status', 'insert_time', 'update_time', 'update_user_id', 'versions_uuid', 'uuid', 'title', 'category', 'document_image', 'sub1', 'sub2', 'sub3', 'sub4', 'sub5', 'search1', 'search2', 'search3', 'search4', 'file_format', 'insert_from', 'insert_at_date', 'file_suffix', 'document_date', 'document_rights_user', 'document_rights_groups', 'dms_extract'], 'safe'],
            [['document_rights_activated', 'document_rights_user_read', 'document_rights_user_write', 'document_rights_user_execute', 'document_rights_group_read', 'document_rights_group_write', 'document_rights_group_execute', 'document_rights_all_read', 'document_rights_all_write', 'document_rights_all_execute'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_time' => $this->insert_time,
            'update_time' => $this->update_time,
            'client' => $this->client,
            'sep_info_1' => $this->sep_info_1,
            'sep_info_2' => $this->sep_info_2,
            'sep_info_3' => $this->sep_info_3,
            'versions_number' => $this->versions_number,
            'size_x' => $this->size_x,
            'size_y' => $this->size_y,
            'insert_at_date' => $this->insert_at_date,
            'insert_from_module' => $this->insert_from_module,
            'document_date' => $this->document_date,
            'document_rights_activated' => $this->document_rights_activated,
            'document_rights_user_read' => $this->document_rights_user_read,
            'document_rights_user_write' => $this->document_rights_user_write,
            'document_rights_user_execute' => $this->document_rights_user_execute,
            'document_rights_group_read' => $this->document_rights_group_read,
            'document_rights_group_write' => $this->document_rights_group_write,
            'document_rights_group_execute' => $this->document_rights_group_execute,
            'document_rights_all_read' => $this->document_rights_all_read,
            'document_rights_all_write' => $this->document_rights_all_write,
            'document_rights_all_execute' => $this->document_rights_all_execute,
            'paired_id' => $this->paired_id,
        ]);

        $query->andFilterWhere(['ilike', 'user_id', $this->user_id])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'update_user_id', $this->update_user_id])
            ->andFilterWhere(['ilike', 'versions_uuid', $this->versions_uuid])
            ->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'category', $this->category])
            ->andFilterWhere(['ilike', 'document_image', $this->document_image])
            ->andFilterWhere(['ilike', 'sub1', $this->sub1])
            ->andFilterWhere(['ilike', 'sub2', $this->sub2])
            ->andFilterWhere(['ilike', 'sub3', $this->sub3])
            ->andFilterWhere(['ilike', 'sub4', $this->sub4])
            ->andFilterWhere(['ilike', 'sub5', $this->sub5])
            ->andFilterWhere(['ilike', 'search1', $this->search1])
            ->andFilterWhere(['ilike', 'search2', $this->search2])
            ->andFilterWhere(['ilike', 'search3', $this->search3])
            ->andFilterWhere(['ilike', 'search4', $this->search4])
            ->andFilterWhere(['ilike', 'file_format', $this->file_format])
            ->andFilterWhere(['ilike', 'insert_from', $this->insert_from])
            ->andFilterWhere(['ilike', 'file_suffix', $this->file_suffix])
            ->andFilterWhere(['ilike', 'document_rights_user', $this->document_rights_user])
            ->andFilterWhere(['ilike', 'document_rights_groups', $this->document_rights_groups])
            ->andFilterWhere(['ilike', 'dms_extract', $this->dms_extract]);

        return $dataProvider;
    }
}
