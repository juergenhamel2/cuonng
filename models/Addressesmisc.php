<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "addresses_misc".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property int|null $address_id
 * @property string|null $line
 * @property string|null $fashion
 * @property string|null $taxnumber
 * @property bool|null $to_calc_tax
 * @property string|null $price_group
 * @property string|null $address_number
 * @property bool|null $is_webshop
 * @property bool|null $update_from_webshop
 * @property int|null $top_id
 * @property string|null $date_of_launch
 * @property int|null $legal_form
 * @property int|null $turnover
 * @property int|null $count_of_employees
 * @property int|null $trade
 * @property int|null $cb_fashion
 * @property string|null $additional_emails
 * @property bool|null $pricegroup1
 * @property bool|null $pricegroup2
 * @property bool|null $pricegroup3
 * @property bool|null $pricegroup4
 * @property bool|null $pricegroup_none
 * @property bool|null $allow_direct_debit
 */
class Addressesmisc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses_misc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time', 'date_of_launch'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id', 'top_id', 'legal_form', 'turnover', 'count_of_employees', 'trade', 'cb_fashion'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id', 'top_id', 'legal_form', 'turnover', 'count_of_employees', 'trade', 'cb_fashion'], 'integer'],
            [['to_calc_tax', 'is_webshop', 'update_from_webshop', 'pricegroup1', 'pricegroup2', 'pricegroup3', 'pricegroup4', 'pricegroup_none', 'allow_direct_debit'], 'boolean'],
            [['additional_emails'], 'string'],
            [['user_id', 'status', 'update_user_id'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
            [['line', 'fashion', 'taxnumber', 'price_group'], 'string', 'max' => 35],
            [['address_number'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'address_id' => Yii::t('app', 'Address ID'),
            'line' => Yii::t('app', 'Line'),
            'fashion' => Yii::t('app', 'Fashion'),
            'taxnumber' => Yii::t('app', 'Taxnumber'),
            'to_calc_tax' => Yii::t('app', 'To Calc Tax'),
            'price_group' => Yii::t('app', 'Price Group'),
            'address_number' => Yii::t('app', 'Address Number'),
            'is_webshop' => Yii::t('app', 'Is Webshop'),
            'update_from_webshop' => Yii::t('app', 'Update From Webshop'),
            'top_id' => Yii::t('app', 'Top ID'),
            'date_of_launch' => Yii::t('app', 'Date Of Launch'),
            'legal_form' => Yii::t('app', 'Legal Form'),
            'turnover' => Yii::t('app', 'Turnover'),
            'count_of_employees' => Yii::t('app', 'Count Of Employees'),
            'trade' => Yii::t('app', 'Trade'),
            'cb_fashion' => Yii::t('app', 'Cb Fashion'),
            'additional_emails' => Yii::t('app', 'Additional Emails'),
            'pricegroup1' => Yii::t('app', 'Pricegroup1'),
            'pricegroup2' => Yii::t('app', 'Pricegroup2'),
            'pricegroup3' => Yii::t('app', 'Pricegroup3'),
            'pricegroup4' => Yii::t('app', 'Pricegroup4'),
            'pricegroup_none' => Yii::t('app', 'Pricegroup None'),
            'allow_direct_debit' => Yii::t('app', 'Allow Direct Debit'),
        ];
    }
}
