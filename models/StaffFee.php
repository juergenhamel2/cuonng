<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staff_fee".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property int|null $staff_id
 * @property string|null $working_time_from
 * @property string|null $working_time_to
 * @property float|null $fee_per_hour
 * @property float|null $fee_per_hour_calc
 * @property float|null $fee_per_hour_invoice
 * @property float|null $fee_per_overtime
 * @property float|null $fee_per_overtime_calc
 * @property float|null $fee_per_overtime_invoice
 *
 * @property Staff $staff
 */
class StaffFee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff_fee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'staff_id'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'staff_id'], 'integer'],
            [['fee_per_hour', 'fee_per_hour_calc', 'fee_per_hour_invoice', 'fee_per_overtime', 'fee_per_overtime_calc', 'fee_per_overtime_invoice'], 'number'],
            [['user_id', 'status', 'update_user_id'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
            [['working_time_from', 'working_time_to'], 'string', 'max' => 5],
            [['staff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['staff_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'staff_id' => Yii::t('app', 'Staff ID'),
            'working_time_from' => Yii::t('app', 'Working Time From'),
            'working_time_to' => Yii::t('app', 'Working Time To'),
            'fee_per_hour' => Yii::t('app', 'Fee Per Hour'),
            'fee_per_hour_calc' => Yii::t('app', 'Fee Per Hour Calc'),
            'fee_per_hour_invoice' => Yii::t('app', 'Fee Per Hour Invoice'),
            'fee_per_overtime' => Yii::t('app', 'Fee Per Overtime'),
            'fee_per_overtime_calc' => Yii::t('app', 'Fee Per Overtime Calc'),
            'fee_per_overtime_invoice' => Yii::t('app', 'Fee Per Overtime Invoice'),
        ];
    }

    /**
     * Gets query for [[Staff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(Staff::className(), ['id' => 'staff_id']);
    }
}
