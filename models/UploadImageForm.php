<?php
   namespace app\models;
   use yii\base\Model;
   use Yii;


   class UploadImageForm extends Model {
      public $image;
      public function rules() {
         return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg, png'],
         ];
      }
      public function upload() {
        Yii::trace('rzyy - upload file function' ,__METHOD__);

           Yii::trace('rzyy - upload file function name: ' . $this->image->baseName ,__METHOD__);
           Yii::trace('rzyy - upload file function extension: ' .  $this->image->extension ,__METHOD__);

      }
   }
?>
