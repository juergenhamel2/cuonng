<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Addressesmisc;

/**
 * AddressesmiscSearch represents the model behind the search form of `app\models\Addressesmisc`.
 */
class AddressesmiscSearch extends Addressesmisc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id', 'top_id', 'legal_form', 'turnover', 'count_of_employees', 'trade', 'cb_fashion'], 'integer'],
            [['user_id', 'status', 'insert_time', 'update_time', 'update_user_id', 'versions_uuid', 'uuid', 'line', 'fashion', 'taxnumber', 'price_group', 'address_number', 'date_of_launch', 'additional_emails'], 'safe'],
            [['to_calc_tax', 'is_webshop', 'update_from_webshop', 'pricegroup1', 'pricegroup2', 'pricegroup3', 'pricegroup4', 'pricegroup_none', 'allow_direct_debit'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
         $address_id =  \Yii::$app->getRequest()->getQueryParam('address_id');
        $query = Addressesmisc::find();

        // add conditions that should always apply here
        $query->andFilterWhere(['address_id' => $address_id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_time' => $this->insert_time,
            'update_time' => $this->update_time,
            'client' => $this->client,
            'sep_info_1' => $this->sep_info_1,
            'sep_info_2' => $this->sep_info_2,
            'sep_info_3' => $this->sep_info_3,
            'versions_number' => $this->versions_number,
            'address_id' => $this->address_id,
            'to_calc_tax' => $this->to_calc_tax,
            'is_webshop' => $this->is_webshop,
            'update_from_webshop' => $this->update_from_webshop,
            'top_id' => $this->top_id,
            'date_of_launch' => $this->date_of_launch,
            'legal_form' => $this->legal_form,
            'turnover' => $this->turnover,
            'count_of_employees' => $this->count_of_employees,
            'trade' => $this->trade,
            'cb_fashion' => $this->cb_fashion,
            'pricegroup1' => $this->pricegroup1,
            'pricegroup2' => $this->pricegroup2,
            'pricegroup3' => $this->pricegroup3,
            'pricegroup4' => $this->pricegroup4,
            'pricegroup_none' => $this->pricegroup_none,
            'allow_direct_debit' => $this->allow_direct_debit,
        ]);

        $query->andFilterWhere(['ilike', 'user_id', $this->user_id])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'update_user_id', $this->update_user_id])
            ->andFilterWhere(['ilike', 'versions_uuid', $this->versions_uuid])
            ->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'line', $this->line])
            ->andFilterWhere(['ilike', 'fashion', $this->fashion])
            ->andFilterWhere(['ilike', 'taxnumber', $this->taxnumber])
            ->andFilterWhere(['ilike', 'price_group', $this->price_group])
            ->andFilterWhere(['ilike', 'address_number', $this->address_number])
            ->andFilterWhere(['ilike', 'additional_emails', $this->additional_emails]);

        return $dataProvider;
    }
}
