<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Partner;

/**
 * PartnerSearch represents the model behind the search form of `app\models\Partner`.
 */
class PartnerSearch extends Partner
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'addressid'], 'integer'],
            [['user_id', 'status', 'insert_time', 'update_time', 'update_user_id', 'versions_uuid', 'uuid', 'address', 'lastname', 'lastname2', 'firstname', 'street', 'country', 'state', 'zip', 'city', 'phone', 'fax', 'email', 'zip_for_postbox', 'postbox', 'suburb', 'state_full', 'letter_address', 'phone_handy', 'email_noification', 'newsletter', 'homepage_url', 'status_info', 'sip', 'skype', 'letter_address2', 'letter_address3', 'letter_address4', 'letter_address5', 'titular', 'phone1', 'phone2', 'homepage', 'faxprivat', 'birthday', 'business_function', 'job', 'department', 'additional_emails'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
         $address_id =  \Yii::$app->getRequest()->getQueryParam('address_id');
       
        $query = Partner::find();

        // add conditions that should always apply here
        $query->andFilterWhere(['addressid' => $address_id]);

        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_time' => $this->insert_time,
            'update_time' => $this->update_time,
            'client' => $this->client,
            'sep_info_1' => $this->sep_info_1,
            'sep_info_2' => $this->sep_info_2,
            'sep_info_3' => $this->sep_info_3,
            'versions_number' => $this->versions_number,
            'addressid' => $this->addressid,
            'birthday' => $this->birthday,
        ]);

        $query->andFilterWhere(['ilike', 'user_id', $this->user_id])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'update_user_id', $this->update_user_id])
            ->andFilterWhere(['ilike', 'versions_uuid', $this->versions_uuid])
            ->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'lastname', $this->lastname])
            ->andFilterWhere(['ilike', 'lastname2', $this->lastname2])
            ->andFilterWhere(['ilike', 'firstname', $this->firstname])
            ->andFilterWhere(['ilike', 'street', $this->street])
            ->andFilterWhere(['ilike', 'country', $this->country])
            ->andFilterWhere(['ilike', 'state', $this->state])
            ->andFilterWhere(['ilike', 'zip', $this->zip])
            ->andFilterWhere(['ilike', 'city', $this->city])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'fax', $this->fax])
            ->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'zip_for_postbox', $this->zip_for_postbox])
            ->andFilterWhere(['ilike', 'postbox', $this->postbox])
            ->andFilterWhere(['ilike', 'suburb', $this->suburb])
            ->andFilterWhere(['ilike', 'state_full', $this->state_full])
            ->andFilterWhere(['ilike', 'letter_address', $this->letter_address])
            ->andFilterWhere(['ilike', 'phone_handy', $this->phone_handy])
            ->andFilterWhere(['ilike', 'email_noification', $this->email_noification])
            ->andFilterWhere(['ilike', 'newsletter', $this->newsletter])
            ->andFilterWhere(['ilike', 'homepage_url', $this->homepage_url])
            ->andFilterWhere(['ilike', 'status_info', $this->status_info])
            ->andFilterWhere(['ilike', 'sip', $this->sip])
            ->andFilterWhere(['ilike', 'skype', $this->skype])
            ->andFilterWhere(['ilike', 'letter_address2', $this->letter_address2])
            ->andFilterWhere(['ilike', 'letter_address3', $this->letter_address3])
            ->andFilterWhere(['ilike', 'letter_address4', $this->letter_address4])
            ->andFilterWhere(['ilike', 'letter_address5', $this->letter_address5])
            ->andFilterWhere(['ilike', 'titular', $this->titular])
            ->andFilterWhere(['ilike', 'phone1', $this->phone1])
            ->andFilterWhere(['ilike', 'phone2', $this->phone2])
            ->andFilterWhere(['ilike', 'homepage', $this->homepage])
            ->andFilterWhere(['ilike', 'faxprivat', $this->faxprivat])
            ->andFilterWhere(['ilike', 'business_function', $this->business_function])
            ->andFilterWhere(['ilike', 'job', $this->job])
            ->andFilterWhere(['ilike', 'department', $this->department])
            ->andFilterWhere(['ilike', 'additional_emails', $this->additional_emails]);

        return $dataProvider;
    }
}
