<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property string|null $address
 * @property string|null $lastname
 * @property string|null $lastname2
 * @property string|null $firstname
 * @property string|null $street
 * @property string|null $country
 * @property string|null $state
 * @property string|null $zip
 * @property string|null $city
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $zip_for_postbox
 * @property string|null $postbox
 * @property string|null $suburb
 * @property string|null $state_full
 * @property string|null $letter_address
 * @property string|null $phone_handy
 * @property string|null $email_noification
 * @property string|null $newsletter
 * @property string|null $homepage_url
 * @property string|null $status_info
 * @property string|null $sip
 * @property string|null $skype
 * @property string|null $letter_address2
 * @property string|null $letter_address3
 * @property string|null $letter_address4
 * @property string|null $letter_address5
 * @property int|null $webshop_address_id
 * @property int|null $webshop_zone_id
 * @property string|null $webshop_gender
 * @property int|null $webshop_customers_id
 * @property int|null $caller_id
 * @property int|null $rep_id
 * @property int|null $salesman_id
 *
 * @property Hibernation[] $hibernations
 * @property Partner[] $partners
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'webshop_address_id', 'webshop_zone_id', 'webshop_customers_id', 'caller_id', 'rep_id', 'salesman_id'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'webshop_address_id', 'webshop_zone_id', 'webshop_customers_id', 'caller_id', 'rep_id', 'salesman_id'], 'integer'],
            [['user_id', 'status', 'update_user_id', 'phone', 'fax'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
            [['address'], 'string', 'max' => 30],
            [['lastname', 'lastname2', 'firstname', 'street', 'city', 'suburb', 'state_full'], 'string', 'max' => 50],
            [['country', 'state'], 'string', 'max' => 6],
            [['zip', 'zip_for_postbox'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 100],
            [['postbox'], 'string', 'max' => 20],
            [['letter_address'], 'string', 'max' => 120],
            [['phone_handy', 'email_noification'], 'string', 'max' => 35],
            [['newsletter'], 'string', 'max' => 17000],
            [['homepage_url', 'status_info'], 'string', 'max' => 250],
            [['sip', 'skype', 'letter_address2', 'letter_address3', 'letter_address4', 'letter_address5'], 'string', 'max' => 225],
            [['webshop_gender'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'address' => Yii::t('app', 'Address'),
            'lastname' => Yii::t('app', 'Lastname'),
            'lastname2' => Yii::t('app', 'Lastname2'),
            'firstname' => Yii::t('app', 'Firstname'),
            'street' => Yii::t('app', 'Street'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'zip' => Yii::t('app', 'Zip'),
            'city' => Yii::t('app', 'City'),
            'phone' => Yii::t('app', 'Phone'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'zip_for_postbox' => Yii::t('app', 'Zip For Postbox'),
            'postbox' => Yii::t('app', 'Postbox'),
            'suburb' => Yii::t('app', 'Suburb'),
            'state_full' => Yii::t('app', 'State Full'),
            'letter_address' => Yii::t('app', 'Letter Address'),
            'phone_handy' => Yii::t('app', 'Phone Handy'),
            'email_noification' => Yii::t('app', 'Email Noification'),
            'newsletter' => Yii::t('app', 'Newsletter'),
            'homepage_url' => Yii::t('app', 'Homepage Url'),
            'status_info' => Yii::t('app', 'Status Info'),
            'sip' => Yii::t('app', 'Sip'),
            'skype' => Yii::t('app', 'Skype'),
            'letter_address2' => Yii::t('app', 'Letter Address2'),
            'letter_address3' => Yii::t('app', 'Letter Address3'),
            'letter_address4' => Yii::t('app', 'Letter Address4'),
            'letter_address5' => Yii::t('app', 'Letter Address5'),
            'webshop_address_id' => Yii::t('app', 'Webshop Address ID'),
            'webshop_zone_id' => Yii::t('app', 'Webshop Zone ID'),
            'webshop_gender' => Yii::t('app', 'Webshop Gender'),
            'webshop_customers_id' => Yii::t('app', 'Webshop Customers ID'),
            'caller_id' => Yii::t('app', 'Caller ID'),
            'rep_id' => Yii::t('app', 'Rep ID'),
            'salesman_id' => Yii::t('app', 'Salesman ID'),
        ];
    }

    /**
     * Gets query for [[Hibernations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHibernations()
    {
        return $this->hasMany(Hibernation::className(), ['addressnumber' => 'id']);
    }

    /**
     * Gets query for [[Partners]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasMany(Partner::className(), ['addressid' => 'id']);
    }
}
