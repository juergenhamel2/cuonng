<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address_bank".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property int|null $address_id
 * @property int|null $bank_id
 * @property string|null $depositor
 * @property string|null $account_number
 * @property string|null $iban
 * @property int|null $bank_ranking
 *
 * @property Address $address
 * @property Bank $bank
 */
class AddressBank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address_bank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id', 'bank_id', 'bank_ranking'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id', 'bank_id', 'bank_ranking'], 'integer'],
            [['user_id', 'status', 'update_user_id'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
            [['depositor', 'iban'], 'string', 'max' => 80],
            [['account_number'], 'string', 'max' => 40],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::className(), 'targetAttribute' => ['bank_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'address_id' => Yii::t('app', 'Address ID'),
            'bank_id' => Yii::t('app', 'Bank ID'),
            'depositor' => Yii::t('app', 'Depositor'),
            'account_number' => Yii::t('app', 'Account Number'),
            'iban' => Yii::t('app', 'Iban'),
            'bank_ranking' => Yii::t('app', 'Bank Ranking'),
        ];
    }

    /**
     * Gets query for [[Address]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * Gets query for [[Bank]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['id' => 'bank_id']);
    }
}
