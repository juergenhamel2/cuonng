<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staff".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property string|null $address
 * @property string|null $lastname
 * @property string|null $lastname2
 * @property string|null $firstname
 * @property string|null $street
 * @property string|null $country
 * @property string|null $state
 * @property string|null $zip
 * @property string|null $city
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $zip_for_postbox
 * @property string|null $postbox
 * @property string|null $suburb
 * @property string|null $state_full
 * @property string|null $letter_address
 * @property string|null $phone_handy
 * @property string|null $email_noification
 * @property string|null $newsletter
 * @property string|null $homepage_url
 * @property string|null $status_info
 * @property string|null $sip
 * @property string|null $skype
 * @property string|null $letter_address2
 * @property string|null $letter_address3
 * @property string|null $letter_address4
 * @property string|null $letter_address5
 * @property string|null $titular
 * @property string|null $phone1
 * @property string|null $phone2
 * @property string|null $homepage
 * @property string|null $faxprivat
 * @property string|null $birthday
 * @property string|null $staff_number
 * @property string|null $cuon_username
 * @property string|null $letter_phrase_1
 * @property string|null $letter_phrase_2
 * @property string|null $my_sign_1
 * @property string|null $signature_1
 * @property string|null $signature_2
 * @property int|null $signature_graphic_1
 * @property int|null $signature_graphic_2
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time', 'birthday'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'signature_graphic_1', 'signature_graphic_2'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'signature_graphic_1', 'signature_graphic_2'], 'integer'],
            [['signature_1', 'signature_2'], 'string'],
            [['user_id', 'status', 'update_user_id', 'phone', 'fax', 'phone1', 'phone2', 'faxprivat'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
            [['address', 'titular'], 'string', 'max' => 30],
            [['lastname', 'lastname2', 'firstname', 'street', 'city', 'suburb', 'state_full'], 'string', 'max' => 50],
            [['country', 'state'], 'string', 'max' => 6],
            [['zip', 'zip_for_postbox'], 'string', 'max' => 10],
            [['email', 'homepage'], 'string', 'max' => 100],
            [['postbox'], 'string', 'max' => 20],
            [['letter_address'], 'string', 'max' => 120],
            [['phone_handy', 'email_noification'], 'string', 'max' => 35],
            [['newsletter'], 'string', 'max' => 17000],
            [['homepage_url', 'status_info'], 'string', 'max' => 250],
            [['sip', 'skype', 'letter_address2', 'letter_address3', 'letter_address4', 'letter_address5'], 'string', 'max' => 225],
            [['staff_number', 'cuon_username'], 'string', 'max' => 55],
            [['letter_phrase_1', 'letter_phrase_2', 'my_sign_1'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'address' => Yii::t('app', 'Address'),
            'lastname' => Yii::t('app', 'Lastname'),
            'lastname2' => Yii::t('app', 'Lastname2'),
            'firstname' => Yii::t('app', 'Firstname'),
            'street' => Yii::t('app', 'Street'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'zip' => Yii::t('app', 'Zip'),
            'city' => Yii::t('app', 'City'),
            'phone' => Yii::t('app', 'Phone'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'zip_for_postbox' => Yii::t('app', 'Zip For Postbox'),
            'postbox' => Yii::t('app', 'Postbox'),
            'suburb' => Yii::t('app', 'Suburb'),
            'state_full' => Yii::t('app', 'State Full'),
            'letter_address' => Yii::t('app', 'Letter Address'),
            'phone_handy' => Yii::t('app', 'Phone Handy'),
            'email_noification' => Yii::t('app', 'Email Noification'),
            'newsletter' => Yii::t('app', 'Newsletter'),
            'homepage_url' => Yii::t('app', 'Homepage Url'),
            'status_info' => Yii::t('app', 'Status Info'),
            'sip' => Yii::t('app', 'Sip'),
            'skype' => Yii::t('app', 'Skype'),
            'letter_address2' => Yii::t('app', 'Letter Address2'),
            'letter_address3' => Yii::t('app', 'Letter Address3'),
            'letter_address4' => Yii::t('app', 'Letter Address4'),
            'letter_address5' => Yii::t('app', 'Letter Address5'),
            'titular' => Yii::t('app', 'Titular'),
            'phone1' => Yii::t('app', 'Phone1'),
            'phone2' => Yii::t('app', 'Phone2'),
            'homepage' => Yii::t('app', 'Homepage'),
            'faxprivat' => Yii::t('app', 'Faxprivat'),
            'birthday' => Yii::t('app', 'Birthday'),
            'staff_number' => Yii::t('app', 'Staff Number'),
            'cuon_username' => Yii::t('app', 'Cuon Username'),
            'letter_phrase_1' => Yii::t('app', 'Letter Phrase 1'),
            'letter_phrase_2' => Yii::t('app', 'Letter Phrase 2'),
            'my_sign_1' => Yii::t('app', 'My Sign 1'),
            'signature_1' => Yii::t('app', 'Signature 1'),
            'signature_2' => Yii::t('app', 'Signature 2'),
            'signature_graphic_1' => Yii::t('app', 'Signature Graphic 1'),
            'signature_graphic_2' => Yii::t('app', 'Signature Graphic 2'),
        ];
    }
}
