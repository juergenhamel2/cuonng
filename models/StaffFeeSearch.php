<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StaffFee;

/**
 * StaffFeeSearch represents the model behind the search form of `app\models\StaffFee`.
 */
class StaffFeeSearch extends StaffFee
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'staff_id'], 'integer'],
            [['user_id', 'status', 'insert_time', 'update_time', 'update_user_id', 'versions_uuid', 'uuid', 'working_time_from', 'working_time_to'], 'safe'],
            [['fee_per_hour', 'fee_per_hour_calc', 'fee_per_hour_invoice', 'fee_per_overtime', 'fee_per_overtime_calc', 'fee_per_overtime_invoice'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StaffFee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_time' => $this->insert_time,
            'update_time' => $this->update_time,
            'client' => $this->client,
            'sep_info_1' => $this->sep_info_1,
            'sep_info_2' => $this->sep_info_2,
            'sep_info_3' => $this->sep_info_3,
            'versions_number' => $this->versions_number,
            'staff_id' => $this->staff_id,
            'fee_per_hour' => $this->fee_per_hour,
            'fee_per_hour_calc' => $this->fee_per_hour_calc,
            'fee_per_hour_invoice' => $this->fee_per_hour_invoice,
            'fee_per_overtime' => $this->fee_per_overtime,
            'fee_per_overtime_calc' => $this->fee_per_overtime_calc,
            'fee_per_overtime_invoice' => $this->fee_per_overtime_invoice,
        ]);

        $query->andFilterWhere(['ilike', 'user_id', $this->user_id])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'update_user_id', $this->update_user_id])
            ->andFilterWhere(['ilike', 'versions_uuid', $this->versions_uuid])
            ->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'working_time_from', $this->working_time_from])
            ->andFilterWhere(['ilike', 'working_time_to', $this->working_time_to]);

        return $dataProvider;
    }
}
