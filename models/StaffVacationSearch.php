<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StaffVacation;

/**
 * StaffVacationSearch represents the model behind the search form of `app\models\StaffVacation`.
 */
class StaffVacationSearch extends StaffVacation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'staff_id'], 'integer'],
            [['user_id', 'status', 'insert_time', 'update_time', 'update_user_id', 'versions_uuid', 'uuid', 'name', 'designation', 'vacation_from', 'vacation_to'], 'safe'],
            [['days'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StaffVacation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'insert_time' => $this->insert_time,
            'update_time' => $this->update_time,
            'client' => $this->client,
            'sep_info_1' => $this->sep_info_1,
            'sep_info_2' => $this->sep_info_2,
            'sep_info_3' => $this->sep_info_3,
            'versions_number' => $this->versions_number,
            'staff_id' => $this->staff_id,
            'vacation_from' => $this->vacation_from,
            'vacation_to' => $this->vacation_to,
            'days' => $this->days,
        ]);

        $query->andFilterWhere(['ilike', 'user_id', $this->user_id])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'update_user_id', $this->update_user_id])
            ->andFilterWhere(['ilike', 'versions_uuid', $this->versions_uuid])
            ->andFilterWhere(['ilike', 'uuid', $this->uuid])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'designation', $this->designation]);

        return $dataProvider;
    }
}
