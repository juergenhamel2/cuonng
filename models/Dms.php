<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dms".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property string|null $title
 * @property string|null $category
 * @property int|null $size_x
 * @property int|null $size_y
 * @property string|null $uploadpath
 * @property string|null $document_image
 * @property string|null $sub1
 * @property string|null $sub2
 * @property string|null $sub3
 * @property string|null $sub4
 * @property string|null $sub5
 * @property string|null $search1
 * @property string|null $search2
 * @property string|null $search3
 * @property string|null $search4
 * @property string|null $file_format
 * @property string|null $insert_from
 * @property string|null $insert_at_date
 * @property int|null $insert_from_module
 * @property string|null $file_suffix
 * @property string|null $document_date
 * @property bool|null $document_rights_activated
 * @property bool|null $document_rights_user_read
 * @property bool|null $document_rights_user_write
 * @property bool|null $document_rights_user_execute
 * @property bool|null $document_rights_group_read
 * @property bool|null $document_rights_group_write
 * @property bool|null $document_rights_group_execute
 * @property bool|null $document_rights_all_read
 * @property bool|null $document_rights_all_write
 * @property bool|null $document_rights_all_execute
 * @property string|null $document_rights_user
 * @property string|null $document_rights_groups
 * @property string|null $dms_extract
 * @property int|null $paired_id
 */

use yii\base\Model;
use yii\web\UploadedFile;

class Dms extends  \yii\db\ActiveRecord
{

   public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time', 'insert_at_date', 'document_date'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'size_x', 'size_y', 'insert_from_module', 'paired_id'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'size_x', 'size_y', 'insert_from_module', 'paired_id'], 'integer'],
            [['uploadpath','document_image', 'dms_extract'], 'string'],
            [['document_rights_activated', 'document_rights_user_read', 'document_rights_user_write', 'document_rights_user_execute', 'document_rights_group_read', 'document_rights_group_write', 'document_rights_group_execute', 'document_rights_all_read', 'document_rights_all_write', 'document_rights_all_execute'], 'boolean'],
            [['user_id', 'status', 'update_user_id'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
            [['title', 'category', 'sub1', 'sub2', 'sub3', 'sub4', 'sub5'], 'string', 'max' => 85],
            [['search1', 'search2', 'search3', 'search4', 'document_rights_user', 'document_rights_groups'], 'string', 'max' => 250],
            [['file_format', 'insert_from', 'file_suffix'], 'string', 'max' => 50],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'title' => Yii::t('app', 'Title'),
            'category' => Yii::t('app', 'Category'),
            'size_x' => Yii::t('app', 'Size X'),
            'size_y' => Yii::t('app', 'Size Y'),
            'uploadpath' =>  Yii::t('app', 'Upload Path'),
            'document_image' => Yii::t('app', 'Document Image'),
            'sub1' => Yii::t('app', 'Sub1'),
            'sub2' => Yii::t('app', 'Sub2'),
            'sub3' => Yii::t('app', 'Sub3'),
            'sub4' => Yii::t('app', 'Sub4'),
            'sub5' => Yii::t('app', 'Sub5'),
            'search1' => Yii::t('app', 'Search1'),
            'search2' => Yii::t('app', 'Search2'),
            'search3' => Yii::t('app', 'Search3'),
            'search4' => Yii::t('app', 'Search4'),
            'file_format' => Yii::t('app', 'File Format'),
            'insert_from' => Yii::t('app', 'Insert From'),
            'insert_at_date' => Yii::t('app', 'Insert At Date'),
            'insert_from_module' => Yii::t('app', 'Insert From Module'),
            'file_suffix' => Yii::t('app', 'File Suffix'),
            'document_date' => Yii::t('app', 'Document Date'),
            'document_rights_activated' => Yii::t('app', 'Document Rights Activated'),
            'document_rights_user_read' => Yii::t('app', 'Document Rights User Read'),
            'document_rights_user_write' => Yii::t('app', 'Document Rights User Write'),
            'document_rights_user_execute' => Yii::t('app', 'Document Rights User Execute'),
            'document_rights_group_read' => Yii::t('app', 'Document Rights Group Read'),
            'document_rights_group_write' => Yii::t('app', 'Document Rights Group Write'),
            'document_rights_group_execute' => Yii::t('app', 'Document Rights Group Execute'),
            'document_rights_all_read' => Yii::t('app', 'Document Rights All Read'),
            'document_rights_all_write' => Yii::t('app', 'Document Rights All Write'),
            'document_rights_all_execute' => Yii::t('app', 'Document Rights All Execute'),
            'document_rights_user' => Yii::t('app', 'Document Rights User'),
            'document_rights_groups' => Yii::t('app', 'Document Rights Groups'),
            'dms_extract' => Yii::t('app', 'Dms Extract'),
            'paired_id' => Yii::t('app', 'Paired ID'),
        ];
    }
}
