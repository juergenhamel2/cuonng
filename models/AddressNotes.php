<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address_notes".
 *
 * @property int $id
 * @property string|null $user_id
 * @property string|null $status
 * @property string|null $insert_time
 * @property string|null $update_time
 * @property string|null $update_user_id
 * @property int|null $client
 * @property int|null $sep_info_1
 * @property int|null $sep_info_2
 * @property int|null $sep_info_3
 * @property int|null $versions_number
 * @property string|null $versions_uuid
 * @property string|null $uuid
 * @property int|null $address_id
 * @property string|null $notes_misc
 * @property string|null $notes_contacter
 * @property string|null $notes_representant
 * @property string|null $notes_salesman
 * @property string|null $notes_organisation
 */
class AddressNotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address_notes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['insert_time', 'update_time'], 'safe'],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id'], 'default', 'value' => null],
            [['client', 'sep_info_1', 'sep_info_2', 'sep_info_3', 'versions_number', 'address_id'], 'integer'],
            [['notes_misc', 'notes_contacter', 'notes_representant', 'notes_salesman', 'notes_organisation'], 'string'],
            [['user_id', 'status', 'update_user_id'], 'string', 'max' => 25],
            [['versions_uuid', 'uuid'], 'string', 'max' => 36],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'insert_time' => Yii::t('app', 'Insert Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'update_user_id' => Yii::t('app', 'Update User ID'),
            'client' => Yii::t('app', 'Client'),
            'sep_info_1' => Yii::t('app', 'Sep Info 1'),
            'sep_info_2' => Yii::t('app', 'Sep Info 2'),
            'sep_info_3' => Yii::t('app', 'Sep Info 3'),
            'versions_number' => Yii::t('app', 'Versions Number'),
            'versions_uuid' => Yii::t('app', 'Versions Uuid'),
            'uuid' => Yii::t('app', 'Uuid'),
            'address_id' => Yii::t('app', 'Address ID'),
            'notes_misc' => Yii::t('app', 'Notes Misc'),
            'notes_contacter' => Yii::t('app', 'Notes Contacter'),
            'notes_representant' => Yii::t('app', 'Notes Representant'),
            'notes_salesman' => Yii::t('app', 'Notes Salesman'),
            'notes_organisation' => Yii::t('app', 'Notes Organisation'),
        ];
    }
}
