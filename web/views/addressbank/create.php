<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddressBank */

$this->title = Yii::t('app', 'Create Address Bank');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Address Banks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-bank-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
