#
#
#  php init
#
# upload_max_filesize = 2G
# post_max_size = 0
#
#
#
# 1 First steps
#
# Please be sure, that postgresql is installed
# also install php with modules, composer,
# then run: composer create-project --prefer-dist yiisoft/yii2-app-basic cuonng
# change to cuonng
# composer dump-autoload
#
#
#
#
# be root
# then
# ------ su postgres
# psql
#create user cuon_admin with encrypted password 'XXXXXXXXXXXXXXX' ;

#create database cuon WITH ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C'
OWNER cuon_admin TEMPLATE template0;

#grant all privileges on database cuon to cuon_admin;


# For easy install change pg_hba.account_info
# ------ nano /etc/postgresql/xx/main/pg_hba.conf
# xx is the postgres version, for example 13 or higher
# change the line :
# "local" is for Unix domain socket connections only
# local   all             all                                     peer
#from peer to trust:
# local   all             all                                     trust
# restart postgres, mostly on DEVUAN I use /etc/init.d/postgresq restart
#Test the connection

# psql -Ucuon_admin cuon
# It shall work WITHOUT password
#
#
# if all is ok, change to your local user, go to the cuonng folder, start the "install.sh" script
# ./install.sh
# mostly you can ignore errors, that are mostly more warnings


# 2


# 3 Start the internal server

php yii serve --port=8888

# or setup it with apache or nginx
